import * as React from "react";
import * as ReactDom from "react-dom";
import { GcNumber } from "@grapecity/inputman.react";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            autoScale: true,
            minScaleFactor: 0.5
        };
    }

    render(){
        return (
            <div>
                <GcNumber
                    ref='number' 
                    className={'displayStyle'}
                    autoScale={this.state.autoScale}
                    minScaleFactor={this.state.minScaleFactor}
                    displayPositivePrefix={'税込  '}
                    displayNegativePrefix={'税込  ▲'}
                    displayPositiveSuffix={'  円'}
                    displayNegativeSuffix={'  円'}
                    formatDigit={'###############'}
                    displayFormatDigit={'[DBNum1]G'}
                    positivePrefix={'$ '}
                    negativePrefix={'-$ '}
                    positiveSuffix={' （税込）'}
                    negativeSuffix={' （税込）'}
                    maxValue={999999999999999}/>&nbsp;
                <button >テキストクリア</button>
                <table class="sample">
                    <tr>
                        <th>長体表示</th>
                        <td>
                            <label><input type="checkbox" checked={this.state.autoScale} onChange={(e)=>this.setState({autoScale: e.target.checked})}/>有効にする</label>
                        </td>
                    </tr>
                    <tr>
                        <th>最小倍率</th>
                        <td>
                            <input type="number" max="1.0" min="0.1" step="0.1" value={this.state.minScaleFactor} onChange={(e)=>this.setState({minScaleFactor: e.target.value})}/>
                        </td>
                    </tr>
                </table>

            </div> 
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));