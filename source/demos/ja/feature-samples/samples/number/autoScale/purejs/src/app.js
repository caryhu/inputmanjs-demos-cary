﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcNumber = new InputMan.GcNumber(document.getElementById('gcNumber'), {
    autoScale:true,
    minScaleFactor:0.5,
    displayPositivePrefix: '税込  ',
    displayNegativePrefix: '税込  ▲',
    displayPositiveSuffix: '  円',
    displayNegativeSuffix: '  円',
    formatDigit: '###############',
    displayFormatDigit: '[DBNum1]G',
    positivePrefix: '$ ',
    negativePrefix: '-$ ',
    positiveSuffix: ' （税込）',
    negativeSuffix: ' （税込）',
    maxValue: 999999999999999,
});

document.getElementById('clearText').addEventListener('click', () => {
    gcNumber.clear();
});

document.getElementById('setAutoScale').addEventListener('click', (e) => {
    gcNumber.setAutoScale(e.target.checked);
});

document.getElementById('setMinScaleFactor').addEventListener('change', (e) => {
    gcNumber.setMinScaleFactor(Number(e.target.value));
});