import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, ViewChild } from '@angular/core';
import { GcNumberComponent } from "@grapecity/inputman.angular";


@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public autoScale: boolean = true;
    public minScaleFactor: number = 0.5;

    @ViewChild(GcNumberComponent) number: GcNumberComponent;

    public clearText(){
        this.number.getNestedIMControl().clear();
    }
}

enableProdMode();