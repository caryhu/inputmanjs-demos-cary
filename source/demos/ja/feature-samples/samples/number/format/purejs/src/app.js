﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcNumber = new InputMan.GcNumber(document.getElementById('gcNumber'), {
    value: 10000,

    // 表示書式の設定
    displayFormatDigit: '##,##0',
    displayPositivePrefix: '税込 ',
    displayNegativePrefix: '税込 ▲',
    displayPositiveSuffix: '円',
    displayNegativeSuffix: '円',

    // 入力書式の設定
    formatDigit: '#####',
    positivePrefix: '$',
    negativePrefix: '-$',
    positiveSuffix: '',
    negativeSuffix: '',

    // 記号などの設定
    currencySymbol: '¥',
    decimalPoint: '.',
    separator: ',',
});

const initGcTextBox = () => {
    var setCurrencySymbol = new InputMan.GcTextBox(document.getElementById('setCurrencySymbol'), {
        text: '¥'
    });
    setCurrencySymbol.onTextChanged(() => {
        gcNumber.setCurrencySymbol(setCurrencySymbol.getText());
    });

    var setDisplayPositivePrefix = new InputMan.GcTextBox(document.getElementById('setDisplayPositivePrefix'), {
        text: '税込 '
    });
    setDisplayPositivePrefix.onTextChanged(() => {
        gcNumber.setDisplayPositivePrefix(setDisplayPositivePrefix.getText());
    });

    var setDisplayPositiveSuffix = new InputMan.GcTextBox(document.getElementById('setDisplayPositiveSuffix'), {
        text: '円'
    });
    setDisplayPositiveSuffix.onTextChanged(() => {
        gcNumber.setDisplayPositiveSuffix(setDisplayPositiveSuffix.getText());
    });

    var setDisplayNegativePrefix = new InputMan.GcTextBox(document.getElementById('setDisplayNegativePrefix'), {
        text: '税込 ▲'
    });
    setDisplayNegativePrefix.onTextChanged(() => {
        gcNumber.setDisplayNegativePrefix(setDisplayNegativePrefix.getText());
    });

    var setDisplayNegativeSuffix = new InputMan.GcTextBox(document.getElementById('setDisplayNegativeSuffix'), {
        text: '円'
    });
    setDisplayNegativeSuffix.onTextChanged(() => {
        gcNumber.setDisplayNegativeSuffix(setDisplayNegativeSuffix.getText());
    });

    var readOnlyTextBox = new InputMan.GcTextBox(document.getElementById('readOnlyTextBox'), {
        readOnly: true,
        text: '（表示書式と同様）'
    });

    var setPositivePrefix = new InputMan.GcTextBox(document.getElementById('setPositivePrefix'), {
        text: '$'
    }); 
    setPositivePrefix.onTextChanged(() => {
        gcNumber.setPositivePrefix(setPositivePrefix.getText());
    });

    var setPositiveSuffix = new InputMan.GcTextBox(document.getElementById('setPositiveSuffix'), {
        text: ''
    });
    setPositiveSuffix.onTextChanged(() => {
        gcNumber.setPositiveSuffix(setPositiveSuffix.getText());
    });

    var setNegativePrefix = new InputMan.GcTextBox(document.getElementById('setNegativePrefix'), {
        text: '-$'
    });
    setNegativePrefix.onTextChanged(() => {
        gcNumber.setNegativePrefix(setNegativePrefix.getText());
    });

    var setNegativeSuffix = new InputMan.GcTextBox(document.getElementById('setNegativeSuffix'), {
        text: ''
    });
    setNegativeSuffix.onTextChanged(() => {
        gcNumber.setNegativeSuffix(setNegativeSuffix.getText());
    });
};
initGcTextBox();

document.getElementById('setDisplayFormatDigit').addEventListener('change', (e) => {
    gcNumber.setDisplayFormatDigit(e.target.value);
});

document.getElementById('setFormatDigit').addEventListener('change', (e) => {
    gcNumber.setFormatDigit(e.target.value);
});