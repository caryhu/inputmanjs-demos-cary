import * as React from "react";
import * as ReactDom from "react-dom";
import { GcNumber, GcTextBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            currencySymbol: '¥',
            displayPositivePrefix: '税込 ',
            displayNegativePrefix: '税込 ▲',
            displayPositiveSuffix: '円',
            displayNegativeSuffix: '円',
            formatDigit: '#####',
            positivePrefix:  '$',
            negativePrefix: '-$',
            positiveSuffix: '',
            negativeSuffix: '',
        
            // 記号などの設定
            decimalPoint: '.',
            separator: ',',
            displayFormatDigit: '##,##0'
        };
    }

    render(){
        return (
            <div>
                <GcNumber 
                    value= {10000}

                    // 表示書式の設定
                    displayFormatDigit= {this.state.displayFormatDigit}
                    displayPositivePrefix= {this.state.displayPositivePrefix}
                    displayNegativePrefix= {this.state.displayNegativePrefix}
                    displayPositiveSuffix= {this.state.displayPositiveSuffix}
                    displayNegativeSuffix= {this.state.displayNegativeSuffix}
                
                    // 入力書式の設定
                    formatDigit= {this.state.formatDigit}
                    positivePrefix= {this.state.positivePrefix}
                    negativePrefix= {this.state.negativePrefix}
                    positiveSuffix= {this.state.positiveSuffix}
                    negativeSuffix= {this.state.negativeSuffix}
                
                    // 記号などの設定
                    currencySymbol= {this.state.currencySymbol}
                    decimalPoint= {this.state.decimalPoint}
                    separator= {this.state.separator}></GcNumber>
                <p>表示書式</p>
                <table class="sample" style={{marginTop: '-1rem'}}>
                    <tr>
                        <th>書式</th>
                        <td>
                            <select id="setDisplayFormatDigit" onChange={(e)=>this.setState({ displayFormatDigit: e.target.value})}>
                                <option>#####</option>
                                <option>00000</option>
                                <option>##,###</option>
                                <option selected>##,##0</option>
                                <option>##,##0.##</option>
                                <option>##,##0.00</option>
                            </select>
                        </td>
                        <th>通貨記号</th>
                        <td><GcTextBox text= {this.state.currencySymbol} onTextChanged={(e)=>{this.setState({currencySymbol: e.getText()})}}></GcTextBox></td>
                    </tr>
                    <tr>
                        <th>接頭書式[正数]</th>
                        <td><GcTextBox text={this.state.displayPositivePrefix} onTextChanged={(e)=>{this.setState({displayPositivePrefix: e.getText()})}}></GcTextBox></td>
                        <th>接尾書式[正数]</th>
                        <td><GcTextBox text={this.state.displayPositiveSuffix} onTextChanged={(e)=>{this.setState({displayPositiveSuffix: e.getText()})}}/></td>
                    </tr>
                    <tr>
                        <th>接頭書式[負数]</th>
                        <td><GcTextBox text={this.state.displayNegativePrefix} onTextChanged={(e)=>{this.setState({displayNegativePrefix: e.getText()})}}/></td>
                        <th>接尾書式[負数]</th>
                        <td><GcTextBox text={this.state.displayNegativeSuffix} onTextChanged={(e)=>{this.setState({displayNegativeSuffix: e.getText()})}}/></td>
                    </tr>
                </table>
                <p>入力書式</p>
                <table class="sample" style={{marginTop: '-1rem'}}>
                    <tr>
                        <th>書式</th>
                        <td>
                            <select id="setFormatDigit" onChange={(e)=>{this.setState({formatDigit: e.target.value})}}>
                                <option>#####</option>
                                <option>00000</option>
                                <option>##,###</option>
                                <option>##,##0</option>
                                <option>##,##0.##</option>
                                <option>##,##0.00</option>
                            </select>
                        </td>
                        <th>通貨記号</th>
                        <td><GcTextBox 
                                readOnly= {true}
                                text= {'（表示書式と同様）'}></GcTextBox></td>
                    </tr>
                    <tr>
                        <th>接頭書式[正数]</th>
                        <td><GcTextBox text={this.state.positivePrefix} onTextChanged={(e)=>this.setState({positivePrefix: e.getText()})}></GcTextBox></td>
                        <th>接尾書式[正数]</th>
                        <td><GcTextBox text={this.state.positiveSuffix} onTextChanged={(e)=>this.setState({positiveSuffix: e.getText()})}></GcTextBox></td>
                    </tr>
                    <tr>
                        <th>接頭書式[負数]</th>
                        <td><GcTextBox text={this.state.negativePrefix} onTextChanged={(e)=>this.setState({negativePrefix: e.getText()})}></GcTextBox></td>
                        <th>接尾書式[負数]</th>
                        <td><GcTextBox text={this.state.negativeSuffix} onTextChanged={(e)=>this.setState({negativeSuffix: e.getText()})}></GcTextBox></td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));