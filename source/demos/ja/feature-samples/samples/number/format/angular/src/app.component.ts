import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public value = 10000;

    // 表示書式の設定
    public displayFormatDigit = '##,##0';
    public displayPositivePrefix = '税込 ';
    public displayNegativePrefix = '税込 ▲';
    public displayPositiveSuffix = '円';
    public displayNegativeSuffix = '円';

    // 入力書式の設定
    public formatDigit = '#####';
    public positivePrefix = '$';
    public negativePrefix = '-$';
    public positiveSuffix = '';
    public negativeSuffix = '';

    // 記号などの設定
    public currencySymbol = '¥';
    public decimalPoint = '.';
    public separator = ',';
}


enableProdMode();