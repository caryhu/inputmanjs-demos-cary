﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcNumber = new InputMan.GcNumber(document.getElementById('gcNumber'), {
    showSpinButton: true,
    showNumericPad: true
});

// イベントハンドラ
gcNumber.onEditStatusChanged((sender, eArgs) => {
    log('onEditStatusChanged');
});
gcNumber.onFocusOut((sender, eArgs) => {
    log('onFocusOut');
});
gcNumber.onInput((sender, eArgs) => {
    log('onInput');
});
gcNumber.onInvalidInput((sender, eArgs) => {
    log('onInvalidInput');
});
gcNumber.onInvalidRange((sender, eArgs) => {
    log('onInvalidRange');
});
gcNumber.onKeyDown((sender, eArgs) => {
    log('onKeyDown');
});
gcNumber.onKeyExit((sender, eArgs) => {
    log('onKeyExit');
});
gcNumber.onKeyUp((sender, eArgs) => {
    log('onKeyUp');
});
gcNumber.onSpinDown((sender, eArgs) => {
    log('onSpinDown');
});
gcNumber.onSpinUp((sender, eArgs) => {
    log('onSpinUp');
});
gcNumber.onSyncValueToOriginalInput((sender, eArgs) => {
    log('onSyncValueToOriginalInput');
});
gcNumber.onTextChanged((sender, eArgs) => {
    log('onTextChanged');
});
gcNumber.onValueChanged((sender, eArgs) => {
    log('onValueChanged');
});
gcNumber.getDropDownWindow().onClosing((sender, eArgs) => {
    log('onClosing');
});
gcNumber.getDropDownWindow().onOpening((sender, eArgs) => {
    log('onOpening');
});
gcNumber.getDropDownWindow().onClose((sender, eArgs) => {
    log('onClose');
});
gcNumber.getDropDownWindow().onOpen((sender, eArgs) => {
    log('onOpen');
});

// テキストボックスにログを出力
const log = (message) => {
    var textarea = document.getElementById('log');
    textarea.value += message + '\n';
    textarea.scrollTop = textarea.scrollHeight;
}