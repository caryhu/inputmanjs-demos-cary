import * as React from "react";
import * as ReactDom from "react-dom";
import { GcNumber } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

class App extends React.Component{
    gcNumber = null;
    constructor(props, context) {
        super(props, context);
        this.state = {
            message: ''
        };
    }

    log(msg){
        var newMessage = this.state.message + msg + '\n';
        this.refs.log.scrollTop = this.refs.log.scrollHeight;
        this.setState({message: newMessage});
    }

    componentDidMount(){
        this.gcNumber.getDropDownWindow().onClose((sender, eArgs) => this.log('onClose'));
        this.gcNumber.getDropDownWindow().onOpen((sender, eArgs) => this.log('onOpen'));
        this.gcNumber.getDropDownWindow().onClosing((sender, eArgs) => this.log("onClosing"));
        this.gcNumber.getDropDownWindow().onOpening((sender, eArgs) => this.log("onOpening"));
    }

    render(){
        return(
            <div>
                <GcNumber
                    showSpinButton= {true}
                    showNumericPad= {true}
                    onInitialized={(sender)=> {
                        this.gcNumber = sender;
                        sender.onSyncValueToOriginalInput(() => this.log('onSyncValueToOriginalInput'))}}
                    onEditStatusChanged={(sender) => {
                        this.log("onEditStatusChanged");
                    }} onInvalidInput={(sender) => {
                        this.log("onInvalidInput");
                    }} onKeyExit={(sender) => {
                        this.log("onKeyExit");
                    }} onTextChanged={(sender) => {
                        this.log("onTextChanged");
                    }} onKeyDown={(sender) => {
                        this.log("onKeyDown");
                    }} onKeyUp={(sender) => {
                        this.log("onKeyUp");
                    }} onFocusOut={(sender) => {
                        this.log("onFocusOut");
                    }} onInput={(sender) => {
                        this.log("onInput");
                    }} onSpinDown={(sender) => {
                        this.log("onSpinDown");
                    }} onSpinUp={(sender) => {
                        this.log("onSpinUp");
                    }} onInvalidRange={(sender) => {
                        this.log("onInvalidRange");
                    }} onValueChanged={(sender) => {
                        this.log("onValueChanged");
                    }}/><br/>
                イベント<br/>
                <textarea value={this.state.message} ref="log" id="log" rows="10" cols="30"></textarea>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));