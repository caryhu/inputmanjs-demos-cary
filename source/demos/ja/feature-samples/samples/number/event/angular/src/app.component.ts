import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, ViewChild, ElementRef } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    @ViewChild('logPanel')
    public logPanel: ElementRef;
    public text: string = "";

    public bindEvent(gcNumber: GC.InputMan.GcNumber): void {
        gcNumber.getDropDownWindow().onClose(() => {
            this.log('onClose');
        });
        gcNumber.getDropDownWindow().onOpen(() => {
            this.log('onOpen');
        });
        gcNumber.getDropDownWindow().onClosing(() => {
            this.log('onClosing');
        });
        gcNumber.getDropDownWindow().onOpening(() => {
            this.log('onOpening');
        });
    }

    // テキストボックスにログを出力
    public log(message: string) {
        this.text = this.text + message + '\n';
        this.logPanel.nativeElement.scrollTop = this.logPanel.nativeElement.scrollHeight;
    }
}


enableProdMode();