数値コントロールでは未入力のときや入力が完了していないとき、様々な補助機能を提供しています。

## 未入力時に表示する代替テキスト

setWatermarkメソッドを使用すれば、コントロールが未入力のときや値がゼロのときに、代わりに表示するテキスト（ウォーターマーク）を設定することができます。setWatermarkメソッドを使用して以下の代替テキストを設定できます。

| メソッド                          | 代替テキストの内容                     |
| ----------------------------- | ----------------------------- |
| setWatermarkDisplayNullText   | コントロールにフォーカスがなく、未入力のときの代替テキスト |
| setWatermarkDisplayZeroText   | コントロールにフォーカスがなく、値がゼロの代替テキスト   |
| setWatermarkNullText          | コントロールにフォーカスがあり、未入力のときの代替テキスト |
| setWatermarkZeroText          | コントロールにフォーカスがあり、値がゼロの代替テキスト   |

次のサンプルコードは、上図のような代替テキストを表示する方法です。

```
// フォーカスがないときの代替テキスト
gcNumber.setWatermarkDisplayNullText('本体価格');
gcNumber.setWatermarkDisplayZeroText('0円です');

// フォーカスがあるときの代替テキスト
gcNumber.setWatermarkNullText('税抜きで入力してください');
gcNumber.setWatermarkZeroText('0円は無効です');
```

