import * as React from "react";
import * as ReactDom from "react-dom";
import { GcNumber } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{

    render(){
        return <GcNumber   
        value = {null}  
        // フォーカスがないときの代替テキスト
        watermarkDisplayNullText= {'本体価格'}
        watermarkDisplayZeroText= {'0円です'}
        // フォーカスがあるときの代替テキスト
        watermarkNullText= {'税抜きで入力してください'}
        watermarkZeroText= {'0円は無効です'}>
        </GcNumber>
    }
}

ReactDom.render(<App />, document.getElementById("app"));