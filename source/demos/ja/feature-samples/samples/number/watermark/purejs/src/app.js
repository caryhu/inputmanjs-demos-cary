﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';
import './styles.css';

const gcNumber = new InputMan.GcNumber(document.getElementById('gcNumber'), {
    // フォーカスがないときの代替テキスト
    watermarkDisplayNullText: '本体価格',
    watermarkDisplayZeroText: '0円です',
    // フォーカスがあるときの代替テキスト
    watermarkNullText: '税抜きで入力してください',
    watermarkZeroText: '0円は無効です'
});
gcNumber.setValue(null);