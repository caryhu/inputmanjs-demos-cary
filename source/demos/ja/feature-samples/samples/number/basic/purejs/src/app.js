﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcNumber = new InputMan.GcNumber(document.getElementById('gcNumber'), {
    value: -100,
    minValue: -100,
    maxValue: 100
});

document.getElementById('setMaxMinBehavior').addEventListener('change', (e) => {
    gcNumber.setMaxMinBehavior(maxMinBehaviors[e.target.selectedIndex]);
});

const maxMinBehaviors = [
    InputMan.MaxMinBehavior.AdjustToMaxMin,
    InputMan.MaxMinBehavior.Clear,
    InputMan.MaxMinBehavior.Restore,
    InputMan.MaxMinBehavior.CancelInput,
    InputMan.MaxMinBehavior.Keep
];

document.getElementById('setAcceptsCrlf').addEventListener('change', (e) => {
    gcNumber.setAcceptsCrlf(crLfModes[e.target.selectedIndex]);
});

const crLfModes = [
    InputMan.CrLfMode.NoControl,
    InputMan.CrLfMode.Filter,
    InputMan.CrLfMode.Cut
];

gcNumber.onInvalidInput(() => {
    alert('不正な文字の入力です。半角数字で入力してください。');
});

gcNumber.onInvalidRange(() => {
    alert('-100以上100以下の数字を入力してください。');
});
