import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public acceptsCrlf: GC.InputMan.CrLfMode;
    public maxMinBehavior: GC.InputMan.MaxMinBehavior;

    public maxMinBehaviors = [
        GC.InputMan.MaxMinBehavior.AdjustToMaxMin,
        GC.InputMan.MaxMinBehavior.Clear,
        GC.InputMan.MaxMinBehavior.Restore,
        GC.InputMan.MaxMinBehavior.CancelInput,
        GC.InputMan.MaxMinBehavior.Keep
    ];
    public crLfModes = [
        GC.InputMan.CrLfMode.NoControl,
        GC.InputMan.CrLfMode.Filter,
        GC.InputMan.CrLfMode.Cut
    ];

    public onInvalidInput() {
        alert('不正な文字の入力です。半角数字で入力してください。');
    }

    public onInvalidRange() {
        alert('-100以上100以下の数字を入力してください。');
    }

}


enableProdMode();