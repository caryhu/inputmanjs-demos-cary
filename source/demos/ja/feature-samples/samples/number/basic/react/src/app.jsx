import * as React from "react";
import * as ReactDom from "react-dom";
import { GcNumber } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

const maxMinBehaviors = [
    InputMan.MaxMinBehavior.AdjustToMaxMin,
    InputMan.MaxMinBehavior.Clear,
    InputMan.MaxMinBehavior.Restore,
    InputMan.MaxMinBehavior.CancelInput,
    InputMan.MaxMinBehavior.Keep
];

const crLfModes = [
    InputMan.CrLfMode.NoControl,
    InputMan.CrLfMode.Filter,
    InputMan.CrLfMode.Cut
];
class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            acceptsCrlf: InputMan.CrLfMode.NoControl
        };
    }
    render(){
        return (
            <div>
                <GcNumber
                    className={'displayStyle'}
                    value= {-100}
                    minValue= {-100}
                    maxValue= {100}
                    acceptsCrlf={this.state.acceptsCrlf}
                    maxMinBehavior={this.state.maxMinBehavior}
                    onInvalidInput = {(sender) => {
                        alert('不正な文字の入力です。半角数字で入力してください。');
                    }}
                    onInvalidRange={(sender) => {
                        alert('-100以上100以下の数字を入力してください。');
                    }}></GcNumber>（-100～100）
                <table class="sample">
                    <tr>
                        <th>範囲外値の制御方法</th>
                        <td>
                            <select id="setMaxMinBehavior" onChange={(e)=>this.setState({maxMinBehavior: maxMinBehaviors[e.target.selectedIndex]})}>
                                <option>最大値か最小値の近い方に設定</option>
                                <option>値を削除</option>
                                <option selected>変更前の値に戻す</option>
                                <option>最後の入力をキャンセルする</option>
                                <option>最後に入力された現在値を保持</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>改行コードの扱い</th>
                        <td>
                            <select id="setAcceptsCrlf" onChange={(e)=>this.setState({acceptsCrlf: crLfModes[e.target.selectedIndex]})}>
                                <option>そのまま使用</option>
                                <option>すべての改行コードを削除</option>
                                <option>改行コード以降の文字列を切り取り</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));