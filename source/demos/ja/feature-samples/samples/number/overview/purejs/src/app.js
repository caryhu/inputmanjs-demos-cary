import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcNumber1 = new InputMan.GcNumber(document.getElementById('gcNumber1'), {
    displayFormatDigit: '#0.00',
    displayPositivePrefix: '重量 ',
    displayPositiveSuffix: ' kg',
    formatDigit: '#0.00',
    positiveSuffix: ' kg',
    showSpinButton: true,
    minValue: 0,
});

const gcNumber2 = new InputMan.GcNumber(document.getElementById('gcNumber2'), {
    displayFormatDigit: '[DBNum1]G',
    displayPositivePrefix: '税込 ',
    displayNegativePrefix: '税込 ▲',
    displayPositiveSuffix: ' 円',
    displayNegativeSuffix: ' 円',
    formatDigit: '#,###,###',
    positivePrefix: '$',
    negativePrefix: '-$',
    positiveSuffix: '（税込）',
    negativeSuffix: '（税込）',
    maxValue: 9999999,
    value: 1234000,
});

const gcNumber3 = new InputMan.GcNumber(document.getElementById('gcNumber3'), {
    displayFormatDigit: '##,##0',
    displayPositivePrefix: '税込 ',
    displayNegativePrefix: '税込 ▲',
    displayPositiveSuffix: ' 円',
    displayNegativeSuffix: ' 円',
    formatDigit: '#####',
    positivePrefix: '$',
    negativePrefix: '-$',
    positiveSuffix: '（税込）',
    negativeSuffix: '（税込）',
    showNumericPad: true
});