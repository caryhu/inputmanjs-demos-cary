import * as React from "react";
import * as ReactDom from "react-dom";
import { GcNumber } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{

    render(){
        return (
            <div class="flexbox">
                <div>
                    スピンボタン
                    <GcNumber
                        displayFormatDigit= {'#0.00'}
                        displayPositivePrefix= {'重量 '}
                        displayPositiveSuffix= {' kg'}
                        formatDigit= {'#0.00'}
                        positiveSuffix= {' kg'}
                        showSpinButton= {true}
                        minValue= {0}></GcNumber>
                </div>
                <div>
                    漢数字表記
                    <GcNumber
                        displayFormatDigit= {'[DBNum1]G'}
                        displayPositivePrefix= {'税込 '}
                        displayNegativePrefix= {'税込 ▲'}
                        displayPositiveSuffix= {' 円'}
                        displayNegativeSuffix= {' 円'}
                        formatDigit= {'#,###,###'}
                        positivePrefix= {'$'}
                        negativePrefix= {'-$'}
                        positiveSuffix= {'（税込）'}
                        negativeSuffix= {'（税込）'}
                        maxValue= {9999999}
                        value= {1234000}></GcNumber>
                </div>
                <div>
                    ドロップダウン数値パッド
                    <GcNumber
                    displayFormatDigit= {'##,##0'}
                    displayPositivePrefix= {'税込 '}
                    displayNegativePrefix= {'税込 ▲'}
                    displayPositiveSuffix= {' 円'}
                    displayNegativeSuffix= {' 円'}
                    formatDigit= {'#####'}
                    positivePrefix= {'$'}
                    negativePrefix= {'-$'}
                    positiveSuffix= {'（税込）'}
                    negativeSuffix= {'（税込）'}
                    showNumericPad= {true}></GcNumber>
                </div>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));