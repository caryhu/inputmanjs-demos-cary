﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcNumber1 = new InputMan.GcNumber(document.getElementById('gcNumber1'), {
    text: 'テキスト'
});

const gcNumber2 = new InputMan.GcNumber(document.getElementById('gcNumber2'), {
    text: 'テキスト',
    enabled: false
});

const gcNumber3 = new InputMan.GcNumber(document.getElementById('gcNumber3'), {
    watermarkDisplayNullText: '本体価格',
    watermarkDisplayZeroText: '0円です',
    watermarkNullText: '税抜きで入力してください',
    watermarkZeroText: '0円は無効です'
});
gcNumber3.setValue(null);

var styleSheet;
const indices = [];
const styles = {
    '.gcim': { width: '200px', height: '40px', backgroundColor: '#ddffdd', borderColor: '#009900', borderWidth: '2px', borderStyle: 'dashed', borderRadius: '12px', boxShadow: '5px 5px 5px rgba(0,0,0,0.5)', color: '#009900' },
    '.gcim__input': { cursor: 'crosshair', fontSize: '20px', fontWeight: 'bold', fontStyle: 'italic', fontFamily: 'serif', textAlign: 'right', textShadow: '1px 1px 1px rgba(0,0,0,0.5)' },
    '.gcim__input:disabled': { backgroundColor: '#666666', color: '#cccccc', cursor: 'wait' },
    '.gcim_focused': { backgroundColor: '#ddddff', borderColor: '#0000ff', color: '#0000ff' },
    '.gcim_watermark_null': { backgroundColor: '#ffdddd', borderColor: '#ff0000', color: '#ff0000' },
    '.gcim_focused.gcim_watermark_null': { backgroundColor: '#ffddff', borderColor: '#990099', color: '#990099' },
    '.gcim_gc-number_negative': {color: 'blue'},
};


const createInitialStyles = () => {
    var element = document.createElement('style');
    document.head.appendChild(element);
    styleSheet = element.sheet;
    var i = 0;
    for (var styleName in styles) {
        styleSheet.insertRule(styleName + '{}', i);
        indices[styleName] = i;
        i++;
    }
}
createInitialStyles();

const updateStyle = (event) => {
    const element = event.target;
    if (element.tagName == 'INPUT') {
        const values = element.value.split(',');
        const styleName = values[0];
        const propertyName = values[1];
        styleSheet.cssRules[indices[styleName]].style[propertyName] = element.checked ? styles[styleName][propertyName] : '';
    }

    var style = '';
    for (var i = 0; i < styleSheet.cssRules.length; i++) {
        if (styleSheet.cssRules[i].style.length > 0) {
            style += styleSheet.cssRules[i].cssText.replace(/{/g, '{\n ').replace(/;/g, ';\n ').replace(/  }/g, '}') + '\n';
        }
    }
    document.getElementById('style').value = style;
}

const copyStyle = () => {
    wijmo.Clipboard.copy(document.getElementById('style').value);
    document.execCommand('copy');
}
var panels = document.getElementsByClassName('peoperty-panel');
for (var i = 0; i < panels.length; i++) {
    panels[i].addEventListener('click', updateStyle);
}
document.getElementById('copyStyle').addEventListener('click', copyStyle);