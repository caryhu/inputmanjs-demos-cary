﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';
import './styles.css';

const gcNumber = new InputMan.GcNumber(document.getElementById('gcNumber'), {
    value: '1234',
    allowDeleteToNull: false,
    showClearButton: true,
});

document.getElementById('showClearButton').addEventListener('click', (e) => {
    gcNumber.setShowClearButton(e.target.checked);
});