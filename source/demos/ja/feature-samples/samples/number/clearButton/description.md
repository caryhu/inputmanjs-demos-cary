数値コントロール内にクリアボタンを表示する機能について解説します。

## クリアボタンを表示する方法
コンストラクタのshowClearButtonオプションにtrueを設定します。
```
new GC.InputMan.GcNumber(document.getElementById('gcNumber'), {
      showClearButton: true
});
```

また、setShowClearButtonメソッドを使用することで、クリアボタンの表示の有無を切り替えることができます。
```
gcNumber.setShowClearButton(true);
```

なお、値が0の時、allowDeleteToNullがtrueの場合はクリアボタンが表示されますが、allowDeleteToNullがfalseの場合はクリアボタンは表示されません。