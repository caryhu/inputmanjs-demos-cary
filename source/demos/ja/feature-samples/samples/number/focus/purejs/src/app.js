﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcNumber1 = new InputMan.GcNumber(document.getElementById('gcNumber1'));
const gcNumber2 = new InputMan.GcNumber(document.getElementById('gcNumber2'));
const gcNumber3 = new InputMan.GcNumber(document.getElementById('gcNumber3'));

// 矢印キーによるフォーカス移動
document.getElementById('setExitOnLeftRightKey').addEventListener('change', (e) => {
    gcNumber1.setExitOnLeftRightKey(exitOnLeftRightKeys[e.target.selectedIndex]);
    gcNumber2.setExitOnLeftRightKey(exitOnLeftRightKeys[e.target.selectedIndex]);
    gcNumber3.setExitOnLeftRightKey(exitOnLeftRightKeys[e.target.selectedIndex]);
});

const exitOnLeftRightKeys = [
    InputMan.ExitOnLeftRightKey.None,
    InputMan.ExitOnLeftRightKey.Both,
    InputMan.ExitOnLeftRightKey.Left,
    InputMan.ExitOnLeftRightKey.Right
];

// Enterキーによるフォーカス移動
const ExitKeys = [
    InputMan.ExitKey.None,
    InputMan.ExitKey.Both,
    InputMan.ExitKey.Enter,
    InputMan.ExitKey.ShiftEnter
];

document.getElementById('setExitOnEnterKey').addEventListener('change', (e) => {
    gcNumber1.setExitOnEnterKey(ExitKeys[e.target.selectedIndex]);
    gcNumber2.setExitOnEnterKey(ExitKeys[e.target.selectedIndex]);
    gcNumber3.setExitOnEnterKey(ExitKeys[e.target.selectedIndex]);
});