import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public autoDropDown: boolean;
    public dropDownButtonVisible: boolean;
    public gcNumber1: GC.InputMan.GcNumber;
    public gcNumber2: GC.InputMan.GcNumber;

    public toggleDropDown(isOpen: boolean) {
        if (isOpen) {
            this.gcNumber1.getDropDownWindow().open();
            this.gcNumber2.getDropDownWindow().open();

        } else {
            this.gcNumber1.getDropDownWindow().close();
            this.gcNumber2.getDropDownWindow().close();
        }
    }
}


enableProdMode();