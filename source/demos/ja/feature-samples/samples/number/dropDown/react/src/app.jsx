import * as React from "react";
import * as ReactDom from "react-dom";
import { GcNumber } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{
    constructor(){
        super();
        this.gcNumber1 = React.createRef();
        this.gcNumber2 = React.createRef();
        this.state = {
            dropDownButtonVisible: true,
            autoDropDown: false
        }
    }

    showDropDown(isShow){
        this.dropDownAction(this.gcNumber1.current.getNestedIMControl(), isShow);
        this.dropDownAction(this.gcNumber2.current.getNestedIMControl(), isShow);
    }

    dropDownAction(gcNumber, isShow){
        if(isShow){
            gcNumber.getDropDownWindow().open();
        }else{
            gcNumber.getDropDownWindow().close();
        }
    }

    componentDidMount(){
        this.gcNumber1.current.getNestedIMControl().setContainer(this.refs.container1);
        this.gcNumber2.current.getNestedIMControl().setContainer(this.refs.container2);
    }

    render(){
        return (
            <div>
                <div class="flexbox">
                    <div id="container1" ref="container1">
                        <p>ドロップダウンが下に表示されます。</p>
                        <GcNumber
                            ref={this.gcNumber1}
                            displayFormatDigit= {'##,##0'}
                            displayPositivePrefix= {'税込 '}
                            displayNegativePrefix= {'税込 ▲'}
                            displayPositiveSuffix= {'円'}
                            displayNegativeSuffix= {'円'}
                            formatDigit= {'#####'}
                            positivePrefix= {'$'}
                            negativePrefix= {'-$'}
                            positiveSuffix= {'（税込）'}
                            negativeSuffix= {'（税込）'}
                            showNumericPad= {true}
                            autoDropDown={this.state.autoDropDown}
                            dropDownButtonVisible={this.state.dropDownButtonVisible}></GcNumber>                                            
                    </div>
                    <div id="container2" ref="container2">
                        <p>ドロップダウンが上に表示されます。</p>
                        <GcNumber
                            ref={this.gcNumber2}
                            displayFormatDigit= {'##,##0'}
                            displayPositivePrefix= {'税込 '}
                            displayNegativePrefix= {'税込 ▲'}
                            displayPositiveSuffix= {'円'}
                            displayNegativeSuffix= {'円'}
                            formatDigit= {'#####'}
                            positivePrefix= {'$'}
                            negativePrefix= {'-$'}
                            positiveSuffix= {'（税込）'}
                            negativeSuffix= {'（税込）'}
                            showNumericPad= {true}
                            autoDropDown={this.state.autoDropDown}
                            dropDownButtonVisible={this.state.dropDownButtonVisible}></GcNumber>
                    </div>
                </div>
                <table class="sample">
                    <tr>
                        <th>フォーカス時のドロップダウン操作</th>
                        <td>
                            <label><input type="checkbox"
                            checked={this.state.autoDropDown} 
                            onChange={(e) => this.setState({autoDropDown: e.target.checked})}/>フォーカス時にドロップダウンを開く</label>
                        </td>
                    </tr>
                    <tr>
                        <th>コードによるドロップダウン操作</th>
                        <td>
                            <button onClick={()=>this.showDropDown(true)}>ドロップダウンを開く</button>&nbsp;
                            <button onClick={()=>this.showDropDown(false)}>ドロップダウンを閉じる</button>
                        </td>
                    </tr>
                    <tr>
                        <th>ドロップダウンボタンの表示</th>
                        <td>
                            <label><input type="checkbox"
                            checked={this.state.dropDownButtonVisible} 
                            onChange={(e) => this.setState({dropDownButtonVisible: e.target.checked})} />ドロップダウンボタンを表示する</label>
                        </td>
                    </tr>
                </table>
        </div>)
    }
}

ReactDom.render(<App />, document.getElementById("app"));