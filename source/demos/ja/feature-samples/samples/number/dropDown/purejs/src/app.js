﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';
import './styles.css';

const gcNumber1 = new InputMan.GcNumber(document.getElementById('gcNumber1'), {
    displayFormatDigit: '##,##0',
    displayPositivePrefix: '税込 ',
    displayNegativePrefix: '税込 ▲',
    displayPositiveSuffix: '円',
    displayNegativeSuffix: '円',
    formatDigit: '#####',
    positivePrefix: '$',
    negativePrefix: '-$',
    positiveSuffix: '（税込）',
    negativeSuffix: '（税込）',
    showNumericPad: true,
    container: document.getElementById('container1')
});
const gcNumber2 = new InputMan.GcNumber(document.getElementById('gcNumber2'), {
    displayFormatDigit: '##,##0',
    displayPositivePrefix: '税込 ',
    displayNegativePrefix: '税込 ▲',
    displayPositiveSuffix: '円',
    displayNegativeSuffix: '円',
    formatDigit: '#####',
    positivePrefix: '$',
    negativePrefix: '-$',
    positiveSuffix: '（税込）',
    negativeSuffix: '（税込）',
    showNumericPad: true,
    container: document.getElementById('container2')
});

document.getElementById('openOnFocus').addEventListener('change', (e) => {
    gcNumber1.setAutoDropDown(e.target.checked);
    gcNumber2.setAutoDropDown(e.target.checked);
});
document.getElementById('open').addEventListener('click', (e) => {
    gcNumber1.getDropDownWindow().open();
    gcNumber2.getDropDownWindow().open();
});
document.getElementById('close').addEventListener('click', (e) => {
    gcNumber1.getDropDownWindow().close();
    gcNumber2.getDropDownWindow().close();
});
document.getElementById('setDropDownButtonVisible').addEventListener('change', (e) => {
    gcNumber1.setDropDownButtonVisible(e.target.checked);
    gcNumber2.setDropDownButtonVisible(e.target.checked);
});
