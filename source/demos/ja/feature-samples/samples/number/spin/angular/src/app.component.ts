import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public allowSpin: boolean = true;
    public spinWrap: boolean = true;
    public spinOnKeys: boolean = true;
    public spinWheel: boolean = true;
}


enableProdMode();