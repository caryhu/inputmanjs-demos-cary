﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcNumber = new InputMan.GcNumber(document.getElementById('gcNumber'), {
    showSpinButton: true,
    value: -100,
    minValue: -100,
    maxValue: 100,
});

document.getElementById('setAllowSpin').addEventListener('change', (e) => {
    gcNumber.setAllowSpin(e.target.checked);
});

document.getElementById('setSpinOnKeys').addEventListener('change', (e) => {
    gcNumber.setSpinOnKeys(e.target.checked);
});

document.getElementById('setSpinWheel').addEventListener('change', (e) => {
    gcNumber.setSpinWheel(e.target.checked);
});

document.getElementById('setSpinWrap').addEventListener('change', (e) => {
    gcNumber.setSpinWrap(e.target.checked);
});