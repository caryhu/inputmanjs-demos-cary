import * as React from "react";
import * as ReactDom from "react-dom";
import { GcNumber } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            allowSpin: true,
            spinWrap: true,
            spinOnKeys: true,
            spinWheel: true
        };
    }

    render(){
        return (
            <div>
                <GcNumber
                    showSpinButton= {true}
                    value= {-100}
                    minValue= {-100}
                    maxValue= {100}
                    spinOnKeys = {this.state.spinOnKeys}
                    spinWrap = {this.state.spinWrap}
                    allowSpin={this.state.allowSpin}
                    spinWheel={this.state.spinWheel}></GcNumber>
                <table class="sample">
                    <tr>
                        <th>スピン機能</th>
                        <td><label><input type="checkbox" checked={this.state.allowSpin} onChange={(e)=> this.setState({allowSpin: e.target.checked})}/>有効にする</label></td>
                    </tr>
                    <tr>
                        <th>上下矢印キーでのスピン操作</th>
                        <td><label><input type="checkbox" checked={this.state.spinOnKeys} onChange={(e)=> this.setState({spinOnKeys: e.target.checked})}/>有効にする</label></td>
                    </tr>
                    <tr>
                        <th>マウスホイールでのスピン操作</th>
                        <td><label><input type="checkbox" checked={this.state.spinWheel} onChange={(e)=> this.setState({spinWheel: e.target.checked})}/>有効にする</label></td>
                    </tr>
                    <tr>
                        <th>有効範囲内でのループ</th>
                        <td><label><input type="checkbox" checked={this.state.spinWrap} onChange={(e)=> this.setState({spinWrap: e.target.checked})}/>有効範囲内でループする</label></td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));