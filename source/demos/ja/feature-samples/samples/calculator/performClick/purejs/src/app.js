import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';

let gcCalculator = new InputMan.GcCalculator(document.getElementById('calculator'));

document.getElementById('performButton').addEventListener('click', (e) => {
    //現在の値に1.1を掛けるように電卓キーを実行していきます
    gcCalculator.performClick(InputMan.GcCalculatorKey.Multiply);
    gcCalculator.performClick(InputMan.GcCalculatorKey.D_1);
    gcCalculator.performClick(InputMan.GcCalculatorKey.Dot);
    gcCalculator.performClick(InputMan.GcCalculatorKey.D_1);
    gcCalculator.performClick(InputMan.GcCalculatorKey.Equal);
});