import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { Component, enableProdMode, Pipe, PipeTransform,ViewChild } from '@angular/core';
import { GcCalculatorComponent } from "@grapecity/inputman.angular";
import GC from '@grapecity/inputman';


@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    @ViewChild(GcCalculatorComponent) calc: GcCalculatorComponent;
    
    constructor() {
    }

    performClick(){
        let gcCalc = this.calc.getNestedIMControl();
        //現在の値に1.1を掛けるように電卓キーを実行していきます
        gcCalc.performClick(GC.InputMan.GcCalculatorKey.Multiply);
        gcCalc.performClick(GC.InputMan.GcCalculatorKey.D_1);
        gcCalc.performClick(GC.InputMan.GcCalculatorKey.Dot);
        gcCalc.performClick(GC.InputMan.GcCalculatorKey.D_1);
        gcCalc.performClick(GC.InputMan.GcCalculatorKey.Equal);
    }
}

@Pipe({
    name: 'dateConvertor',
    pure: true
})
export class DateConvertorPipe implements PipeTransform {

    transform(value: any, args?: any): Date {
        return new Date(value);
    }
}

enableProdMode();