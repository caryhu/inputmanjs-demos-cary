import * as React from "react";
import * as ReactDom from "react-dom";
import { GcCalculator } from "@grapecity/inputman.react";
import {InputMan} from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';


class App extends React.Component{
    constructor(){
        super();
        this.calc = React.createRef();
    }

    render(){
        return <React.Fragment>
            <div>
                <GcCalculator ref={this.calc} />
            </div>

            <table class="sample">
                <tr>
                    <th>税込み計算する</th>
                    <td>
                    <button onClick={this.performClick.bind(this)}>実行</button>
                    </td>
                </tr>
            </table>
        </React.Fragment>
    }

    performClick(){
        let calculator = this.calc.current.getNestedIMControl();
        //現在の値に1.1を掛けるように電卓キーを実行していきます
        calculator.performClick(InputMan.GcCalculatorKey.Multiply);
        calculator.performClick(InputMan.GcCalculatorKey.D_1);
        calculator.performClick(InputMan.GcCalculatorKey.Dot);
        calculator.performClick(InputMan.GcCalculatorKey.D_1);
        calculator.performClick(InputMan.GcCalculatorKey.Equal);
    }
}

ReactDom.render(<App />, document.getElementById("app"));