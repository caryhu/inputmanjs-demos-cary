電卓コントロールは、javascriptコードを利用して電卓のボタンクリックを実現できます。

## コードによるボタンの実行
performClickメソッドの引数に実行したいボタンを指定します。

```javascript
let gcCalculator = new GC.InputMan.GcCalculator(document.getElementById('calculator'));

//「1」ボタンを実行する
gcCalculator.performClick(GC.InputMan.GcCalculatorKey.D_1);

//「BS」ボタンを実行する
gcCalculator.performClick(GC.InputMan.GcCalculatorKey.BS);
```

メソッドの引数に指定できるボタンの種類は[GcCalculatorKey列挙型](</inputmanjs/api/enums/gc.inputman.gccalculatorkey.html>)の値となります。