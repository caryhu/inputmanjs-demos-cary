import * as React from "react";
import * as ReactDom from "react-dom";
import { GcCalculator, GcNumber } from "@grapecity/inputman.react";
import {InputMan} from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';


class App extends React.Component{
    render(){
        return <div class="flexbox">
                <div>
                    電卓コントロール
                    <GcCalculator />
                    <br/>
                </div>
                <div>
                    ドロップダウン電卓<br/>
                    <GcNumber showCalculatorAsDropDown={true} calculatorButtonPosition={InputMan.DropDownButtonAlignment.RightSide} />
                    <br/>
                </div>
                <div>
                    ポップアップ電卓<br/>
                    <GcNumber showCalculatorAsPopup={true} calculatorButtonPosition={InputMan.DropDownButtonAlignment.LeftSide} />
                </div>
            </div>
    }

    
}

ReactDom.render(<App />, document.getElementById("app"));