import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';

// 電卓
let gcCalculator = new InputMan.GcCalculator(document.getElementById('calculator'));

// ドロップダウン電卓
let gcNumber1 = new InputMan.GcNumber(document.getElementById('number'), {
    showCalculatorAsDropDown: true,
    calculatorButtonPosition: InputMan.DropDownButtonAlignment.RightSide,
});

// ポップアップ電卓
let gcNumber2 = new InputMan.GcNumber(document.getElementById('number2'), {
    showCalculatorAsPopup: true,
    calculatorButtonPosition: InputMan.DropDownButtonAlignment.LeftSide,
});