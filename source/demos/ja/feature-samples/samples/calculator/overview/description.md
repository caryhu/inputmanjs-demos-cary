電卓（GcCalculator）コントロールは、メモリ機能や四則演算などの一般的な電卓機能を提供するコントロールです。電卓はドロップダウンやポップアップで表示することができます。

## キーボードからの操作
電卓コントロールは、キーをマウスでクリックするほか、キーボードでも操作することが可能です。

| ボタン       | 操作                    | 説明                      |
| ----------------------------------------------- | ----------------------------------------------- | ----------------------------------------------- |
| ＜0～9＞	 | 	［0～9］      | 数字を挿入します。  |
| ＜+/-＞	 | 	［F9］        | 数値の符号を切り替えます。  |
| ＜.＞	     | 	［.］または［,］      | 小数点を挿入します。  |
| ＜+＞      | 	［+］      | 加算を行います。  |
| ＜-＞	     | 	［-］     | 減算を行います。  |
| ＜*＞	     | 	 ［\*］    | 乗算を行います。  |
| ＜/＞	     |  ［/］      | 除算を行います。  |
| ＜√＞	     |［@］    	| 平方根を算出します。|
| ＜%＞    	|［%］|	除算の結果を百分率で表示します。|
| ＜1/x＞    |	［r］	| 逆数を計算します。 |
| ＜=＞	    |［=］または［Enter］	| 計算を実行します。 |
| ＜MC＞ |	［Ctrl］＋［Alt］＋［L］|	メモリ内の数値をクリアします。 |
| ＜MR＞ |	［Ctrl］＋［Alt］＋［R］|	メモリ内の数値を呼び出します。 |
| ＜MS＞ |	［Ctrl］＋［Alt］＋［M］|	数値をメモリ内に格納します。 |
| ＜M+＞ |	［Ctrl］＋［Alt］＋［P］|	メモリ内の数値に加算します。 |
| ＜BS＞ |	［BackSpace］ |	最後の桁をクリアします。 |
| ＜CE＞ |	［Del］ |	現在の数値をクリアします。 |
| ＜C＞	 |［Ctrl］＋［Del］または［Esc］ |	計算をクリアします。 |