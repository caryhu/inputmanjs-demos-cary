import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { Component, enableProdMode, Pipe, PipeTransform } from '@angular/core';

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    constructor() {
    }
}

@Pipe({
    name: 'dateConvertor',
    pure: true
})
export class DateConvertorPipe implements PipeTransform {

    transform(value: any, args?: any): Date {
        return new Date(value);
    }
}

enableProdMode();