import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';

let gcCalculator1 = new InputMan.GcCalculator(document.getElementById('calculator1'));

let gcCalculator2 = new InputMan.GcCalculator(document.getElementById('calculator2'));

let gcCalculator3 = new InputMan.GcCalculator(document.getElementById('calculator3'));

let gcNumber = new InputMan.GcNumber(document.getElementById('number'), {
    showCalculatorAsPopup: true,
    calculatorButtonPosition: InputMan.DropDownButtonAlignment.LeftSide,
});