import * as React from "react";
import * as ReactDom from "react-dom";
import { GcCalculator, GcNumber } from "@grapecity/inputman.react";
import {InputMan} from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';


class App extends React.Component{
    render(){
        return <div class="flexbox">
            <div>
                デフォルト(スタイルの変更なし)
                <GcCalculator className="calculator1" />
            </div>
            <div>
                電卓ボタンのスタイルを変更する
                <GcCalculator className="calculator2" />
            </div>
            <div>
                電卓のサイズと背景色、結果パネルを変更する
                <GcCalculator className="calculator3" />
            </div>
            <div>
                ポップアップ電卓(サイズを小さくして表示)<br/>
                <GcNumber showCalculatorAsPopup={true} calculatorButtonPosition={InputMan.DropDownButtonAlignment.LeftSide} className="has_popup" /><br/>
            </div>
        </div>
    }

    
}

ReactDom.render(<App />, document.getElementById("app"));