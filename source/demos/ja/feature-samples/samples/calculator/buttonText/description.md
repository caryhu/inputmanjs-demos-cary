電卓コントロールは、電卓のボタンのテキストを変更することができます。

## ボタンのテキストを変更する方法
変更したいボタンに対して値を設定後、GcCalculatorクラスのbuttonTextプロパティに値を設定します。

```javascript
let gcCalculator = new GC.InputMan.GcCalculator(document.getElementById('calculator'));
//変更したいボタンを格納する変数を定義する
let buttonText = {};

//「1」のボタンを「one」と設定する
buttonText[GC.InputMan.GcCalculatorKey.D_1] = "one";
//「3」のボタンを「three」と設定する
buttonText[GC.InputMan.GcCalculatorKey.D_3] = "three";

//buttonTextプロパティに変更したいボタンの配列を指定する
gcCalculator.buttonText = buttonText;

```

変更できるボタンの種類は[GcCalculatorKey列挙型](</inputmanjs/api/enums/gc.inputman.gccalculatorkey.html>)の値となります。


またMStatusボタンのみ、2種類の値(メモリに値がある場合とそれ以外の場合)を設定することができます。
MStatusボタンで値を1つだけ設定した場合は、メモリに値がある場合の値となります。

```javascript
// メモリに値がある場合は「MM」と表示させ、それ以外では「NULL」と表示する。
buttonText[GC.InputMan.GcCalculatorKey.MStatus] = { hasValue: 'MM', noValue: 'NULL' };

// 値を1つ設定した時は、メモリに値がある場合の値を設定します。
buttonText[GC.InputMan.GcCalculatorKey.MStatus] = "MM";
```

## 変更したボタンのテキストを元に戻す方法
restoreButtonTextメソッドを使用することで、ボタンに設定したテキストを元に戻すことができます。

```javascript
gcCalculator.restoreButtonText();
```