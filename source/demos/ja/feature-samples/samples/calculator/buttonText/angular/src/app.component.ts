import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { Component, enableProdMode, Pipe, PipeTransform,ViewChild } from '@angular/core';
import { GcCalculatorComponent } from "@grapecity/inputman.angular";
import GC from '@grapecity/inputman';

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    @ViewChild(GcCalculatorComponent) calc: GcCalculatorComponent;

    btnText= {};
    
    constructor() {
    }

    updateButtonText(){
        let buttonText = {};
        buttonText[GC.InputMan.GcCalculatorKey.D_0] = "零";
        buttonText[GC.InputMan.GcCalculatorKey.D_1] = "一";
        buttonText[GC.InputMan.GcCalculatorKey.D_2] = "二";
        buttonText[GC.InputMan.GcCalculatorKey.D_3] = "三";
        buttonText[GC.InputMan.GcCalculatorKey.D_4] = "四";
        buttonText[GC.InputMan.GcCalculatorKey.D_5] = "五";
        buttonText[GC.InputMan.GcCalculatorKey.D_6] = "六";
        buttonText[GC.InputMan.GcCalculatorKey.D_7] = "七";
        buttonText[GC.InputMan.GcCalculatorKey.D_8] = "八";
        buttonText[GC.InputMan.GcCalculatorKey.D_9] = "九";

        buttonText[GC.InputMan.GcCalculatorKey.Add] = "加算";
        buttonText[GC.InputMan.GcCalculatorKey.Sub] = "減算";
        buttonText[GC.InputMan.GcCalculatorKey.Multiply] = "乗算";
        buttonText[GC.InputMan.GcCalculatorKey.Divide] = "除算";
        
        buttonText[GC.InputMan.GcCalculatorKey.Sign] = "符号";
        buttonText[GC.InputMan.GcCalculatorKey.Sqrt] = "平方根";
        buttonText[GC.InputMan.GcCalculatorKey.Fraction] = "逆数";
        buttonText[GC.InputMan.GcCalculatorKey.Dot] = "小数点";
        buttonText[GC.InputMan.GcCalculatorKey.Equal] = "実行";
        buttonText[GC.InputMan.GcCalculatorKey.Percentage] = "割合";

        buttonText[GC.InputMan.GcCalculatorKey.BS] = "最後の桁を消去";
        buttonText[GC.InputMan.GcCalculatorKey.CE] = "現在の値を消去";
        buttonText[GC.InputMan.GcCalculatorKey.C] = "計算を消去";

        buttonText[GC.InputMan.GcCalculatorKey.MC] = "記憶消去";
        buttonText[GC.InputMan.GcCalculatorKey.MR] = "記憶呼出";
        buttonText[GC.InputMan.GcCalculatorKey.MS] = "記憶格納";
        buttonText[GC.InputMan.GcCalculatorKey.MAdd] = "記憶加算";
        buttonText[GC.InputMan.GcCalculatorKey.MStatus] = "記憶有";
        this.btnText = buttonText;
    }

    restoreButtonText(){
        this.btnText = {};
        this.calc.getNestedIMControl().restoreButtonText();
    }
}

@Pipe({
    name: 'dateConvertor',
    pure: true
})
export class DateConvertorPipe implements PipeTransform {

    transform(value: any, args?: any): Date {
        return new Date(value);
    }
}

enableProdMode();