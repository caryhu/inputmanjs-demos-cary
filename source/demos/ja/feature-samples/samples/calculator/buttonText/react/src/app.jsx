import * as React from "react";
import * as ReactDom from "react-dom";
import { GcCalculator } from "@grapecity/inputman.react";
import {InputMan} from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';


class App extends React.Component{

    constructor(){
        super();
        this.state = {
            buttonText: {}
        }
        this.calc = React.createRef();
    }

    render(){
        return <React.Fragment>
                <div>
                    <GcCalculator buttonText={this.state.buttonText} ref={this.calc}></GcCalculator>
                </div>
                <br />
                <div>
                    <button onClick={this.updateButtonText.bind(this)}>ボタンテキストの変更する</button>
                    <button onClick={this.restoreButtonText.bind(this)}>ボタンテキストを初期化する</button>
                </div>
            </React.Fragment>
    }

    updateButtonText(){
        let buttonText = {};
        buttonText[InputMan.GcCalculatorKey.D_0] = "零";
        buttonText[InputMan.GcCalculatorKey.D_1] = "一";
        buttonText[InputMan.GcCalculatorKey.D_2] = "二";
        buttonText[InputMan.GcCalculatorKey.D_3] = "三";
        buttonText[InputMan.GcCalculatorKey.D_4] = "四";
        buttonText[InputMan.GcCalculatorKey.D_5] = "五";
        buttonText[InputMan.GcCalculatorKey.D_6] = "六";
        buttonText[InputMan.GcCalculatorKey.D_7] = "七";
        buttonText[InputMan.GcCalculatorKey.D_8] = "八";
        buttonText[InputMan.GcCalculatorKey.D_9] = "九";

        buttonText[InputMan.GcCalculatorKey.Add] = "加算";
        buttonText[InputMan.GcCalculatorKey.Sub] = "減算";
        buttonText[InputMan.GcCalculatorKey.Multiply] = "乗算";
        buttonText[InputMan.GcCalculatorKey.Divide] = "除残";
        
        buttonText[InputMan.GcCalculatorKey.Sign] = "符号";
        buttonText[InputMan.GcCalculatorKey.Sqrt] = "平方根";
        buttonText[InputMan.GcCalculatorKey.Fraction] = "逆数";
        buttonText[InputMan.GcCalculatorKey.Dot] = "小数点";
        buttonText[InputMan.GcCalculatorKey.Equal] = "実行";
        buttonText[InputMan.GcCalculatorKey.Percentage] = "割合";

        buttonText[InputMan.GcCalculatorKey.BS] = "最後の桁を消去";
        buttonText[InputMan.GcCalculatorKey.CE] = "現在の値を消去";
        buttonText[InputMan.GcCalculatorKey.C] = "計算を消去";

        buttonText[InputMan.GcCalculatorKey.MC] = "記憶消去";
        buttonText[InputMan.GcCalculatorKey.MR] = "記憶呼出";
        buttonText[InputMan.GcCalculatorKey.MS] = "記憶格納";
        buttonText[InputMan.GcCalculatorKey.MAdd] = "記憶加算";
        buttonText[InputMan.GcCalculatorKey.MStatus] = "記憶有";
        this.setState({buttonText});
    }

    restoreButtonText(){
        this.setState({buttonText: {}});
        this.calc.current.getNestedIMControl().restoreButtonText();
    }
}

ReactDom.render(<App />, document.getElementById("app"));