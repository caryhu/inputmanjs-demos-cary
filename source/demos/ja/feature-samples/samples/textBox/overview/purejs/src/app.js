import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcTextBox1 = new InputMan.GcTextBox(document.getElementById('gcTextBox1'), {
    format: 'N',
    autoConvert: true
});
const gcTextBox2 = new InputMan.GcTextBox(document.getElementById('gcTextBox2'), {
    format: 'Ｚ',
    autoConvert: true
});
const gcTextBox3 = new InputMan.GcTextBox(document.getElementById('gcTextBox3'), {
    IMEReadingStringAppend: false
});

gcTextBox3.onIMEReadingStringOutput((sender, eArgs) => {
    document.getElementById('furigana').innerText = eArgs.readingString;
});