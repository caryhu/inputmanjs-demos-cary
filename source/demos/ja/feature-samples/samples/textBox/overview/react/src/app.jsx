import * as React from "react";
import * as ReactDom from "react-dom";
import { GcTextBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

class App extends React.Component{

    render(){
        return (
            <div class="flexbox">
                <div>
                    半角カタカナを入力<br/>
                    <GcTextBox format= {'N'} autoConvert= {true}></GcTextBox>
                </div>
                <div>
                    全角文字を入力<br/>
                    <GcTextBox format= {'Ｚ'} autoConvert= {true}></GcTextBox>
                </div>
                <div>
                    ふりがなの取得
                    <GcTextBox 
                        IMEReadingStringAppend= {false} 
                        onIMEReadingStringOutput={(eArgs) => {
                            var lbl = document.getElementById("furigana");
                            lbl.innerText = eArgs.getIMEReadingString();
                        }}>
                    </GcTextBox>
                    ｶﾅ: <span id="furigana"></span>
                </div>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));