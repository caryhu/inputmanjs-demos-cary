﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';
import './styles.css';

const gcTextBox = new InputMan.GcTextBox(document.getElementById('gcTextBox'), {
    text: 'テキスト',
    showClearButton: true,
});

document.getElementById('showClearButton').addEventListener('click', (e) => {
    gcTextBox.setShowClearButton(e.target.checked);
});