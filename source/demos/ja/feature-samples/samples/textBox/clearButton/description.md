テキストコントロール内にクリアボタンを表示する機能について解説します。

## クリアボタンを表示する方法
クリアボタンをクリックすると、入力されている値をクリアします。

クリアボタンを表示するには、コンストラクタのshowClearButtonオプションにtrueを設定します。
```
new GC.InputMan.GcTextBox(document.getElementById('gcTextBox'), {
      showClearButton: true
});
```

また、setShowClearButtonメソッドを使用することで、クリアボタンの表示の有無を切り替えることができます。
```
gcTextBox.setShowClearButton(true);
```
