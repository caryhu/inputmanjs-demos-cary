﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

// コントロールを初期化
const gcTextBox1 = new InputMan.GcTextBox(document.getElementById('gcTextBox1'), {
    maxLength: 3
});
const gcTextBox2 = new InputMan.GcTextBox(document.getElementById('gcTextBox2'), {
    maxLength: 3
});
const gcTextBox3 = new InputMan.GcTextBox(document.getElementById('gcTextBox3'), {
    maxLength: 3
});

// 自動フォーカス移動
document.getElementById('setExitOnLastChar').addEventListener('change', (e) => {
    const isExitOnLastChar = e.target.checked;
    gcTextBox1.setExitOnLastChar(isExitOnLastChar);
    gcTextBox2.setExitOnLastChar(isExitOnLastChar);
    gcTextBox3.setExitOnLastChar(isExitOnLastChar);
});

// 矢印キーによるフォーカス移動
const ExitOnLeftRightKeys = [
    InputMan.ExitOnLeftRightKey.None,
    InputMan.ExitOnLeftRightKey.Both,
    InputMan.ExitOnLeftRightKey.Left,
    InputMan.ExitOnLeftRightKey.Right
];
document.getElementById('setExitOnLeftRightKey').addEventListener('change', (e) => {
    gcTextBox1.setExitOnLeftRightKey(ExitOnLeftRightKeys[e.target.selectedIndex]);
    gcTextBox2.setExitOnLeftRightKey(ExitOnLeftRightKeys[e.target.selectedIndex]);
    gcTextBox3.setExitOnLeftRightKey(ExitOnLeftRightKeys[e.target.selectedIndex]);
});

// Enterキーによるフォーカス移動
const ExitKeys = [
    InputMan.ExitKey.None,
    InputMan.ExitKey.Both,
    InputMan.ExitKey.Enter,
    InputMan.ExitKey.ShiftEnter
];
document.getElementById('setExitOnEnterKey').addEventListener('change', (e) => {
    gcTextBox1.setExitOnEnterKey(ExitKeys[e.target.selectedIndex]);
    gcTextBox2.setExitOnEnterKey(ExitKeys[e.target.selectedIndex]);
    gcTextBox3.setExitOnEnterKey(ExitKeys[e.target.selectedIndex]);
});