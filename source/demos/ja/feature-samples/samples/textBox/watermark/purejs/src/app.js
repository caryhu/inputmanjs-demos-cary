﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';
import './styles.css';

const gcTextBox = new InputMan.GcTextBox(document.getElementById('gcTextBox'), {
    // フォーカスがないときの代替テキスト
    watermarkDisplayNullText: '氏名',
    // フォーカスがあるときの代替テキスト
    watermarkNullText: '全角で入力してください'
});