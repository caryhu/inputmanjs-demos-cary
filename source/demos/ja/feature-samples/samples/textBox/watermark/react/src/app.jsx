import * as React from "react";
import * as ReactDom from "react-dom";
import { GcTextBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{

    render(){
        return <GcTextBox 
                // フォーカスがないときの代替テキスト
                watermarkDisplayNullText= {'氏名'}
                // フォーカスがあるときの代替テキスト
                watermarkNullText= {'全角で入力してください'}
            />
    }
}

ReactDom.render(<App />, document.getElementById("app"));