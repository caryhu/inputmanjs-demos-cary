import * as React from "react";
import * as ReactDom from "react-dom";
import { GcTextBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

class App extends React.Component{

    render(){
        return (
            <div>
                テキスト<br/>
                <GcTextBox></GcTextBox><br/>
                標準のテキストボックス<br/>
                <input></input>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));