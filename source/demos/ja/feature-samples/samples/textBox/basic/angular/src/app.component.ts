import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public maxLength: number = 5;
    public lengthAsByte: boolean;
    public acceptsCrlf: GC.InputMan.CrLfMode;
    public crLfModes = [
        GC.InputMan.CrLfMode.NoControl,
        GC.InputMan.CrLfMode.Filter,
        GC.InputMan.CrLfMode.Cut
    ];

}

enableProdMode();