﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcTextBox = new InputMan.GcTextBox(document.getElementById('gcTextBox'), {
    maxLength: 5
});

document.getElementById('setMaxLength').addEventListener('change', (e) => {
    gcTextBox.setMaxLength(e.target.value);
});

document.getElementById('setLengthNotAsByte').addEventListener('change', (e) => {
    gcTextBox.setLengthAsByte(!e.target.checked);
});

document.getElementById('setLengthAsByte').addEventListener('change', (e) => {
    gcTextBox.setLengthAsByte(e.target.checked);
});

document.getElementById('setAcceptsCrlf').addEventListener('change', (e) => {
    gcTextBox.setAcceptsCrlf(crLfModes[e.target.selectedIndex]);
});

const crLfModes = [
    InputMan.CrLfMode.NoControl,
    InputMan.CrLfMode.Filter,
    InputMan.CrLfMode.Cut
];