import * as React from "react";
import * as ReactDom from "react-dom";
import { GcTextBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

const crLfModes = [
    InputMan.CrLfMode.NoControl,
    InputMan.CrLfMode.Filter,
    InputMan.CrLfMode.Cut
];
class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            maxLength: 5,
            lengthAsByte: false,
            acceptsCrlf: InputMan.CrLfMode.NoControl
        };
    }
    render(){
        return (
            <div>
                <GcTextBox maxLength={this.state.maxLength} lengthAsByte={this.state.lengthAsByte} acceptsCrlf={this.state.acceptsCrlf}></GcTextBox>
                <table class="sample">
                    <tr>
                        <th>最大文字数</th>
                        <td>
                            <input type="number" value={this.state.maxLength} min={0} id="setMaxLength" onChange={(e)=>this.setState({maxLength: Number(e.target.value)})}/>&nbsp;
                            <label><input type="radio" name="setLengthAsByte" onChange={(e)=>this.setState({lengthAsByte: !e.target.checked})} id="setLengthNotAsByte" checked={!this.state.lengthAsByte}/>文字単位</label>&nbsp;
                            <label><input type="radio" name="setLengthAsByte" onChange={(e)=>{this.setState({lengthAsByte: e.target.checked});}} id="setLengthAsByte" checked={this.state.lengthAsByte}/>バイト単位</label>
                        </td>
                    </tr>
                    <tr>
                        <th>改行コードの扱い</th>
                        <td>
                            <select id="setAcceptsCrlf" onChange={(e)=>this.setState({acceptsCrlf: crLfModes[e.target.selectedIndex]})}>
                                <option>そのまま使用</option>
                                <option>すべての改行コードを削除</option>
                                <option>改行コード以降の文字列を切り取り</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));