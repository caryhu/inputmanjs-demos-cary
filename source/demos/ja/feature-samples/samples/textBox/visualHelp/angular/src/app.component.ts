import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public ellipsisString: string="...";
    public ellipsisMode: GC.InputMan.EllipsisMode;

    public ellipsisModes = [
        GC.InputMan.EllipsisMode.None,
        GC.InputMan.EllipsisMode.EllipsisEnd,
        GC.InputMan.EllipsisMode.EllipsisPath
    ];
}

enableProdMode();