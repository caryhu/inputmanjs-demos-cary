System.config({
    transpiler: 'plugin-babel',
    babelOptions: {
        es2015: true
    },
    meta: {
        '*.css': { loader: 'css' }
    },
    paths: {
        // paths serve as alias
        'npm:': 'node_modules/'
    },
    // map tells the System loader where to look for things
    map: {
        '@grapecity/inputman': 'npm:@grapecity/inputman/index.js',
        '@grapecity/inputman/CSS': 'npm:@grapecity/inputman/CSS',
        'css': 'npm:systemjs-plugin-css/css.js',
        'plugin-babel': 'npm:systemjs-plugin-babel/plugin-babel.js',
        'systemjs-babel-build': 'npm:systemjs-plugin-babel/systemjs-babel-browser.js'
    },
    // packages tells the System loader how to load when no filename and/or no extension
    packages: {
        src: {
            defaultExtension: 'js'
        },
        "node_modules": {
            defaultExtension: 'js'
        },
    }
});

