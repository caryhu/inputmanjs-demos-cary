﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcTextBox = new InputMan.GcTextBox(document.getElementById('gcTextBox'), {
    text: '宮城県仙台市泉区紫山３－１－４'
});

document.getElementById('setEllipsis').addEventListener('change', (e) => {
    gcTextBox.setEllipsis(ellipsisModes[e.target.selectedIndex]);
});

const ellipsisString = new InputMan.GcTextBox(document.getElementById('setEllipsisString'),{
    text: '...'
});
ellipsisString.onTextChanged(() => {
    gcTextBox.setEllipsisString(ellipsisString.getText());
})

const ellipsisModes = [
    InputMan.EllipsisMode.None,
    InputMan.EllipsisMode.EllipsisEnd,
    InputMan.EllipsisMode.EllipsisPath
];