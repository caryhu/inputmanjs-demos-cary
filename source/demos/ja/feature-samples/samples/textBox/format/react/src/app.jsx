import * as React from "react";
import * as ReactDom from "react-dom";
import { GcTextBox, GcComboBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            format: '',
            autoConvert: true
        };
    }
    render(){
        return (
            <div>
                <GcTextBox id="gcTextBox" format={this.state.format} autoConvert={this.state.autoConvert}/>
                <table class="sample">
                    <tr>
                        <th>文字種の設定</th>
                        <td>
                            <GcComboBox id="format"
                            items= {[
                                { value: 'Ａ', desc: '全角大文字のアルファベット' },
                                { value: 'A', desc: '半角大文字のアルファベット' },
                                { value: 'ａ', desc: '全角小文字のアルファベット' },
                                { value: 'a', desc: '半角小文字のアルファベット' },
                                { value: 'Ｋ', desc: '全角カタカナ（促音・拗音の小書き表記あり）' },
                                { value: 'K', desc: '半角カタカナ（促音・拗音の小書き表記あり）' },
                                { value: 'Ｎ', desc: '全角カタカナ（促音・拗音の小書き表記なし）' },
                                { value: 'N', desc: '半角カタカナ（促音・拗音の小書き表記なし）' },
                                { value: '９', desc: '全角数字' },
                                { value: '	9', desc: '半角数字' },
                                { value: '＃', desc: '全角数字および数字関連記号' },
                                { value: '#', desc: '半角数字および数字関連記号' },
                                { value: '＠', desc: '全角記号' },
                                { value: '@', desc: '半角記号' },
                                { value: 'Ｂ', desc: '全角２進数' },
                                { value: 'B', desc: '半角２進数' },
                                { value: 'Ｘ', desc: '全角16進数' },
                                { value: 'X', desc: '半角16進数' },
                                { value: 'Ｓ', desc: '全角空白文字' },
                                { value: 'S', desc: '半角空白文字' },
                                { value: 'Ｊ', desc: 'ひらがな（促音・拗音の小書き表記あり）' },
                                { value: 'Ｇ', desc: 'ひらがな（促音・拗音の小書き表記なし）' },
                                { value: 'Ｚ', desc: '空白文字以外のすべての全角文字' },
                                { value: '^Ｔ', desc: 'サロゲートペア文字以外' },
                                { value: 'Ｉ', desc: 'JIS X 0208で構成された文字' },
                                { value: 'Ｍ', desc: 'Shift JISで構成された文字' },
                                { value: '^Ｖ', desc: 'IVS文字以外' },
                                { value: 'Ｄ', desc: '空白文字以外の２バイト文字' },
                                { value: 'H', desc: '空白文字以外のすべての半角文字' },
                            ]}
                            columns= {[
                                { name: 'value', label: '値', width: 50 },
                                { name: 'desc', label: '説明', width: 300 }
                            ]}
                            displayMemberPath= {'value'}
                            valueMemberPath= {'value'}
                            dropDownWidth= {'auto'}
                            isMultiSelect= {true}
                            checkedChanged={(e)=>{this.setState({format: e.getCheckedValues().join('')})}}></GcComboBox>
                        </td>
                    </tr>
                    <tr>
                        <th>自動変換</th>
                        <td>
                            <label><input type="checkbox" id="setAutoConvert" checked={this.state.autoConvert} onChange={(e)=> this.setState({autoConvert: e.target.checked})}/>指定した文字にあわせて自動的に変換する</label>
                        </td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));