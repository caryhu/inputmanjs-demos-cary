﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';
import './styles.css';

const gcTextBox = new InputMan.GcTextBox(document.getElementById('gcTextBox'), {
    format: ''
});

const format = new InputMan.GcComboBox(document.getElementById('format'), {
    items: [
        { value: 'Ａ', desc: '全角大文字のアルファベット' },
        { value: 'A', desc: '半角大文字のアルファベット' },
        { value: 'ａ', desc: '全角小文字のアルファベット' },
        { value: 'a', desc: '半角小文字のアルファベット' },
        { value: 'Ｋ', desc: '全角カタカナ（促音・拗音の小書き表記あり）' },
        { value: 'K', desc: '半角カタカナ（促音・拗音の小書き表記あり）' },
        { value: 'Ｎ', desc: '全角カタカナ（促音・拗音の小書き表記なし）' },
        { value: 'N', desc: '半角カタカナ（促音・拗音の小書き表記なし）' },
        { value: '９', desc: '全角数字' },
        { value: '	9', desc: '半角数字' },
        { value: '＃', desc: '全角数字および数字関連記号' },
        { value: '#', desc: '半角数字および数字関連記号' },
        { value: '＠', desc: '全角記号' },
        { value: '@', desc: '半角記号' },
        { value: 'Ｂ', desc: '全角２進数' },
        { value: 'B', desc: '半角２進数' },
        { value: 'Ｘ', desc: '全角16進数' },
        { value: 'X', desc: '半角16進数' },
        { value: 'Ｓ', desc: '全角空白文字' },
        { value: 'S', desc: '半角空白文字' },
        { value: 'Ｊ', desc: 'ひらがな（促音・拗音の小書き表記あり）' },
        { value: 'Ｇ', desc: 'ひらがな（促音・拗音の小書き表記なし）' },
        { value: 'Ｚ', desc: '空白文字以外のすべての全角文字' },
        { value: '^Ｔ', desc: 'サロゲートペア文字以外' },
        { value: 'Ｉ', desc: 'JIS X 0208で構成された文字' },
        { value: 'Ｍ', desc: 'Shift JISで構成された文字' },
        { value: '^Ｖ', desc: 'IVS文字以外' },
        { value: 'Ｄ', desc: '空白文字以外の２バイト文字' },
        { value: 'H', desc: '空白文字以外のすべての半角文字' },
    ],
    columns: [
        { name: 'value', label: '値', width: 50 },
        { name: 'desc', label: '説明', width: 300 }
    ],
    displayMemberPath: 'value',
    valueMemberPath: 'value',
    dropDownWidth: 'auto',
    isMultiSelect: true
});
format.addEventListener(InputMan.GcComboBoxEvent.CheckedChanged, (control, args) => {
    gcTextBox.setFormat(format.getCheckedValues().join(''));
});

document.getElementById('setAutoConvert').addEventListener('click', (e) => {
    gcTextBox.setAutoConvert(e.target.checked);
});