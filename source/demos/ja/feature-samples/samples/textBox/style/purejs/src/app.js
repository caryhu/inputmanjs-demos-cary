﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcTextBox1 = new InputMan.GcTextBox(document.getElementById('gcTextBox1'), {
    text: 'テキスト'
});

const gcTextBox2 = new InputMan.GcTextBox(document.getElementById('gcTextBox2'), {
    text: 'テキスト',
    enabled: false
});

const gcTextBox3 = new InputMan.GcTextBox(document.getElementById('gcTextBox3'), {
    watermarkDisplayNullText: '氏名',
    watermarkNullText: '全角で入力してください'
});

var styleSheet;
var indices = [];
const styles = {
    '.gcim': { width: '200px', height: '40px', backgroundColor: '#ddffdd', borderColor: '#009900', borderWidth: '2px', borderStyle: 'dashed', borderRadius: '12px', boxShadow: '5px 5px 5px rgba(0,0,0,0.5)', color: '#009900' },
    '.gcim__input': { cursor: 'crosshair', fontSize: '20px', fontWeight: 'bold', fontStyle: 'italic', fontFamily: 'serif', textAlign: 'right', textShadow: '1px 1px 1px rgba(0,0,0,0.5)' },
    '.gcim__input:disabled': { backgroundColor: '#666666', color: '#cccccc', cursor: 'wait' },
    '.gcim_focused': { backgroundColor: '#ddddff', borderColor: '#0000ff', color: '#0000ff' },
    '.gcim_watermark_null': { backgroundColor: '#ffdddd', borderColor: '#ff0000', color: '#ff0000' },
    '.gcim_focused.gcim_watermark_null': { backgroundColor: '#ffddff', borderColor: '#990099', color: '#990099' },
};


const createInitialStyles = () => {
    const element = document.createElement('style');
    document.head.appendChild(element);
    styleSheet = element.sheet;
    var i = 0;
    for (const styleName in styles) {
        styleSheet.insertRule(styleName + '{}', i);
        indices[styleName] = i;
        i++;
    }
}
createInitialStyles();

const updateStyle = (event) => {
    console.log(event);
    var element = event.target;
    if (element.tagName == 'INPUT') {
        var values = element.value.split(',');
        var styleName = values[0];
        var propertyName = values[1];
        styleSheet.cssRules[indices[styleName]].style[propertyName] = element.checked ? styles[styleName][propertyName] : '';
    }

    var style = '';
    for (var i = 0; i < styleSheet.cssRules.length; i++) {
        if (styleSheet.cssRules[i].style.length > 0) {
            style += styleSheet.cssRules[i].cssText.replace(/{/g, '{\n ').replace(/;/g, ';\n ').replace(/  }/g, '}') + '\n';
        }
    }
    document.getElementById('style').value = style;
}

const copyStyle = () => {
    wijmo.Clipboard.copy(document.getElementById('style').value);
    document.execCommand('copy');
}

var panels = document.getElementsByClassName('peoperty-panel');
for (var i = 0; i < panels.length; i++) {
    panels[i].addEventListener('click', updateStyle);
}
document.getElementById('copyStyle').addEventListener('click', copyStyle);