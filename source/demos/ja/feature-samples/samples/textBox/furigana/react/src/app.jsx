import * as React from "react";
import * as ReactDom from "react-dom";
import { GcTextBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

const kanaModes = [
    InputMan.KanaMode.KatakanaHalf,
    InputMan.KanaMode.Katakana,
    InputMan.KanaMode.Hiragana
];
class App extends React.Component{
    gcTextBox = null;
    render(){
        return (
            <div>
                <label>漢字入力: </label>
                <GcTextBox 
                    className={'displayStyle'}
                    IMEReadingStringAppend={false} 
                    onIMEReadingStringOutput={(eArgs) => {
                        var furigana = document.getElementById('furigana');
                        furigana.innerText =  eArgs.getIMEReadingString();
                    }}
                    onInitialized={(imCtrl)=>{ this.gcTextBox = imCtrl; }}>        
                </GcTextBox>
                <br/>
                <p>ふりがな: <span id="furigana"></span></p>
                <button id="clearText" onClick={(e)=>{
                    var furigana = document.getElementById('furigana');
                    this.gcTextBox.setText('');
                    furigana.innerText = '';
                    }}>クリア</button>

                <table class="sample">
                    <tr>
                        <th>ふりがなの文字種</th>
                        <td>
                            <select id="setReadingImeStringKanaMode" onChange={(e)=>{this.gcTextBox.setReadingImeStringKanaMode(kanaModes[e.target.selectedIndex])}}>
                                <option>半角カタカナ</option>
                                <option>全角カタカナ</option>
                                <option>ひらがな</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));