﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcTextBox = new InputMan.GcTextBox(document.getElementById('gcTextBox'), {
    IMEReadingStringAppend: false
});
const furigana = document.getElementById('furigana');
gcTextBox.onIMEReadingStringOutput((sender, eArgs) => {
    furigana.innerText = eArgs.readingString;
});

document.getElementById('clearText').addEventListener('click', () => {
    gcTextBox.setText('');
    furigana.innerText = '';
});

document.getElementById('setReadingImeStringKanaMode').addEventListener('change', (e) => {
    gcTextBox.setReadingImeStringKanaMode(kanaModes[e.target.selectedIndex]);
});

const kanaModes = [
    InputMan.KanaMode.KatakanaHalf,
    InputMan.KanaMode.Katakana,
    InputMan.KanaMode.Hiragana
];