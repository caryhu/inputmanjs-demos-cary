﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcTextBox = new InputMan.GcTextBox(document.getElementById('gcTextBox'), {
    autoScale:true,
    minScaleFactor:0.5
});

document.getElementById('clearText').addEventListener('click', () => {
    gcTextBox.clear();
});

document.getElementById('setAutoScale').addEventListener('click', (e) => {
    gcTextBox.setAutoScale(e.target.checked);
});

document.getElementById('setMinScaleFactor').addEventListener('change', (e) => {
    gcTextBox.setMinScaleFactor(Number(e.target.value));
});