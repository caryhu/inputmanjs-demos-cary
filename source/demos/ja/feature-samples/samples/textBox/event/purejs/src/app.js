﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcTextBox = new InputMan.GcTextBox(document.getElementById('gcTextBox'));

// イベントハンドラ
gcTextBox.onEditStatusChanged( (sender, eArgs)=> {
    log('onEditStatusChanged');
});
gcTextBox.onFocusOut( (sender, eArgs) =>{
    log('onFocusOut');
});
gcTextBox.onIMEReadingStringOutput( (sender, eArgs) =>{
    log('onIMEReadingStringOutput');
});
gcTextBox.onInput( (sender, eArgs) =>{
    log('onInput');
});
gcTextBox.onInvalidInput( (sender, eArgs) =>{
    log('onInvalidInput');
});
gcTextBox.onKeyDown( (sender, eArgs) =>{
    log('onKeyDown');
});
gcTextBox.onKeyExit( (sender, eArgs) =>{
    log('onKeyExit');
});
gcTextBox.onKeyUp( (sender, eArgs)=> {
    log('onKeyUp');
});
gcTextBox.onSyncValueToOriginalInput( (sender, eArgs)=> {
    log('onSyncValueToOriginalInput');
});
gcTextBox.onTextChanged( (sender, eArgs)=> {
    log('onTextChanged');
});

// テキストボックスにログを出力
const log = (message) => {
    const textarea = document.getElementById('log');
    textarea.value += message + '\n';
    textarea.scrollTop = textarea.scrollHeight;
}