import * as React from "react";
import * as ReactDom from "react-dom";
import { GcTextBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            passwordChar:  '●',
            showPassword: true
        };
    }
    render(){
        return (
            <div>
                <div>
                    通常のパスワード文字表示(オプションなし)<br/>
                    <GcTextBox
                        ref="textbox1"
                        passwordChar={this.state.passwordChar}
                        passwordRevelationMode={InputMan.PasswordRevelationMode.None} />
                </div>
                <br/>
                <div>
                    パスワード表示アイコンをクリックしている間だけテキストを表示する<br/>
                    <GcTextBox 
                        ref="textbox2"
                        passwordChar={this.state.passwordChar}
                        passwordRevelationMode={InputMan.PasswordRevelationMode.ShowEyeButton} />
                </div>
                <br/>
                <div>
                    入力から1秒後にパスワード文字表示にする<br/>
                    <GcTextBox 
                        ref="textbox3"
                        passwordChar={this.state.passwordChar}
                        passwordRevelationMode={InputMan.PasswordRevelationMode.ShowLastTypedChar} />
                </div>
                <br/>
                <div>
                    チェックボックスでパスワード表示を切り替える<br/>
                    <GcTextBox 
                        ref="textbox4"
                        className={'displayStyle'}
                        passwordChar={this.state.passwordChar}
                        passwordRevelationMode={InputMan.PasswordRevelationMode.None} />&nbsp;
                    <input type="checkbox" checked={this.state.showPassword} onChange={(e)=>{
                        this.setState({showPassword: e.target.checked});
                        this.refs.textbox4.getNestedIMControl().setPasswordChar(e.target.checked ? this.state.passwordChar : '');
                    }}/><label>表示する</label>
                </div>
                <br/>
                <button onClick={()=>{
                    this.refs.textbox1.getNestedIMControl().setText('');
                    this.refs.textbox2.getNestedIMControl().setText('');
                    this.refs.textbox3.getNestedIMControl().setText('');
                    this.refs.textbox4.getNestedIMControl().setText('');
                }}>パスワードをクリア</button>
                <table class="sample">
                    <tr>
                    <th>パスワード表示に使用する文字</th>
                    <td> 
                        <GcTextBox text={this.state.passwordChar} onTextChanged={(e)=> {
                            this.setState({passwordChar: e.getText()});
                            }} /> 
                    </td>
                    </tr>
                </table>
            </div> 
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));