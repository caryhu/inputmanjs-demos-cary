import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, ViewChild } from '@angular/core';
import * as GC from "@grapecity/inputman";
import { GcTextBoxComponent } from "@grapecity/inputman.angular";
 
@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    
    public passwordChar:string = '●';
    public showPassword:boolean = true;

    @ViewChild("textbox1")
    textbox1: GcTextBoxComponent;

    @ViewChild("textbox2")
    textbox2: GcTextBoxComponent;

    @ViewChild("textbox3")
    textbox3: GcTextBoxComponent;

    @ViewChild("textbox4")
    textbox4: GcTextBoxComponent;

    clearText(){
       this.textbox1.getNestedIMControl().setText('');
       this.textbox2.getNestedIMControl().setText('');
       this.textbox3.getNestedIMControl().setText('');
       this.textbox4.getNestedIMControl().setText('');
    }

    onPasswordCharChanged(sender: GC.InputMan.GcTextBox){
        this.passwordChar = sender.getText();
        this.update();
    }

    onShowPasswordChanged(e: Event){
        this.showPassword = (e.target as HTMLInputElement).checked;
        this.update();
    }

    update(){
        this.textbox4.getNestedIMControl().setPasswordChar(this.showPassword ? this.passwordChar : '');
    }
}

enableProdMode();