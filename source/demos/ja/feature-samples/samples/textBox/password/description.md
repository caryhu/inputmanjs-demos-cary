テキストコントロールが提供するパスワード文字の表示機能について解説します。

## 入力されたテキストをパスワード文字表示する方法
コンストラクタのuseSystemPasswordCharオプションにtrueを設定するか、setUseSystemPasswordCharメソッドを使用すると、テキストをパスワード文字表示にすることができます。
このとき、パスワード文字は、Windows／iOS／iPadOS環境では「●」で表示され、Mac環境では「*」で表示されます。
```
new GC.InputMan.GcTextBox(document.getElementById('gcTextBox'), {
      useSystemPasswordChar : true,
});

gcTextBox.setUseSystemPasswordChar(true);
```

また、パスワード文字を自身で設定したい場合は、passwordCharオプションもしくは、setPasswordCharメソッドを利用することもできます。

```
new GC.InputMan.GcTextBox(document.getElementById('gcTextBox'), {
      /// パスワード文字を*で表示する
      passwordChar : '*',
});

gcTextBox.setPasswordChar('*');
```

useSystemPasswordCharとpasswordCharが同時に指定されている時は、useSystemPasswordCharの設定が優先されます。

## 表示されているパスワード文字のアクション

setPasswordRevelationModeメソッドを利用すると、表示されているパスワード文字列のオプションを設定することができます。設定できる値は次のとおりで、既定値はPasswordRevelationMode.Noneです。

| PasswordRevelationModeの値                    | 説明                                          |
| --------------------------------------------- | --------------------------------------------- |
| None                                          | 通常のパスワード文字表示が行われます               |
| ShowEyeButton                                 | パスワード表示アイコンが表示されるようになり、クリックしている間だけテキストを表示します|
| ShowLastTypedChar                             | 最後に入力した文字を、入力から1秒後にパスワード文字表示します|

なお、PasswordRevelationMode.ShowEyeButtonの場合、パスワード入力中にコントロールからフォーカスを外し、再度コントロールにフォーカスすると、パスワード表示アイコンは表示されなくなります。これは、セキュリティのために設計された動作です。
入力されているパスワードを全て削除するか、入力されているパスワードを全選択して上書き入力すると、パスワード表示アイコンを再表示されます。