﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcTextBox1 = new InputMan.GcTextBox(document.getElementById('gcTextBox1'), {
    passwordChar : '●',
    passwordRevelationMode:InputMan.PasswordRevelationMode.None,
});
const gcTextBox2 = new InputMan.GcTextBox(document.getElementById('gcTextBox2'), {
    passwordChar : '●',
    passwordRevelationMode:InputMan.PasswordRevelationMode.ShowEyeButton,
});
const gcTextBox3 = new InputMan.GcTextBox(document.getElementById('gcTextBox3'), {
    passwordChar : '●',
    passwordRevelationMode:InputMan.PasswordRevelationMode.ShowLastTypedChar,
});

const gcTextBox4 = new InputMan.GcTextBox(document.getElementById('gcTextBox4'), {
    passwordChar : '●',
    passwordRevelationMode:InputMan.PasswordRevelationMode.None,
});

document.getElementById('showPassword').addEventListener('click', (e) => {
    gcTextBox4.setPasswordChar(e.target.checked ? passwordString.getText(): '')
});

const passwordString = new InputMan.GcTextBox(document.getElementById('setPasswordString'),{
    text: '●',
    maxLength:1
});

document.getElementById('clearPasswords').addEventListener('click', () => {
    gcTextBox1.setText('');
    gcTextBox2.setText('');
    gcTextBox3.setText('');
    gcTextBox4.setText('');
});

passwordString.onTextChanged(() => {
    gcTextBox1.setPasswordChar(passwordString.getText());
    gcTextBox2.setPasswordChar(passwordString.getText());
    gcTextBox3.setPasswordChar(passwordString.getText());
    gcTextBox4.setPasswordChar(passwordString.getText());
})