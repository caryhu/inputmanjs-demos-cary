import * as React from "react";
import * as ReactDom from "react-dom";
import { GcDateTime } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            message: ''
        };
        this.datetime = React.createRef();
    }

    log(msg){
        var newMessage = this.state.message + msg + '\n';
        this.refs.log.scrollTop = this.refs.log.scrollHeight;
        this.setState({message: newMessage});
    }

    componentDidMount(){
        var gcDateTime = this.datetime.current.getNestedIMControl();
        gcDateTime.getDropDownWindow().onClose(() => this.log('onClose'));
        gcDateTime.getDropDownWindow().onOpen(() => this.log('onOpen'));
        gcDateTime.getDropDownWindow().onClosing(() => this.log('onClosing'));
        gcDateTime.getDropDownWindow().onOpening(() => this.log('onOpening'));
    }

    render(){
        return(
            <div>
                <GcDateTime
                    ref={this.datetime}
                    showDropDownButton={true}
                    dropDownConfig={
                        {dropDownType: InputMan.DateDropDownType.Calendar}
                    }
                    onInitialized={
                        (sender)=> sender.onSyncValueToOriginalInput(() => this.log('onSyncValueToOriginalInput'))
                    }
                    onEditStatusChanged={
                        (sender)=>{
                            this.log("onEditStatusChanged");
                        }
                    }
                    onInvalidInput={
                        (sender)=>{
                            this.log("onInvalidInput");
                        }
                    }
                    onKeyExit={
                        (sender)=>{
                            this.log("onKeyExit");
                        }
                    }
                    onTextChanged={
                        (sender)=>{
                            this.log("onTextChanged");
                        }
                    }
                    onKeyDown={
                        (sender)=>{
                            this.log("onKeyDown");
                        }
                    }
                    onKeyUp={
                        (sender)=>{
                            this.log("onKeyUp");
                        }
                    }
                    onFocusOut={
                        (sender)=>{
                            this.log("onFocusOut");
                        }
                    }
                    onInput={
                        (sender)=>{
                            this.log("onInput");
                        }
                    }
                    onSpinDown={
                        (sender)=>{
                            this.log("onSpinDown");
                        }
                    }
                    onSpinUp={
                        (sender)=>{
                            this.log("onSpinUp");
                        }
                    }
                    onInvalidRange={
                        (sender)=>{
                            this.log("onInvalidRange");
                        }
                    }
                    onInvalidValue={
                        (sender)=>{
                            this.log("onInvalidValue");
                        }
                    }
                    onNumberChanged={
                        (sender)=>{
                            this.log("onNumberChanged");
                        }
                    }
                    onValueChanged={
                        (sender)=>{
                            this.log("onValueChanged");
                        }
                    }></GcDateTime><br/>
                イベント<br/>
                <textarea value={this.state.message} ref="log" id="log" rows="10" cols="30"></textarea>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));