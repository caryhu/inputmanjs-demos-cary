import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, ViewChild, ElementRef } from '@angular/core';
import GC from "@grapecity/inputman";
import { GcDateTimeComponent } from "@grapecity/inputman.angular";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {

    public logStr: string = "";
    @ViewChild('logPanel')
    @ViewChild(GcDateTimeComponent) datetime: GcDateTimeComponent;
    public logPanel: ElementRef;
    public dropDownConfig: object = {
        dropDownType: GC.InputMan.DateDropDownType.Calendar
    }

    public ngAfterViewInit(){
        var gcDateTime: GC.InputMan.GcDateTime = this.datetime.getNestedIMControl();
        gcDateTime.getDropDownWindow().onClose(() => this.appendLog('onClose'));
        gcDateTime.getDropDownWindow().onOpen(() => this.appendLog('onOpen'));
        gcDateTime.getDropDownWindow().onClosing(() => this.appendLog('onClosing'));
        gcDateTime.getDropDownWindow().onOpening(() => this.appendLog('onOpening'));
    }

    public appendLog(str: string) {
        this.logStr = this.logStr + str + '\n';
        this.logPanel.nativeElement.scrollTop = this.logPanel.nativeElement.scrollHeight;
    }
}


enableProdMode();