import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcDateTime = new InputMan.GcDateTime(document.getElementById('gcDateTime'), {
    showDropDownButton: true,
    dropDownConfig: {
        dropDownType: InputMan.DateDropDownType.Calendar
    }
});

// イベントハンドラ
gcDateTime.onEditStatusChanged((sender, eArgs) => {
    log('onEditStatusChanged');
});
gcDateTime.onFocusOut((sender, eArgs) => {
    log('onFocusOut');
});
gcDateTime.onInput((sender, eArgs) => {
    log('onInput');
});
gcDateTime.onInvalidInput((sender, eArgs) => {
    log('onInvalidInput');
});
gcDateTime.onInvalidRange((sender, eArgs) => {
    log('onInvalidRange');
});
gcDateTime.onInvalidValue((sender, eArgs) => {
    log('onInvalidValue');
});
gcDateTime.onKeyDown((sender, eArgs) => {
    log('onKeyDown');
});
gcDateTime.onKeyExit((sender, eArgs) => {
    log('onKeyExit');
});
gcDateTime.onKeyUp((sender, eArgs) => {
    log('onKeyUp');
});
gcDateTime.onNumberChanged((sender, eArgs) => {
    log('onNumberChanged');
});
gcDateTime.onSpinDown((sender, eArgs) => {
    log('onSpinDown');
});
gcDateTime.onSpinUp((sender, eArgs) => {
    log('onSpinUp');
});
gcDateTime.onSyncValueToOriginalInput((sender, eArgs) => {
    log('onSyncValueToOriginalInput');
});
gcDateTime.onTextChanged((sender, eArgs) => {
    log('onTextChanged');
});
gcDateTime.onValueChanged((sender, eArgs) => {
    log('onValueChanged');
});
gcDateTime.getDropDownWindow().onClosing((sender, eArgs) => {
    log('onClosing');
});
gcDateTime.getDropDownWindow().onOpening((sender, eArgs) => {
    log('onOpening');
});
gcDateTime.getDropDownWindow().onClose((sender, eArgs) => {
    log('onClose');
});
gcDateTime.getDropDownWindow().onOpen((sender, eArgs) => {
    log('onOpen');
});

// テキストボックスにログを出力
const log = (message) => {
    const textarea = document.getElementById('log');
    textarea.value += message + '\n';
    textarea.scrollTop = textarea.scrollHeight;
}