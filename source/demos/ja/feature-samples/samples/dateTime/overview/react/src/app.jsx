import * as React from "react";
import * as ReactDom from "react-dom";
import { GcDateTime } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{

    render(){
        return (
            <div class="flexbox">
            <div>
                和暦日付
                <GcDateTime
                    displayFormatPattern= {'ggg E年M月d日'}
                    formatPattern= {'yyyy/MM/dd'}
                    showSpinButton= {true}></GcDateTime>
            </div>
            <div>
                時刻
                <GcDateTime
                    displayFormatPattern= {'tt h時m分s秒'}
                    formatPattern= {'HH:mm:ss'}
                    showSpinButton= {true}></GcDateTime>
            </div>
            <div>
                ドロップダウンカレンダー
                <GcDateTime
                    displayFormatPattern= {'yyy年M月d日'}
                    formatPattern= {'yyyy/MM/dd'}
                    showDropDownButton= {true}
                    dropDownConfig= {{
                        dropDownType: InputMan.DateDropDownType.Calendar
                    }}></GcDateTime>
            </div>
            <div>
                ドロップダウン日付時刻ピッカー
                <GcDateTime showDropDownButton= {true}></GcDateTime>
            </div>
        </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));