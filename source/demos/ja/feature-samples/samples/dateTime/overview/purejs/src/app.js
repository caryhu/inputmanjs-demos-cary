import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcDateTime1 = new InputMan.GcDateTime(document.getElementById('gcDateTime1'), {
    displayFormatPattern: 'ggg E年M月d日',
    formatPattern: 'yyyy/MM/dd',
    showSpinButton: true
});

const gcDateTime2 = new InputMan.GcDateTime(document.getElementById('gcDateTime2'), {
    displayFormatPattern: 'tt h時m分s秒',
    formatPattern: 'HH:mm:ss',
    showSpinButton: true
});

const gcDateTime3 = new InputMan.GcDateTime(document.getElementById('gcDateTime3'), {
    displayFormatPattern: 'yyy年M月d日',
    formatPattern: 'yyyy/MM/dd',
    showDropDownButton: true,
    dropDownConfig: {
        dropDownType: InputMan.DateDropDownType.Calendar
    }
});

const gcDateTime4 = new InputMan.GcDateTime(document.getElementById('gcDateTime4'), {
    showDropDownButton: true
});