import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public exitOnLastChar: boolean;
    public exitOnLeftRightKey: GC.InputMan.ExitOnLeftRightKey;
    public tabAction: GC.InputMan.TabAction;
    public highlightText: GC.InputMan.HighlightText;
    public exitOnEnterKey:GC.InputMan.ExitKey;

    public exitOnLeftRightKeys = [
        GC.InputMan.ExitOnLeftRightKey.None,
        GC.InputMan.ExitOnLeftRightKey.Both,
        GC.InputMan.ExitOnLeftRightKey.Left,
        GC.InputMan.ExitOnLeftRightKey.Right
    ];

    public tabActions = [
        GC.InputMan.TabAction.Control,
        GC.InputMan.TabAction.Field
    ];

    public highlightTexts = [
        GC.InputMan.HighlightText.None,
        GC.InputMan.HighlightText.Field,
        GC.InputMan.HighlightText.All
    ];

    // Enterキーによるフォーカス移動
    public ExitKeys = [
        GC.InputMan.ExitKey.None,
        GC.InputMan.ExitKey.Both,
        GC.InputMan.ExitKey.Enter,
        GC.InputMan.ExitKey.ShiftEnter
    ];
}


enableProdMode();