import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcDateTime1 = new InputMan.GcDateTime(document.getElementById('gcDateTime1'));
const gcDateTime2 = new InputMan.GcDateTime(document.getElementById('gcDateTime2'));
const gcDateTime3 = new InputMan.GcDateTime(document.getElementById('gcDateTime3'));

// 自動フォーカス移動
document.getElementById('setExitOnLastChar').addEventListener('change', (e) => {
    const isExitOnLastChar = e.target.checked;
    gcDateTime1.setExitOnLastChar(isExitOnLastChar);
    gcDateTime2.setExitOnLastChar(isExitOnLastChar);
    gcDateTime3.setExitOnLastChar(isExitOnLastChar);
});

// 矢印キーによるフォーカス移動
document.getElementById('setExitOnLeftRightKey').addEventListener('change', (e) => {
    gcDateTime1.setExitOnLeftRightKey(exitOnLeftRightKeys[e.target.selectedIndex]);
    gcDateTime2.setExitOnLeftRightKey(exitOnLeftRightKeys[e.target.selectedIndex]);
    gcDateTime3.setExitOnLeftRightKey(exitOnLeftRightKeys[e.target.selectedIndex]);
});

document.getElementById('setTabAction').addEventListener('change', (e) => {
    gcDateTime1.setTabAction(tabActions[e.target.selectedIndex]);
    gcDateTime2.setTabAction(tabActions[e.target.selectedIndex]);
    gcDateTime3.setTabAction(tabActions[e.target.selectedIndex]);
});

document.getElementById('setHighlightText').addEventListener('change', (e) => {
    gcDateTime1.setHighlightText(highlightTexts[e.target.selectedIndex]);
    gcDateTime2.setHighlightText(highlightTexts[e.target.selectedIndex]);
    gcDateTime3.setHighlightText(highlightTexts[e.target.selectedIndex]);
});

const exitOnLeftRightKeys = [
    InputMan.ExitOnLeftRightKey.None,
    InputMan.ExitOnLeftRightKey.Both,
    InputMan.ExitOnLeftRightKey.Left,
    InputMan.ExitOnLeftRightKey.Right
];

const tabActions = [
    InputMan.TabAction.Control,
    InputMan.TabAction.Field
];

const highlightTexts = [
    InputMan.HighlightText.None,
    InputMan.HighlightText.Field,
    InputMan.HighlightText.All
];

// Enterキーによるフォーカス移動
const ExitKeys = [
    InputMan.ExitKey.None,
    InputMan.ExitKey.Both,
    InputMan.ExitKey.Enter,
    InputMan.ExitKey.ShiftEnter
];

document.getElementById('setExitOnEnterKey').addEventListener('change', (e) => {
    gcDateTime1.setExitOnEnterKey(ExitKeys[e.target.selectedIndex]);
    gcDateTime2.setExitOnEnterKey(ExitKeys[e.target.selectedIndex]);
    gcDateTime3.setExitOnEnterKey(ExitKeys[e.target.selectedIndex]);
});