import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public showDropDownButton: boolean = true;
    public dropDownConfig: object = {
        dropDownType: GC.InputMan.DateDropDownType.Calendar
    };
    public value: Date = new Date(2018, 0, 1);
    public minDate: Date = new Date(2018, 0, 1);
    public maxDate: Date = new Date(2018, 11, 31);
    public maxMinBehavior: GC.InputMan.MaxMinBehavior;
    public acceptsCrlf: GC.InputMan.CrLfMode;
    public rawValue: string | number;
    private gcDateTime: GC.InputMan.GcDateTime;

    public maxMinBehaviors = [
        GC.InputMan.MaxMinBehavior.AdjustToMaxMin,
        GC.InputMan.MaxMinBehavior.Clear,
        GC.InputMan.MaxMinBehavior.Restore,
        GC.InputMan.MaxMinBehavior.CancelInput,
        GC.InputMan.MaxMinBehavior.Keep
    ];

    public crLfModes = [
        GC.InputMan.CrLfMode.NoControl,
        GC.InputMan.CrLfMode.Filter,
        GC.InputMan.CrLfMode.Cut
    ];

    public onInvalidInput(): void {
        alert('不正な文字の入力です。半角数字で入力してください。');
    }

    public onInvalidValue(): void {
        alert('日付として無効な入力です。');
    }

    public onInvalidRange(): void {
        alert('今日から1カ月以内の日付を入力してください。');
    }

    public onInitialized(sender: GC.InputMan.GcDateTime) {
        this.gcDateTime = sender;
    }

    public getValue() {
        this.rawValue = this.gcDateTime.getNumber();
    }

    public setValue(): void {
        this.gcDateTime.setNumber(this.rawValue);
    }

}


enableProdMode();