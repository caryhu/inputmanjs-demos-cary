import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';
import './styles.css';

const gcDateTime = new InputMan.GcDateTime(document.getElementById('gcDateTime'), {
    showDropDownButton: true,
    dropDownConfig: {
        dropDownType: InputMan.DateDropDownType.Calendar
    },
    value: new Date(2018, 0, 1),
    minDate: new Date(2018, 0, 1),
    maxDate: new Date(2018, 11, 31)
});

document.getElementById('setMaxMinBehavior').addEventListener('change', (e) => {
    gcDateTime.setMaxMinBehavior(maxMinBehaviors[e.target.selectedIndex]);
});

const maxMinBehaviors = [
    InputMan.MaxMinBehavior.AdjustToMaxMin,
    InputMan.MaxMinBehavior.Clear,
    InputMan.MaxMinBehavior.Restore,
    InputMan.MaxMinBehavior.CancelInput,
    InputMan.MaxMinBehavior.Keep
];

document.getElementById('setAcceptsCrlf').addEventListener('change', (e) => {
    gcDateTime.setAcceptsCrlf(crLfModes[e.target.selectedIndex]);
});

gcDateTime.onInvalidInput(() => {
    alert('不正な文字の入力です。半角数字で入力してください。');
});

gcDateTime.onInvalidValue(() => {
    alert('日付として無効な入力です。');
});

gcDateTime.onInvalidRange(() => {
    alert('今日から1カ月以内の日付を入力してください。');
});

const crLfModes = [
    InputMan.CrLfMode.NoControl,
    InputMan.CrLfMode.Filter,
    InputMan.CrLfMode.Cut
];

const rawValue = new InputMan.GcTextBox(document.getElementById('rawValue'));

document.getElementById('getValue').addEventListener('click', () => {
    rawValue.setText(gcDateTime.getNumber());
});

document.getElementById('setValue').addEventListener('click', () => {
    gcDateTime.setNumber(rawValue.getText());
});