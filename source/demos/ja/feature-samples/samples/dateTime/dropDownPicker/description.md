日付時刻コントロールには、日付や時刻をスクロールすることで入力できる日付時刻ピッカーをドロップダウン表示させることが可能です。

## 概要

日付時刻コントロールのaddDropDownメソッドを実行して、第2引数にDropDownType.Pickerを指定すると、ドロップダウンボタンの押下により、日付時刻ピッカーが表示されます。この日付時刻ピッカーは、コントロールへの視覚的な日付・時刻入力を可能にする入力補助機能で、特にタッチデバイスでの操作がより快適になります。なお、日付時刻ピッカーでの値選択のスクロールは、タッチによるスワイプ操作とマウスホイールの両方で可能です。

また、addDropDownメソッドで取得できるGcDateTimePickerオブジェクトのメソッドを使用することで、日付・時刻ピッカーに対して、以下のようなカスタマイズを行うことも可能です。

- 設定可能な値を日付・時刻のいずれかのみに制限する。
- 入力可能な最大日付・最小日付を設定する。
- 時刻の分・秒の間隔を設定する。

<!-- -->

## 種類

setPickerTypeメソッドを日付・時刻ピッカーで設定可能な値の種類を設定できます。setPickerTypeメソッドに設定できる値（PickerType列挙体）は、以下の通りです。

| 値                    | 説明                   |
| -------------------- | -------------------- |
| Date                 | 日付のみを表示します。          |
| DateTime             | 日付と時刻の両方を表示します。（既定値） |
| Time                 | 時刻のみを表示します。          |

以下のサンプルコードでは、日付・時刻ピッカーで設定可能な値を「日付」のみに設定しています。

```
var gcDateTime = new GC.InputMan.GcDateTime(document.getElementById('gcDateTime'));
var picker = gcDateTime.addDropDown(GC.InputMan.DropDownButtonAlignment.RightSide, GC.InputMan.DateDropDownType.Picker);
picker.setPickerType(GC.InputMan.PickerType.Date);
```

## 最小日付・最大日付

日付・時刻ピッカーでは設定可能な日付の範囲を設定することが可能です。setMinDateメソッドで最小日付を、setMaxDateメソッドで最大日付を設定できます。

以下のサンプルコードでは、日付・時刻ピッカーで設定可能な値を2018年度内の日付に設定しています。

```
picker.setMinDate(new Date(2018, 3, 1));
picker.setMaxDate(new Date(2019, 2, 31));
```

## 分・秒の間隔

日付・時刻ピッカーで時刻を設定する場合、ピッカー上で選択する分・秒の間隔を設定できます。setMinuteIntervalメソッドで「分」の間隔を、setSecondsIntervalメソッドで「秒」の間隔を設定可能です。これらのメソッドで設定できる値（Interval列挙体）は、以下の通りです。

| 値                             | 説明                            |
| ----------------------------- | ----------------------------- |
| Default                       | デフォルトのインターバル。1分または1秒ごとに表示します。 |
| IntervalOf10                  | 10分、10秒の間隔で表示します。             |
| IntervalOf15                  | 15分、15秒の間隔で表示します。             |
| IntervalOf20                  | 20分、20秒の間隔で表示します。             |
| IntervalOf30                  | 30分、30秒の間隔で表示します。             |
| IntervalOf5                   | 5分、5秒の間隔で表示します。               |

以下のサンプルコードでは、日付・時刻ピッカーで設定可能な時刻の間隔をそれぞれ「5分」と「30秒」に設定しています。

```
picker.setMinuteInterval(GC.InputMan.Interval.IntervalOf5);
picker.setSecondsInterval(GC.InputMan.Interval.IntervalOf30);
```

## ドロップダウン操作

コントロールがフォーカスを取得したときに自動的にドロップダウンを開くには、引数にtrueを指定してsetAutoDropDownメソッドを実行します。

```js
gcDateTime.setAutoDropDown(true);
```

任意のタイミングで手動でドロップダウンを開くには、ドロップダウンウィンドウ（getDropDownWindowメソッドで取得します）のopenメソッドを実行します。また、ドロップダウンを開くにはcloseメソッドを実行します。

```js
gcDateTime.getDropDownWindow().open();
gcDateTime.getDropDownWindow().close();
```

## ドロップダウンボタン

ドロップダウンボタンの表示有無は、setDropDownButtonVisibleメソッドで設定することが可能です。引数をtrueに設定すると、ドロップダウンボタンが表示されます。（既定値はtrueです。）

```js
gcDateTime.setDropDownButtonVisible(true);
```

## ドロップダウンの方向

ドロップダウンは通常コントロールの下に表示されますが、コントロールの下にドロップダウンを表示できる十分なスペースがない場合は、ドロップダウンがコントロールの上に表示されます。ドロップダウンの表示スペースを判定するとき、既定ではWebページ本体（body要素）をコンテナとして扱いますが、setContainerメソッドで任意の要素をコンテナとして指定することも可能です。

```js
gcDateTime.setContainer(document.getElementById('container1'));
```
