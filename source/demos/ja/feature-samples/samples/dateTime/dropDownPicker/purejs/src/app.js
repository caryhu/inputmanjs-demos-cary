import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';

const gcDateTime1 = new InputMan.GcDateTime(document.getElementById('gcDateTime1'), {
    container: document.getElementById('container1')
});
const gcDateTime2 = new InputMan.GcDateTime(document.getElementById('gcDateTime2'), {
    container: document.getElementById('container2')
});
const picker1 = gcDateTime1.addDropDown(InputMan.DropDownButtonAlignment.RightSide, InputMan.DateDropDownType.Picker);
const picker2 = gcDateTime2.addDropDown(InputMan.DropDownButtonAlignment.RightSide, InputMan.DateDropDownType.Picker);

document.getElementById('setPickerType').addEventListener('change', (e) => {
    picker1.setPickerType(pickerTypes[e.target.selectedIndex]);
    picker2.setPickerType(pickerTypes[e.target.selectedIndex]);
});

const minDate = new InputMan.GcDateTime(document.getElementById('minDate'), {
    displayFormatPattern: 'yyy/MM/dd',
    formatPattern: 'yyy/MM/dd',
    showDropDownButton: true,
    dropDownConfig: {
        dropDownType: InputMan.DateDropDownType.Calendar
    }
});
minDate.setValue(null);
minDate.onValueChanged((control, args) => {
    picker1.setMinDate(minDate.getValue());
    picker2.setMinDate(minDate.getValue());
});

const maxDate = new InputMan.GcDateTime(document.getElementById('maxDate'), {
    displayFormatPattern: 'yyy/MM/dd',
    formatPattern: 'yyy/MM/dd',
    showDropDownButton: true,
    dropDownConfig: {
        dropDownType: InputMan.DateDropDownType.Calendar
    }
});
maxDate.setValue(null);
maxDate.onValueChanged((control, args) => {
    picker1.setMaxDate(maxDate.getValue());
    picker2.setMaxDate(maxDate.getValue());
});

document.getElementById('setMinuteInterval').addEventListener('change', (e) => {
    picker1.setMinuteInterval(intervals[e.target.selectedIndex]);
    picker2.setMinuteInterval(intervals[e.target.selectedIndex]);
});

document.getElementById('setSecondsInterval').addEventListener('change', (e) => {
    picker1.setSecondsInterval(intervals[e.target.selectedIndex]);
    picker2.setSecondsInterval(intervals[e.target.selectedIndex]);
});

const pickerTypes = [
    InputMan.PickerType.DateTime,
    InputMan.PickerType.Date,
    InputMan.PickerType.Time
];
const intervals = [
    InputMan.Interval.Default,
    InputMan.Interval.IntervalOf5,
    InputMan.Interval.IntervalOf10,
    InputMan.Interval.IntervalOf15,
    InputMan.Interval.IntervalOf20,
    InputMan.Interval.IntervalOf30
];

document.getElementById('openOnFocus').addEventListener('change', (e) => {
    gcDateTime1.setAutoDropDown(e.target.checked);
    gcDateTime2.setAutoDropDown(e.target.checked);
});
document.getElementById('open').addEventListener('click', (e) => {
    gcDateTime1.getDropDownWindow().open();
    gcDateTime2.getDropDownWindow().open();
});
document.getElementById('close').addEventListener('click', (e) => {
    gcDateTime1.getDropDownWindow().close();
    gcDateTime2.getDropDownWindow().close();
});
document.getElementById('setDropDownButtonVisible').addEventListener('change', (e) => {
    gcDateTime1.setDropDownButtonVisible(e.target.checked);
    gcDateTime2.setDropDownButtonVisible(e.target.checked);
});
