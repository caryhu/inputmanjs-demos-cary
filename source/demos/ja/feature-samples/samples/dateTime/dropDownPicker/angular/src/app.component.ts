import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    private gcDateTime1: GC.InputMan.GcDateTime;
    private gcDateTime2: GC.InputMan.GcDateTime;
    public dropDownButtonAlignment: GC.InputMan.DropDownButtonAlignment = GC.InputMan.DropDownButtonAlignment.RightSide;
    public autoDropDown: boolean = false;
    public dropDownButtonVisible: boolean = true;

    public pickerTypes = [
        GC.InputMan.PickerType.DateTime,
        GC.InputMan.PickerType.Date,
        GC.InputMan.PickerType.Time
    ];
    public intervals = [
        GC.InputMan.Interval.Default,
        GC.InputMan.Interval.IntervalOf5,
        GC.InputMan.Interval.IntervalOf10,
        GC.InputMan.Interval.IntervalOf15,
        GC.InputMan.Interval.IntervalOf20,
        GC.InputMan.Interval.IntervalOf30
    ];

    private get picker1(): GC.InputMan.GcDateTimePicker {
        var dropDown = this.gcDateTime1.getDropDownCalendar();
        return <any>dropDown;
    }

    private get picker2(): GC.InputMan.GcDateTimePicker {
        var dropDown = this.gcDateTime2.getDropDownCalendar();
        return <any>dropDown;
    }

    public dropDownCalendarConfig: object = {
        dropDownType: GC.InputMan.DateDropDownType.Calendar
    }

    public setMinuteInterval(index: number) {
        this.picker1.setMinuteInterval(this.intervals[index]);
        this.picker2.setMinuteInterval(this.intervals[index]);
    }
    public setSecondsInterval(index: number) {
        this.picker1.setSecondsInterval(this.intervals[index]);
        this.picker2.setSecondsInterval(this.intervals[index]);
    }
    public setPickerType(index: number) {
        this.picker1.setPickerType(this.pickerTypes[index]);
        this.picker2.setPickerType(this.pickerTypes[index]);
    }
    public setPickerMaxDate(date: Date) {
        this.picker1.setMaxDate(date);
        this.picker2.setMaxDate(date);
    }
    public setPickerMinDate(date: Date) {
        this.picker1.setMinDate(date);
        this.picker2.setMinDate(date);
    }
    public showDropDown(isShow: boolean){
        if(isShow){
            this.gcDateTime1.getDropDownWindow().open();
            this.gcDateTime2.getDropDownWindow().open();
        }else{
            this.gcDateTime1.getDropDownWindow().close();
            this.gcDateTime2.getDropDownWindow().close();
        }
    }
}


enableProdMode();