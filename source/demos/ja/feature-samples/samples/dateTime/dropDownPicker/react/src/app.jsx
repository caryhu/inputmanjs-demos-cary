import * as React from "react";
import * as ReactDom from "react-dom";
import { GcDateTime } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

const pickerTypes = [
    InputMan.PickerType.DateTime,
    InputMan.PickerType.Date,
    InputMan.PickerType.Time
];
const intervals = [
    InputMan.Interval.Default,
    InputMan.Interval.IntervalOf5,
    InputMan.Interval.IntervalOf10,
    InputMan.Interval.IntervalOf15,
    InputMan.Interval.IntervalOf20,
    InputMan.Interval.IntervalOf30
];

class App extends React.Component{
    gcDateTime1 = null;
    gcDateTime2 = null;
    picker = null;

    constructor(props, context) {
        super(props, context);
        this.state = {
            dropDownButtonVisible: true,
            autoDropDown: false
        };
    }

    showDropDown(isShow){
        this.dropDownAction(this.gcDateTime1, isShow);
        this.dropDownAction(this.gcDateTime2, isShow);
    }

    dropDownAction(gcDateTime, isShow){
        if(isShow){
            gcDateTime.getDropDownWindow().open();
        }else{
            gcDateTime.getDropDownWindow().close();
        }
    }

    componentDidMount(){
        this.gcDateTime1.setContainer(this.refs.container1);
        this.gcDateTime2.setContainer(this.refs.container2);
    }

    render(){
        return (
            <div>
                <div class="flexbox">
                    <div id="container1" ref="container1">
                        <p>ドロップダウンが下に表示されます。</p>
                        <GcDateTime
                            showDropDownButton= {true}
                            dropDownButtonVisible={this.state.dropDownButtonVisible}
                            autoDropDown={this.state.autoDropDown}
                            onInitialized={(imCtrl)=>{ 
                                this.gcDateTime1 = imCtrl; 
                                this.picker1 = imCtrl.addDropDown(InputMan.DropDownButtonAlignment.RightSide, InputMan.DateDropDownType.Picker);
                            }}></GcDateTime>
                    </div>
                    <div id="container2" ref="container2">
                        <p>ドロップダウンが上に表示されます。</p>
                        <GcDateTime
                            showDropDownButton= {true}
                            dropDownButtonVisible={this.state.dropDownButtonVisible}
                            autoDropDown={this.state.autoDropDown}
                            onInitialized={(imCtrl)=>{ 
                                this.gcDateTime2 = imCtrl; 
                                this.picker2 = imCtrl.addDropDown(InputMan.DropDownButtonAlignment.RightSide, InputMan.DateDropDownType.Picker);
                            }}></GcDateTime>
                    </div>
                </div>
                <table class="sample">
                    <tr>
                        <th>種類</th>
                        <td>
                            <select id="setPickerType" onChange={(e)=>{
                                   this.picker1.setPickerType(pickerTypes[e.target.selectedIndex]);
                                   this.picker2.setPickerType(pickerTypes[e.target.selectedIndex]);
                                }}>
                                <option>日付と時刻</option>
                                <option>日付</option>
                                <option>時刻</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>最小日付</th>
                        <td>
                            <GcDateTime
                            value= {this.state.minDate? this.state.minDate: null}
                            displayFormatPattern= {'yyy/MM/dd'}
                            formatPattern= {'yyy/MM/dd'}
                            showDropDownButton= {true}
                            dropDownConfig= {{
                                dropDownType: InputMan.DateDropDownType.Calendar
                            }}
                            onValueChanged={(eArgs) => {
                                this.picker1.setMinDate(eArgs.getValue());
                                this.picker2.setMinDate(eArgs.getValue());
                            }}
                            ></GcDateTime>
                        </td>
                    </tr>
                    <tr>
                        <th>最大日付</th>
                        <td>
                            <GcDateTime
                            value= {this.state.maxDate? this.state.maxDate: null}
                            displayFormatPattern= {'yyy/MM/dd'}
                            formatPattern= {'yyy/MM/dd'}
                            showDropDownButton= {true}
                            dropDownConfig= {{
                                dropDownType: InputMan.DateDropDownType.Calendar
                            }}
                            onValueChanged={(eArgs) => {
                                this.picker1.setMaxDate(eArgs.getValue());
                                this.picker2.setMaxDate(eArgs.getValue());
                            }}></GcDateTime>
                        </td>
                    </tr>
                    <tr>
                        <th>分の間隔</th>
                        <td>
                            <select id="setMinuteInterval" onChange={(e)=>{
                                    this.picker1.setMinuteInterval(intervals[e.target.selectedIndex]);
                                    this.picker2.setMinuteInterval(intervals[e.target.selectedIndex]);
                                }}>
                                <option>1分</option>
                                <option>5分</option>
                                <option>10分</option>
                                <option>15分</option>
                                <option>20分</option>
                                <option>30分</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>秒の間隔</th>
                        <td>
                            <select id="setSecondsInterval" onChange={(e)=>{
                                    this.picker1.setSecondsInterval(intervals[e.target.selectedIndex]);
                                    this.picker2.setSecondsInterval(intervals[e.target.selectedIndex]);
                                }}>
                                <option>1秒</option>
                                <option>5秒</option>
                                <option>10秒</option>
                                <option>15秒</option>
                                <option>20秒</option>
                                <option>30秒</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>フォーカス時のドロップダウン操作</th>
                        <td>
                            <label><input type="checkbox" checked={this.state.autoDropDown} 
                            onChange={(e) => this.setState({autoDropDown: e.target.checked})} />フォーカス時にドロップダウンを開く</label>
                        </td>
                    </tr>
                    <tr>
                        <th>コードによるドロップダウン操作</th>
                        <td>
                            <button id="open" onClick={() => this.showDropDown(true)}>ドロップダウンを開く</button>&nbsp;
                            <button id="close" onClick={() => this.showDropDown(false)}>ドロップダウンを閉じる</button>
                        </td>
                    </tr>
                    <tr>
                        <th>ドロップダウンボタンの表示</th>
                        <td>
                            <label><input type="checkbox" checked={this.state.dropDownButtonVisible} 
                            onChange={(e) => this.setState({dropDownButtonVisible: e.target.checked})} />ドロップダウンボタンを表示する</label>
                        </td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));