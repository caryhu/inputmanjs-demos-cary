import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public value: Date;
    public promptChar: string = "_";

    public onInitialized() {
        this.value = new Date(1850, 0, 1);
    }
}


enableProdMode();