日付コントロールでは未入力のときや入力が完了していないとき、様々な補助機能を提供しています。

## 未入力時に表示する代替テキスト

setWatermarkメソッドを使用すれば、コントロールが未入力のときや和暦表示できない日付が設定された場合に、代わりに表示するテキスト（ウォーターマーク）を設定することができます。setWatermarkメソッドを使用して以下の代替テキストを設定できます。

| メソッド                                  | 代替テキストの内容                             |
| ------------------------------------- | ------------------------------------- |
| setWatermarkDisplayNullText           | コントロールにフォーカスがなく、未入力のときの代替テキスト         |
| setWatermarkDisplayEmptyEraText       | コントロールにフォーカスがなく、その値が和暦表示できないときの代替テキスト |
| setWatermarkNullText                  | コントロールにフォーカスがあり、未入力のときの代替テキスト         |
| setWatermarkEmptyEraText              | コントロールにフォーカスがあり、その値が和暦表示できないときの代替テキスト |

- 和暦表示が可能な日付の範囲は、1868年9月8日～2087年12月31日です。
- setWatermarkDisplayEmptyEraTextおよびsetWatermarkEmptyEraTextメソッドは、setDisplayFormatPattern、setFormatPatternメソッドによる書式設定で和暦を指定している場合に有効です。

<!-- -->

次のサンプルコードは、上図のような代替テキストを表示する方法です。

```
// フォーカスがないときの代替テキスト
gcDateTime.setWatermarkDisplayNullText('生年月日');
gcDateTime.setWatermarkDisplayEmptyEraText('和暦表示できない日付です');

// フォーカスがあるときの代替テキスト
gcDateTime.setWatermarkNullText('和暦で入力してください');
gcDateTime.setWatermarkEmptyEraText('和暦表示できない日付です');
```

## プロンプト文字

フィールドに何も入力されていないときに表示するプロンプト文字は、setPromptCharメソッドを使用して設定します。プロンプト文字はsetFormatPatternメソッドに定義された入力マスクの位置にプレースホルダとして表示され、ユーザに入力を促します。なお、setPromptCharメソッドには、半角または全角の文字を設定できますが、文字列は設定できません。

代替テキストを設定している場合、コントロールに文字が入力されていないときには代替テキストの設定が優先されます。

