import * as React from "react";
import * as ReactDom from "react-dom";
import { GcDateTime, GcTextBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
        };
    }
    render(){
        return (
            <div>
                <GcDateTime
                    className={'displayStyle'}
                    promptChar= {this.state.promptChar}
                    value= {null}
                    formatPattern= {'yyyy/MM/dd'}
                    displayFormatPattern= {'yyy/M/d'}
                    watermarkDisplayNullText= {'生年月日'}
                    watermarkNullText= {'西暦で入力してください'}></GcDateTime>&nbsp;
                <GcDateTime
                    className={'displayStyle'}
                    formatPattern= {'gggee年MM月dd日'}
                    displayFormatPattern= {'gggE年M月d日'}
                    watermarkDisplayEmptyEraText= {'和暦表示できない日付です'}
                    watermarkEmptyEraText= {'正しい和暦を入力してください'}
                    onInitialized={(imCtrl) => { 
                            imCtrl.setValue(new Date(1850, 0, 1));
                        }}></GcDateTime>
                <table class="sample">
                    <tr>
                        <th>プロンプト文字</th>
                        <td><GcTextBox value={this.state.promptChar?'_': this.state.promptChar} onTextChanged={(s) => this.setState({promptChar: s.getText()})}></GcTextBox></td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));