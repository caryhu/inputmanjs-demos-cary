import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';

const gcDateTime1 = new InputMan.GcDateTime(document.getElementById('gcDateTime1'), {
    formatPattern: 'yyyy/MM/dd',
    displayFormatPattern: 'yyy/M/d',
    // 未入力のときの代替テキスト
    watermarkDisplayNullText:'生年月日',
    watermarkNullText:'西暦で入力してください'
});
gcDateTime1.setValue(null);

const gcDateTime2 = new InputMan.GcDateTime(document.getElementById('gcDateTime2'), {
    formatPattern: 'gggee年MM月dd日',
    displayFormatPattern: 'gggE年M月d日',
    // 値が和暦表示できないときの代替テキスト
    watermarkDisplayEmptyEraText: '和暦表示できない日付です',
    watermarkEmptyEraText: '正しい和暦を入力してください'
});
gcDateTime2.setValue(new Date(1850, 0, 1));

const setPromptChar = new InputMan.GcTextBox(document.getElementById('setPromptChar'), {
    text: '_'
});
setPromptChar.onTextChanged(() => {
    gcDateTime1.setPromptChar(setPromptChar.getText());
});