import * as React from "react";
import * as ReactDom from "react-dom";
import { GcDateTime } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{
    constructor(){
        super();
        this.gcdatetime1 = React.createRef();
        this.gcdatetime2 = React.createRef();
        this.state = {
            dropDownButtonVisible: true,
            autoDropDown: false
        }
    }

    showDropDown(isShow){
        this.dropDownAction(this.gcdatetime1.current.getNestedIMControl(), isShow);
        this.dropDownAction(this.gcdatetime2.current.getNestedIMControl(), isShow);
    }

    dropDownAction(gcDateTime, isShow){
        if(isShow){
            gcDateTime.getDropDownWindow().open();
        }else{
            gcDateTime.getDropDownWindow().close();
        }
    }

    componentDidMount(){
        this.gcdatetime1.current.getNestedIMControl().setContainer(this.refs.container1);
        this.gcdatetime2.current.getNestedIMControl().setContainer(this.refs.container2);
    }

    render(){
        return (
            <div>
                <div class="flexbox">
                    <div id="container1" ref="container1">
                        <p>ドロップダウンが下に表示されます。</p>
                        <GcDateTime 
                            ref={this.gcdatetime1}
                            showDropDownButton={true}
                            dropDownButtonVisible= {this.state.dropDownButtonVisible}
                            autoDropDown={this.state.autoDropDown}
                            dropDownConfig= {{
                                dropDownType: InputMan.DateDropDownType.Calendar
                            }}/>
                    </div>
                    <div id="container2" ref="container2">
                        <p>ドロップダウンが上に表示されます。</p>
                        <GcDateTime 
                            ref={this.gcdatetime2}
                            showDropDownButton={true}
                            dropDownButtonVisible= {this.state.dropDownButtonVisible}
                            autoDropDown={this.state.autoDropDown}
                            dropDownConfig= {{
                                dropDownType: InputMan.DateDropDownType.Calendar
                            }}/>
                    </div>
                </div>
                <table class="sample">
                    <tr>
                        <th>フォーカス時のドロップダウン操作</th>
                        <td><label><input type="checkbox" 
                        checked={this.state.autoDropDown} 
                        onChange={(e) => this.setState({autoDropDown: e.target.checked})} />フォーカス時にドロップダウンを開く</label></td>
                    </tr>
                    <tr>
                        <th>コードによるドロップダウン操作</th>
                        <td>
                            <button id="open" onClick={() => this.showDropDown(true)}>ドロップダウンを開く</button>&nbsp;
                            <button id="close" onClick={() => this.showDropDown(false)}>ドロップダウンを閉じる</button>
                        </td>
                    </tr>
                    <tr>
                        <th>ドロップダウンボタンの表示</th>
                        <td>
                            <label><input type="checkbox" 
                                checked={this.state.dropDownButtonVisible} 
                                onChange={(e) => this.setState({dropDownButtonVisible: e.target.checked})} />ドロップダウンボタンを表示する</label>
                        </td>
                    </tr>
                </table>
            </div>)
    }
}

ReactDom.render(<App />, document.getElementById("app"));