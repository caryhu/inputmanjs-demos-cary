import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';

const gcDateTime1 = new InputMan.GcDateTime(document.getElementById('gcDateTime1'), {
    showDropDownButton: true,
    dropDownConfig: {
        dropDownType: InputMan.DateDropDownType.Calendar
    },
    container: document.getElementById('container1')
});
const gcDateTime2 = new InputMan.GcDateTime(document.getElementById('gcDateTime2'), {
    showDropDownButton: true,
    dropDownConfig: {
        dropDownType: InputMan.DateDropDownType.Calendar
    },
    container: document.getElementById('container2')
});

document.getElementById('openOnFocus').addEventListener('change', (e) => {
    gcDateTime1.setAutoDropDown(e.target.checked);
    gcDateTime2.setAutoDropDown(e.target.checked);
});
document.getElementById('open').addEventListener('click', (e) => {
    gcDateTime1.getDropDownWindow().open();
    gcDateTime2.getDropDownWindow().open();
});
document.getElementById('close').addEventListener('click', (e) => {
    gcDateTime1.getDropDownWindow().close();
    gcDateTime2.getDropDownWindow().close();
});
document.getElementById('setDropDownButtonVisible').addEventListener('change', (e) => {
    gcDateTime1.setDropDownButtonVisible(e.target.checked);
    gcDateTime2.setDropDownButtonVisible(e.target.checked);
});
