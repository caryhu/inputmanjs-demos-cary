import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {

    gcDateTime1: GC.InputMan.GcDateTime;
    gcDateTime2: GC.InputMan.GcDateTime;

    public dropDownConfig: object = {
        dropDownType: GC.InputMan.DateDropDownType.Calendar
    }
    public autoDropDown: boolean = false;
    public dropDownButtonVisible: boolean = true;

    public showDropdown(isShow: boolean) {
        if (isShow) {
            this.gcDateTime1.getDropDownWindow().open();
            this.gcDateTime2.getDropDownWindow().open();
        } else {
            this.gcDateTime1.getDropDownWindow().close();
            this.gcDateTime2.getDropDownWindow().close();
        }
    }
}


enableProdMode();