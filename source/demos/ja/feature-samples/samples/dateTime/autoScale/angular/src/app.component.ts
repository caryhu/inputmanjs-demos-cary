import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, ViewChild } from '@angular/core';
import { GcDateTimeComponent } from "@grapecity/inputman.angular";


@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public autoScale: boolean = true;
    public minScaleFactor: number = 0.5;

    @ViewChild(GcDateTimeComponent) datetime: GcDateTimeComponent;

    public clearText(){
        this.datetime.getNestedIMControl().clear();
    }
}

enableProdMode();