﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcDateTime = new InputMan.GcDateTime(document.getElementById('gcDateTime'), {
    autoScale: true,
    minScaleFactor: 0.5,
    displayFormatPattern: 'yyyy年MM月dd日 HH時mm分ss秒',
    formatPattern: 'yyyy年MM月dd日 HH時mm分ss秒'
});

document.getElementById('clearText').addEventListener('click', () => {
    gcDateTime.clear();
});

document.getElementById('setAutoScale').addEventListener('click', (e) => {
    gcDateTime.setAutoScale(e.target.checked);
});

document.getElementById('setMinScaleFactor').addEventListener('change', (e) => {
    gcDateTime.setMinScaleFactor(Number(e.target.value));
});