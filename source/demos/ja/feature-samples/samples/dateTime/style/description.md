本製品ではコントロールの高さや幅などのサイズのほか、背景色や文字色などを含め、コントロールの外観はすべてCSS(Cascading Style Sheet)により設定します。

コントロールで使用されるCSSクラスの詳細については、以下の製品ヘルプをご参照ください。

- [コントロールの外観設定](<//docs.grapecity.com/help/inputman-js/#Appearance.html>)
- [入力コントロールのCSS](<//docs.grapecity.com/help/inputman-js/#CSS_Input.html>)

<!-- -->

