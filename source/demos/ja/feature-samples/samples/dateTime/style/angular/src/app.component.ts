import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public styleText: string;
    private styleSheet: CSSStyleSheet = null;
    private indices: Array<number> = [];
    private styles = {
        '.gcim': { width: '200px', height: '40px', backgroundColor: '#ddffdd', borderColor: '#009900', borderWidth: '2px', borderStyle: 'dashed', borderRadius: '12px', boxShadow: '5px 5px 5px rgba(0,0,0,0.5)', color: '#009900' },
        '.gcim__input': { cursor: 'crosshair', fontSize: '20px', fontWeight: 'bold', fontStyle: 'italic', fontFamily: 'serif', textAlign: 'right', textShadow: '1px 1px 1px rgba(0,0,0,0.5)' },
        '.gcim__input:disabled': { backgroundColor: '#666666', color: '#cccccc', cursor: 'wait' },
        '.gcim_focused': { backgroundColor: '#ddddff', borderColor: '#0000ff', color: '#0000ff' },
        '.gcim_watermark_null': { backgroundColor: '#ffdddd', borderColor: '#ff0000', color: '#ff0000' },
        '.gcim_focused.gcim_watermark_null': { backgroundColor: '#ffddff', borderColor: '#990099', color: '#990099' },
        '.gcim_watermark_empty-era': { backgroundColor: '#ddffff', borderColor: '#00dddd', color: '#00dddd' },
        '.gcim_focused.gcim_watermark_empty-era': { backgroundColor: '#ffdd66', borderColor: '#ff9900', color: '#ff9900' },
    };

    constructor() {
        this.createInitialStyles();
    }

    public createInitialStyles() {
        const element = document.createElement('style');
        document.head.appendChild(element);
        this.styleSheet = element.sheet as CSSStyleSheet;
        var i = 0;
        for (var styleName in this.styles) {
            this.styleSheet.insertRule(styleName + '{}', i);
            this.indices[styleName] = i;
            i++;
        }
    }

    public updateStyle(event: Event) {
        const element = event.target as HTMLInputElement;
        if (element.tagName == 'INPUT') {
            const values = element.value.split(',');
            const styleName = values[0];
            const propertyName = values[1];
            (this.styleSheet.cssRules[this.indices[styleName]] as CSSStyleRule).style[propertyName] = element.checked ? this.styles[styleName][propertyName] : '';
        }

        var style = '';
        for (var i = 0; i < this.styleSheet.cssRules.length; i++) {
            if ((this.styleSheet.cssRules[i] as CSSStyleRule).style.length > 0) {
                style += this.styleSheet.cssRules[i].cssText.replace(/{/g, '{\n ').replace(/;/g, ';\n ').replace(/  }/g, '}') + '\n';
            }
        }
        this.styleText = style;
    }

    public copyStyle() {
        (window as any).wijmo.Clipboard.copy(this.styleText);
        document.execCommand('copy');
    }

}


enableProdMode();