import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public formatPattern: string;
    public displayFormatPattern: string;

    public displayFormatPatternItems: Array<ComboItem<string, string>> = [
        { value: 'yyy/M/d H:m:s', text: '日付と時刻（2018/1/1 13:30:1）' },
        { value: 'yyy年M月d日 H時m分s秒', text: '日付と時刻（2018年1月1日 13時30分1秒）' },
        { value: 'y/M/d', text: '日付（18/1/1）' },
        { value: 'y年M月d日（ddd）', text: '日付（18年1月1日（木））' },
        { value: 'gggE年M月d日 dddd', text: '日付（平成元年12月1日 金曜日）' },
        { value: 'gg e年M月d日', text: '日付（平 1年12月1日）' },
        { value: 'g. e/M/d', text: '日付（H. 1/12/1）' },
        { value: 'H:m:s', text: '時刻（13:30:1）' },
        { value: 'tt h時m分s秒', text: '時刻（午後 1時30分1秒）' },
    ];
    public formatPatternItems: Array<ComboItem<string, string>> = [
        { value: 'yyyy/MM/dd HH:mm:ss', text: '日付と時刻（2018/01/01 13:30:01）' },
        { value: 'yyyy年MM月dd日 HH時mm分ss秒', text: '日付と時刻（2018年01月01日 13時30分01秒）' },
        { value: 'yyyy/MM/dd', text: '日付（2018/01/01）' },
        { value: 'yy年MM月dd日', text: '日付（18年01月01日）' },
        { value: 'gggee年MM月dd日 HH時mm分ss秒', text: '日付と時刻（平成01年12月01日 13時30分01秒）' },
        { value: 'gg ee年MM月dd日', text: '日付（平 01年12月01日）' },
        { value: 'g. ee/MM/dd', text: '日付（H. 01/12/01）' },
        { value: 'HH:mm:ss', text: '時刻（13:30:01）' },
        { value: 'tt hh時mm分ss秒', text: '時刻（午後 01時30分01秒）' },
    ];
}

export interface ComboItem<T1, T2> {
    value: T1;
    text: T2
}


enableProdMode();