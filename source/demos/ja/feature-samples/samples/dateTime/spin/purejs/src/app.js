import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

var gcDateTime = new InputMan.GcDateTime(document.getElementById('gcDateTime'), {
    showSpinButton: true,
    value: new Date(2018, 0, 1),
    minDate: new Date(2018, 0, 1),
    maxDate: new Date(2018, 11, 31)
});

document.getElementById('setAllowSpin').addEventListener('change', (e) => {
    gcDateTime.setAllowSpin(e.target.checked);
});

document.getElementById('setSpinOnKeys').addEventListener('change', (e) => {
    gcDateTime.setSpinOnKeys(e.target.checked);
});

document.getElementById('setSpinWheel').addEventListener('change', (e) => {
    gcDateTime.setSpinWheel(e.target.checked);
});

document.getElementById('setSpinWrap').addEventListener('change', (e) => {
    gcDateTime.setSpinWrap(e.target.checked);
});