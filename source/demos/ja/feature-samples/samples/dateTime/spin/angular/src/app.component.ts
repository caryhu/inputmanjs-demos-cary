import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public allowSpin: boolean = true;
    public spinOnKeys: boolean = true;
    public spinWrap: boolean = true;
    public spinWheel: boolean= true;
    public value: Date = new Date(2018, 0, 1);
    public minDate: Date = new Date(2018, 0, 1);
    public maxDate: Date = new Date(2018, 11, 31);
}


enableProdMode();