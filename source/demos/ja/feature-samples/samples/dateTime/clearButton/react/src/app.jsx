import * as React from "react";
import * as ReactDom from "react-dom";
import { GcDateTime } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            showClearButton: true
        };
    }
    render(){
        return (
            <div >
            <GcDateTime
                showClearButton={this.state.showClearButton} />
            <table class="sample">
                <tr>
                    <th>クリアボタンの表示</th>
                    <td>
                        <label><input type="checkbox" checked={this.state.showClearButton} onChange={(e)=>{this.setState({showClearButton: e.target.checked})}}/>表示する</label>
                    </td>
                </tr>
            </table>
        </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));