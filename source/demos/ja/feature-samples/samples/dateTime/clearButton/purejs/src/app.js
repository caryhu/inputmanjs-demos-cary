﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';
import './styles.css';

const gcDateTime = new InputMan.GcDateTime(document.getElementById('gcDateTime'), {
    showClearButton: true,
});

document.getElementById('showClearButton').addEventListener('click', (e) => {
    gcDateTime.setShowClearButton(e.target.checked);
});