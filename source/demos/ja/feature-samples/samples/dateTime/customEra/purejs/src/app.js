import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

// 年号をカスタマイズします。
InputMan.updateCustomEra([
    { name: '明治', abbreviation: '明', symbol: 'M', startDate: '1868/09/08', shortcuts: '1,M', },
    { name: '大正', abbreviation: '大', symbol: 'T', startDate: '1912/07/30', shortcuts: '2,T', },
    { name: '昭和', abbreviation: '昭', symbol: 'S', startDate: '1926/12/25', shortcuts: '3,S', },
    { name: '平成', abbreviation: '平', symbol: 'H', startDate: '1989/01/08', shortcuts: '4,H', },
    { name: '令和', abbreviation: '令', symbol: 'R', startDate: '2019/05/01', shortcuts: '5,R', },
    // 2100/1/1から「新規」という年号が開始される場合の例
    { name: '新規', abbreviation: '新', symbol: 'N', startDate: '2100/01/01', shortcuts: '6,N', }
]);
const gcDateTime1 = new InputMan.GcDateTime(document.getElementById('gcDateTime1'), {
    value: new Date(2100, 0, 1),
    displayFormatPattern: 'gggE年M月d日',
    formatPattern: 'ggge年MM月dd日'
});
const gcDateTime2 = new InputMan.GcDateTime(document.getElementById('gcDateTime2'), {
    value: new Date(2100, 0, 1),
    displayFormatPattern: 'ggE年M月d日',
    formatPattern: 'gge年MM月dd日'
});
const gcDateTime3 = new InputMan.GcDateTime(document.getElementById('gcDateTime3'), {
    value: new Date(2100, 0, 1),
    displayFormatPattern: 'g.e/M/d',
    formatPattern: 'g.e/MM/dd'
});