InputManJSのコントロールを使用したVue.jsアプリケーションを作成するには、次の手順に従います。（その他のコントロールの作成方法は、各コントロールののサンプルを参照してください。）

以下の手順では、事前に[NodeJS](https://nodejs.org/ja/)と[Vue CLI](https://cli.vuejs.org/)がコンピュータにインストールされている必要があります。

1. コマンドライン（コマンドプロンプトやターミナル）で次のコマンドを実行して、新しいVue.jsアプリケーションを作成します。
``` sh
vue create inputmanjs-vue -d
```

2. 次のコマンドを実行して、作成したアプリケーションのディレクトリに移動します。
``` sh
cd inputmanjs-vue
```

3. 次のコマンドを実行して、InputManJSのVue.jsパッケージをインストールします。
``` sh
npm install @grapecity/inputman.vue
``` 

4. 「`src/App.vue`」ファイルで以下の内容を設定します。ここでは、次の設定を行います。
- InputManJSのコンポーネントとスタイルをインポートします。
- 連結するデータを定義します。
- InputManJSのコンポーネントを生成します。

```vue
<template>
  <div id="app">
    テキストコントロール<br>
    <gc-text-box :text="textValue"></gc-text-box><br><br>
    複数行テキストコントロール<br>
    <gc-multiline-textbox :text="textValue"></gc-multiline-textbox><br><br>
    マスクコントロール<br>
    <gc-mask :value="maskValue" :formatPattern="maskFormatPattern"></gc-mask><br><br>
    日付時刻コントロール<br>
    <gc-datetime :value="dateValue"></gc-datetime><br><br>
    数値コントロール<br>
    <gc-number :value="numberValue"></gc-number><br><br>
    カレンダーコントロール<br>
    <gc-calendar :selectedDate="dateValue" :focusDate="dateValue"></gc-calendar><br><br>
    コンボコントロール<br>
    <gc-combobox :items="listItems" :selectedIndex="selectedListIndex"></gc-combobox><br><br>
    リストコントロール<br>
    <gc-listbox :items="listItems" :selectedIndex="selectedListIndex"></gc-listbox>
  </div>
</template>

<script>
import '@grapecity/inputman.vue';
import "@grapecity/inputman/CSS/gc.inputman-js.css";

export default {
  name: 'App',
  components: {
  },
  data: function () {
    return {
      textValue: 'テキスト',
      maskValue: '9813205',
      maskFormatPattern: '〒\\D{3}-\\D{4}',
      dateValue: new Date(2020, 0, 1),
      numberValue: 1,
      listItems: ['項目1', '項目2', '項目3', '項目4', '項目5'],
      selectedListIndex: 0
    }
  }
}
</script>

<style>
</style>
```

5. 次のコマンドを実行して、アプリケーションを起動します。
``` sh
npm run serve
```

6. ブラウザで「`http://localhost:8080/`」を参照し、アプリケーションを表示します。