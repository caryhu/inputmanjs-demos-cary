import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    textValue = 'テキスト';
    maskValue = '9813205';
    dateValue = new Date(2020, 0, 1);
    numberValue = 1;
    listItems = ['項目1', '項目2', '項目3', '項目4', '項目5'];
    selectedListIndex = 0;
}

enableProdMode();