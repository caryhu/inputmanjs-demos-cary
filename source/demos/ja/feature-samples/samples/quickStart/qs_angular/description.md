InputManJSのコントロールを使用したAngularアプリケーションを作成するには、次の手順に従います。（その他のコントロールの作成方法は、各コントロールののサンプルを参照してください。）

以下の手順では、事前に[NodeJS](https://nodejs.org/ja/)と[Angular CLI](https://cli.angular.io/)がコンピュータにインストールされている必要があります。

1. コマンドライン（コマンドプロンプトやターミナル）で次のコマンドを実行して、新しいAngularアプリケーションを作成します。
``` sh
ng new inputmanjs-angular --defaults
```

2. 次のコマンドを実行して、作成したアプリケーションのディレクトリに移動します。
``` sh
cd inputmanjs-angular
```

3. 次のコマンドを実行して、InputManJSのAngularパッケージをインストールします。
``` sh
npm install @grapecity/inputman.angular
``` 

4. 「`src/styles.css`」ファイルで以下の内容を設定します。ここでは、InputManJSのスタイルをインポートします。
```css
@import '@grapecity/inputman/CSS/gc.inputman-js.css';
```

5. 「`src/app/app.module.ts`」ファイルで以下の内容を設定します。ここでは、InputManJSのAngularモジュールをインポートします。
```ts
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { InputManModule } from '@grapecity/inputman.angular';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    InputManModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

6. 「`src/app/app.component.ts`」ファイルで以下の内容を設定します。ここでは、連結するデータを定義します。
```ts
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  textValue = 'テキスト';
  maskValue = '9813205';
  dateValue = new Date(2020, 0, 1);
  numberValue = 1;
  listItems = ['項目1', '項目2', '項目3', '項目4', '項目5'];
  selectedListIndex = 0;
}
```

7. 「`src/app/app.component.html`」ファイルで以下の内容を設定します。ここでは、InputManJSのコンポーネントを生成します。
```html
テキストコントロール<br>
<gc-text-box [(text)]="textValue"></gc-text-box><br><br>
複数行テキストコントロール<br>
<gc-multiline-textbox [(text)]="textValue"></gc-multiline-textbox><br><br>
マスクコントロール<br>
<gc-mask [(value)]="maskValue" formatPattern="〒\D{3}-\D{4}"></gc-mask><br><br>
日付時刻コントロール<br>
<gc-datetime [(value)]="dateValue"></gc-datetime><br><br>
数値コントロール<br>
<gc-number [(value)]="numberValue"></gc-number><br><br>
カレンダーコントロール<br>
<gc-calendar [(selectedDate)]="dateValue" [focusDate]="dateValue"></gc-calendar><br><br>
コンボコントロール<br>
<gc-combo-box [items]="listItems" [(selectedIndex)]="selectedListIndex"></gc-combo-box><br><br>
リストコントロール<br>
<gc-list-box [items]="listItems" [(selectedIndex)]="selectedListIndex"></gc-list-box>
```

8. 次のコマンドを実行して、アプリケーションを起動します。
``` sh
npm start
```

9. ブラウザで「`http://localhost:4200/`」を参照し、アプリケーションを表示します。