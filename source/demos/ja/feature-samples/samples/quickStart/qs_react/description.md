InputManJSのコントロールを使用したReactアプリケーションを作成するには、次の手順に従います。（その他のコントロールの作成方法は、各コントロールののサンプルを参照してください。）

以下の手順では、事前に[NodeJS](https://nodejs.org/ja/)と[Create React App](https://create-react-app.dev/)がコンピュータにインストールされている必要があります。

1. コマンドライン（コマンドプロンプトやターミナル）で次のコマンドを実行して、新しいReactアプリケーションを作成します。
``` sh
create-react-app inputmanjs-react
```

2. 次のコマンドを実行して、作成したアプリケーションのディレクトリに移動します。
``` sh
cd inputmanjs-react
```

3. 次のコマンドを実行して、InputManJSのReactパッケージをインストールします。
``` sh
npm install @grapecity/inputman.react
``` 

4. 「`src/app.js`」ファイルで以下の内容を設定します。ここでは、次の設定を行います。
- InputManJSのコンポーネントとスタイルをインポートします。
- stateまたはhookを定義します。
- InputManJSのコンポーネントを生成します。

#### stateを使用する場合

```jsx
import React from 'react';
import ReactDom from 'react-dom';
import { GcTextBox, GcMask, GcMultiLineTextBox, GcComboBox, GcListBox, GcDateTime, GcNumber, GcCalendar } from '@grapecity/inputman.react';
import '@grapecity/inputman/CSS/gc.inputman-js.css';

class App extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      textValue: 'テキスト',
      maskValue: '9813205',
      dateValue: new Date(2020, 0, 1),
      numberValue: 1,
      listItems: ['項目1', '項目2', '項目3', '項目4', '項目5'],
      selectedListIndex: 0
    };
  }
  render() {
    return (
      <div>
        テキストコントロール
        <GcTextBox text={this.state.textValue}></GcTextBox><br/>
        複数行テキストコントロール
        <GcMultiLineTextBox text={this.state.textValue}></GcMultiLineTextBox><br/>
        マスクコントロール
        <GcMask value={this.state.maskValue} formatPattern={'〒\\D{3}-\\D{4}'}></GcMask><br/>
        日付時刻コントロール
        <GcDateTime value={this.state.dateValue}></GcDateTime><br/>
        数値コントロール
        <GcNumber value={this.state.numberValue}></GcNumber><br/>
        カレンダーコントロール
        <GcCalendar selectedDate={this.state.dateValue} focusDate={this.state.dateValue}></GcCalendar><br/>
        コンボコントロール
        <GcComboBox items={this.state.listItems} selectedIndex={this.state.selectedListIndex}></GcComboBox><br/>
        リストコントロール
        <GcListBox items={this.state.listItems} selectedIndex={this.state.selectedListIndex}></GcListBox>
      </div>
    )
  }
}

ReactDom.render(<App />, document.getElementById('app'));
```

#### hookを使用する場合

```jsx
import React, { useState } from 'react';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { GcTextBox, GcMultiLineTextBox, GcMask, GcDateTime, GcNumber, GcCalendar, GcComboBox, GcListBox } from '@grapecity/inputman.react';

function App() {
  const [textValue, setTextValue] = useState('テキスト');
  const [maskValue, setMaskValue] = useState('9813205');
  const [dateValue, setDateValue] = useState(new Date(2020, 0, 1));
  const [numberValue, setNumberValue] = useState(1);
  const [listItems, setListItems] = useState(['項目1', '項目2', '項目3', '項目4', '項目5']);
  const [selectedListIndex, setSelectedListIndex] = useState(0);
  return (
    <div className="App">
      テキストコントロール
      <GcTextBox text={textValue}></GcTextBox><br/>
      複数行テキストコントロール
      <GcMultiLineTextBox text={textValue}></GcMultiLineTextBox><br/>
      マスクコントロール
      <GcMask value={maskValue} formatPattern={'〒\\D{3}-\\D{4}'}></GcMask><br/>
      日付時刻コントロール
      <GcDateTime value={dateValue}></GcDateTime><br/>
      数値コントロール
      <GcNumber value={numberValue}></GcNumber><br/>
      カレンダーコントロール
      <GcCalendar selectedDate={dateValue} focusDate={dateValue}></GcCalendar><br/>
      コンボコントロール
      <GcComboBox items={listItems} selectedIndex={selectedListIndex}></GcComboBox><br/>
      リストコントロール
      <GcListBox items={listItems} selectedIndex={selectedListIndex}></GcListBox>
    </div>
  );
}

export default App;
```

5. 次のコマンドを実行して、アプリケーションを起動します。
``` sh
npm start
```

6. ブラウザで「`http://localhost:3000/`」を参照し、アプリケーションを表示します。