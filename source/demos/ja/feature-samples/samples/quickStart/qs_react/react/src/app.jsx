import React from 'react';
import ReactDom from 'react-dom';
import { GcTextBox, GcMask, GcMultiLineTextBox, GcComboBox, GcListBox, GcDateTime, GcNumber, GcCalendar } from '@grapecity/inputman.react';
import '@grapecity/inputman/CSS/gc.inputman-js.css';

class App extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            textValue: 'テキスト',
            maskValue: '9813205',
            dateValue: new Date(2020, 0, 1),
            numberValue: 1,
            listItems: ['項目1', '項目2', '項目3', '項目4', '項目5'],
            selectedListIndex: 0
        };
    }
    render() {
        return (
            <div>
                テキストコントロール
                <GcTextBox text={this.state.textValue}></GcTextBox><br/>
                複数行テキストコントロール
                <GcMultiLineTextBox text={this.state.textValue}></GcMultiLineTextBox><br/>
                マスクコントロール
                <GcMask value={this.state.maskValue} formatPattern={'〒\\D{3}-\\D{4}'}></GcMask><br/>
                日付時刻コントロール
                <GcDateTime value={this.state.dateValue}></GcDateTime><br/>
                数値コントロール
                <GcNumber value={this.state.numberValue}></GcNumber><br/>
                カレンダーコントロール
                <GcCalendar selectedDate={this.state.dateValue} focusDate={this.state.dateValue}></GcCalendar><br/>
                コンボコントロール
                <GcComboBox items={this.state.listItems} selectedIndex={this.state.selectedListIndex}></GcComboBox><br/>
                リストコントロール
                <GcListBox items={this.state.listItems} selectedIndex={this.state.selectedListIndex}></GcListBox>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById('app'));