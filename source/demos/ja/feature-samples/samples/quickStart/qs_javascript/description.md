InputManJSのコントロールを使用したアプリケーションを作成するには、次の手順に従います。（その他のコントロールの作成方法は、各コントロールののサンプルを参照してください。）

### 手順

1. <span> InputManJSのJSファイルとCSSファイルを参照します。 </span>
```
   <link rel="stylesheet" href="css/gc.inputman-js.css" />
   <script src="js/gc.inputman-js.ja.js"></script>
```

2. <span> HTMLコードで、コントロールの元になるDOM要素を追加します。DOM要素の種類はコントロールによって異なります。カレンダーコントロールではdiv要素を、複数行テキストコントロールではtextarea要素を、コンボコントロールとリストコントロールではselect要素を、その他の入力コントロールではinput要素を追加します。 </span>
```html
   テキストコントロール<br>
   <input id="gcTextBox"><br><br>
   複数行テキストコントロール<br>
   <textarea id="gcMultiLineTextBox"></textarea><br><br>
   マスクコントロール<br>
   <input id="gcMask"><br><br>
   日付時刻コントロール<br>
   <input id="gcDateTime"><br><br>
   数値コントロール<br>
   <input id="gcNumber"><br><br>
   カレンダーコントロール<br>
   <div id="gcCalendar"></div><br><br>
   コンボコントロール<br>
   <select id="gcComboBox"></select><br><br>
   リストコントロール<br>
   <select id="gcListBox"></select>
```

3. <span>JavaScriptコードで、コントロールのコンストラクタを実行してコントロールを生成します。コンストラクタの第1引数にはDOM要素を指定します。DOM要素は、document.getElementByIdメソッドやdocument.querySelectorメソッドなどで取得できます。コントロールのオプションは、コンストラクタの第2引数にオプションを含むJavaScriptオブジェクトを指定して設定できます。また、setメソッドを実行してコントロールのオプションを設定することもできます。次の例では、GcComboBoxはコンストラクタのオプションを使用してitemsオプションを設定し、GcListBoxはsetメソッドを実行してitemsオプションを設定しています。</span>
```js
   var gcTextBox = new GC.InputMan.GcTextBox(document.getElementById('gcTextBox'));
   var gcMultiLineTextBox = new GC.InputMan.GcMultiLineTextBox(document.getElementById('gcMultiLineTextBox'));
   var gcMask = new GC.InputMan.GcMask(document.getElementById('gcMask'), {
     formatPattern: '〒\\D{3}-\\D{4}'
   });
   var gcDateTime = new GC.InputMan.GcDateTime(document.getElementById('gcDateTime'));
   var gcNumber = new GC.InputMan.GcNumber(document.getElementById('gcNumber'));
   var gcCalendar = new GC.InputMan.GcCalendar(document.getElementById('gcCalendar'));
   var gcComboBox = new GC.InputMan.GcComboBox(document.getElementById('gcComboBox'), {
     items: [ '項目1', '項目2', '項目3', '項目4', '項目5' ]
   });
   var gcListBox = new GC.InputMan.GcListBox(document.getElementById('gcListBox'));
   gcListBox.setItems([ '項目1', '項目2', '項目3', '項目4', '項目5' ]);
```

<!-- -->

