﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcTextBox = new InputMan.GcTextBox(document.getElementById('gcTextBox'));
const gcMultiLineTextBox = new InputMan.GcMultiLineTextBox(document.getElementById('gcMultiLineTextBox'));
const gcMask = new InputMan.GcMask(document.getElementById('gcMask'), {
    formatPattern: '〒\\D{3}-\\D{4}'
});
const gcDateTime = new InputMan.GcDateTime(document.getElementById('gcDateTime'));
const gcNumber = new InputMan.GcNumber(document.getElementById('gcNumber'));
const gcCalendar = new InputMan.GcCalendar(document.getElementById('gcCalendar'));
const gcComboBox = new InputMan.GcComboBox(document.getElementById('gcComboBox'), {
    items: ['項目1', '項目2', '項目3', '項目4', '項目5']
});
const gcListBox = new InputMan.GcListBox(document.getElementById('gcListBox'),{
    items:['項目1', '項目2', '項目3', '項目4', '項目5']
});