import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcSoftKeyboard = new InputMan.GcSoftKeyboard(document.getElementById('gcSoftKeyboard'), {
    target: document.getElementById('input'),
    displayType: InputMan.DisplayType.Numeric | InputMan.DisplayType.Alphabet
});
document.getElementById('input').addEventListener('focus', () => {
    gcSoftKeyboard.open();
});

// 数字キーをすべて削除して、代わりにひらがなキーを追加します。
gcSoftKeyboard.numericKeys.removeKey('1234567890'.split(''));
for (let c = 'ぁ'.charCodeAt(); c <= 'ん'.charCodeAt(); c++) {
    gcSoftKeyboard.numericKeys.addKey(String.fromCharCode(c));
}
