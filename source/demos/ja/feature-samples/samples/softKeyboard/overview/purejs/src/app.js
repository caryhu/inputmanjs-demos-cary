import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcSoftKeyboard = new InputMan.GcSoftKeyboard(document.getElementById('gcSoftKeyboard'), {
    target: document.getElementById('input')
});
document.getElementById('input').addEventListener('focus', () => {
    gcSoftKeyboard.open();
});