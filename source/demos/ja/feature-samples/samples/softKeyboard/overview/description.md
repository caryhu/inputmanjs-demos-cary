ソフトウェアキーボード（GcSoftKeyboard）コントロールは、画面上にキーボードを表示して、マウス操作で文字を入力することができます。ソフトウェアキーボードは、ハードウェアキーボードを使用せずに文字を入力することで、キーボード入力を監視して情報を盗まれることを防ぎやすくなります。

ソフトウェアキーボードは、HTML入力要素とGcTextBoxで文字入力をサポートします。
