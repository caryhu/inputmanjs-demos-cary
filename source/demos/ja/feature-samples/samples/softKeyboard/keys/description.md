ソフトウェアキーボードでは、表示するキーの種類やレイアウトを変更できます。

## 表示するキーの種類

表示するキーの種類は、GcSoftKeyboard.displayTypeプロパティで設定します。`DisplayType.Numeric | DisplayType.Alphabet`のように、ビット論理和を使用して複数の値を指定できます。

| 値 | 説明 |
| -- | -- |
| DisplayType.Numeric | 数字キー（0〜9） |
| DisplayType.Alphabet | アルファベット（a〜z） |
| DisplayType.Symbol | 記号（!?#$.,;:"'`*()<>[]{}_\|\/-+=~^&@%） |
| DisplayType.All | すべてのキー |

## 大文字アルファベットキーの表示

アルファベットキーは、小文字のみを表示するか、大文字と小文字を両方表示するかを選択できます。小文字のみを表示した場合は、CapsキーとShiftキーも表示されます。GcSoftKeyboard.showCapitalLettersプロパティで設定します。

| 値 | 説明 |
| -- | -- |
| true | 大文字と小文字を両方表示します。 |
| false | 小文字のみを表示します。 |

## キーのレイアウト

キーのレイアウトは、ABC順とQWERTY順（標準的なキーボードと同じ順）の2種類から選択できます。GcSoftKeyboard.qwertyLayoutプロパティで設定します。

| 値 | 説明 |
| -- | -- |
| true | QWERTY順のレイアウトで表示します。 |
| false | ABC順のレイアウトで表示します。 |
