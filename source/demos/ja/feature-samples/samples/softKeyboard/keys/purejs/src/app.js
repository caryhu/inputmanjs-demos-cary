import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcSoftKeyboard = new InputMan.GcSoftKeyboard(document.getElementById('gcSoftKeyboard'), {
    target: document.getElementById('input')
});
document.getElementById('input').addEventListener('focus', () => {
    gcSoftKeyboard.open();
});

// 数字キーを表示するかどうか
document.getElementById('displayTypeNumeric').addEventListener('change', () => {
    gcSoftKeyboard.displayType ^= InputMan.DisplayType.Numeric;
});
// アルファベットキーを表示するかどうか
document.getElementById('displayTypeAlphabet').addEventListener('change', () => {
    gcSoftKeyboard.displayType ^= InputMan.DisplayType.Alphabet;
});
// 記号キーを表示するかどうか
document.getElementById('displayTypeSymbol').addEventListener('change', () => {
    gcSoftKeyboard.displayType ^= InputMan.DisplayType.Symbol;
});
// 大文字アルファベットキーを表示するかどうか
document.getElementById('showCapitalLetters').addEventListener('change', (e) => {
    gcSoftKeyboard.showCapitalLetters = e.target.checked;
});
// QWERTYレイアウトで表示するかどうか
document.getElementById('qwertyLayout').addEventListener('change', (e) => {
    gcSoftKeyboard.qwertyLayout = e.target.checked;
});
