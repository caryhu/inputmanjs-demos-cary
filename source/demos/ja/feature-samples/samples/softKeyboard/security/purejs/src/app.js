import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

// セキュリティを考慮した設定
const gcTextBox1 = new InputMan.GcTextBox(document.getElementById('gcTextBox1'));
gcTextBox1.addDropDownSoftKeyboard(null, {
    securityLevel: InputMan.SecurityLevel.High,
    randomDisplay: true,
    hideButtonWhenMouseOn: true
}, true);

// 既定の設定
const gcTextBox2 = new InputMan.GcTextBox(document.getElementById('gcTextBox2'));
gcTextBox2.addDropDownSoftKeyboard(null, null, true);
