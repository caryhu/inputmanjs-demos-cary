import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcSoftKeyboard = new InputMan.GcSoftKeyboard(document.getElementById('gcSoftKeyboard'), {
    target: document.getElementById('input')
});
// フォーカス時にソフトウェアキーボードを表示する
document.getElementById('input').addEventListener('focus', () => {
    gcSoftKeyboard.open();
});
// ボタンクリック時にソフトウェアキーボードを表示する
document.getElementById('showKeyboard').addEventListener('click', () => {
    gcSoftKeyboard.open();
});

// ソフトウェアキーボードをドロップダウン表示する
const gcTextBox1 = new InputMan.GcTextBox(document.getElementById('gcTextBox1'));
gcTextBox1.addDropDownSoftKeyboard();

// ソフトウェアキーボードをポップアップ表示する
const gcTextBox2 = new InputMan.GcTextBox(document.getElementById('gcTextBox2'));
gcTextBox2.addDropDownSoftKeyboard(null, null, true);
