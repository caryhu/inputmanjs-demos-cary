import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";
import { products, orders, gcProducts } from './data'

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {

    public products: Array<string> = products;
    public orders: Array<object> = orders;
    public gcProducts: Array<object> = gcProducts;

    public itemTemplate: string = `<div class="template-item">
    <div class="image">
        <img src="{!logo}">
    </div>
    <div class="names">
        <div class="name">{!name}</div>
        <div class="category">{!category}</div>
    </div>
    <div class="description">{!description}</div>
    </div>`;

    public generatingItem(args: GC.InputMan.IItemGeneratingArgs) {
        args.item.classList.add(args.index % 2 ? 'odd' : 'even');
    }

}


enableProdMode();