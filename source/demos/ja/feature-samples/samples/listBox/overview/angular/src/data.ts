export const products = ['果汁100%オレンジ', '果汁100%グレープ', '果汁100%レモン', '果汁100%ピーチ', 'コーヒーマイルド', 'コーヒービター'];

export const orders = [
    { id: 15, product: 'ピュアデミグラスソース', date: '2017/01/10', price: 200, amount: 30 },
    { id: 17, product: 'だしこんぶ', date: '2017/01/08', price: 290, amount: 50 },
    { id: 18, product: 'ピリカラタバスコ', date: '2017/01/12', price: 200, amount: 20 },
    { id: 84, product: 'なまわさび', date: '2017/01/21', price: 200, amount: 40 },
    { id: 85, product: 'なまからし', date: '2017/01/13', price: 200, amount: 40 },
    { id: 86, product: 'なましょうが', date: '2017/01/20', price: 200, amount: 40 },
];

export const gcProducts = [
    { logo: 'img/AR.png', name: 'ActiveReports', category: '帳票・レポート', description: '日本仕様の帳票開発に必要な機能を搭載したコンポーネント' },
    { logo: 'img/SP.png', name: 'SPREAD', category: '表計算・グリッド', description: 'Excel風のビューと表計算機能を実現するUIコンポーネント' },
    { logo: 'img/C1.png', name: 'ComponentOne', category: 'コンポーネントセット', description: 'Visual Studioで利用する.NET Framework用コンポーネント' },
    { logo: 'img/IM.png', name: 'InputMan', category: '入力支援', description: '快適な入力を実現する日本仕様入力コンポーネントセット' },
    { logo: 'img/MR.png', name: 'MultiRow', category: '多段明細', description: '1レコード複数行&日付表示に最適なグリッドコンポーネント' },
    { logo: 'img/DD.png', name: 'DioDocs', category: 'ドキュメントAPI', description: 'ドキュメントを生成、更新するAPIライブラリ' },
];