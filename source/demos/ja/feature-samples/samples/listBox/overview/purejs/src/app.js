import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';
import { products, orders, gcProducts } from './data'

const gcListBox1 = new InputMan.GcListBox(document.getElementById('gcListBox1'), {
    items: products
});

const gcListBox2 = new InputMan.GcListBox(document.getElementById('gcListBox2'), {
    items: orders,
    columns: [
        { name: 'id', label: '商品コード', width: 80, clickSort: true },
        { name: 'product', label: '商品名', width: 200 },
        { name: 'date', label: '受注日', width: 120, clickSort: true },
        { name: 'price', label: '単価', width: 80, clickSort: true },
        { name: 'amount', label: '数量', width: 80, clickSort: true },
    ],
    showCheckBox: true
});

const gcListBox3 = new InputMan.GcListBox(document.getElementById('gcListBox3'), {
    items: gcProducts,
    itemHeight: 50,
    itemTemplate: `<div class="template-item">
        <div class="image">
            <img src="{!logo}">
        </div>
        <div class="names">
            <div class="name">{!name}</div>
            <div class="category">{!category}</div>
        </div>
        <div class="description">{!description}</div>
    </div>`,
    generatingItem: (args) => {
        args.item.classList.add(args.index % 2 ? 'odd' : 'even');
    }
});