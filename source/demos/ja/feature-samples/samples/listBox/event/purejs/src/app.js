import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';
import products from './data';

var gcListBox = new InputMan.GcListBox(document.getElementById('gcListBox'), {
    items: products
});

// イベントハンドラ
gcListBox.addEventListener(
    InputMan.GcListBoxEvent.CheckedChanged, 
    () =>  log('CheckedChanged')
);
gcListBox.addEventListener(
    InputMan.GcListBoxEvent.FocusedChanged, 
    () => log('FocusedChanged')
);
gcListBox.addEventListener(
    InputMan.GcListBoxEvent.ItemClick, 
    () => log('ItemClick')
);
gcListBox.addEventListener(
    InputMan.GcListBoxEvent.ItemsChanged, 
    () => log('ItemsChanged')
);
gcListBox.addEventListener(
    InputMan.GcListBoxEvent.LoadComplete,
    () => log('LoadComplete')
);
gcListBox.addEventListener(
    InputMan.GcListBoxEvent.SelectedChanged,
    () => log('SelectedChanged')
);

// テキストボックスにログを出力
const log = (message) => {
    var textarea = document.getElementById('log');
    textarea.value += message + '\n';
    textarea.scrollTop = textarea.scrollHeight;
}