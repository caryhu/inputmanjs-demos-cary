import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, ViewChild, ElementRef } from '@angular/core';
import GC from "@grapecity/inputman";
import products from './data';

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public products: Array<string> = products;
    public message: string = "";
    @ViewChild('logPanel')
    logPanel: ElementRef;

    // テキストボックスにログを出力
    public log(message: string) {
        this.message += message + '\n';
        this.logPanel.nativeElement.scrollTop = this.logPanel.nativeElement.scrollHeight;
    }
}


enableProdMode();