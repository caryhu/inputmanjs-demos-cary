import * as React from "react";
import * as ReactDom from "react-dom";
import { GcListBox } from "@grapecity/inputman.react";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import products from './data';

class App extends React.Component{

    constructor(props, context) {
        super(props, context);
        this.state = {
            message: ''
        };
    }

    log(msg){
        var newMessage = this.state.message + msg + '\n';
        this.refs.log.scrollTop = this.refs.log.scrollHeight;
        this.setState({message: newMessage});
    }

    render(){
        return (
            <div>
                <GcListBox items={products}
                checkedChanged={(eArgs) => {
                    this.log('CheckedChanged');
                }}
                selectedChanged={(eArgs) => {
                    this.log('SelectedChanged');
                }}
                focusedChanged={(eArgs) => {
                    this.log('FocusedChanged');
                }}
                itemClick={(eArgs) => {
                    this.log('ItemClick');
                }}
                loadComplete={(eArgs) => {
                    this.log('LoadComplete');
                }}
                itemsChanged={(eArgs) => {
                    this.log('ItemsChanged');
                }}></GcListBox><br/>
                イベント<br/>
                <textarea value={this.state.message} ref="log" id="log" rows="10" cols="30"></textarea>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));