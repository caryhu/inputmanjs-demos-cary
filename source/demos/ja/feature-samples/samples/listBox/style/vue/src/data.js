export const products = [
    '果汁100%オレンジ',
    '果汁100%グレープ',
    '果汁100%レモン',
    '果汁100%ピーチ',
    'コーヒーマイルド',
    'コーヒービター'
];

export const orders = [
    { id: 15, product: 'ピュアデミグラスソース', date: '2017/01/10', price: 200, amount: 30 },
    { id: 17, product: 'だしこんぶ', date: '2017/01/08', price: 290, amount: 50 },
    { id: 18, product: 'ピリカラタバスコ', date: '2017/01/12', price: 200, amount: 20 },
    { id: 84, product: 'なまわさび', date: '2017/01/21', price: 200, amount: 40 },
    { id: 85, product: 'なまからし', date: '2017/01/13', price: 200, amount: 40 },
    { id: 86, product: 'なましょうが', date: '2017/01/20', price: 200, amount: 40 },
];

export const styles = {
    '.gcim__listbox': {
        backgroundColor: '#ddffdd',
        borderColor: '#009900',
        borderWidth: '2px',
        borderStyle: 'dashed',
        borderRadius: '12px',
        boxShadow: '5px 5px 5px rgba(0,0,0,0.5)',
        color: '#009900',
        fontSize: '20px',
        fontWeight: 'bold',
        fontStyle: 'italic',
        fontFamily: 'serif',
        textShadow: '1px 1px 1px rgba(0,0,0,0.5)'
    },
    '.gcim__listbox > .viewport': {
        cursor: 'crosshair'
    },
    '.gcim__listbox .column-header': {
        color: '#009900',
        fontSize: '20px',
        fontWeight: 'bold',
        fontStyle: 'italic',
        fontFamily: 'serif',
        textShadow: '1px 1px 1px rgba(0,0,0,0.5)'
    },
    '.gcim__listbox .footer_style': {
        color: '#009900',
        fontSize: '20px',
        fontWeight: 'bold',
        fontStyle: 'italic',
        fontFamily: 'serif',
        textAlign: 'right',
        textShadow: '1px 1px 1px rgba(0,0,0,0.5)'
    },
    '.list_overlay': {
        opacity: 0.8
    },
}

export const specRules = {
    'text_style_align': [
        {
            selector: '.gcim__listbox .viewport .list-item',
            style: 'text-align: right;'
        },
        {
            selector: '.gcim__listbox .text-content',
            style: 'text-align: right !important;'
        },
        {
            selector: '.gcim__listbox .footer',
            style: 'text-align: right;'
        }
    ]  
}