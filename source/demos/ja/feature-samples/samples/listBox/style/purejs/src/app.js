import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';
import { orders, products, styles, specRules } from './data';

const SpecPrefix = "$spec:";

class OSpecCssStyleMap {
    map = {};

    constructor() { }

    addRules(key, rules) {
        if (!this.map[key]) {
            this.map[key] = [];
        }

        rules.forEach(rule => {
            this.map[key].push(rule);
        })
    }

    deleteAllRuleByKey(key) {
        this.map[key] = [];
    }

    getAllRule() {
        let arr = [];
        Object.keys(this.map).forEach(key => {
            this.map[key].forEach((rule) => {
                arr.push(rule);
            })
        }); 
        return arr;
    }
}

let styleSheet = createStyleSheet();
let specStyleSheet = createStyleSheet();
let specCssStyleMap = new OSpecCssStyleMap();

initStyleSheet(styleSheet, styles);
initIMControls();
attachEvent();


function initIMControls() {
    const gcListBox1 = new InputMan.GcListBox(document.getElementById('gcListBox1'), {
        items: products
    });
    gcListBox1.setSelectedIndex(0);
    
    const gcListBox2 = new InputMan.GcListBox(document.getElementById('gcListBox2'), {
        items: products
    });
    gcListBox2.setSelectedIndex(0);
    gcListBox2.setEnabled(false);
    
    const gcListBox3 = new InputMan.GcListBox(document.getElementById('gcListBox3'), {
        items: orders,
        columns: [
            { name: 'id', label: '商品コード', width: 80 },
            { name: 'product', label: '商品名', width: 200 },
        ],
        footerTemplate: '商品を選択してください'
    });
    gcListBox3.setSelectedIndex(0);
}

function attachEvent() {
    document.getElementById('container').addEventListener('click', (e) => {
        if(e.target.tagName == 'INPUT'){
            inputBtnClickHandler(e);
        }else if(e.target.tagName == 'BUTTON'){
            copyStyle()
        }
    })
}

function inputBtnClickHandler(evt) {
    let el = evt.target;

    if (startsWith(el.value, SpecPrefix)) {
        let key = el.value.slice(SpecPrefix.length, el.value.length);
        processSpecKey(key, el.checked);
    } else {
        processKey(el.value, el.checked);
    }

    showCssText([styleSheet, specStyleSheet]);
}

function initStyleSheet(styleSheet, styles) {
    Object.keys(styles).forEach((styleName, index) => styleSheet.insertRule(`${styleName}{}`, index));
}

function processKey(key, checked) {
    let values = key.split(','),
    selector = values[0],
    propName = values[1];

    let cssRules = findCssRule(styleSheet, selector);
    cssRules.style[propName] = checked ? styles[selector][propName] : '';
}

function processSpecKey(key, checked) {
    let specStyle = specRules[key];
    if (checked) {
        specCssStyleMap.addRules(key, specStyle);
    } else {
        specCssStyleMap.deleteAllRuleByKey(key);
    }

    syncSpecStyleMapToStyleSheet(specCssStyleMap, specStyleSheet);
}

//#region helperMethod
function findCssRule(styleSheet, selector) {
    for(let i = 0; i < styleSheet.cssRules.length; i++){
        if(styleSheet.cssRules[i].selectorText == selector){
            return styleSheet.cssRules[i];
        }
    }
    return null;
}

function syncSpecStyleMapToStyleSheet(styleMap, styleSheet) {
    clearStyleSheet(styleSheet);

    let specRules = styleMap.getAllRule();
    specRules.forEach((rule) => {
        styleSheet.insertRule(`${rule.selector} { ${rule.style} }`, styleSheet.cssRules.length);
    });
}

function showCssText(styleSheets) {
    let cssTexts = [];
    styleSheets.forEach((styleSheet) => {
        appendCssText(styleSheet, cssTexts);
    });
    document.getElementById('style').value = cssTexts.join('\r\n');
}

function appendCssText(styleSheet, cssTexts) {
    for(let i = 0; i < styleSheet.cssRules.length; i++){
        if(styleSheet.cssRules[i].style.length > 0){
            cssTexts.push(styleSheet.cssRules[i].cssText
                .replace(/{/g, '{\r\n  ')
                .replace(/;/g, ';\r\n  ')
                .replace(/  }/g, '}'));
        }
    }
}

function clearStyleSheet(cssStyleSheet) {
    let len = cssStyleSheet.cssRules.length;
    while(len--) {
        cssStyleSheet.deleteRule(0);
    }
}

function createStyleSheet() {
    let element = document.createElement('style');
    document.head.appendChild(element);
    return element.sheet;
}

function copyStyle() {
    document.getElementById('style').select();
    document.execCommand('copy');
}

function startsWith(str, searchString) {
    return str.slice(0, searchString.length) == searchString;
}
//#endregion