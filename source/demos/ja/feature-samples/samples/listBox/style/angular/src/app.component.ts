import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, ViewChild, ElementRef } from '@angular/core';
import GC from "@grapecity/inputman";
import { orders, products, styles, specRules} from './data';

const SpecPrefix = "$spec:";

class OSpecCssStyleMap {
    map = {};

    constructor() { }

    addRules(key: string, rules: any[]) {
        if (!this.map[key]) {
            this.map[key] = [];
        }

        rules.forEach(rule => {
            this.map[key].push(rule);
        })
    }

    deleteAllRuleByKey(key: string) {
        this.map[key] = [];
    }

    getAllRule() {
        let arr :any[] = [];
        Object.keys(this.map).forEach(key => {
            this.map[key].forEach((rule: any) => {
                arr.push(rule);
            })
        }); 
        return arr;
    }
}

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public orders: Array<object> = orders;
    public products: Array<string> = products;
    public styleSheet: CSSStyleSheet;
    public specStyleSheet: CSSStyleSheet;
    public specCssStyleMap: OSpecCssStyleMap;
    public cssText: string;
    @ViewChild('style')
    public style: ElementRef;

    constructor() {
        this.styleSheet = this.createStyleSheet();
        this.specStyleSheet = this.createStyleSheet();
        this.specCssStyleMap = new OSpecCssStyleMap();

        this.initStyleSheet(this.styleSheet, styles);
    }

    public containerClickHandler(e: Event): void {
        if ((e.target as HTMLElement).tagName == 'INPUT') {
            this.inputBtnClickHandler(e);
        } else if ((e.target as HTMLElement).tagName == 'BUTTON') {
            this.copyStyle()
        }
    }

    inputBtnClickHandler(evt: Event) {
        let el = evt.target as HTMLInputElement;
    
        if (this.startsWith(el.value, SpecPrefix)) {
            let key = el.value.slice(SpecPrefix.length, el.value.length);
            this.processSpecKey(key, el.checked);
        } else {
            this.processKey(el.value, el.checked);
        }
    
        this.showCssText([this.styleSheet, this.specStyleSheet]);
    }
    
    initStyleSheet(styleSheet: CSSStyleSheet, styles: any) {
        Object.keys(styles).forEach((styleName, index) => styleSheet.insertRule(`${styleName}{}`, index));
    }
    
    processKey(key: string, checked: boolean) {
        let values = key.split(','),
        selector = values[0],
        propName = values[1];
    
        let cssRules = this.findCssRule(this.styleSheet, selector);
        (cssRules as CSSFontFaceRule).style[propName] = checked ? styles[selector][propName] : '';
    }
    
    processSpecKey(key: string, checked: boolean) {
        let specStyle = specRules[key];
        if (checked) {
            this.specCssStyleMap.addRules(key, specStyle);
        } else {
            this.specCssStyleMap.deleteAllRuleByKey(key);
        }
    
        this.syncSpecStyleMapToStyleSheet(this.specCssStyleMap, this.specStyleSheet);
    }

    //#region helperMethod
    findCssRule(styleSheet: CSSStyleSheet, selector: string) {
        for(let i = 0; i < styleSheet.cssRules.length; i++){
            if((styleSheet.cssRules[i] as CSSStyleRule).selectorText == selector){
                return styleSheet.cssRules[i];
            }
        }
        return null;
    }

    syncSpecStyleMapToStyleSheet(styleMap: OSpecCssStyleMap, styleSheet: CSSStyleSheet) {
        this.clearStyleSheet(styleSheet);

        let specRules = styleMap.getAllRule();
        specRules.forEach((rule) => {
            styleSheet.insertRule(`${rule.selector} { ${rule.style} }`, styleSheet.cssRules.length);
        });
    }

    showCssText(styleSheets: CSSStyleSheet[]) {
        let cssTexts: string[] = [];
        styleSheets.forEach((styleSheet) => {
            this.appendCssText(styleSheet, cssTexts);
        });
        this.cssText = cssTexts.join('\r\n');
    }

    appendCssText(styleSheet: CSSStyleSheet, cssTexts: string[]) {
        for(let i = 0; i < styleSheet.cssRules.length; i++){
            if((styleSheet.cssRules[i] as CSSFontFaceRule).style.length > 0){
                cssTexts.push(styleSheet.cssRules[i].cssText
                    .replace(/{/g, '{\r\n  ')
                    .replace(/;/g, ';\r\n  ')
                    .replace(/  }/g, '}'));
            }
        }
    }

    clearStyleSheet(cssStyleSheet: CSSStyleSheet) {
        let len = cssStyleSheet.cssRules.length;
        while(len--) {
            cssStyleSheet.deleteRule(0);
        }
    }

    createStyleSheet() {
        let element = document.createElement('style');
        document.head.appendChild(element);
        return element.sheet;
    }

    copyStyle() {
        this.style.nativeElement.select();
        document.execCommand('copy');
    }
    startsWith(str: string, searchString: string) {
        return str.slice(0, searchString.length) == searchString;
    }
    //#endregion
}


enableProdMode();