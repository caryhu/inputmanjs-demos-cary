import * as React from "react";
import * as ReactDom from "react-dom";
import { GcListBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

class App extends React.Component{

    columns =[
        { name: 'name', label: '製品名', width: '*' },
        { name: 'category', label: '製品カテゴリー', width: '1.5*' },
        { name: 'description', label: '製品紹介', width: '2.5*' }
    ]
    
    items =  [
        { name: 'ActiveReports', category: '帳票・レポート', description: '日本仕様の帳票開発に必要な機能を搭載したコンポーネント' },
        { name: 'SPREAD', category: '表計算・グリッド', description: 'Excel風のビューと表計算機能を実現するUIコンポーネント' },
        { name: 'ComponentOne', category: 'コンポーネントセット', description: 'Visual Studioで利用する.NET Framework用コンポーネント' },
        { name: 'InputMan', category: '入力支援', description: '快適な入力を実現する日本仕様入力コンポーネントセット' },
        { name: 'MultiRow', category: '多段明細', description: '1レコード複数行&日付表示に最適なグリッドコンポーネント' },
        { name: 'DioDocs', category: 'ドキュメントAPI', description: 'ドキュメントを生成、更新するAPIライブラリ' },
    ]

    render(){
        return (
            <div>
            <GcListBox
                ref="listbox"
                items={this.items}
                columns={this.columns}
                width={500}
                allowResize={true}
                allowColumnResize={true}
                overflow={"both"} />
            <table class="sample">
                <tr>
                    <th>列の操作</th>
                    <td>
                        <button onClick={(e)=>{
                            this.columns.push({ name: 'sample', label: '追加列', width: '*' });
                            this.refs.listbox.getNestedIMControl().setColumns(this.columns);
                        }}>列を追加する</button>&nbsp;
                        <button onClick={(e)=>{
                            var columnName = this.columns.length !==0 ? this.columns.pop().name: '';
                            this.refs.listbox.getNestedIMControl().deleteColumn(columnName);
                        }}>列を削除する</button>
                    </td>
                </tr>
            </table>
        </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));