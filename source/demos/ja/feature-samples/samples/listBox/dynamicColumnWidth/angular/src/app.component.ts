import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, ViewChild } from '@angular/core';
import { GcListBoxComponent } from "@grapecity/inputman.angular";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    @ViewChild(GcListBoxComponent)
    listbox: GcListBoxComponent;

    public items = [
        { name: 'ActiveReports', category: '帳票・レポート', description: '日本仕様の帳票開発に必要な機能を搭載したコンポーネント' },
        { name: 'SPREAD', category: '表計算・グリッド', description: 'Excel風のビューと表計算機能を実現するUIコンポーネント' },
        { name: 'ComponentOne', category: 'コンポーネントセット', description: 'Visual Studioで利用する.NET Framework用コンポーネント' },
        { name: 'InputMan', category: '入力支援', description: '快適な入力を実現する日本仕様入力コンポーネントセット' },
        { name: 'MultiRow', category: '多段明細', description: '1レコード複数行&日付表示に最適なグリッドコンポーネント' },
        { name: 'DioDocs', category: 'ドキュメントAPI', description: 'ドキュメントを生成、更新するAPIライブラリ' },
    ];
    public columns = [
        { name: 'name', label: '製品名', width: '*' },
        { name: 'category', label: '製品カテゴリー', width: '1.5*' },
        { name: 'description', label: '製品紹介', width: '2.5*' }
    ];
    
    public addColumn(){
        this.columns.push({ name: 'sample', label: '追加列', width: '*' });
        this.listbox.getNestedIMControl().setColumns(this.columns);
    }
    public deleteColumn(){
        var columnName = this.columns.length !==0 ? this.columns.pop().name: '';
        this.listbox.getNestedIMControl().deleteColumn(columnName);
    }
}

enableProdMode();