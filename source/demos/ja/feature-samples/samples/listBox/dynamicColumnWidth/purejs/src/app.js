import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

var column = [
    { name: 'name', label: '製品名', width: '*' },
    { name: 'category', label: '製品カテゴリー', width: '1.5*' },
    { name: 'description', label: '製品紹介', width: '2.5*' }
];

var gcListBox = new InputMan.GcListBox(document.getElementById('listbox1'), {
    items: [
        { name: 'ActiveReports', category: '帳票・レポート', description: '日本仕様の帳票開発に必要な機能を搭載したコンポーネント' },
        { name: 'SPREAD', category: '表計算・グリッド', description: 'Excel風のビューと表計算機能を実現するUIコンポーネント' },
        { name: 'ComponentOne', category: 'コンポーネントセット', description: 'Visual Studioで利用する.NET Framework用コンポーネント' },
        { name: 'InputMan', category: '入力支援', description: '快適な入力を実現する日本仕様入力コンポーネントセット' },
        { name: 'MultiRow', category: '多段明細', description: '1レコード複数行&日付表示に最適なグリッドコンポーネント' },
        { name: 'DioDocs', category: 'ドキュメントAPI', description: 'ドキュメントを生成、更新するAPIライブラリ' },
    ],
    columns: column,
    width: '500',
    allowResize: true,
    allowColumnResize: true,
    overflow: 'both',
});

document.getElementById('addColumn').addEventListener('click', (e) => {
    column.push({ name: 'sample', label: '追加列', width: '*' });
    gcListBox.setColumns(column);
});

document.getElementById('deleteColumn').addEventListener('click', (e) => {
    var columnName = column.length !==0 ? column.pop().name: '';
    gcListBox.deleteColumn(columnName)
});