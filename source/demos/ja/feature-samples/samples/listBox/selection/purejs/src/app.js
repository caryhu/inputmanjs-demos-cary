import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';
import {products, allIndecies} from './data';

let gcListBox;

var listBoxSelectionModes = [
    InputMan.ListBoxSelectionMode.Single,
    InputMan.ListBoxSelectionMode.Multiple,
    InputMan.ListBoxSelectionMode.MultipleExt
];
document.getElementById('setSelectionMode').addEventListener('change', (e) => {
    gcListBox.setSelectionMode(listBoxSelectionModes[e.target.selectedIndex]);
    document.getElementById('selectAll').disabled = e.target.selectedIndex == 0;
});

const attachClick = (id, handler) => {
    document.getElementById(id).addEventListener('click', handler);
}

const render = (showCheckBox) => {
    const container = document.getElementById('container');
    container.innerHTML = '';
    gcListBox = new InputMan.GcListBox(container, {
        items: products,
        showCheckBox
    });
    gcListBox.addEventListener('SelectedChanged', (sender) => {
        document.getElementById('selectedItems').innerText = sender.getSelectedValues().join('\n');
    });
    gcListBox.addEventListener('CheckedChanged', (sender) => {
        document.getElementById('checkedItems').innerText = sender.getCheckedItems().map(item => item.Text).join('\n');
    });
}

render(true);

attachClick('selectAll', () => gcListBox.setSelectedIndices(allIndecies))

attachClick('unselectAll', () => gcListBox.clearSelected());

attachClick('checkAll', () => gcListBox.setCheckedIndices(allIndecies))

attachClick('uncheckAll', () => gcListBox.clearChecked());

document.getElementById('showCheckBox').addEventListener('change', (e) => {
    let checked = e.target.checked;
    document.getElementById('checkAll').disabled = !checked;
    document.getElementById('uncheckAll').disabled = !checked;
    gcListBox.clearSelected();
    gcListBox.clearChecked();
    render(checked);
});