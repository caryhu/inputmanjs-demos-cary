import * as React from "react";
import * as ReactDom from "react-dom";
import { GcListBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import {products, allIndecies} from './data';

var listBoxSelectionModes = [
    InputMan.ListBoxSelectionMode.Single,
    InputMan.ListBoxSelectionMode.Multiple,
    InputMan.ListBoxSelectionMode.MultipleExt
];
class App extends React.Component{
    gcListBox = null;
    constructor(props, context) {
        super(props, context);
        this.state = {
            key: (new Date()).valueOf(),
            showCheckBox: true,
            selectionMode: InputMan.ListBoxSelectionMode.Single,
            selectAllDisabled: true
        };
    }

    componentDidMount(){
        this.attachClick('selectAll', () => this.gcListBox.setSelectedIndices(allIndecies))

        this.attachClick('unselectAll', () => this.gcListBox.clearSelected());
        
        this.attachClick('checkAll', () => this.gcListBox.setCheckedIndices(allIndecies))
        
        this.attachClick('uncheckAll', () => this.gcListBox.clearChecked());
    }

    attachClick = (id, handler) => {
        document.getElementById(id).addEventListener('click', handler);
    }

    render(){
        return (
            <div>
                <div class="flexbox">
                    <div>
                       <GcListBox 
                               key ={(new Date()).valueOf()}
                               items= {products}
                               showCheckBox={this.state.showCheckBox}
                               selectionMode={this.state.selectionMode}
                               onInitialized={(imCtrl)=>{ this.gcListBox = imCtrl;}}
                               checkedChanged={(eArgs) => {
                                document.getElementById('checkedItems').innerText = eArgs.getCheckedItems().map(item => item.Text).join('\n');
                               }}
                               selectedChanged={(eArgs) => {
                                document.getElementById('selectedItems').innerText = eArgs.getSelectedValues().join('\n');
                               }}></GcListBox>
                    </div>
                    <div>
                        選択された項目：<div id="selectedItems"></div>
                    </div>
                    <div>
                        チェックされた項目：<div id="checkedItems"></div>
                    </div>
                </div>
                <table class="sample">
                    <tr>
                        <th>選択モード</th>
                        <td>
                            <select id="setSelectionMode" onChange={(e)=>{
                                    var disabled = e.target.selectedIndex > 0 ? false : true;
                                    this.setState({selectionMode: listBoxSelectionModes[e.target.selectedIndex], selectAllDisabled: disabled});
                                }}>
                                <option>1つの項目のみ選択</option>
                                <option>クリックで複数の項目を選択</option>
                                <option>Ctrl／Shift+クリックで複数の項目を選択</option>
                            </select><br/>
                            <button id="selectAll" disabled={this.state.selectAllDisabled} >すべて選択</button>&nbsp;
                            <button id="unselectAll">すべて選択解除</button>
                        </td>
                    </tr>
                    <tr>
                        <th>チェックボックス</th>
                        <td>
                            <label><input type="checkbox" id="showCheckBox" checked={this.state.showCheckBox} onChange={(e)=>{this.setState({key: (new Date()).valueOf(), showCheckBox: e.target.checked})}}/> チェックボックスを表示</label>
                            <br/>
                            <button id="checkAll">すべてチェック</button>&nbsp;
                            <button id="uncheckAll">すべてチェック解除</button>
                        </td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));