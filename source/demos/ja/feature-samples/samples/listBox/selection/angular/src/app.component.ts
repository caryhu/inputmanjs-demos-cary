import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, ViewChild, ViewContainerRef, TemplateRef, AfterContentInit } from '@angular/core';
import GC from "@grapecity/inputman";
import { products, allIndecies } from './data';

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent implements AfterContentInit {
    public products: Array<string> = products;
    public allIndecies: Array<number> = allIndecies;

    public selectedItemsStr: string;
    public checkedItemsStr: string;

    public selectionMode: GC.InputMan.ListBoxSelectionMode;
    public gcListBox: GC.InputMan.GcListBox;

    public selectAllDisabled: boolean = true
    public showCheckBox: boolean = true;

    public listBoxSelectionModes = [
        GC.InputMan.ListBoxSelectionMode.Single,
        GC.InputMan.ListBoxSelectionMode.Multiple,
        GC.InputMan.ListBoxSelectionMode.MultipleExt
    ];

    public onInitialized(sender: GC.InputMan.GcListBox): void {
        this.gcListBox = sender;
    }

    public updateSelectedItem(sender: GC.InputMan.GcComboBox): void {
        this.selectedItemsStr = (sender as any).getSelectedValues().join('\n');
    }

    public updateCheckItem(sender: GC.InputMan.GcComboBox): void {
        this.checkedItemsStr = sender.getCheckedItems().map(item => (item as any).Text).join('\n');
    }

    public selectAll() {
        this.gcListBox.setSelectedIndices(allIndecies);
    }

    public unSelectAll() {
        this.gcListBox.clearSelected();
    }

    public checkAll() {
        this.gcListBox.setCheckedIndices(allIndecies);
    }

    public unCheckAll() {
        this.gcListBox.clearChecked();
    }

    public updateShowCheckBox(show: boolean): void {
        this.showCheckBox = show;
        this.gcListBox.clearSelected();
        this.gcListBox.clearChecked();
        this.Render();
    }

    public updateSelectionMode(selectionMode: GC.InputMan.ListBoxSelectionMode) {
        this.selectionMode = selectionMode;
        this.selectAllDisabled = (selectionMode === this.listBoxSelectionModes[0]);
    }

    @ViewChild("outlet", { read: ViewContainerRef })
    public outletRef: ViewContainerRef;
    @ViewChild("content", { read: TemplateRef })
    public contentRef: TemplateRef<any>;

    private Render() {
        this.outletRef.clear();
        this.outletRef.createEmbeddedView(this.contentRef);
    }

    ngAfterContentInit() {
        this.outletRef.createEmbeddedView(this.contentRef);
    }
}


enableProdMode();