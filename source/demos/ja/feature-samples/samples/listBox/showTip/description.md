リストコントロールでは、表示したアイテムが見切れてしまうときにチップ表示させることが可能です。

## 概要
リストコントロール全体でチップを表示させるには、コンストラクタのshowTipオプションにtrueを設定します。また、列のshowTipオプションにfalseを設定することで、その列のみチップを非表示にできます。

```
var gcListBox = new GC.InputMan.GcListBox(document.getElementById('listbox1'), {
    columns: [
        { name: 'name',label:'製品名',width: 50},
        { name: 'category',label:'製品カテゴリー',width: 100},
        { name: 'description',label:'製品紹介',width: 80, showTip: false}
    ],
    showTip: true,
});
```

任意の列のみにチップを表示させるには、列のshowTipオプションをtrueに設定します。

```
var gcListBox2 = new GC.InputMan.GcListBox(document.getElementById('listbox2'), {
    columns: [
        { name: 'name',label:'製品名',width: 50},
        { name: 'category',label:'製品カテゴリー',width: 100},
        { name: 'description',label:'製品紹介',width: 80, showTip: true}
    ]
});
```