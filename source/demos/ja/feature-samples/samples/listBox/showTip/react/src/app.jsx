import * as React from "react";
import * as ReactDom from "react-dom";
import { GcListBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

class App extends React.Component{

    render(){
        return (
            <div class="flexbox">
                <div> ListBox全体でチップを表示する<br/>
                    <GcListBox
                        items={
                            [
                                { name: 'ActiveReports', category: '帳票・レポート', description: '日本仕様の帳票開発に必要な機能を搭載したコンポーネント' },
                                { name: 'SPREAD', category: '表計算・グリッド', description: 'Excel風のビューと表計算機能を実現するUIコンポーネント' },
                                { name: 'ComponentOne', category: 'コンポーネントセット', description: 'Visual Studioで利用する.NET Framework用コンポーネント' },
                                { name: 'InputMan', category: '入力支援', description: '快適な入力を実現する日本仕様入力コンポーネントセット' },
                                { name: 'MultiRow', category: '多段明細', description: '1レコード複数行&日付表示に最適なグリッドコンポーネント' },
                                { name: 'DioDocs', category: 'ドキュメントAPI', description: 'ドキュメントを生成、更新するAPIライブラリ' },
                            ]
                        }
                        columns={
                            [
                            { name: 'name',label:'製品名',width: 50},
                            { name: 'category',label:'製品カテゴリー',width: 100},
                            { name: 'description',label:'製品紹介',width: 80}
                        ]}
                        showTip={ true } />
                </div>
                <div> 製品紹介列のみチップを表示する<br/>
                    <GcListBox
                        items={
                            [
                                { name: 'ActiveReports', category: '帳票・レポート', description: '日本仕様の帳票開発に必要な機能を搭載したコンポーネント' },
                                { name: 'SPREAD', category: '表計算・グリッド', description: 'Excel風のビューと表計算機能を実現するUIコンポーネント' },
                                { name: 'ComponentOne', category: 'コンポーネントセット', description: 'Visual Studioで利用する.NET Framework用コンポーネント' },
                                { name: 'InputMan', category: '入力支援', description: '快適な入力を実現する日本仕様入力コンポーネントセット' },
                                { name: 'MultiRow', category: '多段明細', description: '1レコード複数行&日付表示に最適なグリッドコンポーネント' },
                                { name: 'DioDocs', category: 'ドキュメントAPI', description: 'ドキュメントを生成、更新するAPIライブラリ' },
                            ]
                        }
                        columns={
                            [
                                { name: 'name',label:'製品名',width: 50},
                                { name: 'category',label:'製品カテゴリー',width: 100},
                                { name: 'description',label:'製品紹介',width: 80, showTip: true}
                            ]
                        }/>
                </div>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));