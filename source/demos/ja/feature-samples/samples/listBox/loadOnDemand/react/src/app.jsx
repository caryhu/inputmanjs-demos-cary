import * as React from "react";
import * as ReactDom from "react-dom";
import { GcListBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import {loadData} from './data';

class App extends React.Component{

    render(){
        return <GcListBox
        width= {100}
        virtualMode= {true}
        load={ (context) =>  loadData(
            { pageNumber: context.pageNumber, pageSize: 20 }, 
            (items) => context.success(items))}></GcListBox>
    }
}

ReactDom.render(<App />, document.getElementById("app"));