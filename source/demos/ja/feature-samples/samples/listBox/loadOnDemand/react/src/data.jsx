export const loadData = (args, cb) => {
    setTimeout(() => {
        var items = generateItems(args);
        cb(items);
    }, 500)
}

const generateItems = (args) => {
    var items = [];
    for (var i = 0; i < args.pageSize; i++) {
        items.push('項目' + (args.pageNumber * args.pageSize + i));
    }
    return items;
}