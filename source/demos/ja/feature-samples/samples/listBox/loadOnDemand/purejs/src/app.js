import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';
import {loadData} from './data';

const gcListBox = new InputMan.GcListBox(document.getElementById('gcListBox'), {
    width: 100,
    virtualMode: true,
    load: (context) =>  loadData(
        { pageNumber: context.pageNumber, pageSize: 20 }, 
        (items) => context.success(items))
});