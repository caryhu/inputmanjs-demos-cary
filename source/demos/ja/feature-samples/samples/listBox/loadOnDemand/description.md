リストコントロールでは、リストアイテムに表示するデータを動的に読み込む（ロードオンデマンド）ことが可能です。

## 概要

loadプロパティを使用することで、リストアイテムがロードされるときに実行する処理を設定することができます。また、pageSizeプロパティを使用することで、ロードオンデマンドでデータを表示する場合に、一度に読み込むアイテムの数を設定することが可能です。

以下のサンプルコードでは、ロードオンデマンドで一度に読み込むアイテムの数を「20」に設定しているので、リストアイテムを最後のアイテムまでスクロールさせると、２０個ずつリストアイテムが動的に追加されます。

```
var gcListBox = new GC.InputMan.GcListBox(document.getElementById('gcListBox'), {
  width: 100,
  virtualMode: true,
  pageSize: 20,
  load: function(context){
    setTimeout(function() {
      var items = [];
      for (var i = 0; i < context.pageSize; i++) {
        items.push('項目' + (context.pageNumber * context.pageSize + i + 1));
      }
      context.success(items);
    }, 500)
  },
});
```

