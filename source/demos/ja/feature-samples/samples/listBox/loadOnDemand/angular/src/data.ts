export const loadData = (args: PageData, cb: Function) => {
    setTimeout(() => {
        var items = generateItems(args);
        cb(items);
    }, 500)
}

const generateItems = (args: PageData) => {
    var items = [];
    for (var i = 0; i < args.pageSize; i++) {
        items.push('項目' + (args.pageNumber * args.pageSize + i));
    }
    return items;
}

export interface PageData {
    pageNumber: number;
    pageSize: number;
}