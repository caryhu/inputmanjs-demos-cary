import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";
import { loadData } from './data';

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {

    public load(context: GC.InputMan.ILoadContext) {
        loadData(
            { pageNumber: context.pageNumber, pageSize: 20 },
            (items: Array<string>) => context.success(items as any));
    }
}


enableProdMode();