module.exports = [
    { id: 105, name: '森上 偉久馬', lastKana: 'モリウエ', firstKana: 'イクマ', lastName: '森上', firstName: '偉久馬', office: '東京本社', department: '第一営業', image: 'img/man1.png', tel: '090-1111-1111' },
    { id: 107, name: '葛城 孝史', lastKana: 'カツラギ', firstKana: 'コウシ', lastName: '葛城', firstName: '孝史', office: '東京本社', department: '第二営業', image: 'img/man2.png', tel: '090-2222-2222' },
    { id: 110, name: '加藤 泰江', lastKana: 'カトウ', firstKana: 'ヤスエ', lastName: '加藤', firstName: '泰江', office: '東京本社', department: '第一営業', image: 'img/woman1.png', tel: '090-3333-3333' },
    { id: 204, name: '川村 匡', lastKana: 'カワムラ', firstKana: 'タダシ', lastName: '川村', firstName: '匡', office: '大阪支社', department: '営業開発', image: 'img/man3.png', tel: '090-4444-4444' },
    { id: 207, name: '松沢 誠一', lastKana: 'マツザワ', firstKana: 'セイイチ', lastName: '松沢', firstName: '誠一', office: '大阪支社', department: '営業開発', image: 'img/man4.png', tel: '090-5555-5555' },
    { id: 210, name: '成宮 真紀', lastKana: 'ナルミヤ', firstKana: 'マキ', lastName: '成宮', firstName: '真紀', office: '大阪支社', department: '営業一', image: 'img/woman2.png', tel: '090-6666-6666' },
];

