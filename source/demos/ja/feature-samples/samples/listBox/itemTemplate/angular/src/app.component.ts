import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";
import employees from './data';

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public items: Array<object> = employees;
    public itemTemplate = `<div class="template-item">
       <div class="id">{!id}</div>
       <div class="image"><img src="{!image}"></div>
       <div class="name"><ruby>{!lastName}<rt>{!lastKana}</rt></ruby>&nbsp;<ruby>{!firstName}<rt>{!firstKana}</rt></ruby></div>
       <div class="note">{!office}<br>{!department}</div>
       </div>`;
    public headerTemplate = '<div class="template-item"><div class="id">ID</div><div class="image">写真</div><div class="name">氏名</div><div class="note">所属</div></div>';
    public footerTemplate = `<div>
        <span>選択されている項目: {!instance.getSelectedItem() == null ? '何も選択されていません' : instance.getSelectedItem().name}</span>
        </div>`
}


enableProdMode();