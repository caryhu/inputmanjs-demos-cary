import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';
import employees from './data';

const gcListBox = new InputMan.GcListBox(document.getElementById('gcListBox'), {
   items: employees,
   itemHeight: 42,
   width: 300,
   itemTemplate: `<div class="template-item">
       <div class="id">{!id}</div>
       <div class="image"><img src="{!image}"></div>
       <div class="name"><ruby>{!lastName}<rt>{!lastKana}</rt></ruby>&nbsp;<ruby>{!firstName}<rt>{!firstKana}</rt></ruby></div>
       <div class="note">{!office}<br>{!department}</div>
       </div>`,
   headerTemplate: '<div class="template-item"><div class="id">ID</div><div class="image">写真</div><div class="name">氏名</div><div class="note">所属</div></div>',
   footerTemplate: `<div>
        <span>選択されている項目: {!instance.getSelectedItem() == null ? '何も選択されていません' : instance.getSelectedItem().name}</span>
        </div>`
});
