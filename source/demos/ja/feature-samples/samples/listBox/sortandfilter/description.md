リストコントロールで任意のソート処理とフィルタ処理を実行する方法について紹介します。

##　ソート
リストコントロールのsortメソットでは、[ソート条件](</inputmanjs/api/interfaces/gc.inputman.sortinfo.html>)（並び替える列と並び替えの方法）を指定して、ソートを実行することができます。
isAscをtrueにすると昇順で、falseにすると降順でソートされます。

```javascript
//id列を降順で並び替える
var sortInfo = { name: 'id', isAsc: false }
listbox.sort(sortInfo);
```

また、sortメソットの引数に[ソート関数](</inputmanjs/api/interfaces/gc.inputman.sortfunc.html>)を指定して、任意のソート処理を実行することができます。

以下はソート実行時にName列とValue列を昇順にソートするサンプルです。
```javascript
var listbox = new GC.InputMan.GcListBox(document.getElementById('list1'), {
  items: [
    { name: 'B', value: -1 },
    { name: 'A', value: 1 },
    { name: 'C', value: 0 },
    { name: 'B', value: 1 },
    { name: 'C', value: -1 },
    { name: 'A', value: -1 },
    { name: 'A', value: 0 },
  ],
  columns: [
    { label: 'Name', name: 'name' },
    { label: 'Value', name: 'value' },
  ],
  height: 240
});

document.getElementById('btn').addEventListener('click', function () {
  listbox.sort(function (first, second) {
    //sortメソッドにsort関数を指定する
    if (first.item.name == second.item.name) {
      //Value列の昇順にソートする
      return first.item.value - second.item.value;
    }
    //Name列の昇順にソートする
    return first.item.name.localeCompare(second.item.name);
});
```
上記のサンプルのように、ソート関数では複数列をソートしたい場合に有効です。

##　フィルタ
リストコントロールのfilterメソットでは、[フィルタ条件](</inputmanjs/api/interfaces/gc.inputman.filterinfo.html>)（フィルタリングする列、[フィルタする値の比較方法](</inputmanjs/api/enums/gc.inputman.filtercomparator.html>)、フィルタで使用する値）を指定して、フィルタを実行することができます。

```javascript
//id列で'3'と同じ値をフィルタする
var filterInfo = [{ name: 'id', comparator: GC.InputMan.FilterComparator.Equal, filterString: '3' }];
listbox.filter(filterInfo);
```

また、filterメソットの引数には[フィルタ関数](<//inputmanjs/api/interfaces/gc.inputman.filterfunc.html>)を指定することで、任意のフィルタ処理を実行することができます。

以下のサンプルは任意の入力要素に入力された値より大きい値のみを表示するサンプルです。

```javascript
var listbox = new GC.InputMan.GcListBox(document.getElementById('list1'), {
  columns: [{ name: 'id', width: 50 }, { name: 'name', width: 150 }],
  items: makeItems(100),
});

document.getElementById('btn').addEventListener('click', function () {
  listbox.filter(function (v) {
    //入力された値より対象のidが大きいか
    return v.id > parseInt(inputElement.value);
  });
});
```
