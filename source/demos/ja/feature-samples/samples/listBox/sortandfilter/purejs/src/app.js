import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';
import orders from './data';
import './styles.css';

const columns = [
    { name: 'id', label: '商品コード', width: 80 },
    { name: 'product', label: '商品名', width: 200 },
    { name: 'date', label: '受注日', width: 120 },
    { name: 'price', label: '単価', width: 80 },
    { name: 'amount', label: '数量', width: 80 },
];

const gcListBox = new InputMan.GcListBox(document.getElementById('gcListBox'), {
    items: orders,
    columns: columns
});

const gcListBox2 = new InputMan.GcListBox(document.getElementById('gcListBox2'), {
    items: orders,
    columns: columns
});

const gcNumber = new InputMan.GcNumber(document.getElementById('filterValue'), {
    minValue: 0,
});

//ソート条件によるソート(昇順)
document.getElementById('ascId').addEventListener('change', (e) => {
    let sortIdInfo = { name: 'id', isAsc: true };
    e.target.checked ? gcListBox.sort(sortIdInfo) : gcListBox.sort(null);
});

//フィルタ条件によるフィルタ
document.getElementById('filterinfo').addEventListener('change', (e) => {
    if (e.target.checked === true) {
        gcListBox.filter(filterIdInfo);
    } else {
        gcListBox.filter(null);
    }
});

//ソート関数によるソート
document.getElementById('customSort').addEventListener('change', (e) => {
    if (e.target.checked === true) {
        gcListBox2.sort(ascFunc);
    } else {
        gcListBox2.sort(null);
    }
});

let filterIdInfo = [{ name: 'product', comparator: GC.InputMan.FilterComparator.Contains, filterString: 'なま' }];

//フィルタ関数によるフィルタ
document.getElementById('fliterBtn').addEventListener('click', () => {
    gcListBox2.filter((v) => {
        //売上がフィルタ値より大きければ表示する
        return v.price * v.amount > gcNumber.value;
    });
});

//昇順関数
const ascFunc = (first, second) => {
    if (first.item.id == second.item.id) {
        return first.item.price - second.item.price;
    }
    return first.item.id - second.item.id;
}