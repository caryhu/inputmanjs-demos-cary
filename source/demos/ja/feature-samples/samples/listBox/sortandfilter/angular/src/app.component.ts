import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { Component, enableProdMode, ViewChild } from '@angular/core';
import data from './data';
import {GcListBoxComponent} from "@grapecity/inputman.angular";
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    @ViewChild('list1') list1: GcListBoxComponent;
    @ViewChild('list2') list2: GcListBoxComponent;

    items = data;
    columns = [
        { name: 'id', label: '商品コード', width: 80 },
        { name: 'product', label: '商品名', width: 200 },
        { name: 'date', label: '受注日', width: 120 },
        { name: 'price', label: '単価', width: 80 },
        { name: 'amount', label: '数量', width: 80 },
    ];
    filterValue = 0;

    //ソート条件によるソート(昇順)
    updateSort(e: any){
        let sortIdInfo = { name: 'id', isAsc: true };
        let gcListBox = this.list1.getNestedIMControl();
        e.target.checked ? gcListBox.sort(sortIdInfo) : gcListBox.sort(null);
    }

    //フィルタ条件によるフィルタ
    updateFilter(e: any){
        let filterIdInfo = [{ name: 'product', comparator: GC.InputMan.FilterComparator.Contains, filterString: 'なま' }];
        let gcListBox = this.list1.getNestedIMControl();
        if (e.target.checked === true) {
            gcListBox.filter(filterIdInfo);
        } else {
            gcListBox.filter(null);
        }
    }

    ////ソート関数によるソート
    doCustomSort(e: any){
        //昇順関数
        const ascFunc = (first: any, second: any) => {
            if (first.item.id == second.item.id) {
                return first.item.price - second.item.price;
            }
            return first.item.id - second.item.id;
        }

        let gcListBox2 = this.list2.getNestedIMControl();
        if (e.target.checked === true) {
            gcListBox2.sort(ascFunc);
        } else {
            gcListBox2.sort(null);
        }
    }

    //フィルタ関数によるフィルタ
    doFilter(){
        this.list2.getNestedIMControl().filter((v: any) => {
            //売上がフィルタ値より大きければ表示する
            return v.price * v.amount > this.filterValue;
        });
    }
}


enableProdMode();