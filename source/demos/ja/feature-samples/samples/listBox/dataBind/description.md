リストコントロールでは、リストアイテムとして表示する内容を配列などと連結させることができます。

## 文字配列に連結

setItemsメソッドを使用することで、文字配列をコンボコントロールに連結し、リストアイテムとして表示させることができます。

次のサンプルコードは、文字配列をリストコントロールに連結する例です。

```
var gcListBoxStrings = new GC.InputMan.GcListBox(document.getElementById('gcListBoxStrings'), {});
gcListBoxStrings.setItems([
  '項目１',
  '項目２',
  '項目３',
]);
```

## オブジェクト配列に連結

コンストラクタの第二引数にItems要素としてオブジェクト配列を設定することで、その配列の内容をリストアイテムとして表示させることができます。

次のサンプルコードは、オブジェクト配列をリストコントロールに連結する例です。

```
var gcListBoxObjects = new GC.InputMan.GcListBox(document.getElementById('gcListBoxObjects'), {
  items: [
    { name: 'ItemA', title: '項目Ａ', description: '説明Ａ' },
    { name: 'ItemB', title: '項目Ｂ', description: '説明Ｂ' },
    { name: 'ItemC', title: '項目Ｃ', description: '説明Ｃ' },
  ],
  displayMemberPath: 'title',
  valueMemberPath: 'description'
});
```

## option要素から生成

リストアイテムとして表示する内容は、option要素から生成することができます。

次のサンプルコードは、option要素を動的に設定する例です。

```
// リスト表示用配列
var arr = [
  {val:'01', title:'項目ａ'},
  {val:'02', title:'項目ｂ'},
  {val:'03', title:'項目ｃ'}
];

// 配列から値を取り出し、option要素としてセット
for(var i=0;i<arr.length;i++){
  let opt = document.createElement('option');
  opt.value = arr[i].val;
  opt.text = arr[i].title;
  document.getElementById('gcListBoxDom').appendChild(opt);
}

var gcListBoxDom = new GC.InputMan.GcListBox(document.getElementById('gcListBoxDom'), {});
```

