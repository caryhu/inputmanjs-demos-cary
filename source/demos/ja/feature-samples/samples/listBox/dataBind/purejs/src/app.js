import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';
import { strProducts, objProducts } from './data';
// 文字配列に連結
var gcListBoxStrings = new InputMan.GcListBox(document.getElementById('gcListBoxStrings'), {
    items: strProducts
});
gcListBoxStrings.addEventListener(
    InputMan.GcListBoxEvent.SelectedChanged, 
    (sender) => document.getElementById('selectedItem').innerText = sender.getSelectedValue()
);

// オブジェクト配列に連結
var gcListBoxObjects = new InputMan.GcListBox(document.getElementById('gcListBoxObjects'), {
    items: objProducts,
    displayMemberPath: 'name',
    valueMemberPath: 'name'
});
gcListBoxObjects.addEventListener(
    InputMan.GcListBoxEvent.SelectedChanged, 
    (sender) => document.getElementById('selectedItem').innerText = sender.getSelectedValue()
);

// option要素から生成
var gcListBoxDom = new InputMan.GcListBox(document.getElementById('gcListBoxDom'));
gcListBoxDom.addEventListener(
    InputMan.GcListBoxEvent.SelectedChanged, 
    (sender) => document.getElementById('selectedItem').innerText = sender.getSelectedValue()
);
