import * as React from "react";
import * as ReactDom from "react-dom";
import { GcListBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { strProducts, objProducts } from './data';

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            innerText: ''
        };
    }

    componentDidMount(){
        // option要素から生成
        var gcListBoxDom = new InputMan.GcListBox(document.getElementById('gcListBoxDom'));
        gcListBoxDom.addEventListener(
            InputMan.GcListBoxEvent.SelectedChanged, 
            (sender) => this.setState({innerText: sender.getSelectedValue()})
        );
    }

    render(){
        return (
            <div>
                <div class="flexbox">
                    <div>
                        文字配列に連結
                        <GcListBox items= {strProducts}
                         selectedChanged={(eArgs) => {
                            this.setState({innerText: eArgs.getSelectedValue()});
                        }}></GcListBox>
                    </div>
                    <div>
                        オブジェクト配列に連結
                        <GcListBox items= {objProducts}
                        displayMemberPath= {'name'}
                        valueMemberPath= {'name'}
                        selectedChanged={(eArgs) => {
                            this.setState({innerText: eArgs.getSelectedValue()});
                        }}></GcListBox>
                    </div>
                    <div>
                        option要素から生成<br/>
                        <select id="gcListBoxDom">
                            <option>果汁100%オレンジ</option>
                            <option>果汁100%グレープ</option>
                            <option>果汁100%レモン</option>
                            <option>果汁100%ピーチ</option>
                            <option>コーヒーマイルド</option>
                            <option>コーヒービター</option>
                        </select>
                    </div>
                </div>
                    選択された項目：<span id="selectedItem">{this.state.innerText}</span>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));