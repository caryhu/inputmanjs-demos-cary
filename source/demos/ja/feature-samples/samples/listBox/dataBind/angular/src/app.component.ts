import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, ViewChild, ElementRef, OnInit } from '@angular/core';
import GC from "@grapecity/inputman";
import { strProducts, objProducts } from './data';

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent implements OnInit {

    public strProducts: Array<string> = strProducts;
    public objProducts: Array<object> = objProducts;
    public selectedItemStr: string;
    @ViewChild('gcListBoxDom')
    public gcListBoxDom: ElementRef;

    public selectedChangedHandler(sender: GC.InputMan.GcComboBox): void {
        this.selectedItemStr = sender.getSelectedValue();
    }

    public ngOnInit(): void {
        var gcListBoxObjects = new GC.InputMan.GcListBox(this.gcListBoxDom.nativeElement, {
            items: objProducts,
            displayMemberPath: 'name',
            valueMemberPath: 'name'
        });
        gcListBoxObjects.addEventListener(
            GC.InputMan.GcListBoxEvent.SelectedChanged,
            this.updateSelectedItem.bind(this)
        );
    }

    public updateSelectedItem(sender: GC.InputMan.GcComboBox): void {
        this.selectedItemStr = sender.getSelectedValue();
    }
}


enableProdMode();