import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, ViewChild, ViewContainerRef, TemplateRef, AfterContentInit } from '@angular/core';
import GC from "@grapecity/inputman";
import orders from './data';

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent implements AfterContentInit {

    public orders: Array<object> = orders;
    public columnClickSort: boolean;
    public allowColumnResize: boolean = true;
    public columnWidthArr: Array<number | string> = [80, 200, 120, 80, 80];
    public gcListBox: GC.InputMan.GcListBox;

    public updateColumnWidth(isAutoColumn: boolean): void {
        if (isAutoColumn) {
            this.columnWidthArr = ['auto', 'auto', 'auto', 'auto', 'auto'];
            this.gcListBox.updateColumnAutoFit([
                { name: 'id', autofit: true },
                { name: 'product', autofit: true },
                { name: 'date', autofit: true },
                { name: 'price', autofit: true },
                { name: 'amount', autofit: true }
            ]);
        } else {
            this.columnWidthArr = [80, 200, 120, 80, 80];
        }
        this.Render();
    }

    public updateColumnClickSort(columnClickSort: boolean) {
        this.columnClickSort = columnClickSort;
        this.Render();
    }

    @ViewChild("outlet", { read: ViewContainerRef })
    public outletRef: ViewContainerRef;
    @ViewChild("content", { read: TemplateRef })
    public contentRef: TemplateRef<any>;

    private Render() {
        this.outletRef.clear();
        this.outletRef.createEmbeddedView(this.contentRef);
    }

    ngAfterContentInit() {
        this.outletRef.createEmbeddedView(this.contentRef);
    }

}


enableProdMode();