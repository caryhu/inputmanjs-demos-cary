リストコントロールでは複数列の一覧（マルチカラムリスト）をリストアイテムとして表示することができます。

## 概要

リストコントロールのcolumnsプロパティに、列ごとの定義情報を設定したcolumnオブジェクトの配列を設定することで、リストアイテムに複数列の一覧を表示することが可能です。

たとえば、以下のようなコードになります。

```js
// リストアイテムに表示する値
var data = [
  { id: 15, product: 'ピュアデミグラスソース', date: '2017/01/10', price: 200, amount: 30 },
  { id: 17, product: 'だしこんぶ', date: '2017/01/08', price: 290, amount: 50 },
  { id: 18, product: 'ピリカラタバスコ', date: '2017/01/12', price: 200, amount: 20 },
  { id: 84, product: 'なまわさび', date: '2017/01/21', price: 200, amount: 40 },
  { id: 85, product: 'なまからし', date: '2017/01/13', price: 200, amount: 40 },
  { id: 86, product: 'なましょうが', date: '2017/01/20', price: 200, amount: 40 },
];

// リストアイテムとして表示する列の情報（columnオブジェクトの配列）
var cols = [
  { name: 'id', label: '商品コード', width: 80 },
  { name: 'product', label: '商品名', width: 200 },
  { name: 'date', label: '受注日', width: 120 },
  { name: 'price', label: '単価', width: 80 },
  { name: 'amount', label: '数量', width: 80 },
];

var gcListBox = new GC.InputMan.GcComboBox(document.getElementById('gcListBox'), {
  items: data,
  columns: cols
});
```

なお、リストアイテムに複数列の一覧を表示する場合、ソートや列幅のリサイズなどの機能を使用できます。詳しくは、以下の説明をご覧ください。

## ソート

リストアイテムに表示されている内容は、ヘッダをクリックすることでソートすることが可能です。ソートの可否は、columnsプロパティに設定したcolumnオブジェクトのclickSortプロパティによって決定されます。したがって、列ごとにソートの可否を設定可能です。（clickSortプロパティの既定値は、falseです。）

たとえば、「受注日」の列のみソート可能にする場合、以下のような配列をリストコントロールのcolumnsプロパティに設定します。

```js
var cols = [
  { name: 'id', label: '商品コード', width: 80 },
  { name: 'product', label: '商品名', width: 200 },
  { name: 'date', label: '受注日', width: 120, clickSort: true },
  { name: 'price', label: '単価', width: 80 },
  { name: 'amount', label: '数量', width: 80 },
];
```

## リサイズ

リストアイテムに表示されている列の幅は、columnsプロパティに設定したcolumnオブジェクトのwidthプロパティによって決定されます。また、ヘッダ上で列の境界線をドラッグすることで列幅を変更することも可能です。

なお、リストコントロールのsetAllowColumnResizeメソッドを使用することで、列幅の変更可否を設定することも可能です。（デフォルトの状態では、変更可能です。）列幅の変更を不可にする場合、以下のようなコードになります。

```js
gcListBox.setAllowColumnResize(false);
```

## 列幅の自動調整

列の境界線をダブルクリックすると、リストの列幅が自動的に調整されます。

コードでリストの列幅を自動的に調整するには、updateColumnAutoFitメソッドを実行して、nameプロパティ（列名）とautofitプロパティ（自動調整するかどうか）を持つオブジェクト配列を引数に指定します。

```js
gcListBox.updateColumnAutoFit([
    { name: 'id', autofit: true },
    { name: 'product', autofit: true },
    { name: 'date', autofit: true },
    { name: 'price', autofit: true },
    { name: 'amount', autofit: true }
]);
```

また、columnオブジェクトのwidthプロパティに'auto'という文字列を設定することもできます。

```js
var cols = [
  { name: 'id', label: '商品コード', width: 'auto' },
  { name: 'product', label: '商品名', width: 'auto' },
  { name: 'date', label: '受注日', width: 'auto', clickSort: true },
  { name: 'price', label: '単価', width: 'auto' },
  { name: 'amount', label: '数量', width: 'auto' },
];
```
