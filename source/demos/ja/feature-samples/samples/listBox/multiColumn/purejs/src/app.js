import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';
import orders from './data';

const columns = [
    { name: 'id', label: '商品コード', width: 80 },
    { name: 'product', label: '商品名', width: 200 },
    { name: 'date', label: '受注日', width: 120 },
    { name: 'price', label: '単価', width: 80 },
    { name: 'amount', label: '数量', width: 80 },
];

const gcListBox = new InputMan.GcListBox(document.getElementById('gcListBox'), {
    items: orders,
    columns: columns
});

document.getElementById('columnClickSort').addEventListener('change', (e) => {
    let allowSort = e.target.checked;
    for (let i = 0; i < columns.length; i++) {
        columns[i].clickSort = allowSort;
    }
    gcListBox.setColumns(columns);
});

document.getElementById('allowColumnResize').addEventListener('change', (e) => {
    let allowColumnResize = e.target.checked;
    gcListBox.setAllowColumnResize(allowColumnResize);
});

document.getElementById('autoWidth').addEventListener('change', (e) => {
    if (e.target.checked) {
        gcListBox.updateColumnAutoFit([
            { name: 'id', autofit: true },
            { name: 'product', autofit: true },
            { name: 'date', autofit: true },
            { name: 'price', autofit: true },
            { name: 'amount', autofit: true }
        ]);
    } else {
        gcListBox.setColumns(columns);
    }
});