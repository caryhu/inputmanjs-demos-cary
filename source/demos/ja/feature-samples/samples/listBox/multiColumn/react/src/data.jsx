module.exports = [
    { id: 15, product: 'ピュアデミグラスソース', date: '2017/01/10', price: 200, amount: 30 },
    { id: 17, product: 'だしこんぶ', date: '2017/01/08', price: 290, amount: 50 },
    { id: 18, product: 'ピリカラタバスコ', date: '2017/01/12', price: 200, amount: 20 },
    { id: 84, product: 'なまわさび', date: '2017/01/21', price: 200, amount: 40 },
    { id: 85, product: 'なまからし', date: '2017/01/13', price: 200, amount: 40 },
    { id: 86, product: 'なましょうが', date: '2017/01/20', price: 200, amount: 40 },
];