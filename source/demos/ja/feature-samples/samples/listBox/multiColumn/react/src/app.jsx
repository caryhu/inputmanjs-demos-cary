import * as React from "react";
import * as ReactDom from "react-dom";
import { GcListBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import orders from './data';

class App extends React.Component{
    columns =  [
        { name: 'id', label: '商品コード', width: 80 },
        { name: 'product', label: '商品名', width: 200 },
        { name: 'date', label: '受注日', width: 120 },
        { name: 'price', label: '単価', width: 80 },
        { name: 'amount', label: '数量', width: 80 },
    ];
    gcListBox = null;
    constructor(props, context) {
        super(props, context);
        this.state = {
            clickSort : false,
            autoWidth : false,
            allowColumnResize: true
        };
    }

    render(){
        return (
            <div>
                <GcListBox     
                allowColumnResize= {this.state.allowColumnResize}
                items= {orders}
                columns= {this.columns}
                width={'auto'}
                onInitialized={(sender)=> {
                    this.gcListBox = sender;
                }}></GcListBox>
                <table class="sample">
                    <tr>
                        <th>ソート</th>
                        <td>
                            <label><input type="checkbox" id="columnClickSort" checked={this.state.clickSort} onChange={(e)=>{
                                    this.setState({clickSort: e.target.checked});
                                    let allowSort = e.target.checked;
                                    for (let i = 0; i < this.columns.length; i++) {
                                        this.columns[i].clickSort = allowSort;
                                    }
                                    this.gcListBox.setColumns(this.columns);
                                }}/>ヘッダをクリックしてソート</label>
                        </td>
                    </tr>
                    <tr>
                        <th>リサイズ</th>
                        <td>
                            <label><input type="checkbox" id="allowColumnResize" checked={this.state.allowColumnResize} onChange={(e)=>{
                                    this.setState({allowColumnResize: e.target.checked, key: (new Date()).valueOf()});
                                }}/>列のリサイズを許可</label>
                        </td>
                    </tr>
                    <tr>
                        <th>列幅の自動調整</th>
                        <td>
                            <label><input type="checkbox" id="autoWidth" checked={this.state.autoWidth} onChange={(e)=>{
                                    this.setState({autoWidth: e.target.checked});
                                    if (e.target.checked) {
                                        this.gcListBox.updateColumnAutoFit([
                                            { name: 'id', autofit: true },
                                            { name: 'product', autofit: true },
                                            { name: 'date', autofit: true },
                                            { name: 'price', autofit: true },
                                            { name: 'amount', autofit: true }
                                        ]);
                                    } else {
                                        this.gcListBox.setColumns(this.columns);
                                    }
                                }}/>列幅を自動調整する</label>
                        </td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));