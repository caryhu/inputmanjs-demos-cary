import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {

    public itemsLengthOption: Array<number> = [1000, 10000, 100000];
    public items: Array<string> = this.generateItems(1000);
    public virtualMode: boolean = true;
    
    private _itemsLength: number = 1000;
    public get itemsLength() {
        return this._itemsLength;
    }
    public set itemsLength(value: number) {
        this._itemsLength = value;
        this.items = this.generateItems(value);
    }

    public generateItems(length: number) {
        var items = [];
        for (var i = 0; i < length; i++) {
            items.push('項目' + i);
        }
        return items;
    }
}


enableProdMode();