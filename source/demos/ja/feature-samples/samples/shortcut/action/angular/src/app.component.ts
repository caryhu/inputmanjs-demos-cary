import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { Component, enableProdMode, ViewChild } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    InputMan = GC.InputMan;
    dropdown = {dropDownType: GC.InputMan.DateDropDownType.Calendar};
    items = ['果汁100%オレンジ', 'コーヒーマイルド', 'ピリピリビール', 'ホワイトソルト', 'ブラックペッパー', 'ピュアシュガー'];
    showValue = function(args: any){
        alert(args.target.value || args.target.selectedValue || args.target.selectedDate);
    }
}

enableProdMode();