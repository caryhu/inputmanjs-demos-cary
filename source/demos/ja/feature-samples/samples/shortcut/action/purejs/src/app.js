import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';

const gcTextBox = new InputMan.GcTextBox(document.getElementById('gcTextBox'), {
    text: 'テキスト'
});
const gcMask = new InputMan.GcMask(document.getElementById('gcMask'), {
    formatPattern: '(北海道|青森県|岩手県|宮城県|秋田県|山形県|福島県|茨城県|栃木県|群馬県|埼玉県|千葉県|東京都|神奈川県|山梨県|長野県|新潟県|富山県|石川県|福井県|岐阜県|静岡県|愛知県|三重県|滋賀県|京都府|大阪府|兵庫県|奈良県|和歌山県|鳥取県|島根県|岡山県|広島県|山口県|徳島県|香川県|愛媛県|高知県|福岡県|佐賀県|長崎県|熊本県|大分県|宮崎県|鹿児島県|沖縄県)',
    showSpinButton: true
});
const gcNumber = new InputMan.GcNumber(document.getElementById('gcNumber'), {
    value: 1,
    showSpinButton: true,
    showNumericPad: true
});
const gcDateTime = new InputMan.GcDateTime(document.getElementById('gcDateTime'), {
    showSpinButton: true,
    showDropDownButton: true,
    dropDownConfig: {
        dropDownType: InputMan.DateDropDownType.Calendar
    }
});
const gcCalendar = new InputMan.GcCalendar(document.getElementById('gcCalendar'));
const gcComboBox = new InputMan.GcComboBox(document.getElementById('gcComboBox'), {
    items: ['果汁100%オレンジ', 'コーヒーマイルド', 'ピリピリビール', 'ホワイトソルト', 'ブラックペッパー', 'ピュアシュガー'],
    showSpinButton: true
});
const input = document.getElementById('text');

var gcShortcut = new InputMan.GcShortcut();

// GcTextBoxにショートカットキーを追加します。
gcShortcut.add({ key: InputMan.GcShortcutKey.P, target: gcTextBox, action: InputMan.GcShortcutAction.PreviousControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.N, target: gcTextBox, action: InputMan.GcShortcutAction.NextControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.C, target: gcTextBox, action: InputMan.GcShortcutAction.Clear });
gcShortcut.add({ key: InputMan.GcShortcutKey.V, target: gcTextBox, action: showValue });

// GcMaskにショートカットキーを追加します。
gcShortcut.add({ key: InputMan.GcShortcutKey.P, target: gcMask, action: InputMan.GcShortcutAction.PreviousFieldControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.N, target: gcMask, action: InputMan.GcShortcutAction.NextFieldControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.C, target: gcMask, action: InputMan.GcShortcutAction.Clear });
gcShortcut.add({ key: InputMan.GcShortcutKey.U, target: gcMask, action: InputMan.GcShortcutAction.SpinUp });
gcShortcut.add({ key: InputMan.GcShortcutKey.D, target: gcMask, action: InputMan.GcShortcutAction.SpinDown });
gcShortcut.add({ key: InputMan.GcShortcutKey.V, target: gcMask, action: showValue });

// GcNumberにショートカットキーを追加します。
gcShortcut.add({ key: InputMan.GcShortcutKey.P, target: gcNumber, action: InputMan.GcShortcutAction.PreviousControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.N, target: gcNumber, action: InputMan.GcShortcutAction.NextControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.C, target: gcNumber, action: InputMan.GcShortcutAction.Clear });
gcShortcut.add({ key: InputMan.GcShortcutKey.R, target: gcNumber, action: InputMan.GcShortcutAction.DropDown });
gcShortcut.add({ key: InputMan.GcShortcutKey.U, target: gcNumber, action: InputMan.GcShortcutAction.SpinUp });
gcShortcut.add({ key: InputMan.GcShortcutKey.D, target: gcNumber, action: InputMan.GcShortcutAction.SpinDown });
gcShortcut.add({ key: InputMan.GcShortcutKey.Z, target: gcNumber, action: InputMan.GcShortcutAction.ThreeZero });
gcShortcut.add({ key: InputMan.GcShortcutKey.V, target: gcNumber, action: showValue });

// GcDateTimeにショートカットキーを追加します。
gcShortcut.add({ key: InputMan.GcShortcutKey.P, target: gcDateTime, action: InputMan.GcShortcutAction.PreviousFieldControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.N, target: gcDateTime, action: InputMan.GcShortcutAction.NextFieldControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.C, target: gcDateTime, action: InputMan.GcShortcutAction.Clear });
gcShortcut.add({ key: InputMan.GcShortcutKey.R, target: gcDateTime, action: InputMan.GcShortcutAction.DropDown });
gcShortcut.add({ key: InputMan.GcShortcutKey.U, target: gcDateTime, action: InputMan.GcShortcutAction.SpinUp });
gcShortcut.add({ key: InputMan.GcShortcutKey.D, target: gcDateTime, action: InputMan.GcShortcutAction.SpinDown });
gcShortcut.add({ key: InputMan.GcShortcutKey.W, target: gcDateTime, action: InputMan.GcShortcutAction.Now });
gcShortcut.add({ key: InputMan.GcShortcutKey.V, target: gcDateTime, action: showValue });

// GcCalendarにショートカットキーを追加します。
gcShortcut.add({ key: InputMan.GcShortcutKey.P, target: gcCalendar, action: InputMan.GcShortcutAction.PreviousControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.N, target: gcCalendar, action: InputMan.GcShortcutAction.NextControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.C, target: gcCalendar, action: InputMan.GcShortcutAction.Clear });
gcShortcut.add({ key: InputMan.GcShortcutKey.W, target: gcCalendar, action: InputMan.GcShortcutAction.Now });
gcShortcut.add({ key: InputMan.GcShortcutKey.V, target: gcCalendar, action: showValue });

// GcComboBoxにショートカットキーを追加します。
gcShortcut.add({ key: InputMan.GcShortcutKey.P, target: gcComboBox, action: InputMan.GcShortcutAction.PreviousControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.N, target: gcComboBox, action: InputMan.GcShortcutAction.NextControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.C, target: gcComboBox, action: InputMan.GcShortcutAction.Clear });
gcShortcut.add({ key: InputMan.GcShortcutKey.R, target: gcComboBox, action: InputMan.GcShortcutAction.DropDown });
gcShortcut.add({ key: InputMan.GcShortcutKey.U, target: gcComboBox, action: InputMan.GcShortcutAction.SpinUp });
gcShortcut.add({ key: InputMan.GcShortcutKey.D, target: gcComboBox, action: InputMan.GcShortcutAction.SpinDown });
gcShortcut.add({ key: InputMan.GcShortcutKey.V, target: gcComboBox, action: showValue });

// input要素にショートカットキーを追加します。
gcShortcut.add({ key: InputMan.GcShortcutKey.P, target: input, action: InputMan.GcShortcutAction.PreviousControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.N, target: input, action: InputMan.GcShortcutAction.NextControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.V, target: input, action: showValue });

// カスタムアクション
function showValue(args) {
    alert(args.target.value || args.target.selectedValue || args.target.selectedDate);
}