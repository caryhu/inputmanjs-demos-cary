import * as React from "react";
import * as ReactDom from "react-dom";
import { GcTextBox, GcMask, GcCalendar, GcComboBox, GcNumber, GcDateTime, GcShortcut, GcShortcutInfo } from "@grapecity/inputman.react";
import {InputMan} from '@grapecity/inputman';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{
    showValue(args) {
        alert(args.target.value || args.target.selectedValue || args.target.selectedDate);
    }
    render(){
        let items = ['果汁100%オレンジ', 'コーヒーマイルド', 'ピリピリビール', 'ホワイトソルト', 'ブラックペッパー', 'ピュアシュガー'];
        return <React.Fragment>
            <div class="flexbox">
                <div>
                    テキストコントロール<br/>
                    <GcTextBox id='textbox' text='テキスト'/>
                </div>
                <div>
                    マスクコントロール<br/>
                    <GcMask id='mask' formatPattern='(北海道|青森県|岩手県|宮城県|秋田県|山形県|福島県|茨城県|栃木県|群馬県|埼玉県|千葉県|東京都|神奈川県|山梨県|長野県|新潟県|富山県|石川県|福井県|岐阜県|静岡県|愛知県|三重県|滋賀県|京都府|大阪府|兵庫県|奈良県|和歌山県|鳥取県|島根県|岡山県|広島県|山口県|徳島県|香川県|愛媛県|高知県|福岡県|佐賀県|長崎県|熊本県|大分県|宮崎県|鹿児島県|沖縄県)' showSpinButton={true}/>
                </div>
                <div>
                    数値コントロール<br/>
                    <GcNumber id='number' showSpinButton={true} showNumericPad={true}/>
                </div>
                <div>
                    日付時刻コントロール<br/>
                    <GcDateTime id='datetime' showSpinButton={true} showDropDownButton={true} dropDownConfig={{dropDownType: InputMan.DateDropDownType.Calendar}}/>
                </div>
                <div>
                    カレンダーコントロール<br/>
                    <GcCalendar id='calendar' />
                </div>
                <div>
                    コンボコントロール<br/>
                    <GcComboBox id='combo' items={items} showSpinButton={true}/>
                </div>
                <div>
                    標準の入力要素<br/>
                    <input id="text" value="テキスト"/>
                </div>
            </div>
            <GcShortcut>
                {/* GcTextBoxにショートカットキーを追加します */}
                <GcShortcutInfo target='#textbox' shortcutKey={InputMan.GcShortcutKey.P} action={InputMan.GcShortcutAction.PreviousControl}></GcShortcutInfo>
                <GcShortcutInfo target='#textbox' shortcutKey={InputMan.GcShortcutKey.N} action={InputMan.GcShortcutAction.NextControl}></GcShortcutInfo>
                <GcShortcutInfo target='#textbox' shortcutKey={InputMan.GcShortcutKey.C} action={InputMan.GcShortcutAction.Clear}></GcShortcutInfo>
                <GcShortcutInfo target='#textbox' shortcutKey={InputMan.GcShortcutKey.V} action={this.showValue}></GcShortcutInfo>

                {/* GcMaskにショートカットキーを追加します。 */}
                <GcShortcutInfo target='#mask' shortcutKey={InputMan.GcShortcutKey.P} action={InputMan.GcShortcutAction.PreviousControl}></GcShortcutInfo>
                <GcShortcutInfo target='#mask' shortcutKey={InputMan.GcShortcutKey.N} action={InputMan.GcShortcutAction.NextControl}></GcShortcutInfo>
                <GcShortcutInfo target='#mask' shortcutKey={InputMan.GcShortcutKey.C} action={InputMan.GcShortcutAction.Clear}></GcShortcutInfo>
                <GcShortcutInfo target='#mask' shortcutKey={InputMan.GcShortcutKey.U} action={InputMan.GcShortcutAction.SpinUp}></GcShortcutInfo>
                <GcShortcutInfo target='#mask' shortcutKey={InputMan.GcShortcutKey.D} action={InputMan.GcShortcutAction.SpinDown}></GcShortcutInfo>
                <GcShortcutInfo target='#mask' shortcutKey={InputMan.GcShortcutKey.V} action={this.showValue}></GcShortcutInfo>

                {/* GcNumberにショートカットキーを追加します。 */}
                <GcShortcutInfo target='#number' shortcutKey={InputMan.GcShortcutKey.P} action={InputMan.GcShortcutAction.PreviousControl}></GcShortcutInfo>
                <GcShortcutInfo target='#number' shortcutKey={InputMan.GcShortcutKey.N} action={InputMan.GcShortcutAction.NextControl}></GcShortcutInfo>
                <GcShortcutInfo target='#number' shortcutKey={InputMan.GcShortcutKey.C} action={InputMan.GcShortcutAction.Clear}></GcShortcutInfo>
                <GcShortcutInfo target='#number' shortcutKey={InputMan.GcShortcutKey.R} action={InputMan.GcShortcutAction.DropDown}></GcShortcutInfo>
                <GcShortcutInfo target='#number' shortcutKey={InputMan.GcShortcutKey.U} action={InputMan.GcShortcutAction.SpinUp}></GcShortcutInfo>
                <GcShortcutInfo target='#number' shortcutKey={InputMan.GcShortcutKey.D} action={InputMan.GcShortcutAction.SpinDown}></GcShortcutInfo>
                <GcShortcutInfo target='#number' shortcutKey={InputMan.GcShortcutKey.Z} action={InputMan.GcShortcutAction.ThreeZero}></GcShortcutInfo>
                <GcShortcutInfo target='#number' shortcutKey={InputMan.GcShortcutKey.V} action={this.showValue}></GcShortcutInfo>

                {/* GcDateTimeにショートカットキーを追加します。 */}
                <GcShortcutInfo target='#datetime' shortcutKey={InputMan.GcShortcutKey.P} action={InputMan.GcShortcutAction.PreviousControl}></GcShortcutInfo>
                <GcShortcutInfo target='#datetime' shortcutKey={InputMan.GcShortcutKey.N} action={InputMan.GcShortcutAction.NextControl}></GcShortcutInfo>
                <GcShortcutInfo target='#datetime' shortcutKey={InputMan.GcShortcutKey.C} action={InputMan.GcShortcutAction.Clear}></GcShortcutInfo>
                <GcShortcutInfo target='#datetime' shortcutKey={InputMan.GcShortcutKey.R} action={InputMan.GcShortcutAction.DropDown}></GcShortcutInfo>
                <GcShortcutInfo target='#datetime' shortcutKey={InputMan.GcShortcutKey.U} action={InputMan.GcShortcutAction.SpinUp}></GcShortcutInfo>
                <GcShortcutInfo target='#datetime' shortcutKey={InputMan.GcShortcutKey.D} action={InputMan.GcShortcutAction.SpinDown}></GcShortcutInfo>
                <GcShortcutInfo target='#datetime' shortcutKey={InputMan.GcShortcutKey.W} action={InputMan.GcShortcutAction.Now}></GcShortcutInfo>
                <GcShortcutInfo target='#datetime' shortcutKey={InputMan.GcShortcutKey.V} action={this.showValue}></GcShortcutInfo>

                {/* GcCalendarにショートカットキーを追加します。 */}
                <GcShortcutInfo target='#calendar' shortcutKey={InputMan.GcShortcutKey.P} action={InputMan.GcShortcutAction.PreviousControl}></GcShortcutInfo>
                <GcShortcutInfo target='#calendar' shortcutKey={InputMan.GcShortcutKey.N} action={InputMan.GcShortcutAction.NextControl}></GcShortcutInfo>
                <GcShortcutInfo target='#calendar' shortcutKey={InputMan.GcShortcutKey.C} action={InputMan.GcShortcutAction.Clear}></GcShortcutInfo>
                <GcShortcutInfo target='#calendar' shortcutKey={InputMan.GcShortcutKey.W} action={InputMan.GcShortcutAction.Now}></GcShortcutInfo>
                <GcShortcutInfo target='#calendar' shortcutKey={InputMan.GcShortcutKey.V} action={this.showValue}></GcShortcutInfo>

                {/* GcComboBoxにショートカットキーを追加します。 */}
                <GcShortcutInfo target='#combo' shortcutKey={InputMan.GcShortcutKey.P} action={InputMan.GcShortcutAction.PreviousControl}></GcShortcutInfo>
                <GcShortcutInfo target='#combo' shortcutKey={InputMan.GcShortcutKey.N} action={InputMan.GcShortcutAction.NextControl}></GcShortcutInfo>
                <GcShortcutInfo target='#combo' shortcutKey={InputMan.GcShortcutKey.C} action={InputMan.GcShortcutAction.Clear}></GcShortcutInfo>
                <GcShortcutInfo target='#combo' shortcutKey={InputMan.GcShortcutKey.R} action={InputMan.GcShortcutAction.DropDown}></GcShortcutInfo>
                <GcShortcutInfo target='#combo' shortcutKey={InputMan.GcShortcutKey.U} action={InputMan.GcShortcutAction.SpinUp}></GcShortcutInfo>
                <GcShortcutInfo target='#combo' shortcutKey={InputMan.GcShortcutKey.D} action={InputMan.GcShortcutAction.SpinDown}></GcShortcutInfo>
                <GcShortcutInfo target='#combo' shortcutKey={InputMan.GcShortcutKey.V} action={this.showValue}></GcShortcutInfo>

                {/* input要素にショートカットキーを追加します。 */}
                <GcShortcutInfo target='#text' shortcutKey={InputMan.GcShortcutKey.P} action={InputMan.GcShortcutAction.PreviousControl}></GcShortcutInfo>
                <GcShortcutInfo target='#text' shortcutKey={InputMan.GcShortcutKey.N} action={InputMan.GcShortcutAction.NextControl}></GcShortcutInfo>
                <GcShortcutInfo target='#text' shortcutKey={InputMan.GcShortcutKey.V} action={this.showValue}></GcShortcutInfo>

            </GcShortcut>
            <table class="sample">
                <tr>
                    <th>キー</th>
                    <th>アクション</th>
                    <th>対象のコントロール</th>
                </tr>
                <tr>
                    <td>V</td>
                    <td>現在の値を表示します。これはカスタムアクションです。</td>
                    <td>全コントロール</td>
                </tr>
                <tr>
                    <td>P</td>
                    <td>前のフィールドまたはコントロールにフォーカスを移動します。</td>
                    <td>全コントロール</td>
                </tr>
                <tr>
                    <td>N</td>
                    <td>次のフィールドまたはコントロールにフォーカスを移動します。</td>
                    <td>全コントロール</td>
                </tr>
                <tr>
                    <td>C</td>
                    <td>値をクリアします。</td>
                    <td>標準入力要素以外の全コントロール</td>
                </tr>
                <tr>
                    <td>R</td>
                    <td>ドロップダウンを開きます。</td>
                    <td>数値、日付時刻、コンボ</td>
                </tr>
                <tr>
                    <td>U</td>
                    <td>上方向のスピン操作を行います。</td>
                    <td>マスク、数値、日付時刻、コンボ</td>
                </tr>
                <tr>
                    <td>D</td>
                    <td>下方向のスピン操作を行います。</td>
                    <td>マスク、数値、日付時刻、コンボ</td>
                </tr>
                <tr>
                    <td>Z</td>
                    <td>現在のカーソル位置に000を挿入します。</td>
                    <td>数値</td>
                </tr>
                <tr>
                    <td>W</td>
                    <td>現在の日時を設定します。</td>
                    <td>日付時刻、カレンダー</td>
                </tr>
            </table>
        </React.Fragment>
    }
}

ReactDom.render(<App />, document.getElementById("app"));