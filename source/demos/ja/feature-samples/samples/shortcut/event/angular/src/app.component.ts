import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    InputMan = GC.InputMan;
    dropdown = {dropDownType: GC.InputMan.DateDropDownType.Calendar};
    items = ['果汁100%オレンジ', 'コーヒーマイルド', 'ピリピリビール', 'ホワイトソルト', 'ブラックペッパー', 'ピュアシュガー'];
    textbox_value = 'テキスト';
    mask_value: any = undefined;
    number_value = 1;
    datetime_value: any = undefined;
    combo_value = -1;
    input_value = 'テキスト';

    preAction(args: any){
        if (args.evt.key == 'p' || args.evt.key == 'n') {
            if (args.target.value == null || args.target.value == "") {
                args.cancel = true;
            }
        }
    }

    postAction(args: any){
        console.log(`${args.evt.key}キーが押されました。`);
        // すべての値をクリアします。
        if (args.evt.key == 'a') {
            this.textbox_value = null;
            this.mask_value = null;
            this.number_value = null;
            this.datetime_value = null;
            this.combo_value = -1;
            this.input_value = null;
        }
    }
}

enableProdMode();