import * as React from "react";
import * as ReactDom from "react-dom";
import { GcTextBox, GcMask, GcComboBox, GcNumber, GcDateTime, GcShortcut, GcShortcutInfo } from "@grapecity/inputman.react";
import {InputMan} from '@grapecity/inputman';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{
    
    constructor(){
        super();
        this.state = {
            texbox_value: 'テキスト',
            mask_value: undefined,
            number_value: 1,
            datetime_value: undefined,
            combo_value: null,
            input_value: 'テキスト',
        }
    }

    preaction(args){
        if (args.evt.key == 'p' || args.evt.key == 'n') {
            if (args.target.value == null || args.target.value == "") {
                args.cancel = true;
            }
        }
    }

    postaction(args){
        console.log(`${args.evt.key}キーが押されました。`);
        // すべての値をクリアします。
        if (args.evt.key == 'a') {
               this.state.texbox_value = null;
               this.state.mask_value = null;
               this.state.number_value = null;
               this.state.datetime_value = null;
               this.state.combo_value = null;
               this.state.input_value = '';
               this.forceUpdate();
        }
    }

    render(){
        let items = ['果汁100%オレンジ', 'コーヒーマイルド', 'ピリピリビール', 'ホワイトソルト', 'ブラックペッパー', 'ピュアシュガー'];
        return <React.Fragment>
            <div class="flexbox">
                <div>
                    テキストコントロール<br/>
                    <GcTextBox id='textbox' text={this.state.texbox_value}/>
                </div>
                <div>
                    マスクコントロール<br/>
                    <GcMask id='mask' value={this.state.mask_value} formatPattern='(北海道|青森県|岩手県|宮城県|秋田県|山形県|福島県|茨城県|栃木県|群馬県|埼玉県|千葉県|東京都|神奈川県|山梨県|長野県|新潟県|富山県|石川県|福井県|岐阜県|静岡県|愛知県|三重県|滋賀県|京都府|大阪府|兵庫県|奈良県|和歌山県|鳥取県|島根県|岡山県|広島県|山口県|徳島県|香川県|愛媛県|高知県|福岡県|佐賀県|長崎県|熊本県|大分県|宮崎県|鹿児島県|沖縄県)' showSpinButton={true}/>
                </div>
                <div>
                    数値コントロール<br/>
                    <GcNumber id='number' value={this.state.number_value} showSpinButton={true} showNumericPad={true}/>
                </div>
                <div>
                    日付時刻コントロール<br/>
                    <GcDateTime id='datetime' value={this.state.datetime_value} showSpinButton={true} showDropDownButton={true} dropDownConfig={{dropDownType: InputMan.DateDropDownType.Calendar}}/>
                </div>
                <div>
                    コンボコントロール<br/>
                    <GcComboBox id='combo' selectedValue={this.combo_value} items={items} showSpinButton={true}/>
                </div>
                <div>
                    標準の入力要素<br/>
                    <input id="text" type='text' value={this.state.input_value} onChange={e=> this.setState({input_value: e.target.value})}/>
                </div>
            </div>
            <GcShortcut preAction={this.preaction.bind(this)} postAction={this.postaction.bind(this)}>
                {/* GcTextBoxにショートカットキーを追加します */}
                <GcShortcutInfo target='#textbox' shortcutKey={InputMan.GcShortcutKey.P} action={InputMan.GcShortcutAction.PreviousControl}></GcShortcutInfo>
                <GcShortcutInfo target='#textbox' shortcutKey={InputMan.GcShortcutKey.N} action={InputMan.GcShortcutAction.NextControl}></GcShortcutInfo>
                <GcShortcutInfo target='#textbox' shortcutKey={InputMan.GcShortcutKey.C} action={InputMan.GcShortcutAction.Clear}></GcShortcutInfo>
                <GcShortcutInfo target='#textbox' shortcutKey={InputMan.GcShortcutKey.A}></GcShortcutInfo>

                {/* GcMaskにショートカットキーを追加します。 */}
                <GcShortcutInfo target='#mask' shortcutKey={InputMan.GcShortcutKey.P} action={InputMan.GcShortcutAction.PreviousControl}></GcShortcutInfo>
                <GcShortcutInfo target='#mask' shortcutKey={InputMan.GcShortcutKey.N} action={InputMan.GcShortcutAction.NextControl}></GcShortcutInfo>
                <GcShortcutInfo target='#mask' shortcutKey={InputMan.GcShortcutKey.C} action={InputMan.GcShortcutAction.Clear}></GcShortcutInfo>
                <GcShortcutInfo target='#mask' shortcutKey={InputMan.GcShortcutKey.A}></GcShortcutInfo>

                {/* GcNumberにショートカットキーを追加します。 */}
                <GcShortcutInfo target='#number' shortcutKey={InputMan.GcShortcutKey.P} action={InputMan.GcShortcutAction.PreviousControl}></GcShortcutInfo>
                <GcShortcutInfo target='#number' shortcutKey={InputMan.GcShortcutKey.N} action={InputMan.GcShortcutAction.NextControl}></GcShortcutInfo>
                <GcShortcutInfo target='#number' shortcutKey={InputMan.GcShortcutKey.C} action={InputMan.GcShortcutAction.Clear}></GcShortcutInfo>
                <GcShortcutInfo target='#number' shortcutKey={InputMan.GcShortcutKey.A}></GcShortcutInfo>

                {/* GcDateTimeにショートカットキーを追加します。 */}
                <GcShortcutInfo target='#datetime' shortcutKey={InputMan.GcShortcutKey.P} action={InputMan.GcShortcutAction.PreviousControl}></GcShortcutInfo>
                <GcShortcutInfo target='#datetime' shortcutKey={InputMan.GcShortcutKey.N} action={InputMan.GcShortcutAction.NextControl}></GcShortcutInfo>
                <GcShortcutInfo target='#datetime' shortcutKey={InputMan.GcShortcutKey.C} action={InputMan.GcShortcutAction.Clear}></GcShortcutInfo>
                <GcShortcutInfo target='#datetime' shortcutKey={InputMan.GcShortcutKey.A}></GcShortcutInfo>

                {/* GcComboBoxにショートカットキーを追加します。 */}
                <GcShortcutInfo target='#combo' shortcutKey={InputMan.GcShortcutKey.P} action={InputMan.GcShortcutAction.PreviousControl}></GcShortcutInfo>
                <GcShortcutInfo target='#combo' shortcutKey={InputMan.GcShortcutKey.N} action={InputMan.GcShortcutAction.NextControl}></GcShortcutInfo>
                <GcShortcutInfo target='#combo' shortcutKey={InputMan.GcShortcutKey.C} action={InputMan.GcShortcutAction.Clear}></GcShortcutInfo>
                <GcShortcutInfo target='#combo' shortcutKey={InputMan.GcShortcutKey.A}></GcShortcutInfo>

                {/* input要素にショートカットキーを追加します。 */}
                <GcShortcutInfo target='#text' shortcutKey={InputMan.GcShortcutKey.P} action={InputMan.GcShortcutAction.PreviousControl}></GcShortcutInfo>
                <GcShortcutInfo target='#text' shortcutKey={InputMan.GcShortcutKey.N} action={InputMan.GcShortcutAction.NextControl}></GcShortcutInfo>
                <GcShortcutInfo target='#text' shortcutKey={InputMan.GcShortcutKey.A}></GcShortcutInfo>

            </GcShortcut>
            <table class="sample">
                <tr>
                    <th>キー</th>
                    <th>アクション</th>
                </tr>
                <tr>
                    <td>P</td>
                    <td>値がnullでない場合に、前のコントロールにフォーカスを移動します。</td>
                </tr>
                <tr>
                    <td>N</td>
                    <td>値がnullでない場合に、次のコントロールにフォーカスを移動します。</td>
                </tr>
                <tr>
                    <td>A</td>
                    <td>すべての値をクリアします。</td>
                </tr>
                <tr>
                    <td>C</td>
                    <td>値をクリアします。</td>
                </tr>
            </table>
        </React.Fragment>
    }
}

ReactDom.render(<App />, document.getElementById("app"));