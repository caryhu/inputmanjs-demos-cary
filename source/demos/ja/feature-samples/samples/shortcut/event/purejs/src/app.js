import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';

const gcTextBox = new InputMan.GcTextBox(document.getElementById('gcTextBox'), {
    text: 'テキスト'
});
const gcMask = new InputMan.GcMask(document.getElementById('gcMask'), {
    formatPattern: '(北海道|青森県|岩手県|宮城県|秋田県|山形県|福島県|茨城県|栃木県|群馬県|埼玉県|千葉県|東京都|神奈川県|山梨県|長野県|新潟県|富山県|石川県|福井県|岐阜県|静岡県|愛知県|三重県|滋賀県|京都府|大阪府|兵庫県|奈良県|和歌山県|鳥取県|島根県|岡山県|広島県|山口県|徳島県|香川県|愛媛県|高知県|福岡県|佐賀県|長崎県|熊本県|大分県|宮崎県|鹿児島県|沖縄県)',
    showSpinButton: true
});
const gcNumber = new InputMan.GcNumber(document.getElementById('gcNumber'), {
    value: 1,
    showSpinButton: true,
    showNumericPad: true
});
const gcDateTime = new InputMan.GcDateTime(document.getElementById('gcDateTime'), {
    showSpinButton: true,
    showDropDownButton: true,
    dropDownConfig: {
        dropDownType: InputMan.DateDropDownType.Calendar
    }
});
const gcComboBox = new InputMan.GcComboBox(document.getElementById('gcComboBox'), {
    items: ['果汁100%オレンジ', 'コーヒーマイルド', 'ピリピリビール', 'ホワイトソルト', 'ブラックペッパー', 'ピュアシュガー'],
    showSpinButton: true
});
const input = document.getElementById('text');

var gcShortcut = new InputMan.GcShortcut();

// アクションが実行される前に行う処理を定義します。
gcShortcut.addEventListener('preaction', (sender, args) => {
    if (args.evt.key == 'p' || args.evt.key == 'n') {
        if (args.target.value == null || args.target.value == "") {
            args.cancel = true;
        }
    }
});
 
// アクションが実行された後に行う処理を定義します。
gcShortcut.addEventListener('postaction', (sender, args) => {
    console.log(`${args.evt.key}キーが押されました。`);
    // すべての値をクリアします。
    if (args.evt.key == 'a') {
        gcTextBox.text = null;
        gcMask.value = null;
        gcNumber.value = null;
        gcDateTime.value = null;
        gcComboBox.selectedValue = null;
        input.value = null;
    }
});

// GcTextBoxにショートカットキーを追加します。
gcShortcut.add({ key: InputMan.GcShortcutKey.P, target: gcTextBox, action: InputMan.GcShortcutAction.PreviousControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.N, target: gcTextBox, action: InputMan.GcShortcutAction.NextControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.C, target: gcTextBox, action: InputMan.GcShortcutAction.Clear });
gcShortcut.add({ key: InputMan.GcShortcutKey.A, target: gcTextBox });

// GcMaskにショートカットキーを追加します。
gcShortcut.add({ key: InputMan.GcShortcutKey.P, target: gcMask, action: InputMan.GcShortcutAction.PreviousControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.N, target: gcMask, action: InputMan.GcShortcutAction.NextControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.C, target: gcMask, action: InputMan.GcShortcutAction.Clear });
gcShortcut.add({ key: InputMan.GcShortcutKey.A, target: gcMask });

// GcNumberにショートカットキーを追加します。
gcShortcut.add({ key: InputMan.GcShortcutKey.P, target: gcNumber, action: InputMan.GcShortcutAction.PreviousControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.N, target: gcNumber, action: InputMan.GcShortcutAction.NextControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.C, target: gcNumber, action: InputMan.GcShortcutAction.Clear });
gcShortcut.add({ key: InputMan.GcShortcutKey.A, target: gcNumber });

// GcDateTimeにショートカットキーを追加します。
gcShortcut.add({ key: InputMan.GcShortcutKey.P, target: gcDateTime, action: InputMan.GcShortcutAction.PreviousControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.N, target: gcDateTime, action: InputMan.GcShortcutAction.NextControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.C, target: gcDateTime, action: InputMan.GcShortcutAction.Clear });
gcShortcut.add({ key: InputMan.GcShortcutKey.A, target: gcDateTime });

// GcComboBoxにショートカットキーを追加します。
gcShortcut.add({ key: InputMan.GcShortcutKey.P, target: gcComboBox, action: InputMan.GcShortcutAction.PreviousControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.N, target: gcComboBox, action: InputMan.GcShortcutAction.NextControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.C, target: gcComboBox, action: InputMan.GcShortcutAction.Clear });
gcShortcut.add({ key: InputMan.GcShortcutKey.A, target: gcComboBox });

// input要素にショートカットキーを追加します。
gcShortcut.add({ key: InputMan.GcShortcutKey.P, target: input, action: InputMan.GcShortcutAction.PreviousControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.N, target: input, action: InputMan.GcShortcutAction.NextControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.A, target: input });
