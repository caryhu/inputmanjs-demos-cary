import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';

const gcNumber = new InputMan.GcNumber(document.getElementById('gcNumber'), {
    value: 1,
    showSpinButton: true,
    showNumericPad: true
});
const gcDateTime = new InputMan.GcDateTime(document.getElementById('gcDateTime'), {
    showSpinButton: true,
    showDropDownButton: true,
    dropDownConfig: {
        dropDownType: InputMan.DateDropDownType.Calendar
    }
});

var gcShortcut = new InputMan.GcShortcut();

// GcNumberにショートカットキーを追加します。
gcShortcut.add({ key: InputMan.GcShortcutKey.P, target: gcNumber, action: InputMan.GcShortcutAction.PreviousControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.N, target: gcNumber, action: InputMan.GcShortcutAction.NextControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.C, target: gcNumber, action: InputMan.GcShortcutAction.Clear });
gcShortcut.add({ key: InputMan.GcShortcutKey.R, target: gcNumber, action: InputMan.GcShortcutAction.DropDown });
gcShortcut.add({ key: InputMan.GcShortcutKey.U, target: gcNumber, action: InputMan.GcShortcutAction.SpinUp });
gcShortcut.add({ key: InputMan.GcShortcutKey.D, target: gcNumber, action: InputMan.GcShortcutAction.SpinDown });

// GcDateTimeにショートカットキーを追加します。
gcShortcut.add({ key: InputMan.GcShortcutKey.P, target: gcDateTime, action: InputMan.GcShortcutAction.PreviousFieldControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.N, target: gcDateTime, action: InputMan.GcShortcutAction.NextFieldControl });
gcShortcut.add({ key: InputMan.GcShortcutKey.C, target: gcDateTime, action: InputMan.GcShortcutAction.Clear });
gcShortcut.add({ key: InputMan.GcShortcutKey.R, target: gcDateTime, action: InputMan.GcShortcutAction.DropDown });
gcShortcut.add({ key: InputMan.GcShortcutKey.U, target: gcDateTime, action: InputMan.GcShortcutAction.SpinUp });
gcShortcut.add({ key: InputMan.GcShortcutKey.D, target: gcDateTime, action: InputMan.GcShortcutAction.SpinDown });