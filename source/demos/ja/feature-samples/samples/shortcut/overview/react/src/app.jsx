import * as React from "react";
import * as ReactDom from "react-dom";
import { GcNumber, GcDateTime, GcShortcut, GcShortcutInfo } from "@grapecity/inputman.react";
import {InputMan} from '@grapecity/inputman';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{
    render(){
        return <React.Fragment>
            <div>
                <div class="flexbox">
                    <div>
                        数値コントロール<br />
                        <GcNumber id='number' value={1} showSpinButton={true} showNumericPad={true}/>
                    </div>
                    <div>
                        日付時刻コントロール<br />
                        <GcDateTime id='datetime'  showSpinButton={true} showDropDownButton={true} dropDownConfig={{dropDownType: InputMan.DateDropDownType.Calendar}}/>
                    </div>
                    <GcShortcut>
                        <GcShortcutInfo target='#number' shortcutKey={InputMan.GcShortcutKey.P} action={InputMan.GcShortcutAction.PreviousControl} />
                        <GcShortcutInfo target='#number' shortcutKey={InputMan.GcShortcutKey.N} action={InputMan.GcShortcutAction.NextControl} />
                        <GcShortcutInfo target='#number' shortcutKey={InputMan.GcShortcutKey.C} action={InputMan.GcShortcutAction.Clear} />
                        <GcShortcutInfo target='#number' shortcutKey={InputMan.GcShortcutKey.R} action={InputMan.GcShortcutAction.DropDown} />
                        <GcShortcutInfo target='#number' shortcutKey={InputMan.GcShortcutKey.U} action={InputMan.GcShortcutAction.SpinUp} />
                        <GcShortcutInfo target='#number' shortcutKey={InputMan.GcShortcutKey.D} action={InputMan.GcShortcutAction.SpinDown} />
                        <GcShortcutInfo target='#datetime' shortcutKey={InputMan.GcShortcutKey.P} action={InputMan.GcShortcutAction.PreviousControl} />
                        <GcShortcutInfo target='#datetime' shortcutKey={InputMan.GcShortcutKey.N} action={InputMan.GcShortcutAction.NextControl} />
                        <GcShortcutInfo target='#datetime' shortcutKey={InputMan.GcShortcutKey.C} action={InputMan.GcShortcutAction.Clear} />
                        <GcShortcutInfo target='#datetime' shortcutKey={InputMan.GcShortcutKey.R} action={InputMan.GcShortcutAction.DropDown} />
                        <GcShortcutInfo target='#datetime' shortcutKey={InputMan.GcShortcutKey.U} action={InputMan.GcShortcutAction.SpinUp} />
                        <GcShortcutInfo target='#datetime' shortcutKey={InputMan.GcShortcutKey.D} action={InputMan.GcShortcutAction.SpinDown} />
                    </GcShortcut>
                </div>
                <table class="sample">
                    <tr>
                        <th>キー</th>
                        <th>アクション</th>
                    </tr>
                    <tr>
                        <td>P</td>
                        <td>前のフィールドまたはコントロールにフォーカスを移動します。</td>
                    </tr>
                    <tr>
                        <td>N</td>
                        <td>次のフィールドまたはコントロールにフォーカスを移動します。</td>
                    </tr>
                    <tr>
                        <td>C</td>
                        <td>値をクリアします。</td>
                    </tr>
                    <tr>
                        <td>R</td>
                        <td>ドロップダウンを開きます。</td>
                    </tr>
                    <tr>
                        <td>U</td>
                        <td>上方向のスピン操作を行います。</td>
                    </tr>
                    <tr>
                        <td>D</td>
                        <td>下方向のスピン操作を行います。</td>
                    </tr>
                </table>
            </div> 
        </React.Fragment>
    }
}

ReactDom.render(<App />, document.getElementById("app"));