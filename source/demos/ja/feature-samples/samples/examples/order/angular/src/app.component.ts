import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import '@grapecity/spread-sheets/styles/gc.spread.sheets.css';
import { Component, enableProdMode,OnInit } from '@angular/core';
import GC from "@grapecity/inputman";
import GC2 from '@grapecity/spread-sheets';
import data from './data';
import './license';


@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent implements OnInit{
    public textFields = ['フリガナ', '氏名', '出荷先名', '住所'];
    public controls: Array<object> = [];
    public 受注 = data;
    private sheet: GC2.Spread.Sheets.Worksheet;
    public displayFormatDigit="##,##0";

    ngOnInit() {
        const spread = new GC2.Spread.Sheets.Workbook(document.getElementById('ss'), { sheetCount: 1 });
        this.sheet = spread.getSheet(0);
    }

    public update() {
        var index = (this.controls as any).受注コード.value - 1001;
        for (var fieldName in this.controls) {
            if (this.textFields.indexOf(fieldName) >= 0) {
                (this.controls[fieldName] as any).text = this.受注[index][fieldName];
            } else {
                (this.controls[fieldName] as any).value = this.受注[index][fieldName];
            }
        }
        this.sheet.autoGenerateColumns = true;
        this.sheet.setDataSource(this.受注[index].受注明細);
        this.sheet.setColumnWidth(0, 100);
        this.sheet.setColumnWidth(1, 220);
        this.sheet.setColumnWidth(2, 100);
        this.sheet.setColumnWidth(3, 120);
    }

    public onSpinUp() {
        this.update();
    }

    public onSpinDown() {
        this.update();
    }

    ngAfterViewInit(){
        this.update();
    }
}


enableProdMode();