import * as React from "react";
import * as ReactDom from "react-dom";
import { GcNumber, GcTextBox, GcMask, GcComboBox, GcListBox, GcDateTime } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

import '@grapecity/spread-sheets/styles/gc.spread.sheets.css';
import './styles.css';
import './license';
import { Spread } from '@grapecity/spread-sheets';
import  受注  from './data';

class App extends React.Component{
    controls = {};
    sheet = null;
    textFields = ['フリガナ', '氏名', '出荷先名', '住所'];
    index = 0;
    componentDidMount(){
        const spread = new Spread.Sheets.Workbook(document.getElementById('ss'), { sheetCount: 1 });
        this.sheet = spread.getSheet(0);

        this.update();
        console.log(受注);

        this.controls.受注コード.onSpinUp((sender, args) => {
            this.update();
        });
        
        this.controls.受注コード.onSpinDown((sender, args) => {
            this.update();
        });
    }

    update = () => {
        this.index = this.controls.受注コード.value - 1001;
        for (var fieldName in this.controls) {
            if (this.textFields.indexOf(fieldName) >= 0) {
                this.controls[fieldName].text = 受注[this.index][fieldName];
            } else {
                this.controls[fieldName].value = 受注[this.index][fieldName];
            }
        }
        this.sheet.autoGenerateColumns = true;
        this.sheet.setDataSource(受注[this.index].受注明細);
        this.sheet.setColumnWidth(0, 100);
        this.sheet.setColumnWidth(1, 220);
        this.sheet.setColumnWidth(2, 100);
        this.sheet.setColumnWidth(3, 120);
    }

    render(){
        return (
            <table class="outer-table">
                <tr>
                    <th colspan="2">受注明細</th>
                </tr>
                <tr>
                    <td colspan="2">
                        <table class="inner-table">
                            <tr>
                                <th>受注コード</th>
                                <td><GcNumber
                                        minValue= {1001}
                                        maxValue= {1120}
                                        showSpinButton= {true}
                                        onInitialized={(imCtrl) => { 
                                            this.controls.受注コード = imCtrl;
                                         }}></GcNumber></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <th>担当社員</th>
                    <th>出荷先</th>
                </tr>
                <tr>
                    <td>
                        <table class="inner-table">
                            <tr>
                                <th>社員コード</th>
                                <td><GcNumber
                                 onInitialized={(imCtrl) => { 
                                    this.controls.社員コード = imCtrl;
                                 }}></GcNumber></td>
                            </tr>
                            <tr>
                                <th>フリガナ</th>
                                <td><GcTextBox
                                 onInitialized={(imCtrl) => { 
                                    this.controls.フリガナ = imCtrl;
                                 }}></GcTextBox></td>
                            </tr>
                            <tr>
                                <th>氏名</th>
                                <td><GcTextBox
                                 onInitialized={(imCtrl) => { 
                                    this.controls.氏名 = imCtrl;
                                 }}></GcTextBox></td>
                            </tr>
                            <tr>
                                <th>在籍支社</th>
                                <td><GcMask
                                 onInitialized={(imCtrl) => { 
                                    this.controls.在籍支社 = imCtrl;
                                 }}></GcMask></td>
                            </tr>
                            <tr>
                                <th>部署名</th>
                                <td><GcMask
                                 onInitialized={(imCtrl) => { 
                                    this.controls.部署名 = imCtrl;
                                 }}></GcMask></td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table class="inner-table">
                            <tr>
                                <th>出荷先名</th>
                                <td><GcTextBox className={'nameStyle'}
                                 onInitialized={(imCtrl) => { 
                                    this.controls.出荷先名 = imCtrl;
                                 }}></GcTextBox></td>
                            </tr>
                            <tr>
                                <th>郵便番号</th>
                                <td><GcMask formatPattern= {'〒\\D{3}-\\D{4}'}
                                 onInitialized={(imCtrl) => { 
                                    this.controls.郵便番号 = imCtrl;
                                 }}></GcMask></td>
                            </tr>
                            <tr>
                                <th>住所</th>
                                <td><GcTextBox className={'nameStyle'}
                                 onInitialized={(imCtrl) => { 
                                    this.controls.住所 = imCtrl;
                                 }}></GcTextBox></td>
                            </tr>
                            <tr>
                                <th>受注日</th>
                                <td><GcDateTime
                                        formatPattern= {'yyy/M/d'}
                                        displayFormatPattern= {'gggE年M月d日'}
                                        onInitialized={(imCtrl) => { 
                                            this.controls.受注日 = imCtrl;
                                         }}></GcDateTime></td>
                            </tr>
                            <tr>
                                <th>出荷日</th>
                                <td><GcDateTime
                                        formatPattern= {'yyy/M/d'}
                                        displayFormatPattern= {'gggE年M月d日'}
                                        onInitialized={(imCtrl) => { 
                                            this.controls.出荷日 = imCtrl;
                                         }}></GcDateTime></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <th colspan="2">商品一覧</th>
                </tr>
                <tr>
                    <td colspan="2">
                        <div id="ss" style={{height: '200px', width: '700px'}}></div>
                    </td>
                </tr>
                <tr>
                    <th colspan="2">合計</th>
                </tr>
                <tr>
                    <td colspan="2">
                        <table class="inner-table">
                            <tr>
                                <th>合計</th>
                                <td><GcNumber
                                        displayFormatDigit= {'##,##0'}
                                        displayPositiveSuffix= {'円'}
                                        onInitialized={(imCtrl) => { 
                                            this.controls.合計 = imCtrl;
                                         }}></GcNumber></td>
                                <th>税額</th>
                                <td><GcNumber
                                        displayFormatDigit= {'##,##0'}
                                        displayPositiveSuffix= {'円'}
                                        onInitialized={(imCtrl) => { 
                                            this.controls.税額 = imCtrl;
                                         }}></GcNumber></td>
                                <th>総額</th>
                                <td><GcNumber
                                        displayFormatDigit= {'##,##0'}
                                        displayPositiveSuffix= {'円'}
                                        onInitialized={(imCtrl) => { 
                                            this.controls.総額 = imCtrl;
                                         }}></GcNumber></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));