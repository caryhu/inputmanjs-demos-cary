(function (global) {
    System.config({
        transpiler: 'plugin-babel',
        babelOptions: {
            es2015: true,
            react: true
        },
        meta: {
            '*.css': { loader: 'css' }
        },
        paths: {
            // paths serve as alias
            'npm:': 'node_modules/'
        },
        // map tells the System loader where to look for things
        map: {
            "@grapecity/inputman": "npm:@grapecity/inputman/index.js",
            "@grapecity/inputman.react": "npm:@grapecity/inputman.react/GcInputMan.component.js",
            "@grapecity/inputman/CSS": "npm:@grapecity/inputman/CSS",
            '@grapecity/spread-sheets': 'npm:@grapecity/spread-sheets/index.js',
            '@grapecity/spread-sheets-resources-ja': 'npm:@grapecity/spread-sheets-resources-ja/index.js',
            '@grapecity/spread-sheets/styles': 'npm:@grapecity/spread-sheets/styles',
            'react': 'npm:react/umd/react.production.min.js',
            'react-dom': 'npm:react-dom/umd/react-dom.production.min.js',
            'css': 'npm:systemjs-plugin-css/css.js',
            'plugin-babel': 'npm:systemjs-plugin-babel/plugin-babel.js',
            'systemjs-babel-build':'npm:systemjs-plugin-babel/systemjs-babel-browser.js'
        },
        // packages tells the System loader how to load when no filename and/or no extension
        packages: {
            src: {
                defaultExtension: 'jsx'
            },
            "node_modules": {
                defaultExtension: 'js'
            },
        }
    });
})(this);
