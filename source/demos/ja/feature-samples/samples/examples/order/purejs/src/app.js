﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import '@grapecity/spread-sheets/styles/gc.spread.sheets.css';
import './styles.css';
import './license';
import { InputMan } from '@grapecity/inputman';
import { Spread } from '@grapecity/spread-sheets';
import  受注  from './data';

const controls = {
    受注コード: new InputMan.GcNumber(document.getElementById('受注コード'), {
        minValue: 1001,
        maxValue: 1120,
        showSpinButton: true
    }),
    社員コード: new InputMan.GcNumber(document.getElementById('社員コード')),
    フリガナ: new InputMan.GcTextBox(document.getElementById('フリガナ')),
    氏名: new InputMan.GcTextBox(document.getElementById('氏名')),
    在籍支社: new InputMan.GcMask(document.getElementById('在籍支社')),
    部署名: new InputMan.GcMask(document.getElementById('部署名')),
    出荷先名: new InputMan.GcTextBox(document.getElementById('出荷先名')),
    郵便番号: new InputMan.GcMask(document.getElementById('郵便番号'), {
        formatPattern: '〒\\D{3}-\\D{4}'
    }),
    住所: new InputMan.GcTextBox(document.getElementById('住所')),
    受注日: new InputMan.GcDateTime(document.getElementById('受注日'), {
        formatPattern: 'yyy/M/d',
        displayFormatPattern: 'gggE年M月d日'
    }),
    出荷日: new InputMan.GcDateTime(document.getElementById('出荷日'), {
        formatPattern: 'yyy/M/d',
        displayFormatPattern: 'gggE年M月d日'
    }),
    合計: new InputMan.GcNumber(document.getElementById('合計'), {
        displayFormatDigit: '##,##0',
        displayPositiveSuffix: '円'
    }),
    税額: new InputMan.GcNumber(document.getElementById('税額'), {
        displayFormatDigit: '##,##0',
        displayPositiveSuffix: '円'
    }),
    総額: new InputMan.GcNumber(document.getElementById('総額'), {
        displayFormatDigit: '##,##0',
        displayPositiveSuffix: '円'
    }),
};

const update = () => {
    index = controls.受注コード.value - 1001;
    for (var fieldName in controls) {
        if (textFields.indexOf(fieldName) >= 0) {
            controls[fieldName].text = 受注[index][fieldName];
        } else {
            controls[fieldName].value =受注[index][fieldName];
        }
    }
    sheet.autoGenerateColumns = true;
    sheet.setDataSource(受注[index].受注明細);
    sheet.setColumnWidth(0, 100);
    sheet.setColumnWidth(1, 220);
    sheet.setColumnWidth(2, 100);
    sheet.setColumnWidth(3, 120);
}

controls.受注コード.onSpinUp((sender, args) => {
    update();
});

controls.受注コード.onSpinDown((sender, args) => {
    update();
});

const spread = new Spread.Sheets.Workbook(document.getElementById('ss'), { sheetCount: 1 });
const sheet = spread.getSheet(0);
var index = 0;
const textFields = ['フリガナ', '氏名', '出荷先名', '住所'];
update();
console.log(受注);

var gcShortcut = new InputMan.GcShortcut();
gcShortcut.add({ key: InputMan.GcShortcutKey.U, target: controls.受注コード, action: InputMan.GcShortcutAction.SpinUp });
gcShortcut.add({ key: InputMan.GcShortcutKey.D, target: controls.受注コード, action: InputMan.GcShortcutAction.SpinDown });