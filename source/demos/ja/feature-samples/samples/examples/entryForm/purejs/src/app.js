﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';

// 氏名
const username = new InputMan.GcTextBox(document.getElementById('username'), {
    watermarkDisplayNullText: '例）葡萄太郎',
    watermarkNullText: 'ふりがなは自動的に出力します',
    exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.Both,
    exitOnEnterKey: InputMan.ExitKey.Both
});
username.onIMEReadingStringOutput((sender, eArgs) => {
    furigana.text = eArgs.readingString;
    validator.validate();
});
username.onInput((sender, eArgs) => {
    if (sender.text.length == 0) sender.clearIMEReadingString();
});

// フリガナ
const furigana = new InputMan.GcTextBox(document.getElementById('furigana'), {
    watermarkDisplayNullText: '全角カナで入力してください',
    watermarkNullText: '氏名を入力してください',
    format: 'Ｎ',
    exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.Both,
    exitOnEnterKey: InputMan.ExitKey.Both
});
furigana.onInput((sender, eArgs) => { 
    if (sender.text.length == 0) username.clearIMEReadingString();
});

// メールアドレス
const mail = new InputMan.GcMask(document.getElementById('mail'), {
    exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.Both,
    exitOnEnterKey: InputMan.ExitKey.Both
});

// 郵便番号
const postal = new InputMan.GcMask(document.getElementById('postal'), {
    formatPattern: '〒\\D{3}-\\D{4}',
    exitOnLastChar: true,
    exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.Both,
    exitOnEnterKey: InputMan.ExitKey.Both
});

// 都道府県
const prefecture = new InputMan.GcTextBox(document.getElementById('prefecture'), {
    watermarkDisplayNullText: '都道府県名を選択してください',
    exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.Both,
    exitOnEnterKey: InputMan.ExitKey.Both
});
const dropDown = prefecture.createDropDown();
dropDown.getElement().appendChild(document.getElementById('prefDropdown'));
document.getElementById('prefDropdown').addEventListener('click', (e) => {
    if (e.srcElement.tagName.toLowerCase() == 'span') {
        prefecture.text = e.srcElement.innerText;
        dropDown.close();
    }
});

// 住所
const address1 = new InputMan.GcTextBox(document.getElementById('address1'), {
    watermarkDisplayNullText: '市区町村名を入力してください',
    exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.Both,
    exitOnEnterKey: InputMan.ExitKey.Both
});
const address2 = new InputMan.GcTextBox(document.getElementById('address2'), {
    watermarkDisplayNullText: '番地以降を入力してください',
    watermarkNullText: '全角文字に自動変換します',
    format: 'Ｚ',
    exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.Both,
    exitOnEnterKey: InputMan.ExitKey.Both
});

// 電話番号
const phone = new InputMan.GcMask(document.getElementById('phone'), {
    formatPattern: '\\D{2,4}-\\D{2,4}-\\D{4}',
    exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.Both,
    exitOnLastChar: true,
    exitOnEnterKey: InputMan.ExitKey.Both
});

// 会社名
const company = new InputMan.GcTextBox(document.getElementById('company'), {
    watermarkDisplayNullText: '例）グレープシティ株式会社',
    exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.Both,
    exitOnEnterKey: InputMan.ExitKey.Both
});

// お問い合わせ内容
const question = new InputMan.GcMultiLineTextBox(document.getElementById('question'), {
    maxLength: 101,
    height: 100
});

// 検証処理
const validator = new InputMan.GcValidator({
    items: [
        // 氏名
        {
            control: username,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required
                }
            ],
        },
        // フリガナ
        {
            control: furigana,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required
                }
            ],
        },
        // メールアドレス
        {
            control: mail,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required
                },
                {
                    rule: (control) => {
                        const regexp = /^[A-Za-z0-9]{1}[A-Za-z0-9_.-]*@{1}[A-Za-z0-9_.-]{1,}\.[A-Za-z0-9]{1,}$/;
                        return regexp.test(control.value);
                    },
                    failMessage: '正しいメールアドレス形式で入力してください'
                }
            ],
        },
        // 郵便番号
        {
            control: postal,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required
                }
            ],
        },
        // 都道府県名
        {
            control: prefecture,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required
                }
            ],
        },
        // 住所
        {
            control: address1,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required
                }
            ],
        },
        {
            control: address2,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required
                }
            ],
        },
        // 電話番号
        {
            control: phone,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required
                }
            ],
        },
        // 会社名
        {
            control: company,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required
                }
            ],
        },
        // お問い合わせ内容
        {
            control: question,
            ruleSet: [
                {
                    rule: (control) => {
                        return control.value.length < 100;
                    },
                    failMessage: '100文字を超える入力はできません'
                },
            ],
            validateWhen: InputMan.ValidateWhen.Typing
        }
    ],
    defaultNotify: {
        fail: {
            icon: true,
            controlState: true
        },
        success: {
            icon: true,
        }
    }
});
let fp = new GC.InputMan.GcFormPersistence(document.getElementById('imfm'));
fp.saveMode = GC.InputMan.SaveMode.SaveOnSubmit;
fp.persist();

validator.validate();