import * as React from "react";
import * as ReactDom from "react-dom";
import { GcTextBox, GcMask, GcMultiLineTextBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{
    username= null;
    furigana =null;
    mail = null;
    postal = null;
    prefecture = null;
    address1 = null;
    address2= null;
    phone = null;
    company = null;
    question = null;

    render(){
        return (
            <div>
                <table class="form">
                    <tr>
                        <th>氏名<span class="required">必須</span></th>
                        <td><GcTextBox
                            watermarkDisplayNullText= {'例）葡萄太郎'}
                            watermarkNullText= {'ふりがなは自動的に出力します'}
                            exitOnLeftRightKey= {InputMan.ExitOnLeftRightKey.Both}
                            exitOnEnterKey= {InputMan.ExitKey.Both}
                            onIMEReadingStringOutput={(sender, eArgs) => {
                                this.furigana.text = eArgs.readingString;
                                this.validator.validate();
                            }}
                            onInput={(sender) => {
                                if (sender.text.length == 0) sender.clearIMEReadingString();
                            }}
                            onInitialized={(imCtrl) => { 
                                this.username = imCtrl;
                             }}></GcTextBox></td>
                    </tr>
                    <tr>
                        <th>フリガナ<span class="required">必須</span></th>
                        <td><GcTextBox
                            watermarkDisplayNullText= {'全角カナで入力してください'}
                            watermarkNullText= {'氏名を入力してください'}
                            format= {'Ｎ'}
                            exitOnLeftRightKey= {InputMan.ExitOnLeftRightKey.Both}
                            exitOnEnterKey= {InputMan.ExitKey.Both}
                            onInput={(sender) => {
                                if (sender.text.length == 0) this.username.clearIMEReadingString();
                            }}
                            onInitialized={(imCtrl) => { 
                                this.furigana = imCtrl;
                             }}></GcTextBox></td>
                    </tr>
                    <tr>
                        <th>メールアドレス<span class="required">必須</span></th>
                        <td><GcMask
                            exitOnLeftRightKey= {InputMan.ExitOnLeftRightKey.Both}
                            exitOnEnterKey= {InputMan.ExitKey.Both}
                            onInitialized={(imCtrl) => { 
                                this.mail = imCtrl;
                             }}></GcMask></td>
                    </tr>
                    <tr>
                        <th rowspan="3">住所<span class="required">必須</span></th>
                        <td><GcMask className= {'postalStyle'}
                            formatPattern= {'〒\\D{3}-\\D{4}'}
                            exitOnLastChar= {true}
                            exitOnLeftRightKey= {InputMan.ExitOnLeftRightKey.Both}
                            exitOnEnterKey= {InputMan.ExitKey.Both}
                            onInitialized={(imCtrl) => { 
                                this.postal = imCtrl;
                             }}></GcMask>
                            <GcTextBox className= {'prefectureStyle'}
                                watermarkDisplayNullText= {'都道府県名を選択してください'}
                                exitOnLeftRightKey= {InputMan.ExitOnLeftRightKey.Both}
                                exitOnEnterKey= {InputMan.ExitKey.Both}
                                onInitialized={(imCtrl) => { 
                                    this.prefecture = imCtrl;
                                    const dropDown = this.prefecture.createDropDown();
                                    dropDown.getElement().appendChild(document.getElementById('prefDropdown'));
                                    document.getElementById('prefDropdown').addEventListener('click', (e) => {
                                        if (e.srcElement.tagName.toLowerCase() == 'span') {
                                            this.prefecture.text = e.srcElement.innerText;
                                            dropDown.close();
                                        }
                                    });
                                 }}></GcTextBox></td>
                    </tr>
                    <tr>
                        <td><GcTextBox
                            watermarkDisplayNullText= {'市区町村名を入力してください'}
                            exitOnLeftRightKey= {InputMan.ExitOnLeftRightKey.Both}
                            exitOnEnterKey= {InputMan.ExitKey.Both}
                            onInitialized={(imCtrl) => { 
                                this.address1 = imCtrl;
                             }}></GcTextBox></td>
                    </tr>
                    <tr>
                        <td><GcTextBox
                            watermarkDisplayNullText= {'番地以降を入力してください'}
                            watermarkNullText= {'全角文字に自動変換します'}
                            format= {'Ｚ'}
                            exitOnLeftRightKey= {InputMan.ExitOnLeftRightKey.Both}
                            exitOnEnterKey= {InputMan.ExitKey.Both}
                            onInitialized={(imCtrl) => { 
                                this.address2 = imCtrl;
                             }}></GcTextBox></td>
                    </tr>
                    <tr>
                        <th>電話番号<span class="required">必須</span></th>
                        <td><GcMask
                            formatPattern= {'\\D{2,4}-\\D{2,4}-\\D{4}'}
                            exitOnLeftRightKey= {InputMan.ExitOnLeftRightKey.Both}
                            exitOnLastChar= {true}
                            exitOnEnterKey= {InputMan.ExitKey.Both}
                            onInitialized={(imCtrl) => { 
                                this.phone = imCtrl;
                             }}></GcMask></td>
                    </tr>
                    <tr>
                        <th>会社名<span class="required">必須</span></th>
                        <td><GcTextBox
                            watermarkDisplayNullText= {'例）グレープシティ株式会社'}
                            exitOnLeftRightKey= {InputMan.ExitOnLeftRightKey.Both}
                            exitOnEnterKey= {InputMan.ExitKey.Both}
                            onInitialized={(imCtrl) => { 
                                this.company = imCtrl;
                             }}></GcTextBox>
                        </td>
                    </tr>
                    <tr>
                        <th>お問い合わせ内容</th>
                        <td><GcMultiLineTextBox
                            maxLength= {101}
                            height= {100}
                            onInitialized={(imCtrl) => { 
                                this.question = imCtrl;
                             }}>
                            </GcMultiLineTextBox></td>
                    </tr>
                </table>

                <div id="prefDropdown">
                    <table>
                        <tr>
                            <th>北海道</th>
                            <td><span>北海道</span></td>
                        </tr>
                        <tr>
                            <th>東北</th>
                            <td><span>青森県</span><span>岩手県</span><span>宮城県</span><span>秋田県</span><span>山形県</span><span>福島県</span>
                            </td>
                        </tr>
                        <tr>
                            <th>関東</th>
                            <td><span>茨城県</span><span>栃木県</span><span>群馬県</span><span>埼玉県</span><span>千葉県</span><span>東京都</span><span>神奈川県</span>
                            </td>
                        </tr>
                        <tr>
                            <th>甲信越</th>
                            <td><span>山梨県</span><span>長野県</span><span>新潟県</span></td>
                        </tr>
                        <tr>
                            <th>北陸</th>
                            <td><span>富山県</span><span>石川県</span><span>福井県</span></td>
                        </tr>
                        <tr>
                            <th>東海</th>
                            <td><span>岐阜県</span><span>静岡県</span><span>愛知県</span><span>三重県</span></td>
                        </tr>
                        <tr>
                            <th>近畿</th>
                            <td><span>滋賀県</span><span>京都府</span><span>大阪府</span><span>兵庫県</span><span>奈良県</span><span>和歌山県</span>
                            </td>
                        </tr>
                        <tr>
                            <th>中国四国</th>
                            <td><span>鳥取県</span><span>島根県</span><span>岡山県</span><span>広島県</span><span>山口県</span><span>徳島県</span><span>香川県</span><span>愛媛県</span><span>高知県</span>
                            </td>
                        </tr>
                        <tr>
                            <th>九州</th>
                            <td><span>福岡県</span><span>佐賀県</span><span>長崎県</span><span>熊本県</span><span>大分県</span><span>宮崎県</span><span>鹿児島県</span>
                            </td>
                        </tr>
                        <tr>
                            <th>沖縄</th>
                            <td><span>沖縄県</span></td>
                        </tr>
                    </table>
                </div>
            </div>
        )
    }

    componentDidMount(){
        // 検証処理
        const validator = new InputMan.GcValidator({
            items: [
                // 氏名
                {
                    control: this.username,
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ],
                },
                // フリガナ
                {
                    control: this.furigana,
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ],
                },
                // メールアドレス
                {
                    control: this.mail,
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        },
                        {
                            rule: (control) => {
                                const regexp = /^[A-Za-z0-9]{1}[A-Za-z0-9_.-]*@{1}[A-Za-z0-9_.-]{1,}\.[A-Za-z0-9]{1,}$/;
                                return regexp.test(control.value);
                            },
                            failMessage: '正しいメールアドレス形式で入力してください'
                        }
                    ],
                },
                // 郵便番号
                {
                    control: this.postal,
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ],
                },
                // 都道府県名
                {
                    control: this.prefecture,
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ],
                },
                // 住所
                {
                    control: this.address1,
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ],
                },
                {
                    control: this.address2,
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ],
                },
                // 電話番号
                {
                    control: this.phone,
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ],
                },
                // 会社名
                {
                    control: this.company,
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ],
                },
                // お問い合わせ内容
                {
                    control: this.question,
                    ruleSet: [
                        {
                            rule: (control) => {
                                return control.value.length < 100;
                            },
                            failMessage: '100文字を超える入力はできません'
                        },
                    ],
                    validateWhen: InputMan.ValidateWhen.Typing
                }
            ],
            defaultNotify: {
                fail: {
                    icon: true,
                    controlState: true
                },
                success: {
                    icon: true,
                }
            }
        });
        validator.validate();
        this.validator = validator;
    }
}

ReactDom.render(<App />, document.getElementById("app"));