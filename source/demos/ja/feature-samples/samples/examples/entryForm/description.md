様々なフォーカス移動機能や、入力検証のメッセージやアイコン、表形式のドロップダウンなどを備えたお問い合わせフォーム画面を作成する方法を紹介します。ここでは以下の機能を実装しています。快適な入力操作をお試しください。

- EnterキーおよびShift+Enterキーでのフォーカス移動
- 左右矢印キーでのフォーカス移動
- 入力完了時の自動フォーカス移動（郵便番号、電話番号）
- 未入力時のウォーターマーク表示
- 未入力を示す背景色と境界線色
- 入力完了を示すアイコン
- ドロップダウンプラグインを使用した任意のドロップダウンオブジェクト（都道府県名）
- ふりがなの自動出力および全角カナ制限
- メールアドレス書式チェック
- 最大文字数チェック（お問い合わせ）
- フォームデータ送信、ブラウザ更新時でのフォームデータの保存
<!-- -->

