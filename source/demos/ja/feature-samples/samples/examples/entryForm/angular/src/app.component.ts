import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, AfterViewInit } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent implements AfterViewInit {

    public exitOnLeftRightKey = GC.InputMan.ExitOnLeftRightKey.Both;
    public exitOnEnterKey = GC.InputMan.ExitKey.Both;
    public username: GC.InputMan.GcTextBox;
    public furigana: GC.InputMan.GcTextBox;
    public mail: GC.InputMan.GcMask;
    public postal: GC.InputMan.GcMask;
    public prefecture: GC.InputMan.GcMask;
    public address1: GC.InputMan.GcTextBox;
    public address2: GC.InputMan.GcTextBox;
    public phone: GC.InputMan.GcMask;
    public company: GC.InputMan.GcTextBox;
    public question: GC.InputMan.GcMultiLineTextBox;
    public furiganaText: string;
    public validator: GC.InputMan.GcValidator;

    public ngAfterViewInit(): void {
        this.addDropDown();
        this.addGcValidator();
    }

    public onIMEReadingStringOutput(sender: object) {
        this.furiganaText = (sender as any).eArgs.readingString;
        this.validator.validate();
    }

    public onUsernameInput(sender: GC.InputMan.GcTextBox) {
        if (sender.text.length == 0) {
            sender.clearIMEReadingString("");
        }
    }

    public onFuriganaInput(sender: GC.InputMan.GcTextBox) {
        if (sender.text.length == 0) {
            this.username.clearIMEReadingString("");
        }
    }

    private addDropDown() {
        const dropDown = this.prefecture.createDropDown();
        dropDown.getElement().appendChild(document.getElementById('prefDropdown'));
        document.getElementById('prefDropdown').addEventListener('click', (e) => {
            if ((e.srcElement as HTMLElement).tagName.toLowerCase() == 'span') {
                this.prefecture.text = (e.srcElement as HTMLElement).innerText;
                dropDown.close();
            }
        });
    }

    public addGcValidator() {
        // 検証処理
        const validator = new GC.InputMan.GcValidator({
            items: [
                // 氏名
                {
                    control: this.username,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ],
                },
                // フリガナ
                {
                    control: this.furigana,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ],
                },
                // メールアドレス
                {
                    control: this.mail,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        },
                        {
                            rule: (control) => {
                                const regexp = /^[A-Za-z0-9]{1}[A-Za-z0-9_.-]*@{1}[A-Za-z0-9_.-]{1,}\.[A-Za-z0-9]{1,}$/;
                                return regexp.test(control.value);
                            },
                            failMessage: '正しいメールアドレス形式で入力してください'
                        }
                    ],
                },
                // 郵便番号
                {
                    control: this.postal,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ],
                },
                // 都道府県名
                {
                    control: this.prefecture,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ],
                },
                // 住所
                {
                    control: this.address1,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ],
                },
                {
                    control: this.address2,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ],
                },
                // 電話番号
                {
                    control: this.phone,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ],
                },
                // 会社名
                {
                    control: this.company,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ],
                },
                // お問い合わせ内容
                {
                    control: this.question,
                    ruleSet: [
                        {
                            rule: (control: GC.InputMan.GcInputManBase) => {
                                return control.value.length < 100;
                            },
                            failMessage: '100文字を超える入力はできません'
                        },
                    ],
                    validateWhen: GC.InputMan.ValidateWhen.Typing
                }
            ],
            defaultNotify: {
                fail: {
                    icon: true,
                    controlState: true
                },
                success: {
                    icon: true,
                }
            }
        });
        validator.validate();
    }
}


enableProdMode();