import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, AfterViewInit } from '@angular/core';
import GC from "@grapecity/inputman";
import { GcCalendarComponent } from '@grapecity/inputman.angular';

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent implements AfterViewInit{
    ngAfterViewInit(): void {
        this.initValidator();
    }
    public cardType: GC.InputMan.GcComboBox;
    public cardNumber: GC.InputMan.GcMask;
    public ownerName: GC.InputMan.GcTextBox;
    public expirationDate: GC.InputMan.GcDateTime;
    public securityCode: GC.InputMan.GcMask;
    
    // カード番号のマスク書式
    public cardNoFormats = {
        VISA: '\\D{4}-\\D{4}-\\D{4}-\\D{4}',
        MasterCard: '\\D{4}-\\D{4}-\\D{4}-\\D{4}',
        JCB: '\\D{4}-\\D{4}-\\D{4}-\\D{4}',
        AmericanExpress: '\\D{4}-\\D{6}-\\D{5}',
        'Diners Club': '\\D{4}-\\D{6}-\\D{4}'
    };
    public cardTypeItems: Array<string> = ['VISA', 'MasterCard', 'JCB', 'AmericanExpress', 'Diners Club'];
    public cardNoFormat = this.cardNoFormats.VISA;
    public cardTypeExitOnLeftRightKey: GC.InputMan.ExitOnLeftRightKey = GC.InputMan.ExitOnLeftRightKey.Both;
    public cardTypeExitOnEnterKey: GC.InputMan.ExitKey = GC.InputMan.ExitKey.Both;

    public cardNumberExitOnLeftRightKey: GC.InputMan.ExitOnLeftRightKey = GC.InputMan.ExitOnLeftRightKey.Both;
    public cardNumberExitOnEnterKey: GC.InputMan.ExitKey = GC.InputMan.ExitKey.Both;

    public ownerNameExitOnLeftRightKey: GC.InputMan.ExitOnLeftRightKey = GC.InputMan.ExitOnLeftRightKey.Both;
    public ownerNameExitOnEnterKey: GC.InputMan.ExitKey = GC.InputMan.ExitKey.Both;

    public expirationDateExitOnLeftRightKey: GC.InputMan.ExitOnLeftRightKey = GC.InputMan.ExitOnLeftRightKey.Both;
    public expirationDateExitOnEnterKey: GC.InputMan.ExitKey = GC.InputMan.ExitKey.Both;
    public expirationDateDropDownConfig = {
        dropDownType: GC.InputMan.DateDropDownType.Calendar,
        calendarType: GC.InputMan.CalendarType.YearMonth
    };
    public expirationDateMinDate: Date = new Date(new Date().getFullYear(), new Date().getMonth(), 1);
    public expirationDateMaxDate: Date = new Date(new Date().getFullYear() + 5, 11, 31);

    public securityCodeExitOnLeftRightKey: GC.InputMan.ExitOnLeftRightKey = GC.InputMan.ExitOnLeftRightKey.Both;
    public securityCodeExitOnEnterKey: GC.InputMan.ExitKey = GC.InputMan.ExitKey.Both;
    public securityPasswordRevelationMode: GC.InputMan.PasswordRevelationMode = GC.InputMan.PasswordRevelationMode.ShowLastTypedChar;

    public initValidator() {
        // 検証処理
        const validator = new GC.InputMan.GcValidator({
            items: [
                // カードブランド
                {
                    control: this.cardType,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required,
                            failMessage: 'カードブランドを選択してください'
                        }
                    ]
                },
                // カード番号
                {
                    control: this.cardNumber,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required,
                            failMessage: 'カード番号を入力してください'
                        }
                    ]
                },
                // カード名義
                {
                    control: this.ownerName,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required,
                            failMessage: '名前を入力してください'
                        }
                    ]
                },
                // 有効期限
                {
                    control: this.expirationDate,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required,
                            failMessage: '日付を入力してください'
                        }
                    ]
                },
                // セキュリティコード
                {
                    control: this.securityCode,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required,
                            failMessage: '数値を入力してください'
                        }
                    ]
                }
            ],
            defaultNotify: {
                tip: {
                    template: '<div style="color:red;min-width:20rem;">{!message}</div>'
                }
            }
        });
        validator.validate();
    }
}


enableProdMode();