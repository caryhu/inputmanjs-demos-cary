import * as React from "react";
import * as ReactDom from "react-dom";
import { GcComboBox, GcTextBox, GcMask, GcDateTime } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

var today = new Date();
var thisYear = today.getFullYear();
var thisMonth = today.getMonth();
// カード番号のマスク書式   
var cardNoFormats = {
    VISA: '\\D{4}-\\D{4}-\\D{4}-\\D{4}',
    MasterCard: '\\D{4}-\\D{4}-\\D{4}-\\D{4}',
    JCB: '\\D{4}-\\D{4}-\\D{4}-\\D{4}',
    AmericanExpress: '\\D{4}-\\D{6}-\\D{5}',
    'Diners Club': '\\D{4}-\\D{6}-\\D{4}'
};
class App extends React.Component{
    cardType = null;
    cardNumber = null;
    ownerName = null;
    expirationDate = null;
    securityCode = null;

    componentDidMount(){
        // 検証処理
        const validator = new InputMan.GcValidator({
            items: [
                // カードブランド
                {
                    control: this.cardType,
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required,
                            failMessage: 'カードブランドを選択してください'
                        }
                    ]
                },
                // カード番号
                {
                    control: this.cardNumber,
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required,
                            failMessage: 'カード番号を入力してください'
                        }
                    ]
                },
                // カード名義
                {
                    control: this.ownerName,
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required,
                            failMessage: '名前を入力してください'
                        }
                    ]
                },
                // 有効期限
                {
                    control: this.expirationDate,
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required,
                            failMessage: '日付を入力してください'
                        }
                    ]
                },
                // セキュリティコード
                {
                    control: this.securityCode,
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required,
                            failMessage: '数値を入力してください'
                        }
                    ]
                }
            ],
            defaultNotify: {
                tip: {
                    template: '<div style="color:red;min-width:20rem;">{!message}</div>'
                }
            }
        });
        validator.validate();
    }

    render(){
        return (
            <table class="form">
                <tr>
                <th>カードブランド</th>
                <td><GcComboBox
                    items= {['VISA', 'MasterCard', 'JCB', 'AmericanExpress', 'Diners Club']}
                    watermarkDisplayNullText= {'選択してください。'}
                    isEditable= {false}
                    exitOnLeftRightKey= {InputMan.ExitOnLeftRightKey.Both}
                    exitOnEnterKey= {InputMan.ExitKey.Both}
                    onInitialized={(imCtrl) => { 
                        this.cardType = imCtrl;
                        imCtrl.addEventListener(InputMan.GcComboBoxEvent.TextChanged, (control, args) => {
                            cardNumber.formatPattern = cardNoFormats[imCtrl.displayText()];
                        });
                     }}></GcComboBox></td>
                </tr>
                <tr>
                <th>カード番号</th>
                <td><GcMask
                    watermarkDisplayNullText= {'半角数字を入力してください。'}
                    formatPattern= {cardNoFormats.VISA}
                    exitOnLastChar= {true}
                    exitOnLeftRightKey= {InputMan.ExitOnLeftRightKey.Both}
                    exitOnEnterKey= {InputMan.ExitKey.Both}
                    onInitialized={(imCtrl) => { 
                        this.cardNumber = imCtrl;
                     }}></GcMask></td>
                </tr>
                <tr>
                <th>カード名義</th>
                <td><GcTextBox
                    watermarkDisplayNullText= {'半角英字を入力してください。'}
                    format= {'AS'}
                    exitOnLeftRightKey= {InputMan.ExitOnLeftRightKey.Both}
                    exitOnEnterKey= {InputMan.ExitKey.Both}
                    onInitialized={(imCtrl) => { 
                        this.ownerName = imCtrl;
                     }}></GcTextBox></td>
                </tr>
                <tr>
                <th>有効期限</th>
                <td><GcDateTime
                    watermarkDisplayNullText= {'年月を入力してください。'}
                    formatPattern= {'yyyy年MM月'}
                    displayFormatPattern= {'MM/yy'}
                    exitOnLastChar= {true}
                    exitOnLeftRightKey= {InputMan.ExitOnLeftRightKey.Both}
                    exitOnEnterKey= {InputMan.ExitKey.Both}
                    showDropDownButton= {true}
                    dropDownConfig={{
                        dropDownType: InputMan.DateDropDownType.Calendar,
                        calendarType: InputMan.CalendarType.YearMonth
                    }}
                    minDate= {new Date(thisYear, thisMonth, 1)}
                    maxDate= {new Date(thisYear + 5, 11, 31)}
                    value={null}
                    onInitialized={(imCtrl) => { 
                        this.expirationDate = imCtrl;
                     }}
                     ></GcDateTime></td>
                </tr>
                <tr>
                <th>セキュリティコード</th>
                <td><GcTextBox
                    watermarkDisplayNullText= {'3～4桁の数値です。'}
                    maxLength= {4}
                    format={'9'}
                    passwordChar={'*'}
                    passwordRevelationMode={InputMan.PasswordRevelationMode.ShowLastTypedChar}
                    exitOnLastChar= {true}
                    exitOnLeftRightKey= {InputMan.ExitOnLeftRightKey.Both}
                    exitOnEnterKey= {InputMan.ExitKey.Both}
                    onInitialized={(imCtrl) => { 
                        this.securityCode = imCtrl;
                     }}></GcTextBox></td>
                </tr>
          </table>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));