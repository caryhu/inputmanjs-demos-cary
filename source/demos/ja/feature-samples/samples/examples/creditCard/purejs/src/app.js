﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';

// カードブランド
const cardType = new InputMan.GcComboBox(document.getElementById('cardType'), {
    items: ['VISA', 'MasterCard', 'JCB', 'AmericanExpress', 'Diners Club'],
    watermarkDisplayNullText: '選択してください。',
    isEditable: false,
    exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.Both,
    exitOnEnterKey: InputMan.ExitKey.Both
});
cardType.addEventListener(InputMan.GcComboBoxEvent.TextChanged, (control, args) => {
    cardNumber.formatPattern = cardNoFormats[cardType.displayText];
});

// カード番号のマスク書式
const cardNoFormats = {
    VISA: '\\D{4}-\\D{4}-\\D{4}-\\D{4}',
    MasterCard: '\\D{4}-\\D{4}-\\D{4}-\\D{4}',
    JCB: '\\D{4}-\\D{4}-\\D{4}-\\D{4}',
    AmericanExpress: '\\D{4}-\\D{6}-\\D{5}',
    'Diners Club': '\\D{4}-\\D{6}-\\D{4}'
};

// カード番号
const cardNumber = new InputMan.GcMask(document.getElementById('cardNumber'), {
    watermarkDisplayNullText: '半角数字を入力してください。',
    formatPattern: cardNoFormats.VISA,
    exitOnLastChar: true,
    exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.Both,
    exitOnEnterKey: InputMan.ExitKey.Both
});

// カード名義
const ownerName = new InputMan.GcTextBox(document.getElementById('ownerName'), {
    watermarkDisplayNullText: '半角英字を入力してください。',
    format: 'AS',
    exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.Both,
    exitOnEnterKey: InputMan.ExitKey.Both
});

// 有効期限
var today = new Date();
var thisYear = today.getFullYear();
var thisMonth = today.getMonth();
const expirationDate = new InputMan.GcDateTime(document.getElementById('expirationDate'), {
    watermarkDisplayNullText: '年月を入力してください。',
    formatPattern: 'yyyy年MM月',
    displayFormatPattern: 'MM/yy',
    exitOnLastChar: true,
    exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.Both,
    exitOnEnterKey: InputMan.ExitKey.Both,
    showDropDownButton: true,
    dropDownConfig: {
        dropDownType: InputMan.DateDropDownType.Calendar,
        calendarType: InputMan.CalendarType.YearMonth
    },
    minDate: new Date(thisYear, thisMonth, 1),
    maxDate: new Date(thisYear + 5, 11, 31)
});
expirationDate.value = null;

// セキュリティコード
const securityCode = new InputMan.GcTextBox(document.getElementById('securityCode'), {
    watermarkDisplayNullText: '3～4桁の数値です。',
    maxLength:4,
    format:'9',
    exitOnLastChar: true,
    exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.Both,
    exitOnEnterKey: InputMan.ExitKey.Both,
    passwordChar : '*',
    passwordRevelationMode:InputMan.PasswordRevelationMode.ShowLastTypedChar,
});
securityCode.addDropDownSoftKeyboard(null, {
    displayType: InputMan.DisplayType.Numeric
}, true);

// 検証処理
const validator = new InputMan.GcValidator({
    items: [
        // カードブランド
        {
            control: cardType,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required,
                    failMessage: 'カードブランドを選択してください'
                }
            ]
        },
        // カード番号
        {
            control: cardNumber,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required,
                    failMessage: 'カード番号を入力してください'
                }
            ]
        },
        // カード名義
        {
            control: ownerName,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required,
                    failMessage: '名前を入力してください'
                }
            ]
        },
        // 有効期限
        {
            control: expirationDate,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required,
                    failMessage: '日付を入力してください'
                }
            ]
        },
        // セキュリティコード
        {
            control: securityCode,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required,
                    failMessage: '数値を入力してください'
                }
            ]
        }
    ],
    defaultNotify: {
        tip: {
            template: '<div style="color:red;min-width:20rem;">{!message}</div>'
        }
    }
});
validator.validate();
