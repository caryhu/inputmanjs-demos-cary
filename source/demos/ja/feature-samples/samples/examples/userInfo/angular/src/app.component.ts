import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {

    public componentJSONStr:string;
    public idControlValue: number;
    public idExitOnLeftRightKey: GC.InputMan.ExitOnLeftRightKey = GC.InputMan.ExitOnLeftRightKey.Both;
    public idExitOnEnterKey: GC.InputMan.ExitKey = GC.InputMan.ExitKey.Both;

    public fullnameControlValue: string;
    public fullnameIMEReadingStringKanaMode: GC.InputMan.KanaMode = GC.InputMan.KanaMode.Katakana;
    public fullnameExitOnLeftRightKey: GC.InputMan.ExitOnLeftRightKey = GC.InputMan.ExitOnLeftRightKey.Both;
    public fullnameExitKey: GC.InputMan.ExitKey = GC.InputMan.ExitKey.Both;

    public kanaControlValue: string;
    public kanaExitOnLeftRightKey: GC.InputMan.ExitOnLeftRightKey = GC.InputMan.ExitOnLeftRightKey.Both;
    public kanaExitOnEnterKey: GC.InputMan.ExitKey = GC.InputMan.ExitKey.Both;

    public postalControlValue: string;
    public postalExitOnLeftRightKey: GC.InputMan.ExitOnLeftRightKey = GC.InputMan.ExitOnLeftRightKey.Both;
    public postalExitOnEnterKey: GC.InputMan.ExitKey = GC.InputMan.ExitKey.Both;

    public addressControlValue: string;
    public addressExitOnLeftRightKey: GC.InputMan.ExitOnLeftRightKey = GC.InputMan.ExitOnLeftRightKey.Both;
    public addressExitOnEnterKey: GC.InputMan.ExitKey = GC.InputMan.ExitKey.Both;

    public telControlValue: string;
    public telExitOnLeftRightKey: GC.InputMan.ExitOnLeftRightKey = GC.InputMan.ExitOnLeftRightKey.Both;
    public telExitOnEnterKey: GC.InputMan.ExitKey = GC.InputMan.ExitKey.Both;

    public birthdayControlValue: Date = null;
    public birthdayExitOnLeftRightKey: GC.InputMan.ExitOnLeftRightKey = GC.InputMan.ExitOnLeftRightKey.Both;
    public birthdayExitOnEnterKey: GC.InputMan.ExitKey = GC.InputMan.ExitKey.Both;
    public birthdayDropDownConfig: object = {
        dropDownType: GC.InputMan.DateDropDownType.Calendar
    }

    public departmentControlValue: string;
    public departmentItems: Array<string> = ['総務部', '企画部', '情報システム部'];
    public departmentExitOnLeftRightKey: GC.InputMan.ExitOnLeftRightKey = GC.InputMan.ExitOnLeftRightKey.Both;
    public departmentlExitOnEnterKey: GC.InputMan.ExitKey = GC.InputMan.ExitKey.Both;

    public extensionControlValue: number;
    public extensionExitOnLeftRightKey: GC.InputMan.ExitOnLeftRightKey = GC.InputMan.ExitOnLeftRightKey.Both;
    public extensionExitOnEnterKey: GC.InputMan.ExitKey = GC.InputMan.ExitKey.Both;

    public noteControlValue: string;

    public departmentControl: GC.InputMan.GcComboBox;

    public onInitialized(gcCombBox: GC.InputMan.GcComboBox): void {
        this.departmentControl = gcCombBox;
    }

    public updateIMEReadingString(sender: any) {
        this.kanaControlValue = sender.eArgs.readingString;
    }

    public getJSONString(): void {
        var obj = {
            '社員番号': this.idControlValue,
            '氏名漢字': this.fullnameControlValue,
            'フリガナ': this.kanaControlValue,
            '郵便番号': this.postalControlValue,
            '住所': this.addressControlValue,
            '電話番号': this.telControlValue,
            '生年月日': this.birthdayControlValue,
            '部署名': this.departmentControlValue,
            '内線': this.extensionControlValue,
            '通信欄': this.noteControlValue
        };
        this.componentJSONStr = JSON.stringify(obj, null, 2);
    }


}


enableProdMode();