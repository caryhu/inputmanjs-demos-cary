﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';

// 社員番号
const id = new InputMan.GcNumber(document.getElementById('id'), {
    formatDigit: '00000',
    displayFormatDigit: '00000',
    showNumericPad: true,
    exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.Both,
    exitOnEnterKey: InputMan.ExitKey.Both
});

// 氏名漢字
const fullname = new InputMan.GcTextBox(document.getElementById('fullname'), {
    IMEReadingStringKanaMode: InputMan.KanaMode.Katakana,
    exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.Both,
    exitOnEnterKey: InputMan.ExitKey.Both
});
fullname.onIMEReadingStringOutput((sender, args) => {
    kana.text = args.readingString;
});

// フリガナ
const kana = new InputMan.GcTextBox(document.getElementById('kana'), {
    exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.Both,
    exitOnEnterKey: InputMan.ExitKey.Both
});

// 郵便番号
const postal = new InputMan.GcMask(document.getElementById('postal'), {
    formatPattern: '〒\\D{3}-\\D{4}',
    exitOnLastChar: true,
    exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.Both,
    exitOnEnterKey: InputMan.ExitKey.Both
});

// 住所
const address = new InputMan.GcTextBox(document.getElementById('address'), {
    exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.Both,
    exitOnEnterKey: InputMan.ExitKey.Both
});

// 電話番号
const tel = new InputMan.GcMask(document.getElementById('tel'), {
    formatPattern: '\\D{2,4}-\\D{2,4}-\\D{4}',
    exitOnLastChar: true,
    exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.Both,
    exitOnEnterKey: InputMan.ExitKey.Both
});

// 生年月日
const birthday = new InputMan.GcDateTime(document.getElementById('birthday'), {
    formatPattern: 'yyy/M/d',
    displayFormatPattern: '',
    exitOnLastChar: true,
    exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.Both,
    exitOnEnterKey: InputMan.ExitKey.Both,
    value: null,
    showDropDownButton: true,
    dropDownConfig: {
        dropDownType: InputMan.DateDropDownType.Calendar
    }
});

// 部署名
const department = new InputMan.GcComboBox(document.getElementById('department'), {
    items: ['総務部', '企画部', '情報システム部'],
    exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.Both,
    exitOnEnterKey: InputMan.ExitKey.Both
});

// 内線
const extension = new InputMan.GcNumber(document.getElementById('extension'), {
    formatDigit: '000',
    displayFormatDigit: '000',
    showNumericPad: true,
    exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.Both,
    exitOnEnterKey: InputMan.ExitKey.Both
});

// 通信欄
const note = new InputMan.GcMultiLineTextBox(document.getElementById('note'));

// JSONデータを取得
const fields = [
    { name: '社員番号', control: id },
    { name: '氏名漢字', control: fullname },
    { name: 'フリガナ', control: kana },
    { name: '郵便番号', control: postal },
    { name: '住所', control: address },
    { name: '電話番号', control: tel },
    { name: '生年月日', control: birthday },
    { name: '部署名', control: department },
    { name: '内線', control: extension },
    { name: '通信欄', control: note }
];

document.getElementById('getData').addEventListener('click', () => {
    var user = {};
    for (var i = 0; i < fields.length; i++) {
        var name = fields[i].name;
        var control = fields[i].control;
        user[name] = name == '部署名' ? control.displayText : control.value;
    }
    document.getElementById('data').value = JSON.stringify(user, null, 2);
});