import * as React from "react";
import * as ReactDom from "react-dom";
import { GcNumber, GcTextBox, GcMask, GcComboBox, GcMultiLineTextBox, GcDateTime } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{
    //kana = null;
    controls = {};
    render(){
        return (
            <div>
                <table class="form">
                    <tr>
                        <th colspan="2">社員番号</th>
                        <td><GcNumber
                            formatDigit= {'00000'}
                            displayFormatDigit= {'00000'}
                            showNumericPad= {true}
                            exitOnLeftRightKey= {InputMan.ExitOnLeftRightKey.Both}
                            exitOnEnterKey= {InputMan.ExitKey.Both}
                            onInitialized={(imCtrl) => { 
                                this.controls.id = imCtrl;
                             }}></GcNumber></td>
                    </tr>
                    <tr>
                        <th rowspan="2">氏名</th>
                        <th>漢字</th>
                        <td><GcTextBox
                            imeReadingStringKanaMode= {InputMan.KanaMode.Katakana}
                            exitOnLeftRightKey= {InputMan.ExitOnLeftRightKey.Both}
                            exitOnEnterKey= {InputMan.ExitKey.Both}
                            onIMEReadingStringOutput={(sender, eArgs)=>{
                                this.controls.kana.text = eArgs.readingString;
                            }}
                            onInitialized={(imCtrl) => { 
                                this.controls.fullname = imCtrl;
                             }}></GcTextBox></td>
                    </tr>
                    <tr>
                        <th>フリガナ</th>
                        <td><GcTextBox     
                            exitOnLeftRightKey= {InputMan.ExitOnLeftRightKey.Both}
                            exitOnEnterKey= {InputMan.ExitKey.Both}
                            onInitialized={(imCtrl) => { 
                                this.controls.kana = imCtrl;
                             }}></GcTextBox></td>
                    </tr>
                    <tr>
                        <th rowspan="2">自宅住所</th>
                        <th>郵便番号</th>
                        <td><GcMask
                            formatPattern= {'〒\\D{3}-\\D{4}'}
                            exitOnLastChar= {true}
                            exitOnLeftRightKey= {InputMan.ExitOnLeftRightKey.Both}
                            exitOnEnterKey= {InputMan.ExitKey.Both}
                            onInitialized={(imCtrl) => { 
                                this.controls.postal = imCtrl;
                             }}></GcMask></td>
                    </tr>
                    <tr>
                        <th>住所</th>
                        <td><GcTextBox
                            className={'wide-input'}
                            exitOnLeftRightKey= {InputMan.ExitOnLeftRightKey.Both}
                            exitOnEnterKey= {InputMan.ExitKey.Both}
                            onInitialized={(imCtrl) => { 
                                this.controls.address = imCtrl;
                             }}></GcTextBox></td>
                    </tr>
                    <tr>
                        <th colspan="2">電話番号</th>
                        <td><GcMask
                            formatPattern= {'\\D{2,4}-\\D{2,4}-\\D{4}'}
                            exitOnLastChar= {true}
                            exitOnLeftRightKey= {InputMan.ExitOnLeftRightKey.Both}
                            exitOnEnterKey= {InputMan.ExitKey.Both}
                            onInitialized={(imCtrl) => { 
                                this.controls.tel = imCtrl;
                             }}>
                            </GcMask></td>
                    </tr>
                    <tr>
                        <th colspan="2">生年月日</th>
                        <td><GcDateTime
                            formatPattern= {'yyy/M/d'}
                            displayFormatPattern= {''}
                            exitOnLastChar= {true}
                            exitOnLeftRightKey= {InputMan.ExitOnLeftRightKey.Both}
                            exitOnEnterKey= {InputMan.ExitKey.Both}
                            value= {null}
                            showDropDownButton= {true}
                            dropDownConfig= {{
                                dropDownType: InputMan.DateDropDownType.Calendar
                            }}
                            onInitialized={(imCtrl) => { 
                                this.controls.birthday = imCtrl;
                             }}></GcDateTime></td>
                    </tr>
                    <tr>
                        <th rowspan="2">所属部署</th>
                        <th>部署名</th>
                        <td><GcComboBox
                            items= {['総務部', '企画部', '情報システム部']}
                            exitOnLeftRightKey= {InputMan.ExitOnLeftRightKey.Both}
                            exitOnEnterKey= {InputMan.ExitKey.Both}
                            onInitialized={(imCtrl) => { 
                                this.controls.department = imCtrl;
                             }}>
                            </GcComboBox></td>
                    </tr>
                    <tr>
                        <th>内線</th>
                        <td><GcNumber
                            formatDigit= {'000'}
                            displayFormatDigit= {'000'}
                            showNumericPad= {true}
                            exitOnLeftRightKey= {InputMan.ExitOnLeftRightKey.Both}
                            exitOnEnterKey= {InputMan.ExitKey.Both}
                            onInitialized={(imCtrl) => { 
                                this.controls.extension = imCtrl;
                             }}></GcNumber></td>
                    </tr>
                    <tr>
                        <th colspan="2">通信欄</th>
                        <td><GcMultiLineTextBox className={'wide-input'}
                        onInitialized={(imCtrl) => { 
                            this.controls.note = imCtrl;
                         }}></GcMultiLineTextBox></td>
                    </tr>
                </table><br/>
                <button id="getData">JSONデータを取得</button><br/>
                <textarea id="data" cols="60" rows="10"></textarea>
            </div>
        )
    }

    componentDidMount(){
        // JSONデータを取得
        const fields = [
            { name: '社員番号', control: this.controls.id },
            { name: '氏名漢字', control: this.controls.fullname },
            { name: 'フリガナ', control: this.controls.kana },
            { name: '郵便番号', control: this.controls.postal },
            { name: '住所', control: this.controls.address },
            { name: '電話番号', control: this.controls.tel },
            { name: '生年月日', control: this.controls.birthday },
            { name: '部署名', control: this.controls.department },
            { name: '内線', control: this.controls.extension },
            { name: '通信欄', control: this.controls.note }
        ];

        document.getElementById('getData').addEventListener('click', () => {
            var user = {};
            for (var i = 0; i < fields.length; i++) {
                var name = fields[i].name;
                var control = fields[i].control;
                user[name] = name == '部署名' ? control.displayText : control.value;
            }
            document.getElementById('data').value = JSON.stringify(user, null, 2);
        });
    }
}

ReactDom.render(<App />, document.getElementById("app"));