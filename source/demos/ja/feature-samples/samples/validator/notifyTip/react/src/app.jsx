import * as React from "react";
import * as ReactDom from "react-dom";
import { GcTextBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

const tipDirections = [
    InputMan.TipDirection.Top,
    InputMan.TipDirection.Bottom,
    InputMan.TipDirection.Left,
    InputMan.TipDirection.Right
];
const tipPositions = [
    InputMan.TipPosition.Start,
    InputMan.TipPosition.Balanced,
    InputMan.TipPosition.End
];

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            template: false
        };
    }

    componentDidMount(){
        this.updateValidator();
    }

    componentDidUpdate(){
        this.updateValidator();
    }

    updateValidator(){
        if(this.validator){
            this.validator.destroy();
        }
        this.validator = new InputMan.GcValidator({
            items: [
                {
                    control: this.refs.gcTextBox.getNestedIMControl(),
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ],
                }
            ],
            defaultNotify: {
                tip: {
                    direction: this.state.direction,
                    position: this.state.position,
                    template: this.state.template ? '<div style="color:red;min-width:10.5rem;">{!message}</div>' : ''
                }
            }
        });
        this.validator.validate();
    }

    render(){
        return (
            <div>
                <GcTextBox ref="gcTextBox"></GcTextBox>
                <table class="sample">
                    <tr>
                        <th>通知の方向</th>
                        <td>
                            <select id="direction" class="update" onChange={(event) => { this.setState({ direction: tipDirections[event.target.selectedIndex] })}}>
                                <option>上</option>
                                <option>下</option>
                                <option>左</option>
                                <option selected>右</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>通知の配置</th>
                        <td>
                            <select id="position" class="update" onChange={(event) => { this.setState({ position: tipPositions[event.target.selectedIndex] })}}>
                                <option>先頭</option>
                                <option selected>中央</option>
                                <option>末尾</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>通知のテンプレート</th>
                        <td>
                            <label><input type="checkbox" id="template" class="update" checked={this.state.template} onChange={(event)=>this.setState({template: event.target.checked})}/>テンプレートを使って表示</label><br/>
                        </td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));