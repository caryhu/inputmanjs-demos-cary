﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcTextBox = new InputMan.GcTextBox(document.getElementById('gcTextBox'));

document.getElementById('direction').addEventListener('change', (e) => {
    update();
});

document.getElementById('position').addEventListener('change', (e) => {
    update();
});

var validator;
const update = () => {
    if (validator) {
        validator.destroy();
    }
    validator = new GCIM.GcValidator({
        items: [
            {
                control: gcTextBox,
                ruleSet: [
                    {
                        rule: InputMan.ValidateType.Required
                    }
                ],
            }
        ],
        defaultNotify: {
            tip: {
                direction: tipDirections[document.getElementById('direction').selectedIndex],
                position: tipPositions[document.getElementById('position').selectedIndex],
                template: document.getElementById('template').checked ? '<div style="color:red;min-width:10.5rem;">{!message}</div>' : ''
            }
        }
    });
    validator.validate();
}

const tipDirections = [
    InputMan.TipDirection.Top,
    InputMan.TipDirection.Bottom,
    InputMan.TipDirection.Left,
    InputMan.TipDirection.Right
];
const tipPositions = [
    InputMan.TipPosition.Start,
    InputMan.TipPosition.Balanced,
    InputMan.TipPosition.End
];

update();

document.getElementById('template').addEventListener('change', update);