import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { Component, enableProdMode, AfterViewInit } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent implements AfterViewInit {
    public tipDirections = [
        GC.InputMan.TipDirection.Top,
        GC.InputMan.TipDirection.Bottom,
        GC.InputMan.TipDirection.Left,
        GC.InputMan.TipDirection.Right
    ];
    public tipPositions = [
        GC.InputMan.TipPosition.Start,
        GC.InputMan.TipPosition.Balanced,
        GC.InputMan.TipPosition.End
    ];
    private validator: GC.InputMan.GcValidator;
    public gcTextBox: GC.InputMan.GcTextBox;
    public direction: GC.InputMan.TipDirection;
    public position: GC.InputMan.TipPosition;
    public template: boolean;

    ngAfterViewInit(): void {
        this.update();
    }

    public update() {
        if (this.validator) {
            this.validator.destroy();
        }
        this.validator = new GC.InputMan.GcValidator({
            items: [
                {
                    control: this.gcTextBox,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ],
                }
            ],
            defaultNotify: {
                tip: {
                    direction: this.direction,
                    position: this.position,
                    template: this.template ? '<div style="color:red;min-width:10.5rem;">{!message}</div>' : ''
                }
            }
        });
        this.validator.validate();
    }
}


enableProdMode();