﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

document.getElementById('gcTextBox').className = 'margin';
const gcTextBox = new InputMan.GcTextBox(document.getElementById('gcTextBox'));

const iconDirections = [
    InputMan.IconDirection.Inside,
    InputMan.IconDirection.Outside
];

document.getElementById('direction').addEventListener('change', (e) => {
    update();
});

var validator;
const update = () => {
    if (validator) {
        validator.destroy();
    }
    validator = new GCIM.GcValidator({
        items: [
            {
                control: gcTextBox,
                ruleSet: [
                    {
                        rule: InputMan.ValidateType.Required
                    }
                ],
            }
        ],
        defaultNotify: false ? null : {
            icon: {
                direction: iconDirections[document.getElementById('direction').selectedIndex],
                failIconSrc: document.getElementById('iconSrc').checked ? 'images/exclamation.png' : ''
            }
        }
    });
    validator.validate();
}
update();

document.getElementById('iconSrc').addEventListener('change', update);