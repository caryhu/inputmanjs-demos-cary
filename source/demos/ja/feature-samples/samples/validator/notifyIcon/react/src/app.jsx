import * as React from "react";
import * as ReactDom from "react-dom";
import { GcTextBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

const iconDirections = [
    InputMan.IconDirection.Inside,
    InputMan.IconDirection.Outside
];

class App extends React.Component{

    constructor(props, context) {
        super(props, context);
        this.state = {
            src: false
        };
    }

    componentDidMount(){
        this.updateValidator();
    }

    componentDidUpdate(){
        this.updateValidator();
    }

    updateValidator(){
        if(this.validator){
            this.validator.destroy();
        }
        this.validator = new InputMan.GcValidator({
            items: [
                {
                    control: this.refs.gcTextBox.getNestedIMControl(),
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ],
                }
            ],
            defaultNotify: false ? null : {
                icon: {
                    direction: this.state.direction,
                    failIconSrc: this.state.src ? 'images/exclamation.png' : ''
                }
            }
        });
        this.validator.validate();
    }

    render(){
        return (
            <div>
                <GcTextBox ref="gcTextBox"></GcTextBox>
                <table class="sample">
                    <tr>
                        <th>通知の位置</th>
                        <td>
                            <select id="direction" class="update" onChange={(event) => { this.setState({ direction: iconDirections[event.target.selectedIndex] })}}>
                                <option>内側</option>
                                <option selected>外側</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>カスタムアイコン</th>
                        <td>
                            <label><input type="checkbox" id="iconSrc" class="update" checked={this.state.src} onChange={(event)=>this.setState({src: event.target.checked})}/>カスタムアイコンを表示</label><br/>
                        </td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));