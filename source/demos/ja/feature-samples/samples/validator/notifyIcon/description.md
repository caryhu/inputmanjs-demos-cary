検証コントロールのアイコン通知機能について解説します。

## 概要

notify（またはdefaultNotify）プロパティのiconプロパティを使用することで、検証結果に応じたアイコンの表示可否（boolean）を設定できますが、このiconプロパティには、アイコンの配置位置や表示するアイコン画像のカスタマイズなどが可能です。

## 通知の位置

アイコン通知の表示位置は、directionプロパティから設定可能です。設定可能な値（IconDirection列挙体）は、以下の通りです。

| 値                            | 説明                           |
| ---------------------------- | ---------------------------- |
| Inside                       | 通知アイコンをコントロールの内側に表示します。      |
| Outside                      | 通知アイコンをコントロールの外側に表示します。（既定値） |

たとえば、通知アイコンをコントロールの外側に表示したい場合のサンプルコードは、以下の通りです。（iconプロパティの設定部分のみ抜粋）

```
icon: {
        direction: GC.InputMan.IconDirection.Outside
}
```

## カスタムアイコン

以下の各プロパティを使用することで、アイコン通知で表示するアイコン画像を設定できます。これらのプロパティには画像リソースへのパスを設定します。

| プロパティ名                | 説明                    |
| --------------------- | --------------------- |
| failIconSrc           | 検証が失敗したときのアイコンを設定します。 |
| successIconSrc        | 検証が成功したときのアイコンを設定します。 |

なお、successIconSrcプロパティを設定する場合、successプロパティ内にアイコン通知の設定を含める必要があります。successプロパティを省略して、notify（またはdefaultNotify）プロパティ内に直接successIconSrcプロパティを設定しても、有効に動作しません。また、failIconSrcプロパティをsuccessプロパティ内で設定しても、有効に動作しません。

検証成功時には独自の通知アイコンをコントロールの外側に、検証失敗時には独自の通知アイコンをコントロールの内側に表示する場合のサンプルコードは、以下の通りです。（iconプロパティの設定部分のみ抜粋）

```
fail: {
  icon: {
    direction: GC.InputMan.IconDirection.Inside,
    failIconSrc: '/images/fail.png'
  }
},
success: {
  icon: {
    direction: GC.InputMan.IconDirection.Outside,
    successIconSrc: '/images/success.png'
  }
}
```

