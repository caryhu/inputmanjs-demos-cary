import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, AfterViewInit } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent implements AfterViewInit {
    private validator: GC.InputMan.GcValidator;
    public iconDirections = [
        GC.InputMan.IconDirection.Inside,
        GC.InputMan.IconDirection.Outside
    ];
    public direction: GC.InputMan.IconDirection;
    public failIconSrc: boolean;
    public gcTextBox: GC.InputMan.GcTextBox;
    
    ngAfterViewInit(): void {
        this.update();
    }

    public update() {
        if (this.validator) {
            this.validator.destroy();
        }
        this.validator = new GC.InputMan.GcValidator({
            items: [
                {
                    control: this.gcTextBox,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ],
                }
            ],
            defaultNotify: false ? null : {
                icon: {
                    direction: this.direction,
                    failIconSrc: this.failIconSrc ? 'images/exclamation.png' : ''
                }
            }
        });
        this.validator.validate();
    }
}


enableProdMode();