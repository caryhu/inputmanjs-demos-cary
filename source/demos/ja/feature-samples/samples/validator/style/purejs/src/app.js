﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcTextBox1 = new InputMan.GcTextBox(document.getElementById('gcTextBox1'));
const gcTextBox2 = new InputMan.GcTextBox(document.getElementById('gcTextBox2'), {
    text: 'テキスト'
});

const validator = new GCIM.GcValidator({
    items: [
        {
            control: gcTextBox1,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required
                }
            ],
        },
        {
            control: gcTextBox2,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required
                }
            ],
        }
    ],
    defaultNotify: {
        fail: {
            tip: {
                direction: InputMan.TipDirection.Bottom
            },
            icon: {
                direction: InputMan.IconDirection.Inside
            },
            controlState: true
        },
        success: {
            tip: {
                direction: InputMan.TipDirection.Bottom
            },
            icon: {
                direction: InputMan.IconDirection.Inside
            },
            controlState: true
        }
    }
});
validator.validate();

var styleSheet;
const indices = {};
const styles = {
    '.gcim-notify__tip-content': { width: '200px', height: '40px', borderRadius: '12px', boxShadow: '5px 5px 5px rgba(0,0,0,0.5)', fontWeight: 'bold', fontStyle: 'italic', fontFamily: 'serif', textAlign: 'right', textShadow: '1px 1px 1px rgba(0,0,0,0.5)' },
    '.gcim-notify__tip-text': { fontSize: '20px' },
    "[gcim-notify__tip-state='fail'] .gcim-notify__tip-content": { backgroundColor: '#ffdddd', color: '#ff0000', borderColor: '#ff9900', borderWidth: '2px', borderStyle: 'dashed' },
    "[gcim-notify__tip-state='success'] .gcim-notify__tip-content": { backgroundColor: '#ddddff', color: '#0000ff', borderColor: '#0099ff', borderWidth: '2px', borderStyle: 'dashed' },
    ".gcim-notify__state-container[gcim-notify__control-state='fail']": { borderColor: '#ff9999', borderWidth: '2px', borderStyle: 'dashed' },
    ".gcim-notify__state-container[gcim-notify__control-state='success']": { borderColor: '#99ff99', borderWidth: '2px', borderStyle: 'dashed' },
};


const createInitialStyles = () => {
    const element = document.createElement('style');
    document.head.appendChild(element);
    styleSheet = element.sheet;
    var i = 0;
    for (const styleName in styles) {
        styleSheet.insertRule(styleName + '{}', i);
        indices[styleName] = i;
        i++;
    }
}
createInitialStyles();

const updateStyle = (event) => {
    var element = event.target;
    if (element.tagName == 'INPUT') {
        var values = element.value.split(',');
        var styleName = values[0];
        var propertyName = values[1];
        styleSheet.cssRules[indices[styleName]].style[propertyName] = element.checked ? styles[styleName][propertyName] : '';
    }

    var style = '';
    for (var i = 0; i < styleSheet.cssRules.length; i++) {
        if (styleSheet.cssRules[i].style.length > 0) {
            style += styleSheet.cssRules[i].cssText.replace(/{/g, '{\n ').replace(/;/g, ';\n ').replace(/  }/g, '}') + '\n';
        }
    }
    document.getElementById('style').value = style;
}

const copyStyle = () => {
    wijmo.Clipboard.copy(document.getElementById('style').value);
    document.execCommand('copy');
}

var panels = document.getElementsByClassName('peoperty-panel');
for (var i = 0; i < panels.length; i++) {
    panels[i].addEventListener('click', updateStyle);
}
document.getElementById('copyStyle').addEventListener('click', copyStyle);