import * as React from "react";
import * as ReactDom from "react-dom";
import { GcTextBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

var styleSheet;
var indices = [];
var styles = {
    '.gcim-notify__tip-content': { width: '200px', height: '40px', borderRadius: '12px', boxShadow: '5px 5px 5px rgba(0,0,0,0.5)', fontWeight: 'bold', fontStyle: 'italic', fontFamily: 'serif', textAlign: 'right', textShadow: '1px 1px 1px rgba(0,0,0,0.5)' },
    '.gcim-notify__tip-text': { fontSize: '20px' },
    "[gcim-notify__tip-state='fail'] .gcim-notify__tip-content": { backgroundColor: '#ffdddd', color: '#ff0000', borderColor: '#ff9900', borderWidth: '2px', borderStyle: 'dashed' },
    "[gcim-notify__tip-state='success'] .gcim-notify__tip-content": { backgroundColor: '#ddddff', color: '#0000ff', borderColor: '#0099ff', borderWidth: '2px', borderStyle: 'dashed' },
    ".gcim-notify__state-container[gcim-notify__control-state='fail']": { borderColor: '#ff9999', borderWidth: '2px', borderStyle: 'dashed' },
    ".gcim-notify__state-container[gcim-notify__control-state='success']": { borderColor: '#99ff99', borderWidth: '2px', borderStyle: 'dashed' },
};
class App extends React.Component{

    createInitialStyles() {
        const element = document.createElement('style');
        document.head.appendChild(element);
        styleSheet = element.sheet;
        var i = 0;
        for (const styleName in styles) {
            styleSheet.insertRule(styleName + '{}', i);
            indices[styleName] = i;
            i++;
        }
    }

    updateStyle () {
        var element = event.target;
        if (element.tagName == 'INPUT') {
            var values = element.value.split(',');
            var styleName = values[0];
            var propertyName = values[1];
            styleSheet.cssRules[indices[styleName]].style[propertyName] = element.checked ? styles[styleName][propertyName] : '';
        }
    
        var style = '';
        for (var i = 0; i < styleSheet.cssRules.length; i++) {
            if (styleSheet.cssRules[i].style.length > 0) {
                style += styleSheet.cssRules[i].cssText.replace(/{/g, '{\n ').replace(/;/g, ';\n ').replace(/  }/g, '}') + '\n';
            }
        }
        document.getElementById('style').value = style;
    }

    copyStyle(){
        wijmo.Clipboard.copy(document.getElementById('style').value);
        document.execCommand('copy');
    }

    componentDidMount(){
        this.createInitialStyles();
        var panels = document.getElementsByClassName('peoperty-panel');
        for (var i = 0; i < panels.length; i++) {
            panels[i].addEventListener('click', this.updateStyle);
        }
        document.getElementById('copyStyle').addEventListener('click', this.copyStyle);

        const validator = new InputMan.GcValidator({
            items: [
                {
                    control: this.refs.gcTextBox1.getNestedIMControl(),
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ],
                },
                {
                    control: this.refs.gcTextBox2.getNestedIMControl(),
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ],
                }
            ],
            defaultNotify: {
                fail: {
                    tip: {
                        direction: InputMan.TipDirection.Bottom
                    },
                    icon: {
                        direction: InputMan.IconDirection.Inside
                    },
                    controlState: true
                },
                success: {
                    tip: {
                        direction: InputMan.TipDirection.Bottom
                    },
                    icon: {
                        direction: InputMan.IconDirection.Inside
                    },
                    controlState: true
                }
            }
        });
        validator.validate();
    }

    render(){
        return (
            <div>
                <div class="flexbox">
                    <div>
                        検証失敗の通知<br/>
                        <GcTextBox ref="gcTextBox1"/>
                    </div>
                    <div>
                        検証成功の通知<br/>
                        <GcTextBox ref="gcTextBox2" text= {'テキスト'}/>
                    </div>
                </div><br/><br/><br/>
            
                <div class="peoperty-header">ツールチップのスタイル</div>
                <div class="peoperty-panel" >
                    <label><input type="checkbox" value=".gcim-notify__tip-content,width"/>幅</label><br/>
                    <label><input type="checkbox" value=".gcim-notify__tip-content,height"/>高さ</label><br/>
                    <label><input type="checkbox" value=".gcim-notify__tip-content,borderRadius"/>境界線の角丸</label><br/>
                    <label><input type="checkbox" value=".gcim-notify__tip-content,boxShadow"/>影</label><br/>
                    <label><input type="checkbox" value=".gcim-notify__tip-text,fontSize"/>フォントのサイズ</label><br/>
                    <label><input type="checkbox" value=".gcim-notify__tip-content,fontWeight"/>フォントの太さ</label><br/>
                    <label><input type="checkbox" value=".gcim-notify__tip-content,fontStyle"/>フォントのスタイル</label><br/>
                    <label><input type="checkbox" value=".gcim-notify__tip-content,fontFamily"/>フォントの種類</label><br/>
                    <label><input type="checkbox" value=".gcim-notify__tip-content,textAlign"/>水平方向の位置</label><br/>
                    <label><input type="checkbox" value=".gcim-notify__tip-content,textShadow"/>テキストの影</label><br/>
                </div>
                <div class="peoperty-header">検証失敗のツールチップのスタイル</div>
                <div class="peoperty-panel" >
                    <label><input type="checkbox" value="[gcim-notify__tip-state='fail'] .gcim-notify__tip-content,backgroundColor"/>背景色</label><br/>
                    <label><input type="checkbox" value="[gcim-notify__tip-state='fail'] .gcim-notify__tip-content,color"/>文字色</label><br/>
                    <label><input type="checkbox" value="[gcim-notify__tip-state='fail'] .gcim-notify__tip-content,borderColor"/>境界線の色</label><br/>
                    <label><input type="checkbox" value="[gcim-notify__tip-state='fail'] .gcim-notify__tip-content,borderWidth"/>境界線の幅</label><br/>
                    <label><input type="checkbox" value="[gcim-notify__tip-state='fail'] .gcim-notify__tip-content,borderStyle"/>境界線のスタイル</label><br/>
                </div>
                <div class="peoperty-header">検証成功のツールチップのスタイル</div>
                <div class="peoperty-panel" >
                    <label><input type="checkbox" value="[gcim-notify__tip-state='success'] .gcim-notify__tip-content,backgroundColor"/>背景色</label><br/>
                    <label><input type="checkbox" value="[gcim-notify__tip-state='success'] .gcim-notify__tip-content,color"/>文字色</label><br/>
                    <label><input type="checkbox" value="[gcim-notify__tip-state='success'] .gcim-notify__tip-content,borderColor"/>境界線の色</label><br/>
                    <label><input type="checkbox" value="[gcim-notify__tip-state='success'] .gcim-notify__tip-content,borderWidth"/>境界線の幅</label><br/>
                    <label><input type="checkbox" value="[gcim-notify__tip-state='success'] .gcim-notify__tip-content,borderStyle"/>境界線のスタイル</label><br/>
                </div>
                <div class="peoperty-header">検証失敗のコントロール状態のスタイル</div>
                <div class="peoperty-panel" >
                    <label><input type="checkbox" value=".gcim-notify__state-container[gcim-notify__control-state='fail'],borderColor"/>境界線の色</label><br/>
                    <label><input type="checkbox" value=".gcim-notify__state-container[gcim-notify__control-state='fail'],borderWidth"/>境界線の幅</label><br/>
                    <label><input type="checkbox" value=".gcim-notify__state-container[gcim-notify__control-state='fail'],borderStyle"/>境界線のスタイル</label><br/>
                </div>
                <div class="peoperty-header">検証成功のコントロール状態のスタイル</div>
                <div class="peoperty-panel" >
                    <label><input type="checkbox" value=".gcim-notify__state-container[gcim-notify__control-state='success'],borderColor"/>境界線の色</label><br/>
                    <label><input type="checkbox" value=".gcim-notify__state-container[gcim-notify__control-state='success'],borderWidth"/>境界線の幅</label><br/>
                    <label><input type="checkbox" value=".gcim-notify__state-container[gcim-notify__control-state='success'],borderStyle"/>境界線のスタイル</label><br/>
                </div>
                <button id="copyStyle">CSSコードをコピー</button><br/>
                <textarea id="style" cols="60" rows="10"></textarea>
        </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));