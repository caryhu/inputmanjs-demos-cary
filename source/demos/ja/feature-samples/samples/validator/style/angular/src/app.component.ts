import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, AfterViewInit, OnInit } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent implements AfterViewInit, OnInit {
    public gcTextBox1: GC.InputMan.GcTextBox;
    public gcTextBox2: GC.InputMan.GcTextBox;
    public styleSheet: CSSStyleSheet;
    public indices: Array<number> = [];
    public styles = {
        '.gcim-notify__tip-content': { width: '200px', height: '40px', borderRadius: '12px', boxShadow: '5px 5px 5px rgba(0,0,0,0.5)', fontWeight: 'bold', fontStyle: 'italic', fontFamily: 'serif', textAlign: 'right', textShadow: '1px 1px 1px rgba(0,0,0,0.5)' },
        '.gcim-notify__tip-text': { fontSize: '20px' },
        "[gcim-notify__tip-state='fail'] .gcim-notify__tip-content": { backgroundColor: '#ffdddd', color: '#ff0000', borderColor: '#ff9900', borderWidth: '2px', borderStyle: 'dashed' },
        "[gcim-notify__tip-state='success'] .gcim-notify__tip-content": { backgroundColor: '#ddddff', color: '#0000ff', borderColor: '#0099ff', borderWidth: '2px', borderStyle: 'dashed' },
        ".gcim-notify__state-container[gcim-notify__control-state='fail']": { borderColor: '#ff9999', borderWidth: '2px', borderStyle: 'dashed' },
        ".gcim-notify__state-container[gcim-notify__control-state='success']": { borderColor: '#99ff99', borderWidth: '2px', borderStyle: 'dashed' },
    };
    public style: string;

    ngOnInit(): void {
        this.createInitialStyles();
    }

    ngAfterViewInit(): void {
        this.createValidator();
    }

    public createValidator() {
        const validator = new GC.InputMan.GcValidator({
            items: [
                {
                    control: this.gcTextBox1,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ],
                },
                {
                    control: this.gcTextBox2,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ],
                }
            ],
            defaultNotify: {
                fail: {
                    tip: {
                        direction: GC.InputMan.TipDirection.Bottom
                    },
                    icon: {
                        direction: GC.InputMan.IconDirection.Inside
                    },
                    controlState: true
                },
                success: {
                    tip: {
                        direction: GC.InputMan.TipDirection.Bottom
                    },
                    icon: {
                        direction: GC.InputMan.IconDirection.Inside
                    },
                    controlState: true
                }
            }
        });
        validator.validate();
    }

    public createInitialStyles() {
        const element = document.createElement('style');
        document.head.appendChild(element);
        this.styleSheet = element.sheet as CSSStyleSheet;
        var i = 0;
        for (const styleName in this.styles) {
            this.styleSheet.insertRule(styleName + '{}', i);
            this.indices[styleName] = i;
            i++;
        }
    }

    public updateStyle(event: Event) {
        var element = event.target;
        if ((element as HTMLInputElement).tagName == 'INPUT') {
            var values = (element as HTMLInputElement).value.split(',');
            var styleName = values[0];
            var propertyName = values[1];
            (this.styleSheet.cssRules[this.indices[styleName]] as CSSStyleRule).style[propertyName] = (element as HTMLInputElement).checked ? this.styles[styleName][propertyName] : '';
        }

        var style = '';
        for (var i = 0; i < this.styleSheet.cssRules.length; i++) {
            if (((this.styleSheet.cssRules[i] as CSSStyleRule).style).length > 0) {
                style += this.styleSheet.cssRules[i].cssText.replace(/{/g, '{\n ').replace(/;/g, ';\n ').replace(/  }/g, '}') + '\n';
            }
        }
        this.style = style;
    }

    public copyStyle = () => {
        (window as any).wijmo.Clipboard.copy(this.style);
        document.execCommand('copy');
    }
}


enableProdMode();