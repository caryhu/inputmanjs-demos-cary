検証コントロールによる検証規則（検証ルール）について解説します。

## 概要

検証規則は、検証コントロールのインスタンスを初期化する際にruleSetプロパティを使用して設定します。検証規則を設定するruleプロパティには、既定の規則と独自の検証規則を設定することが可能です。なお、ruleSetプロパティには検証成功時のメッセージ（successMessageプロパティ）と検証失敗時のメッセージ（failMessageプロパティ）も設定することが可能です。これらのメッセージのデフォルト設定は、以下の通りです。

| 検証結果         | メッセージ        |
| ------------ | ------------ |
| 検証成功         | 入力値が正常です     |
| 検証失敗         | 入力値が正しくありません |

## 既定の検証規則

検証コントロールにあらかじめ定義された既定の検証規則は、以下の２つです。

| 検証規則                                                  | 説明                                                    |
| ----------------------------------------------------- | ----------------------------------------------------- |
| OutOfRange                                            | 値が最大値と最小値の範囲内に収まっているかどうか検証します。範囲外の値が入力された場合は検証に失敗します。 |
| Required                                              | コントロールに値が入力されているかどうか検証します。値が空の場合は検証に失敗します。            |

なお、OutOfRangeが有効に動作する入力コントロールは、最大値と最小値を設定可能なコントロール（GcNumber、GcDateTime）のみとなります。また、検証対象コントロールのmaxMinBehaviorプロパティは"Keep"に設定されている必要があります。

たとえば、日付時刻コントロールに対して範囲チェックを行う場合、以下のようなサンプルコードになります。

```
// 日付時刻コントロール
var gcDateTime1 = new GC.InputMan.GcDateTime(document.getElementById('gcDateTime1'));
gcDateTime1.setDisplayFormatPattern('yyyy年M月d日');
gcDateTime1.setFormatPattern('yyyy年M月d日');
gcDateTime1.setValue(new Date(2018, 9, 31));
gcDateTime1.setMinDate(new Date(2018, 0, 1));
gcDateTime1.setMaxDate(new Date(2018, 11, 31));
gcDateTime1.setMaxMinBehavior(GC.InputMan.MaxMinBehavior.Keep);

// 検証コントロール
var GcValidator1 = new GC.InputMan.GcValidator({
 items: [
    {
      control: gcDateTime1,
      ruleSet: [
        {
          // 検証規則を「範囲チェック」とする
          rule: GC.InputMan.ValidateType.OutOfRange
          failMessage: '日付が範囲外です'
        }
      ],
      notify:
      {
        tip:true
      }
    }
  ]
});
```

## 独自の検証規則

検証コントロールでは、独自の検証ロジックを設定することが可能です。上記のサンプルでは、入力された値が偶数かどうかをチェックするロジックを以下のようにruleSet.ruleプロパティに直接設定しています。（以下のコードは抜粋です）

```
var validator = new GC.InputMan.GcValidator({
  items: [
    {
      control: gcNumber3,
      ruleSet: [
        {
          // 独自の検証ロジックを使用
          rule: function (control) {
            return control.getValue() % 2 == 0;
          },
          failMessage: function (control, context) {
            return '偶数を入力してください。奇数' + control.getValue() + 'が入力されました。';
          }
        }
      ]
    },
```

また、registerValidateRuleメソッドで検証ロジックをあらかじめ登録し、そのロジック名をruleプロパティに設定する方法も提供しています。たとえば、以下のようなコードになります。

```
// 独自の検証ロジックの登録
GC.InputMan.GcValidator.registerValidateRule('chkEven', function(control,context){
   var value = control.getValue();
   if (value === undefined || value === null){
      return undefined;
   }
   return value % 2 == 0;
});

// 検証コントロール（抜粋）
var validator = new GC.InputMan.GcValidator({
  items: [
    {
      control: gcNumber3,
      ruleSet: [
        {
          // あらかじめ登録した検証ロジックを使用
          rule: "chkEven",
          failMessage: function (control, context) {
            return '偶数を入力してください。奇数' + control.getValue() + 'が入力されました。';
          }
        }
      ]
    },
```

