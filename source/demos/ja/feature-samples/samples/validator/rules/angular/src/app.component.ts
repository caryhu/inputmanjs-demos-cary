import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, AfterViewInit } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent implements AfterViewInit {
    public gcNumber1: GC.InputMan.GcNumber;
    public gcNumber2: GC.InputMan.GcNumber;
    public gcNumber3: GC.InputMan.GcNumber;
    public gcNumber4: GC.InputMan.GcNumber;
    public maxMinBehavior: GC.InputMan.MaxMinBehavior = GC.InputMan.MaxMinBehavior.Keep;

    ngAfterViewInit(): void {
        this.createValidator();
    }

    public createValidator() {
        const validator = new GC.InputMan.GcValidator({
            items: [
                // 入力必須
                {
                    control: this.gcNumber1,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required,
                            failMessage: '数値を入力してください'
                        }
                    ]
                },
                // 正数のみ入力可
                {
                    control: this.gcNumber2,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.OutOfRange,
                            failMessage: '正数を入力してください'
                        }
                    ]
                },
                // 偶数のみ入力可
                {
                    control: this.gcNumber3,
                    ruleSet: [
                        {
                            rule: (control: GC.InputMan.GcInputManBase) => {
                                return control.getValue() % 2 == 0;
                            },
                            failMessage: (control: GC.InputMan.GcInputManBase, context) => {
                                return '偶数を入力してください。奇数' + control.getValue() + 'が入力されました。';
                            }
                        }
                    ]
                },
                // すべての検証規則
                {
                    control: this.gcNumber4,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required,
                            failMessage: '数値を入力してください'
                        },
                        {
                            rule: GC.InputMan.ValidateType.OutOfRange,
                            failMessage: '正数を入力してください'
                        },
                        {
                            rule: (control: GC.InputMan.GcInputManBase) => {
                                return control.getValue() % 2 == 0;
                            },
                            failMessage: (control: GC.InputMan.GcInputManBase, context) => {
                                return '偶数を入力してください。奇数' + control.getValue() + 'が入力されました。';
                            }
                        }
                    ]
                }
            ],
            defaultNotify: {
                tip: true
            }
        });
        validator.validate();
    }
}


enableProdMode();