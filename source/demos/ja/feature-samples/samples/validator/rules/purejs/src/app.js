﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcNumber1 = new InputMan.GcNumber(document.getElementById('gcNumber1'), {
    allowDeleteToNull: true
});
gcNumber1.setValue(null);

const gcNumber2 = new InputMan.GcNumber(document.getElementById('gcNumber2'), {
    minValue: 1,
    maxValue: 100,
    maxMinBehavior: InputMan.MaxMinBehavior.Keep,
});
gcNumber2.setValue(-1);

const gcNumber3 = new InputMan.GcNumber(document.getElementById('gcNumber3'), {
    value: 1
});

const gcNumber4 = new InputMan.GcNumber(document.getElementById('gcNumber4'), {
    allowDeleteToNull: true,
    minValue: 1,
    maxValue: 10000,
    maxMinBehavior: InputMan.MaxMinBehavior.Keep
});
gcNumber4.setValue(null);

const validator = new InputMan.GcValidator({
    items: [
        // 入力必須
        {
            control: gcNumber1,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required,
                    failMessage: '数値を入力してください'
                }
            ]
        },
        // 正数のみ入力可
        {
            control: gcNumber2,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.OutOfRange,
                    failMessage: '正数を入力してください'
                }
            ]
        },
        // 偶数のみ入力可
        {
            control: gcNumber3,
            ruleSet: [
                {
                    rule: (control) => {
                        return control.getValue() % 2 == 0;
                    },
                    failMessage: (control, context) => {
                        return '偶数を入力してください。奇数' + control.getValue() + 'が入力されました。';
                    }
                }
            ]
        },
        // すべての検証規則
        {
            control: gcNumber4,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required,
                    failMessage: '数値を入力してください'
                },
                {
                    rule: InputMan.ValidateType.OutOfRange,
                    failMessage: '正数を入力してください'
                },
                {
                    rule: (control) => {
                        return control.getValue() % 2 == 0;
                    },
                    failMessage: (control, context) => {
                        return '偶数を入力してください。奇数' + control.getValue() + 'が入力されました。';
                    }
                }
            ]
        }
    ],
    defaultNotify: {
        tip: true
    }
});
validator.validate();