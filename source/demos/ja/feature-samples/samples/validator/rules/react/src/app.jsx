import * as React from "react";
import * as ReactDom from "react-dom";
import { GcNumber } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

class App extends React.Component{

    render(){
        return (
            <div>
                入力必須<br/>
                <GcNumber ref="gcNumber1" allowDeleteToNull= {true} value={null}/><br/>
                正数のみ入力可<br/>
                <GcNumber ref="gcNumber2" minValue= {1} maxValue= {100} maxMinBehavior= {InputMan.MaxMinBehavior.Keep} value={-1}/><br/>
                偶数のみ入力可<br/>
                <GcNumber ref="gcNumber3" value={1}/><br/>
                すべての検証規則<br/>
                <GcNumber ref="gcNumber4" allowDeleteToNull= {true} minValue= {1} maxValue= {10000} maxMinBehavior= {InputMan.MaxMinBehavior.Keep} value={null}/>
            </div>
        )
    }

    componentDidMount(){
        const validator = new InputMan.GcValidator({
            items: [
                // 入力必須
                {
                    control: this.refs.gcNumber1.getNestedIMControl(),
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required,
                            failMessage: '数値を入力してください'
                        }
                    ]
                },
                // 正数のみ入力可
                {
                    control: this.refs.gcNumber2.getNestedIMControl(),
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.OutOfRange,
                            failMessage: '正数を入力してください'
                        }
                    ]
                },
                // 偶数のみ入力可
                {
                    control: this.refs.gcNumber3.getNestedIMControl(),
                    ruleSet: [
                        {
                            rule: (control) => {
                                return control.getValue() % 2 == 0;
                            },
                            failMessage: (control, context) => {
                                return '偶数を入力してください。奇数' + control.getValue() + 'が入力されました。';
                            }
                        }
                    ]
                },
                // すべての検証規則
                {
                    control: this.refs.gcNumber4.getNestedIMControl(),
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required,
                            failMessage: '数値を入力してください'
                        },
                        {
                            rule: InputMan.ValidateType.OutOfRange,
                            failMessage: '正数を入力してください'
                        },
                        {
                            rule: (control) => {
                                return control.getValue() % 2 == 0;
                            },
                            failMessage: (control, context) => {
                                return '偶数を入力してください。奇数' + control.getValue() + 'が入力されました。';
                            }
                        }
                    ]
                }
            ],
            defaultNotify: {
                tip: true
            }
        });
        validator.validate();
    }
}

ReactDom.render(<App />, document.getElementById("app"));