﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcTextBox = new InputMan.GcTextBox(document.getElementById('gcTextBox'));
const gcValidator = new InputMan.GcValidator({
    items: [
        {
            control: gcTextBox,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required
                }
            ]
        }
    ],
    defaultNotify: {
        tip: {
            direction: InputMan.TipDirection.Right
        }
    }
});

// イベントハンドラ
gcValidator.addEventListener(InputMan.GcValidatorEvent.ValidationFailed, (sender, eArgs) => {
    log('ValidationFailed');
});

// テキストボックスにログを出力
const log = (message) => {
    const textarea = document.getElementById('log');
    textarea.value += message + '\n';
    textarea.scrollTop = textarea.scrollHeight;
}

gcValidator.validate();