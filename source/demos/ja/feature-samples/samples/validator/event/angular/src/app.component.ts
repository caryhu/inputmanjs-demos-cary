import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent implements AfterViewInit {

    @ViewChild('logPanel')
    logPanel: ElementRef;
    private gcTextBox: GC.InputMan.GcTextBox;
    public message: string = "";

    ngAfterViewInit(): void {
        this.InitValidator();
    }

    // テキストボックスにログを出力
    public log(message: string) {
        this.message += message + '\n';
        this.logPanel.nativeElement.scrollTop = this.logPanel.nativeElement.scrollHeight;
    }

    private InitValidator(): void {
        const gcValidator = new GC.InputMan.GcValidator({
            items: [
                {
                    control: this.gcTextBox,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ]
                }
            ],
            defaultNotify: {
                tip: {
                    direction: GC.InputMan.TipDirection.Right
                }
            }
        });

        // イベントハンドラ
        gcValidator.addEventListener(GC.InputMan.GcValidatorEvent.ValidationFailed, (sender, eArgs) => {
            this.log('ValidationFailed');
        });

        gcValidator.validate();
    }
}


enableProdMode();