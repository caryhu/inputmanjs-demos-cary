import * as React from "react";
import * as ReactDom from "react-dom";
import { GcTextBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            message: ''
        };
    }

    componentDidMount(){
        this.InitValidator();
    }

    log(msg){
        var newMessage = this.state.message + msg + '\n';
        this.refs.log.scrollTop = this.refs.log.scrollHeight;
        this.setState({message: newMessage});
    }

    render(){
        return (
            <div>
                <GcTextBox ref='gcTextBox'></GcTextBox><br/>
                イベント<br/>
                <textarea ref="log" rows="10" cols="30" value={this.state.message}></textarea>
            </div>
        )
    }

    InitValidator() {
        const gcValidator = new InputMan.GcValidator({
            items: [
                {
                    control: this.refs.gcTextBox.getNestedIMControl(),
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ]
                }
            ],
            defaultNotify: {
                tip: {
                    direction: InputMan.TipDirection.Right
                }
            }
        });

        // イベントハンドラ
        gcValidator.addEventListener(InputMan.GcValidatorEvent.ValidationFailed, (sender, eArgs) => {
            this.log('ValidationFailed');
        });

        gcValidator.validate();
    }
}

ReactDom.render(<App />, document.getElementById("app"));