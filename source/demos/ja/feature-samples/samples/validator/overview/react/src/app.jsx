import * as React from "react";
import * as ReactDom from "react-dom";
import { GcTextBox, GcMask, GcNumber, GcDateTime, GcComboBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

const products = ['果汁100%オレンジ', '果汁100%グレープ', '果汁100%レモン', '果汁100%ピーチ', 'コーヒーマイルド', 'コーヒービター'];
class App extends React.Component{

    render(){
        return (
            <div>
                テキストコントロール<br/>
                <GcTextBox ref="gcTextBox"></GcTextBox><br/>
                マスクコントロール<br/>
                <GcMask ref="gcMask" formatPattern= {'〒\\D{3}-\\D{4}'}></GcMask><br/>
                数値コントロール<br/>
                <GcNumber ref="gcNumber" allowDeleteToNull= {true} value={null}></GcNumber><br/>
                日付時刻コントロール<br/>
                <GcDateTime ref="gcDateTime" value={null}></GcDateTime><br/>
                コンボコントロール<br/>
                <GcComboBox ref="gcComboBox" items= {products}></GcComboBox>
            </div>
        )
    }

    componentDidMount(){
        const validator = new InputMan.GcValidator({
            items: [
                // テキストコントロール
                {
                    control: this.refs.gcTextBox.getNestedIMControl(),
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ]
                },
                // マスクコントロール
                {
                    control: this.refs.gcMask.getNestedIMControl(),
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ]
                },
                // 数値コントロール
                {
                    control: this.refs.gcNumber.getNestedIMControl(),
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ]
                },
                // 日付時刻コントロール
                {
                    control: this.refs.gcDateTime.getNestedIMControl(),
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ]
                },
                // コンボコントロール
                {
                    control: this.refs.gcComboBox.getNestedIMControl(),
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ]
                }
            ],
            defaultNotify: {
                tip: true
            }
        });
        validator.validate();
    }
}

ReactDom.render(<App />, document.getElementById("app"));