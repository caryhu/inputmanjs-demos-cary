import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const products = ['果汁100%オレンジ', '果汁100%グレープ', '果汁100%レモン', '果汁100%ピーチ', 'コーヒーマイルド', 'コーヒービター'];

const gcTextBox = new InputMan.GcTextBox(document.getElementById('gcTextBox'));

const gcMask = new InputMan.GcMask(document.getElementById('gcMask'), {
    formatPattern: '〒\\D{3}-\\D{4}'
});

const gcNumber = new InputMan.GcNumber(document.getElementById('gcNumber'), {
    allowDeleteToNull: true
});
gcNumber.setValue(null);

const gcDateTime = new InputMan.GcDateTime(document.getElementById('gcDateTime'));
gcDateTime.setValue(null);

const gcComboBox = new InputMan.GcComboBox(document.getElementById('gcComboBox'), {
    items: products
});

const validator = new InputMan.GcValidator({
    items: [
        // テキストコントロール
        {
            control: gcTextBox,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required
                }
            ]
        },
        // マスクコントロール
        {
            control: gcMask,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required
                }
            ]
        },
        // 数値コントロール
        {
            control: gcNumber,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required
                }
            ]
        },
        // 日付時刻コントロール
        {
            control: gcDateTime,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required
                }
            ]
        },
        // コンボコントロール
        {
            control: gcComboBox,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required
                }
            ]
        }
    ],
    defaultNotify: {
        tip: true
    }
});
validator.validate();