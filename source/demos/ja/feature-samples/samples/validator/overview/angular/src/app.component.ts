import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, AfterViewInit } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent implements AfterViewInit {
    public products = ['果汁100%オレンジ', '果汁100%グレープ', '果汁100%レモン', '果汁100%ピーチ', 'コーヒーマイルド', 'コーヒービター'];
    public gcTextBox: GC.InputMan.GcTextBox;
    public gcMask: GC.InputMan.GcMask;
    public gcNumber: GC.InputMan.GcNumber;
    public gcDateTime: GC.InputMan.GcDateTime;
    public gcComboBox: GC.InputMan.GcComboBox;
    private validator: GC.InputMan.GcValidator;

    ngAfterViewInit(): void {
        this.createValidator();
    }

    public createValidator() {
        this.validator = new GC.InputMan.GcValidator({
            items: [
                // テキストコントロール
                {
                    control: this.gcTextBox,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ]
                },
                // マスクコントロール
                {
                    control: this.gcMask,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ]
                },
                // 数値コントロール
                {
                    control: this.gcNumber,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ]
                },
                // 日付時刻コントロール
                {
                    control: this.gcDateTime,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ]
                },
                // コンボコントロール
                {
                    control: this.gcComboBox,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ]
                }
            ],
            defaultNotify: {
                tip: true
            }
        });
        this.validator.validate();
    }
}


enableProdMode();