import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent implements AfterViewInit {
    private gcTextBox1: GC.InputMan.GcTextBox;
    private gcTextBox2: GC.InputMan.GcTextBox;
    private validator: GC.InputMan.GcValidator;
    public failTip: boolean = true;
    public failIcon: boolean = true;
    public failControlState: boolean = true;
    public successTip: boolean = true;
    public successIcon: boolean = true;
    public successControlState: boolean = true;

    ngAfterViewInit(): void {
        this.update();
    }

    private update(): void {
        if (this.validator) {
            this.validator.destroy();
        }
        this.validator = new GC.InputMan.GcValidator({
            items: [
                {
                    control: this.gcTextBox1,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ],
                },
                {
                    control: this.gcTextBox2,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ],
                }
            ],
            defaultNotify: {
                fail: {
                    tip: this.failTip,
                    icon: this.failIcon ? {
                        direction: GC.InputMan.IconDirection.Inside
                    } : false,
                    controlState: this.failControlState
                },
                success: {
                    tip: this.successTip,
                    icon: this.successIcon ? {
                        direction: GC.InputMan.IconDirection.Inside
                    } : false,
                    controlState: this.successControlState
                }
            }
        });
        this.validator.validate();
    }

    public refreshValidator(event: Event): void {
        if ((event.target as any).type === 'checkbox') {
            this.update();
        }
    }
}


enableProdMode();