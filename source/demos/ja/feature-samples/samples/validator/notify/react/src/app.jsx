import * as React from "react";
import * as ReactDom from "react-dom";
import { GcTextBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

class App extends React.Component{

    constructor(props, context) {
        super(props, context);
        this.state = {
            failTip: true,
            failIcon: true,
            failControlState: true,
            successTip: true,
            successIcon: true,
            successControlState: true
        };
    }

    componentDidMount(){
        this.updateValidator();
    }

    componentDidUpdate(){
        this.updateValidator();
    }

    updateValidator(){
        if(this.validator){
            this.validator.destroy();
        }
        this.validator = new InputMan.GcValidator({
            items: [
                {
                    control: this.refs.gcTextBox1.getNestedIMControl(),
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ],
                },
                {
                    control: this.refs.gcTextBox2.getNestedIMControl(),
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ],
                }
            ],
            defaultNotify: {
                fail: {
                    tip: this.state.failTip,
                    icon: this.state.failIcon ? {
                        direction: InputMan.IconDirection.Inside
                    } : false,
                    controlState: this.state.failControlState
                },
                success: {
                    tip: this.state.successTip,
                    icon: this.state.successIcon ? {
                        direction: InputMan.IconDirection.Inside
                    } : false,
                    controlState: this.state.successControlState
                }
            }
        });
        this.validator.validate();
    }

    render(){
        return (
            <div>
                <GcTextBox ref="gcTextBox1"></GcTextBox><br/>
                <GcTextBox ref="gcTextBox2" text= {'テキスト'}></GcTextBox>
                <table class="sample">
                    <tr>
                        <th>検証失敗の通知</th>
                        <td>
                            <label><input type="checkbox" id="failTip" checked={this.state.failTip} onChange={(event)=>this.setState({failTip: event.target.checked})}/>ツールチップ</label><br/>
                            <label><input type="checkbox" id="failIcon" checked={this.state.failIcon} onChange={(event)=>this.setState({failIcon: event.target.checked})}/>アイコン</label><br/>
                            <label><input type="checkbox" id="failControlState" checked={this.state.failControlState} onChange={(event)=>this.setState({failControlState: event.target.checked})}/>コントロール状態</label><br/>
                        </td>
                    </tr>
                    <tr>
                        <th>検証成功の通知</th>
                        <td>
                            <label><input type="checkbox" id="successTip" checked={this.state.successTip} onChange={(event)=>this.setState({successTip: event.target.checked})}/>ツールチップ</label><br/>
                            <label><input type="checkbox" id="successIcon" checked={this.state.successIcon} onChange={(event)=>this.setState({successIcon: event.target.checked})}/>アイコン</label><br/>
                            <label><input type="checkbox" id="successControlState" checked={this.state.successControlState} onChange={(event)=>this.setState({successControlState: event.target.checked})}/>コントロール状態</label><br/>
                        </td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));