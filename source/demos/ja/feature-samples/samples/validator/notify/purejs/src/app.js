﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcTextBox1 = new InputMan.GcTextBox(document.getElementById('gcTextBox1'));
const gcTextBox2 = new InputMan.GcTextBox(document.getElementById('gcTextBox2'), {
    text: 'テキスト'
});

var validator;
const update = () => {
    if (validator) {
        validator.destroy();
    }
    validator = new GCIM.GcValidator({
        items: [
            {
                control: gcTextBox1,
                ruleSet: [
                    {
                        rule: InputMan.ValidateType.Required
                    }
                ],
            },
            {
                control: gcTextBox2,
                ruleSet: [
                    {
                        rule: InputMan.ValidateType.Required
                    }
                ],
            }
        ],
        defaultNotify: {
            fail: {
                tip: document.getElementById('failTip').checked,
                icon: document.getElementById('failIcon').checked ? {
                    direction: InputMan.IconDirection.Inside
                } : false,
                controlState: document.getElementById('failControlState').checked
            },
            success: {
                tip: document.getElementById('successTip').checked,
                icon: document.getElementById('successIcon').checked ? {
                    direction: InputMan.IconDirection.Inside
                } : false,
                controlState: document.getElementById('successControlState').checked
            }
        }
    });
    validator.validate();
}
update();

var checkboxArr = document.querySelectorAll('input[type="checkbox"]');
for (var i = 0; i < checkboxArr.length; i++) {
    checkboxArr[i].addEventListener('change', (e) => {
        update();
    });
}