import * as React from "react";
import * as ReactDom from "react-dom";
import { GcTextBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

class App extends React.Component{

    componentDidMount(){
        var validator1 = new InputMan.GcValidator({
            items: [
                {
                    control: this.refs.gcTextBox1.getNestedIMControl(),
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ],
                    // フォーカスを失ったときに検証する
                    validateWhen: InputMan.ValidateWhen.LostFocus
                },
                {
                    control: this.refs.gcTextBox2.getNestedIMControl(),
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ],
                    // 入力中に検証する
                    validateWhen: InputMan.ValidateWhen.Typing
                }
            ],
            defaultNotify: {
                tip: true
            }
        });
        
        var validator2 = new InputMan.GcValidator({
            items: [
                {
                    control: this.refs.gcTextBox3.getNestedIMControl(),
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ],
                    // 手動で検証する
                    validateWhen: InputMan.ValidateWhen.Manual
                }
            ],
            defaultNotify: {
                tip: true
            }
        });

        document.getElementById('btn').addEventListener('click', () => {
            if (validator2.validate()) {
                alert('検証に成功しました。');
            } else {
                alert('検証に失敗しました。');
            }
        });
    }

    render(){
        return (
            <div>
                フォーカスを失ったときに検証する<br/>
                <GcTextBox ref="gcTextBox1"/><br/>
                入力中に検証する<br/>
                <GcTextBox ref="gcTextBox2"/><br/>
                手動で検証する<br/>
                <GcTextBox ref="gcTextBox3"/>
                <button id='btn'>検証</button>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));