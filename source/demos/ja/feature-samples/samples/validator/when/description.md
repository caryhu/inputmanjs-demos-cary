検証コントロールによる検証方法（検証タイミング）について解説します。

## 概要

検証方法は、検証コントロールのインスタンスを初期化する際にvalidateWhenプロパティを使用して設定します。このプロパティに設定可能な値は、以下の３つです。

| validateWhenの値                 | 説明                             |
| ------------------------------ | ------------------------------ |
| LostFocus                      | コントロールがフォーカスを失ったときに検証します。（既定値） |
| Manual                         | Validateメソッドを実行したときに検証します。     |
| Typing                         | 入力中にリアルタイムで検証します。              |

たとえば、テキストボックス「gcTextBox1」に対する必須入力チェックを、Validateメソッドを実行した時に行う場合のサンプルコードは、以下の通りです。

```
var GcValidator1 = new GC.InputMan.GcValidator({
 items: [
    {
      // 検証対象コントロール
      control: gcTextBox1,
      ruleSet: [
        {
          // 検証規則を「必須入力」とする
          rule: GC.InputMan.ValidateType.Required
        }
      ],
      // Validateメソッドを実行した時に検証する
      validateWhen: GC.InputMan.ValidateWhen.Typing,
      notify:
      {
        // 検証結果をツールチップで表示する
        tip:true
      }
    }
  ]
});
```

以下のように、onclickイベントからValidateメソッドを呼び出すことで、ボタンクリック時に検証することができます。

```
<button onclick="GcValidator1.validate()">検証</button>
```

なお、 validateWhenプロパティを省略した場合、既定値である「LostFocus」が検証方法に適用されます。

