import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, AfterViewInit } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent implements AfterViewInit {
    public gcTextBox1: GC.InputMan.GcTextBox;
    public gcTextBox2: GC.InputMan.GcTextBox;
    public gcTextBox3: GC.InputMan.GcTextBox;
    private validator1: GC.InputMan.GcValidator;
    private validator2: GC.InputMan.GcValidator;

    ngAfterViewInit(): void {
        this.createValidator();
    }

    public createValidator() {
        this.validator1 = new GC.InputMan.GcValidator({
            items: [
                {
                    control: this.gcTextBox1,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ],
                    // フォーカスを失ったときに検証する
                    validateWhen: GC.InputMan.ValidateWhen.LostFocus
                },
                {
                    control: this.gcTextBox2,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ],
                    // 入力中に検証する
                    validateWhen: GC.InputMan.ValidateWhen.Typing
                }
            ],
            defaultNotify: {
                tip: true
            }
        });

        this.validator2 = new GC.InputMan.GcValidator({
            items: [
                {
                    control: this.gcTextBox3,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ],
                    // 手動で検証する
                    validateWhen: GC.InputMan.ValidateWhen.Manual
                }
            ],
            defaultNotify: {
                tip: true
            }
        });
    }

    public validate(): void {
        if (this.validator2.validate()) {
            alert('検証に成功しました。');
        } else {
            alert('検証に失敗しました。');
        }
    }
}


enableProdMode();