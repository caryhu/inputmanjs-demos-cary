﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcTextBox1 = new InputMan.GcTextBox(document.getElementById('gcTextBox1'));
const gcTextBox2 = new InputMan.GcTextBox(document.getElementById('gcTextBox2'));
const gcTextBox3 = new InputMan.GcTextBox(document.getElementById('gcTextBox3'));

const validator1 = new InputMan.GcValidator({
    items: [
        {
            control: gcTextBox1,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required
                }
            ],
            // フォーカスを失ったときに検証する
            validateWhen: InputMan.ValidateWhen.LostFocus
        },
        {
            control: gcTextBox2,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required
                }
            ],
            // 入力中に検証する
            validateWhen: InputMan.ValidateWhen.Typing
        }
    ],
    defaultNotify: {
        tip: true
    }
});

const validator2 = new InputMan.GcValidator({
    items: [
        {
            control: gcTextBox3,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required
                }
            ],
            // 手動で検証する
            validateWhen: InputMan.ValidateWhen.Manual
        }
    ],
    defaultNotify: {
        tip: true
    }
});

document.getElementById('btn').addEventListener('click', () => {
    if (validator2.validate()) {
        alert('検証に成功しました。');
    } else {
        alert('検証に失敗しました。');
    }
});