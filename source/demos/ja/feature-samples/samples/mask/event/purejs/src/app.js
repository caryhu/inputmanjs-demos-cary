﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcMask1 = new InputMan.GcMask(document.getElementById('gcMask1'), {
    formatPattern: '〒\\D{3}-\\D{4}'
});
const gcMask2 = new InputMan.GcMask(document.getElementById('gcMask2'), {
    formatPattern: '(AAA|BBB|CCC|DDD|EEE)',
    showSpinButton: true
});

// イベントハンドラ
gcMask1.onEditStatusChanged((sender, eArgs) => {
    log('onEditStatusChanged');
});
gcMask2.onEditStatusChanged((sender, eArgs) => {
    log('onEditStatusChanged');
});
gcMask1.onFocusOut((sender, eArgs) => {
    log('onFocusOut');
});
gcMask2.onFocusOut((sender, eArgs) => {
    log('onFocusOut');
});
gcMask1.onInput((sender, eArgs) => {
    log('onInput');
});
gcMask2.onInput((sender, eArgs) => {
    log('onInput');
});
gcMask1.onInvalidInput((sender, eArgs) => {
    log('onInvalidInput');
});
gcMask2.onInvalidInput((sender, eArgs) => {
    log('onInvalidInput');
});
gcMask1.onKeyDown((sender, eArgs) => {
    log('onKeyDown');
});
gcMask2.onKeyDown((sender, eArgs) => {
    log('onKeyDown');
});
gcMask1.onKeyExit((sender, eArgs) => {
    log('onKeyExit');
});
gcMask2.onKeyExit((sender, eArgs) => {
    log('onKeyExit');
});
gcMask1.onKeyUp((sender, eArgs) => {
    log('onKeyUp');
});
gcMask2.onKeyUp((sender, eArgs) => {
    log('onKeyUp');
});
gcMask1.onSpinDown((sender, eArgs) => {
    log('onSpinDown');
});
gcMask2.onSpinDown((sender, eArgs) => {
    log('onSpinDown');
});
gcMask1.onSpinUp((sender, eArgs) => {
    log('onSpinUp');
});
gcMask2.onSpinUp((sender, eArgs) => {
    log('onSpinUp');
});
gcMask1.onSyncValueToOriginalInput((sender, eArgs) => {
    log('onSyncValueToOriginalInput');
});
gcMask2.onSyncValueToOriginalInput((sender, eArgs) => {
    log('onSyncValueToOriginalInput');
});
gcMask1.onTextChanged((sender, eArgs) => {
    log('onTextChanged');
});
gcMask2.onTextChanged((sender, eArgs) => {
    log('onTextChanged');
});
gcMask1.onTextChanging((sender, eArgs) => {
    log('onTextChanging');
});
gcMask2.onTextChanging((sender, eArgs) => {
    log('onTextChanging');
});
gcMask1.onValueChanged((sender, eArgs) => {
    log('onValueChanged');
});
gcMask2.onValueChanged((sender, eArgs) => {
    log('onValueChanged');
});

// テキストボックスにログを出力
const log = (message) => {
    const textarea = document.getElementById('log');
    textarea.value += message + '\n';
    textarea.scrollTop = textarea.scrollHeight;
}