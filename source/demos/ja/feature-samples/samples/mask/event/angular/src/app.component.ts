import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, ViewChild, ElementRef } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    @ViewChild('logPanel')
    public logPanel: ElementRef;
    public text: string = "";

    // テキストボックスにログを出力
    public log(message: string) {
        this.text = this.text + message + '\n';
        this.logPanel.nativeElement.scrollTop = this.logPanel.nativeElement.scrollHeight;
    }
}


enableProdMode();