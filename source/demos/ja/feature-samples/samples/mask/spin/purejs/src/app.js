﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcMask = new InputMan.GcMask(document.getElementById('gcMask'), {
    formatPattern: '(AAA|BBB|CCC|DDD|EEE)',
    showSpinButton: true
});

document.getElementById('setAllowSpin').addEventListener('change', (e) => {
    gcMask.setAllowSpin(e.target.checked);
});

document.getElementById('setSpinOnKeys').addEventListener('change', (e) => {
    gcMask.setSpinOnKeys(e.target.checked);
});

document.getElementById('setSpinWheel').addEventListener('change', (e) => {
    gcMask.setSpinWheel(e.target.checked);
});