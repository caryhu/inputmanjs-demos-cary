import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, ViewChild } from '@angular/core';
import GC from "@grapecity/inputman";
import { GcMaskComponent } from "@grapecity/inputman.angular";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    @ViewChild(GcMaskComponent) mask: GcMaskComponent;

    public clearText(){
        this.mask.getNestedIMControl().clear();
    }

    public allowSpin: boolean = true;
    public spinOnKeys: boolean = true;
    public spinWheel: boolean = true;
}

enableProdMode();