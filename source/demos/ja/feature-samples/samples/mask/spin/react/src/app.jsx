import * as React from "react";
import * as ReactDom from "react-dom";
import { GcMask } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

class App extends React.Component{

    constructor(props, context) {
        super(props, context);
        this.state = {
            allowSpin: true,
            spinOnKeys: true,
            spinWheel: true
        };
    }

    render(){
        return (
        <div>
            <GcMask 
                formatPattern= {'(AAA|BBB|CCC|DDD|EEE)'}
                showSpinButton= {true}
                allowSpin={this.state.allowSpin}
                spinOnKeys={this.state.spinOnKeys}
                spinWheel={this.state.spinWheel}
            />
            <table class="sample">
                <tr>
                    <th>スピン機能</th>
                    <td><label><input type="checkbox" checked={this.state.allowSpin} onChange={(e)=> {
                                    this.setState({allowSpin: e.target.checked})}}/>有効にする</label></td>
                </tr>
                <tr>
                    <th>上下矢印キーでのスピン操作</th>
                    <td><label><input type="checkbox" checked={this.state.spinOnKeys} onChange={(e)=> {
                                    this.setState({spinOnKeys: e.target.checked})}}/>有効にする</label></td>
                </tr>
                <tr>
                    <th>マウスホイールでのスピン操作</th>
                    <td><label><input type="checkbox" checked={this.state.spinWheel} onChange={(e)=> {
                                    this.setState({spinWheel: e.target.checked})}}/>有効にする</label></td>
                </tr>
            </table>
        </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));