import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, ViewChild } from '@angular/core';
import { GcMaskComponent } from "@grapecity/inputman.angular";


@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public autoScale: boolean = true;
    public minScaleFactor: number = 0.5;

    @ViewChild(GcMaskComponent) mask: GcMaskComponent;

    public clearText(){
        this.mask.getNestedIMControl().clear();
    }
}

enableProdMode();