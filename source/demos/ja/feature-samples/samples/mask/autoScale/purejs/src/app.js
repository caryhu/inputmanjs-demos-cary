﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcMask = new InputMan.GcMask(document.getElementById('gcMask'), {
    autoScale:true,
    minScaleFactor:0.5,
    formatPattern: ' \\D{4}-\\D{4}-\\D{4}-\\D{4}-\\D{4}-\\D{4}',
});

document.getElementById('clearText').addEventListener('click', () => {
    gcMask.clear();
});

document.getElementById('setAutoScale').addEventListener('click', (e) => {
    gcMask.setAutoScale(e.target.checked);
});

document.getElementById('setMinScaleFactor').addEventListener('change', (e) => {
    gcMask.setMinScaleFactor(Number(e.target.value));
});