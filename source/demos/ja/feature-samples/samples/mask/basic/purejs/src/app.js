﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcMask = new InputMan.GcMask(document.getElementById('gcMask'));
gcMask.setFormatPattern('〒\\D{3}-\\D{4}');
const rawValue = new InputMan.GcTextBox(document.getElementById('rawValue'));

document.getElementById('getValueBtn').addEventListener('click', () => {
    rawValue.setText(gcMask.getValue());
});

document.getElementById('setValueBtn').addEventListener('click', (e) => {
    gcMask.setValue(rawValue.getText());
});

document.getElementById('setAcceptsCrlf').addEventListener('change', (e) => {
    gcMask.setAcceptsCrlf(crLfModes[e.target.selectedIndex]);
});

const crLfModes = [
    InputMan.CrLfMode.NoControl,
    InputMan.CrLfMode.Filter,
    InputMan.CrLfMode.Cut
];