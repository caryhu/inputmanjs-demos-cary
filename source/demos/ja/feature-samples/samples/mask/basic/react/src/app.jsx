import * as React from "react";
import * as ReactDom from "react-dom";
import { GcMask, GcTextBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

const crLfModes = [
    InputMan.CrLfMode.NoControl,
    InputMan.CrLfMode.Filter,
    InputMan.CrLfMode.Cut
];

class App extends React.Component{
    gcMask = null;
    gcTextBox = null;
    constructor(props, context) {
        super(props, context);
        this.state = {
            value: '',
            acceptsCrlf: InputMan.CrLfMode.NoControl
        };
    }
    render(){
        return (
            <div>
                <GcMask 
                 value={this.state.value} formatPattern={'〒\\D{3}-\\D{4}'}
                 acceptsCrlf={this.state.acceptsCrlf}
                 onInitialized={(imCtrl)=>{ this.gcMask = imCtrl; }}>
                </GcMask>
                <table class="sample">
                    <tr>
                        <th>リテラル文字列を含まない値</th>
                        <td>
                            <GcTextBox className={'displayStyle'}
                            onInitialized={(imCtrl)=>{ this.gcTextBox = imCtrl; }}></GcTextBox>&nbsp;
                            <button id="getValueBtn" onClick={(e)=>{
                                this.gcTextBox.setText(this.gcMask.getValue());
                                }}>値の取得</button>&nbsp;
                            <button id="setValueBtn" onClick={(e)=>this.setState({value: this.gcTextBox.getText()})}>値の設定</button>
                        </td>
                    </tr>
                    <tr>
                        <th>改行コードの扱い</th>
                        <td>
                            <select id="setAcceptsCrlf" onChange={(e)=>this.setState({acceptsCrlf: crLfModes[e.target.selectedIndex]})}>
                                <option>そのまま使用</option>
                                <option>すべての改行コードを削除</option>
                                <option>改行コード以降の文字列を切り取り</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));