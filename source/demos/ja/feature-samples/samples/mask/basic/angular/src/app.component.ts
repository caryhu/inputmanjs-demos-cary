import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public acceptsCrlf: GC.InputMan.CrLfMode;
    public gcMaskValue: string;
    public inputText: string;

    public crLfModes = [
        GC.InputMan.CrLfMode.NoControl,
        GC.InputMan.CrLfMode.Filter,
        GC.InputMan.CrLfMode.Cut
    ];
}


enableProdMode();