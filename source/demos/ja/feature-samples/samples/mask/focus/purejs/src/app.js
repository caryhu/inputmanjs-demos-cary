﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcMask1 = new InputMan.GcMask(document.getElementById('gcMask1'), {
    formatPattern: '〒\\D{3}-\\D{4}'
});
const gcMask2 = new InputMan.GcMask(document.getElementById('gcMask2'), {
    formatPattern: '〒\\D{3}-\\D{4}'
});
const gcMask3 = new InputMan.GcMask(document.getElementById('gcMask3'), {
    formatPattern: '〒\\D{3}-\\D{4}'
});

// 自動フォーカス移動
document.getElementById('setExitOnLastChar').addEventListener('change', (e) => {
    const isExitOnLastChar = e.target.checked;
    gcMask1.setExitOnLastChar(isExitOnLastChar);
    gcMask2.setExitOnLastChar(isExitOnLastChar);
    gcMask3.setExitOnLastChar(isExitOnLastChar);
});

// 矢印キーによるフォーカス移動
document.getElementById('setExitOnLeftRightKey').addEventListener('change', (e) => {
    gcMask1.setExitOnLeftRightKey(exitOnLeftRightKeys[e.target.selectedIndex]);
    gcMask2.setExitOnLeftRightKey(exitOnLeftRightKeys[e.target.selectedIndex]);
    gcMask3.setExitOnLeftRightKey(exitOnLeftRightKeys[e.target.selectedIndex]);
});

document.getElementById('setTabAction').addEventListener('change', (e) => {
    gcMask1.setTabAction(tabActions[e.target.selectedIndex]);
    gcMask2.setTabAction(tabActions[e.target.selectedIndex]);
    gcMask3.setTabAction(tabActions[e.target.selectedIndex]);
});

document.getElementById('setHighlightText').addEventListener('change', (e) => {
    gcMask1.setHighlightText(highlightTexts[e.target.selectedIndex]);
    gcMask2.setHighlightText(highlightTexts[e.target.selectedIndex]);
    gcMask3.setHighlightText(highlightTexts[e.target.selectedIndex]);
});

const exitOnLeftRightKeys = [
    InputMan.ExitOnLeftRightKey.None,
    InputMan.ExitOnLeftRightKey.Both,
    InputMan.ExitOnLeftRightKey.Left,
    InputMan.ExitOnLeftRightKey.Right
];

const tabActions = [
    InputMan.TabAction.Control,
    InputMan.TabAction.Field
];

const highlightTexts = [
    InputMan.HighlightText.None,
    InputMan.HighlightText.Field,
    InputMan.HighlightText.All
];

// Enterキーによるフォーカス移動
const ExitKeys = [
    InputMan.ExitKey.None,
    InputMan.ExitKey.Both,
    InputMan.ExitKey.Enter,
    InputMan.ExitKey.ShiftEnter
];
document.getElementById('setExitOnEnterKey').addEventListener('change', (e) => {
    gcMask1.setExitOnEnterKey(ExitKeys[e.target.selectedIndex]);
    gcMask2.setExitOnEnterKey(ExitKeys[e.target.selectedIndex]);
    gcMask3.setExitOnEnterKey(ExitKeys[e.target.selectedIndex]);
});