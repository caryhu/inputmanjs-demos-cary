import * as React from "react";
import * as ReactDom from "react-dom";
import { GcMask } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

const ExitOnLeftRightKeys = [
    InputMan.ExitOnLeftRightKey.None,
    InputMan.ExitOnLeftRightKey.Both,
    InputMan.ExitOnLeftRightKey.Left,
    InputMan.ExitOnLeftRightKey.Right
];

const TabActions = [
    InputMan.TabAction.Control,
    InputMan.TabAction.Field
];

const HighlightTexts = [
    InputMan.HighlightText.None,
    InputMan.HighlightText.Field,
    InputMan.HighlightText.All
];

// Enterキーによるフォーカス移動
const ExitKeys = [
    InputMan.ExitKey.None,
    InputMan.ExitKey.Both,
    InputMan.ExitKey.Enter,
    InputMan.ExitKey.ShiftEnter
];

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            exitOnLastChar: false,
            exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.None,
            exitOnEnterKey: InputMan.ExitKey.None
        };
    }

    render(){
        return (
            <div>
                <GcMask 
                    formatPattern= {'〒\\D{3}-\\D{4}'} 
                    exitOnLastChar={this.state.exitOnLastChar} 
                    exitOnLeftRightKey={this.state.exitOnLeftRightKey} 
                    tabAction={this.state.tabAction}
                    highlightText={this.state.highlightText}
                    exitOnEnterKey={this.state.exitOnEnterKey}>
                </GcMask>
                <GcMask 
                    formatPattern= {'〒\\D{3}-\\D{4}'} 
                    exitOnLastChar={this.state.exitOnLastChar} 
                    exitOnLeftRightKey={this.state.exitOnLeftRightKey} 
                    tabAction={this.state.tabAction}
                    highlightText={this.state.highlightText}
                    exitOnEnterKey={this.state.exitOnEnterKey}>
                </GcMask>
                <GcMask 
                    formatPattern= {'〒\\D{3}-\\D{4}'} 
                    exitOnLastChar={this.state.exitOnLastChar} 
                    exitOnLeftRightKey={this.state.exitOnLeftRightKey} 
                    tabAction={this.state.tabAction}
                    highlightText={this.state.highlightText}
                    exitOnEnterKey={this.state.exitOnEnterKey}>
                </GcMask>
                <table class="sample">
                    <tr>
                        <th>自動フォーカス移動</th>
                        <td><label><input type="checkbox" checked={this.state.exitOnLastChar} id="setExitOnLastChar" onChange={(e)=>this.setState({exitOnLastChar: e.target.checked})}/>入力完了時に自動的に次のコントロールに移動する</label>
                        </td>
                    </tr>
                    <tr>
                        <th>矢印キーによるフォーカス移動</th>
                        <td>
                            <select id="setExitOnLeftRightKey" onChange={(e)=>{this.setState({exitOnLeftRightKey: ExitOnLeftRightKeys[e.target.selectedIndex]})}}>
                                <option>なし</option>
                                <option>両方</option>
                                <option>左</option>
                                <option>右</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>フォーカスの移動先</th>
                        <td>
                            <select id="setTabAction" onChange={(e)=>{this.setState({tabAction: TabActions[e.target.selectedIndex]})}}>
                                <option>コントロール間を移動</option>
                                <option>フィールド間を移動</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>フォーカス移動後の状態</th>
                        <td>
                            <select id="setHighlightText" onChange={(e)=>{this.setState({highlightText: HighlightTexts[e.target.selectedIndex]})}}>
                                <option>キャレットをフィールドの先頭に移動</option>
                                <option>フィールドを選択</option>
                                <option>コントロールのすべての内容を選択</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Enterキーによるフォーカス移動</th>
                        <td>
                            <select id="setExitOnEnterKey" onChange={(e)=>{this.setState({exitOnEnterKey: ExitKeys[e.target.selectedIndex]})}}>
                                <option>なし</option>
                                <option>両方</option>
                                <option>Enter</option>
                                <option>Shift+Enter</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));