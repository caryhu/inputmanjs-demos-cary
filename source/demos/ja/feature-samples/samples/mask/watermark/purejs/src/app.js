﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';
import './styles.css';

const gcMask = new InputMan.GcMask(document.getElementById('gcMask'), {
    formatPattern: '\\D{2,4}-\\D{2,4}-\\D{4}',
    // フォーカスがないときの代替テキスト
    watermarkDisplayNullText: '電話番号',
    // フォーカスがあるときの代替テキスト
    watermarkNullText: '市外局番から入力ください'
});

document.getElementById('setWatermarkDisplayNullText').addEventListener('change', (e) => {
    gcMask.setWatermarkDisplayNullText(e.target.checked ? '電話番号' : '');
});

document.getElementById('setWatermarkNullText').addEventListener('change', (e) => {
    gcMask.setWatermarkNullText(e.target.checked ? '市外局番から入力ください' : '');
});

const setPromptChar = new InputMan.GcTextBox(document.getElementById('setPromptChar'), {
    text: '_'
});
setPromptChar.onTextChanged(() => {
    gcMask.setPromptChar(setPromptChar.getText());
});