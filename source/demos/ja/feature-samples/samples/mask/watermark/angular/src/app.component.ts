import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public watermarkDisplayNullText: string = "電話番号";
    public watermarkNullText: string = "市外局番から入力ください";
    public promptChar:string='_';
}


enableProdMode();