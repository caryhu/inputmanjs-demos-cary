import * as React from "react";
import * as ReactDom from "react-dom";
import { GcMask, GcTextBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            watermarkDisplayNullText: '電話番号',
            watermarkNullText: '市外局番から入力ください',
            promptChar: '_'
        };
    }
    render(){
        return (
            <div>
                <GcMask formatPattern= {'\\D{2,4}-\\D{2,4}-\\D{4}'}
                // フォーカスがないときの代替テキスト
                watermarkDisplayNullText= {this.state.watermarkDisplayNullText}
                // フォーカスがあるときの代替テキスト
                watermarkNullText= {this.state.watermarkNullText}
                promptChar={this.state.promptChar}></GcMask>
                <table class="sample">
                    <tr>
                        <th>代替テキスト</th>
                        <td>
                            <label><input type="checkbox" checked={this.state.watermarkDisplayNullText} onChange={(e)=>this.setState({watermarkDisplayNullText: e.target.checked? '電話番号' : ''})}
                                    id="setWatermarkDisplayNullText"/>フォーカスがないときの代替テキスト</label><br/>
                            <label><input type="checkbox" checked={this.state.watermarkNullText} onChange={(e)=>this.setState({watermarkNullText: e.target.checked? '市外局番から入力ください' : ''})}
                                    id="setWatermarkNullText"/>フォーカスがあるときの代替テキスト</label>
                        </td>
                    </tr>
                    <tr>
                        <th>プロンプト文字</th>
                        <td><GcTextBox text={this.state.promptChar} onTextChanged={(e)=>{this.setState({promptChar: e.getText()})}}></GcTextBox></td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));