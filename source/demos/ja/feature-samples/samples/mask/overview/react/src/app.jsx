import * as React from "react";
import * as ReactDom from "react-dom";
import { GcMask } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

// 年号をカスタマイズします。
InputMan.updateCustomEra([
    { name: '明治', abbreviation: '明', symbol: 'M', startDate: '1868/09/08', shortcuts: '1,M', },
    { name: '大正', abbreviation: '大', symbol: 'T', startDate: '1912/07/30', shortcuts: '2,T', },
    { name: '昭和', abbreviation: '昭', symbol: 'S', startDate: '1926/12/25', shortcuts: '3,S', },
    { name: '平成', abbreviation: '平', symbol: 'H', startDate: '1989/01/08', shortcuts: '4,H', },
    { name: '令和', abbreviation: '令', symbol: 'R', startDate: '2019/05/01', shortcuts: '5,R', },
    // 2100/1/1から「新規」という年号が開始される場合の例
    { name: '新規', abbreviation: '新', symbol: 'N', startDate: '2100/01/01', shortcuts: '6,N', }
]);

class App extends React.Component{

    render(){
        return (
            <div class="flexbox">
                <div>
                    電話番号
                    <GcMask formatPattern= {'℡ \\D{2,4}-\\D{2,4}-\\D{4}'}></GcMask>
                </div>
                <div>
                    郵便番号
                    <GcMask formatPattern={'〒 \\D{3}-\\D{4}'}></GcMask>
                </div>
                <div>
                    都道府県
                    <GcMask
                     formatPattern={'(北海道|青森県|岩手県|宮城県|秋田県|山形県|福島県|茨城県|栃木県|群馬県|埼玉県|千葉県|東京都|神奈川県|山梨県|長野県|新潟県|富山県|石川県|福井県|岐阜県|静岡県|愛知県|三重県|滋賀県|京都府|大阪府|兵庫県|奈良県|和歌山県|鳥取県|島根県|岡山県|広島県|山口県|徳島県|香川県|愛媛県|高知県|福岡県|佐賀県|長崎県|熊本県|大分県|宮崎県|鹿児島県|沖縄県)'}
                     showSpinButton={true}></GcMask>
                </div>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));