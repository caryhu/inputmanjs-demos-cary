マスクコントロール内にクリアボタンを表示する機能について解説します。

## クリアボタンを表示する方法
コンストラクタのshowClearButtonオプションにtrueを設定します。
```
new GC.InputMan.GcMask(document.getElementById('gcMask'), {
      showClearButton: true
});
```

また、setShowClearButtonメソッドを使用することで、クリアボタンの表示の有無を切り替えることができます。
```
gcMask.setShowClearButton(true);
```
