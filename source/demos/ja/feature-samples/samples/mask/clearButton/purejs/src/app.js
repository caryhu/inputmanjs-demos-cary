﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';
import './styles.css';

const gcMask = new InputMan.GcMask(document.getElementById('gcMask'), {
    formatPattern: '℡ \\D{2,4}-\\D{2,4}-\\D{4}',
    value: '123412341234',
    showClearButton: true,
});

document.getElementById('showClearButton').addEventListener('click', (e) => {
    gcMask.setShowClearButton(e.target.checked);
});