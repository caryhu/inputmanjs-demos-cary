﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';
import './styles.css';

const gcMask = new InputMan.GcMask(document.getElementById('gcMask'));

const formatPattern = new InputMan.GcComboBox(document.getElementById('formatPattern'), {
    items: [
        { value: '℡:\\D{2,4}-\\D{2,4}-\\D{4}', desc: '電話番号の定型書式' },
        { value: '〒\\D{3}-\\D{4}', desc: '郵便番号の定型書式' },
        { value: '製造番号：\\A{3}\\D{8}', desc: '異なる文字種の組み合わせ' },
        { value: '[\\D^123]{3,10}', desc: '「1,2,3」以外で3～10文字の半角数字の入力を許可' },
        { value: '[^\\Ｊ]{0,5}', desc: 'ひらがな以外の入力を5文字まで許可' },
        { value: '[\\Ｚ^\\Ｔ]{0,}', desc: 'サロゲートペア文字以外の全角文字の入力を許可' },
    ],
    columns: [
        { name: 'value', label: '値', width: 180 },
        { name: 'desc', label: '説明', width: 350 }
    ],
    dropDownWidth: 'auto',
    displayMemberPath: 'value',
    valueMemberPath: 'value'
});
formatPattern.addEventListener(InputMan.GcComboBoxEvent.SelectedChanged, (control, args) => {
    if (formatPattern.getSelectedValue()) {
        gcMask.setFormatPattern(formatPattern.getSelectedValue());
    }
});
formatPattern.addEventListener(InputMan.GcComboBoxEvent.TextChanged, (control, args) => {
    gcMask.setFormatPattern(formatPattern.getDisplayText());
});

document.getElementById('setAutoConvert').addEventListener('change', (e) => {
    gcMask.setAutoConvert(e.target.checked);
});