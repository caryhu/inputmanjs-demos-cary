import * as React from "react";
import * as ReactDom from "react-dom";
import { GcMask, GcComboBox } from "@grapecity/inputman.react";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            formatPattern: '',
            autoConvert: true
        };
    }

    render(){
        return (
            <div>
                <GcMask formatPattern={this.state.formatPattern} autoConvert={this.state.autoConvert}></GcMask>
                <table class="sample">
                    <tr>
                        <th>書式の設定</th>
                        <td>
                            <GcComboBox
                                items= {[
                                    { value: '℡:\\D{2,4}-\\D{2,4}-\\D{4}', desc: '電話番号の定型書式' },
                                    { value: '〒\\D{3}-\\D{4}', desc: '郵便番号の定型書式' },
                                    { value: '製造番号：\\A{3}\\D{8}', desc: '異なる文字種の組み合わせ' },
                                    { value: '[\\D^123]{3,10}', desc: '「1,2,3」以外で3～10文字の半角数字の入力を許可' },
                                    { value: '[^\\Ｊ]{0,5}', desc: 'ひらがな以外の入力を5文字まで許可' },
                                    { value: '[\\Ｚ^\\Ｔ]{0,}', desc: 'サロゲートペア文字以外の全角文字の入力を許可' },
                                ]}
                                columns= {[
                                    { name: 'value', label: '値', width: 180 },
                                    { name: 'desc', label: '説明', width: 350 }
                                ]}
                                dropDownWidth= {'auto'}
                                displayMemberPath= {'value'}
                                valueMemberPath= {'value'}
                                selectedChanged={(e)=>{this.setState({formatPattern: e.getSelectedValue()})}}></GcComboBox>
                        </td>
                    </tr>
                    <tr>
                        <th>自動変換</th>
                        <td>
                            <label><input type="checkbox" id="setAutoConvert" checked={this.state.autoConvert} onChange={(e)=> this.setState({autoConvert: e.target.checked})}/>指定した文字種にあわせて自動的に変換する</label>
                        </td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));