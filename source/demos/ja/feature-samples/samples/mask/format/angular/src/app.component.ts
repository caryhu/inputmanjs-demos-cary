import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public combobox_items: Array<ComboItem<string, string>> = [
        {
            text: "電話番号の定型書式",
            value: '℡:\\D{2,4}-\\D{2,4}-\\D{4}'
        },
        {
            text: "'郵便番号の定型書式",
            value: '〒\\D{3}-\\D{4}'
        },
        {
            text: "異なる文字種の組み合わせ",
            value: '製造番号：\\A{3}\\D{8}'
        },
        {
            text: "「1,2,3」以外で3～10文字の半角数字の入力を許可",
            value: '[\\D^123]{3,10}'
        },
        {
            text: "ひらがな以外の入力を5文字まで許可",
            value: '[^\\Ｊ]{0,5}'
        },
        {
            text: "サロゲートペア文字以外の全角文字の入力を許可",
            value: '[\\Ｚ^\\Ｔ]{0,}'
        }
    ];
    public autoConvert: boolean;
    public formatPattern: string;
}

export interface ComboItem<T1, T2> {
    value: T1;
    text: T2
}


enableProdMode();