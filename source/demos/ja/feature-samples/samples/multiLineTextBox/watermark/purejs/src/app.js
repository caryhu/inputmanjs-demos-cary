﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';
import './styles.css';

const gcMultiLineTextBox = new InputMan.GcMultiLineTextBox(document.getElementById('gcMultiLineTextBox'), {
    // フォーカスがないときの代替テキスト
    watermarkDisplayNullText: '住所',
    // フォーカスがあるときの代替テキスト
    watermarkNullText: '全角で入力してください'
});