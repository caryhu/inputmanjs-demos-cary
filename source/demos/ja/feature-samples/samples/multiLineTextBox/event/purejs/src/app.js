﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcMultiLineTextBox = new InputMan.GcMultiLineTextBox(document.getElementById('gcMultiLineTextBox'));

// イベントハンドラ
gcMultiLineTextBox.onEditStatusChanged((sender, eArgs) => {
    log('onEditStatusChanged');
});
gcMultiLineTextBox.onFocusOut((sender, eArgs) => {
    log('onFocusOut');
});
gcMultiLineTextBox.onIMEReadingStringOutput((sender, eArgs) => {
    log('onIMEReadingStringOutput');
});
gcMultiLineTextBox.onInput((sender, eArgs) => {
    log('onInput');
});
gcMultiLineTextBox.onInvalidInput((sender, eArgs) => {
    log('onInvalidInput');
});
gcMultiLineTextBox.onKeyDown((sender, eArgs) => {
    log('onKeyDown');
});
gcMultiLineTextBox.onKeyExit((sender, eArgs) => {
    log('onKeyExit');
});
gcMultiLineTextBox.onKeyUp((sender, eArgs) => {
    log('onKeyUp');
});
gcMultiLineTextBox.onSyncValueToOriginalInput((sender, eArgs) => {
    log('onSyncValueToOriginalInput');
});
gcMultiLineTextBox.onTextChanged((sender, eArgs) => {
    log('onTextChanged');
});

// テキストボックスにログを出力
const log = (message) => {
    const textarea = document.getElementById('log');
    textarea.value += message + '\n';
    textarea.scrollTop = textarea.scrollHeight;
}