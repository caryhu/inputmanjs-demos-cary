import * as React from "react";
import * as ReactDom from "react-dom";
import { GcMultiLineTextBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            message: ''
        };
    }

    log(msg){
        var newMessage = this.state.message + msg + '\n';
        this.refs.log.scrollTop = this.refs.log.scrollHeight;
        this.setState({message: newMessage});
    }

    render(){
        return (
            <div>
                <GcMultiLineTextBox
                 onInitialized={(sender)=> sender.onSyncValueToOriginalInput(() => this.log('onSyncValueToOriginalInput'))}
                 onEditStatusChanged={(s) => this.log("onEditStatusChanged")}
                 onInvalidInput={(s) => this.log("onInvalidInput")}
                 onKeyExit={(s) => this.log("onKeyExit")}
                 onTextChanged={(s) => this.log("onTextChanged")}
                 onKeyDown={(s) => this.log("onKeyDown")}
                 onKeyUp={(s) => this.log("onKeyUp")}
                 onFocusOut={(s) => this.log("onFocusOut")}
                 onInput={(s) => this.log("onInput")}
                 onIMEReadingStringOutput={(eArgs) => {this.log('onIMEReadingStringOutput');
                 }}></GcMultiLineTextBox><br/>
                イベント<br/>
                <textarea ref='log' id="log" rows="10" cols="30" value={this.state.message}></textarea>
                        </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));