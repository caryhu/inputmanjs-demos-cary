import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";
import { AfterContentInit } from "@angular/core";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent implements AfterContentInit {
    public styleStr: string;
    private styleSheet: CSSStyleSheet;
    private indices: Array<number> = [];
    private styles = {
        '.gcim': { backgroundColor: '#ddffdd', borderColor: '#009900', borderWidth: '2px', borderStyle: 'dashed', borderRadius: '12px', boxShadow: '5px 5px 5px rgba(0,0,0,0.5)' },
        '.gcim__textarea': { width: '200px', height: '40px', cursor: 'crosshair', fontSize: '20px', fontWeight: 'bold', fontStyle: 'italic', fontFamily: 'serif', textAlign: 'right', textShadow: '1px 1px 1px rgba(0,0,0,0.5)', color: '#009900' },
        '.gcim__textarea:disabled': { backgroundColor: '#666666', color: '#cccccc', cursor: 'wait' },
        '.gcim_focused': { backgroundColor: '#ddddff', borderColor: '#0000ff', color: '#0000ff' },
        '.gcim_watermark_null': { backgroundColor: '#ffdddd', borderColor: '#ff0000', color: '#ff0000' },
        '.gcim_focused.gcim_watermark_null': { backgroundColor: '#ffddff', borderColor: '#990099', color: '#990099' },
    };

    public ngAfterContentInit(): void {
        this.createInitialStyles();
    }

    public createInitialStyles() {
        const element = document.createElement('style');
        document.head.appendChild(element);
        this.styleSheet = element.sheet as CSSStyleSheet;
        var i = 0;
        for (const styleName in this.styles) {
            (this.styleSheet as CSSStyleSheet).insertRule(styleName + '{}', i);
            this.indices[styleName] = i;
            i++;
        }
    }

    public updateStyle(event: Event) {
        var element = event.target as HTMLInputElement;
        if (element.tagName == 'INPUT') {
            var values = element.value.split(',');
            var styleName = values[0];
            var propertyName = values[1];
            ((this.styleSheet as CSSStyleSheet).cssRules[this.indices[styleName]] as CSSStyleRule).style[propertyName] = element.checked ? this.styles[styleName][propertyName] : '';
        }

        var style = '';
        for (var i = 0; i < this.styleSheet.cssRules.length; i++) {
            if ((this.styleSheet.cssRules[i] as CSSStyleRule).style.length > 0) {
                style += this.styleSheet.cssRules[i].cssText.replace(/{/g, '{\n ').replace(/;/g, ';\n ').replace(/  }/g, '}') + '\n';
            }
        }
        this.styleStr = style;
    }

    public copyStyle() {
        (window as any).wijmo.Clipboard.copy((document.getElementById('style') as HTMLInputElement).value);
        document.execCommand('copy');
    }

}


enableProdMode();