import * as React from "react";
import * as ReactDom from "react-dom";
import { GcMultiLineTextBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

var styleSheet;
const indices = [];
const styles = {
    '.gcim': { backgroundColor: '#ddffdd', borderColor: '#009900', borderWidth: '2px', borderStyle: 'dashed', borderRadius: '12px', boxShadow: '5px 5px 5px rgba(0,0,0,0.5)' },
    '.gcim__textarea': { width: '200px', height: '40px', cursor: 'crosshair', fontSize: '20px', fontWeight: 'bold', fontStyle: 'italic', fontFamily: 'serif', textAlign: 'right', textShadow: '1px 1px 1px rgba(0,0,0,0.5)', color: '#009900' },
    '.gcim__textarea:disabled': { backgroundColor: '#666666', color: '#cccccc', cursor: 'wait' },
    '.gcim_focused': { backgroundColor: '#ddddff', borderColor: '#0000ff', color: '#0000ff' },
    '.gcim_watermark_null': { backgroundColor: '#ffdddd', borderColor: '#ff0000', color: '#ff0000' },
    '.gcim_focused.gcim_watermark_null': { backgroundColor: '#ffddff', borderColor: '#990099', color: '#990099' },
};

class App extends React.Component{
    componentDidMount(){
        this.createInitialStyles();
        var panels = document.getElementsByClassName('peoperty-panel');
        for (var i = 0; i < panels.length; i++) {
            panels[i].addEventListener('click', this.updateStyle);
        }
        document.getElementById('copyStyle').addEventListener('click', this.copyStyle);
    }

    createInitialStyles() {
        const element = document.createElement('style');
        document.head.appendChild(element);
        styleSheet = element.sheet;
        var i = 0;
        for (const styleName in styles) {
            styleSheet.insertRule(styleName + '{}', i);
            indices[styleName] = i;
            i++;
        }
    }
    
    updateStyle = (event) => {
        console.log(event);
        var element = event.target;
        if (element.tagName == 'INPUT') {
            var values = element.value.split(',');
            var styleName = values[0];
            var propertyName = values[1];
            styleSheet.cssRules[indices[styleName]].style[propertyName] = element.checked ? styles[styleName][propertyName] : '';
        }

        var style = '';
        for (var i = 0; i < styleSheet.cssRules.length; i++) {
            if (styleSheet.cssRules[i].style.length > 0) {
                style += styleSheet.cssRules[i].cssText.replace(/{/g, '{\n ').replace(/;/g, ';\n ').replace(/  }/g, '}') + '\n';
            }
        }
        document.getElementById('style').value = style;
    }

    copyStyle = () => {
        wijmo.Clipboard.copy(document.getElementById('style').value);
        document.execCommand('copy');
    }

    render(){
        return (
            <div>
                <div class="flexbox">
                    <div>
                        通常の状態
                        <GcMultiLineTextBox text= {'テキスト'}></GcMultiLineTextBox>
                    </div>
                    <div>
                        無効な状態
                        <GcMultiLineTextBox text= {'テキスト'} enabled={false}></GcMultiLineTextBox>
                    </div>
                    <div>
                        ウォーターマーク
                        <GcMultiLineTextBox watermarkDisplayNullText= {'氏名'} watermarkNullText= {'全角で入力してください'}></GcMultiLineTextBox>
                    </div>
                </div>
                <div class="peoperty-header">コントロール全般のスタイル</div>
                <div class="peoperty-panel">
                    <label><input type="checkbox" value=".gcim__textarea,width"/>コントロールの幅</label><br/>
                    <label><input type="checkbox" value=".gcim__textarea,height"/>コントロールの高さ</label><br/>
                    <label><input type="checkbox" value=".gcim,backgroundColor"/>背景色</label><br/>
                    <label><input type="checkbox" value=".gcim,borderColor"/>境界線の色</label><br/>
                    <label><input type="checkbox" value=".gcim,borderWidth"/>境界線の幅</label><br/>
                    <label><input type="checkbox" value=".gcim,borderStyle"/>境界線のスタイル</label><br/>
                    <label><input type="checkbox" value=".gcim,borderRadius"/>境界線の角丸</label><br/>
                    <label><input type="checkbox" value=".gcim__textarea,cursor"/>カーソルの形</label><br/>
                    <label><input type="checkbox" value=".gcim,boxShadow"/>コントロールの影</label><br/>
                </div>
                <div class="peoperty-header">テキストのスタイル</div>
                <div class="peoperty-panel">
                    <label><input type="checkbox" value=".gcim__textarea,color"/>文字色</label><br/>
                    <label><input type="checkbox" value=".gcim__textarea,fontSize"/>フォントのサイズ</label><br/>
                    <label><input type="checkbox" value=".gcim__textarea,fontWeight"/>フォントの太さ</label><br/>
                    <label><input type="checkbox" value=".gcim__textarea,fontStyle"/>フォントのスタイル</label><br/>
                    <label><input type="checkbox" value=".gcim__textarea,fontFamily"/>フォントの種類</label><br/>
                    <label><input type="checkbox" value=".gcim__textarea,textAlign"/>水平方向の位置</label><br/>
                    <label><input type="checkbox" value=".gcim__textarea,textShadow"/>テキストの影</label><br/>
                </div>
                <div class="peoperty-header">コントロール無効時のスタイル</div>
                <div class="peoperty-panel">
                    <label><input type="checkbox" value=".gcim__textarea:disabled,backgroundColor"/>背景色</label><br/>
                    <label><input type="checkbox" value=".gcim__textarea:disabled,color"/>文字色</label><br/>
                    <label><input type="checkbox" value=".gcim__textarea:disabled,cursor"/>カーソルの形</label><br/>
                </div>
                <div class="peoperty-header">フォーカス時のスタイル</div>
                <div class="peoperty-panel">
                    <label><input type="checkbox" value=".gcim_focused,backgroundColor"/>背景色</label><br/>
                    <label><input type="checkbox" value=".gcim_focused,borderColor"/>境界線の色</label><br/>
                    <label><input type="checkbox" value=".gcim_focused,color"/>文字色</label><br/>
                </div>
                <div class="peoperty-header">ウォーターマークのスタイル</div>
                <div class="peoperty-panel">
                    <label><input type="checkbox" value=".gcim_watermark_null,backgroundColor"/>背景色</label><br/>
                    <label><input type="checkbox" value=".gcim_watermark_null,borderColor"/>境界線の色</label><br/>
                    <label><input type="checkbox" value=".gcim_watermark_null,color"/>文字色</label><br/>
                </div>
                <div class="peoperty-header">ウォーターマークのフォーカス時のスタイル</div>
                <div class="peoperty-panel">
                    <label><input type="checkbox" value=".gcim_focused.gcim_watermark_null,backgroundColor"/>背景色</label><br/>
                    <label><input type="checkbox" value=".gcim_focused.gcim_watermark_null,borderColor"/>境界線の色</label><br/>
                    <label><input type="checkbox" value=".gcim_focused.gcim_watermark_null,color"/>文字色</label><br/>
                </div>
                <button id="copyStyle">CSSコードをコピー</button><br/>
                <textarea id="style" cols="60" rows="10"></textarea>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));