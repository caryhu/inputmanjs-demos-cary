複数行テキストコントロールでは、フォーカス取得時のカーソル位置を指定することができます。

## カーソル位置の指定
cursorPositionプロパティを利用して、フォーカス時のカーソル位置を指定することができます。cursorPositionプロパティには、次の[MultiLineCursorPosition列挙型](</inputmanjs/api/enums/inputman.multilinecursorposition.html>)の値を指定することができます。既定値はMultiLineCursorPosition.Noneです。

| 値                                             | 説明                                            |
| --------------------------------------------- | --------------------------------------------- |
| None                                     | マウスでクリックされた場所にカーソルが移動します。       |
| Start                                        | 先頭の文字列の前にカーソルが移動します。  |
| End                                           | 最後の文字列の後にカーソルが移動します。 |
