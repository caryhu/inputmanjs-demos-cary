﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

var gcMultiLineTextBox = new InputMan.GcMultiLineTextBox(document.getElementById('gcMultiLineTextBox'), {
    text: '〒９８１−３２０５　宮城県仙台市泉区紫山３－１－４'
});

document.getElementById('cursorPositions').addEventListener('change', (e) => {
    gcMultiLineTextBox.cursorPosition = cursorPosition[e.target.selectedIndex]
});

const cursorPosition = [
    InputMan.MultiLineCursorPosition.None,
    InputMan.MultiLineCursorPosition.Start,
    InputMan.MultiLineCursorPosition.End
];