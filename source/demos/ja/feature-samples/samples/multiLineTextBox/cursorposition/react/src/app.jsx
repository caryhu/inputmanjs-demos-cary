import * as React from "react";
import * as ReactDom from "react-dom";
import { GcMultiLineTextBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';


class App extends React.Component{
    constructor() {
        super();
        this.state = {
            position: InputMan.MultiLineCursorPosition.None
        }
    }
    render(){
        return (
            <React.Fragment>
                <GcMultiLineTextBox text="〒９８１−３２０５　宮城県仙台市泉区紫山３－１－４" cursorPosition={this.state.position}></GcMultiLineTextBox>
                <table class="sample">
                    <tr>
                        <th>フォーカス時のカーソル位置</th>
                        <td>
                            <select value={this.state.position} onChange={(e) => this.setState({position: e.target.value})}>
                                <option value={InputMan.MultiLineCursorPosition.None}>指定なし</option>
                                <option value={InputMan.MultiLineCursorPosition.Start}>先頭</option>
                                <option value={InputMan.MultiLineCursorPosition.End}>末尾</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </React.Fragment>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));