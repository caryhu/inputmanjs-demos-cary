import * as React from "react";
import * as ReactDom from "react-dom";
import { GcMultiLineTextBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

const ScrollBars = [
    InputMan.ScrollBars.None,
    InputMan.ScrollBars.Both,
    InputMan.ScrollBars.Vertical,
    InputMan.ScrollBars.Horizontal
];

const ScrollBarModes = [
    InputMan.ScrollBarMode.Automatic,
    InputMan.ScrollBarMode.Fixed
];

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            wordWrap: true,
            countWrappedLine: false,
            scrollBars: InputMan.ScrollBars.None,
            scrollBarMode: InputMan.ScrollBarMode.Automatic
        };
    }
    render(){
        return (
            <div>
                <GcMultiLineTextBox
                    text= {'〒９８１−３２０５　宮城県仙台市泉区紫山３－１－４'}
                    wordWrap={this.state.wordWrap} 
                    maxLineCount={this.state.maxLineCount} 
                    countWrappedLine={this.state.countWrappedLine}
                    scrollBars={this.state.scrollBars}
                    scrollBarMode={this.state.scrollBarMode}>
                </GcMultiLineTextBox>
                <table class="sample">
                    <tr>
                        <th>ワードラップ</th>
                        <td><label><input type="checkbox" checked={this.state.wordWrap} id="setWordWrap" onChange={(e)=>this.setState({wordWrap: e.target.checked})}/>自動的に改行する</label></td>
                    </tr>
                    <tr>
                        <th>入力可能な行数</th>
                        <td>
                            <input type="number" value={this.state.maxLineCount === undefined? 0: this.state.maxLineCount} id="setMaxLineCount" onChange={(e)=>this.setState({maxLineCount: Number(e.target.value)})}/>&nbsp;
                            <label><input type="checkbox" id="setCountWrappedLine" onChange={(e)=>this.setState({countWrappedLine: e.target.checked})}/>折り返した改行を含める</label>
                        </td>
                    </tr>
                    <tr>
                        <th>スクロールバー</th>
                        <td>
                            <label><select id="setScrollBars" onChange={(e)=> this.setState({scrollBars: ScrollBars[e.target.selectedIndex]})}>
                                    <option>なし</option>
                                    <option>水平と垂直</option>
                                    <option>垂直</option>
                                    <option>水平</option>
                                </select> 種類</label><br/>
                            <label><select id="setScrollBarMode" onChange={(e)=> this.setState({scrollBarMode: ScrollBarModes[e.target.selectedIndex]})}>
                                    <option>表示領域を超えた場合のみ表示</option>
                                    <option>常に表示</option>
                                </select> 表示方法</label>
                        </td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));