﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

var gcMultiLineTextBox = new InputMan.GcMultiLineTextBox(document.getElementById('gcMultiLineTextBox'), {
    text: '〒９８１−３２０５　宮城県仙台市泉区紫山３－１－４'
});

document.getElementById('setWordWrap').addEventListener('change', (e) => {
    gcMultiLineTextBox.setWordWrap(e.target.checked);
});

document.getElementById('setCountWrappedLine').addEventListener('change', (e) => {
    gcMultiLineTextBox.setCountWrappedLine(e.target.checked);
});

document.getElementById('setMaxLineCount').addEventListener('change', (e) => {
    gcMultiLineTextBox.setMaxLineCount(e.target.value);
});

document.getElementById('setScrollBars').addEventListener('change', (e) => {
    gcMultiLineTextBox.setScrollBars(scrollBars[e.target.selectedIndex]);
});

document.getElementById('setScrollBarMode').addEventListener('change', (e) => {
    gcMultiLineTextBox.setScrollBarMode(scrollBarModes[e.target.selectedIndex]);
});

(() => {
    for (var i = 0; i < document.styleSheets.length; i++) {
        var sheet = document.styleSheets[i];
        var cssRules = sheet.cssRules;
        for (var j = 0; j < cssRules.length; j++) {
            var rule = cssRules[j];
            if (rule.selectorText && rule.selectorText.indexOf('::-webkit-scrollbar') !== -1) {
                sheet.removeRule(j);
            }
        }
    }
})();

const scrollBars = [
    InputMan.ScrollBars.None,
    InputMan.ScrollBars.Both,
    InputMan.ScrollBars.Vertical,
    InputMan.ScrollBars.Horizontal
];

const scrollBarModes = [
    InputMan.ScrollBarMode.Automatic,
    InputMan.ScrollBarMode.Fixed
];