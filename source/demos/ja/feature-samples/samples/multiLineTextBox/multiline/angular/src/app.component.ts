import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {

    public text: string = '〒９８１−３２０５　宮城県仙台市泉区紫山３－１－４';
    public wordWrap: boolean=true;
    public countWrappedLine: boolean;
    public maxLineCount: number=0;
    public scrollBars: GC.InputMan.ScrollBars;
    public scrollBarMode: GC.InputMan.ScrollBarMode;


    public scrollBarEnum = [
        GC.InputMan.ScrollBars.None,
        GC.InputMan.ScrollBars.Both,
        GC.InputMan.ScrollBars.Vertical,
        GC.InputMan.ScrollBars.Horizontal
    ];

    public scrollBarModes = [
        GC.InputMan.ScrollBarMode.Automatic,
        GC.InputMan.ScrollBarMode.Fixed
    ];
}

enableProdMode();