import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public imeReadingStringKanaMode: GC.InputMan.KanaMode;
    public furiganaText: string;
    public text: string;

    public kanaModes = [
        GC.InputMan.KanaMode.KatakanaHalf,
        GC.InputMan.KanaMode.Katakana,
        GC.InputMan.KanaMode.Hiragana
    ];

    public clear(): void {
        this.furiganaText = "";
        this.text = "";
    }
}

export interface ComboItem<T1, T2> {
    value: T1;
    text: T2
}

enableProdMode();