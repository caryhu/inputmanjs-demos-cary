﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcMultiLineTextBox = new InputMan.GcMultiLineTextBox(document.getElementById('gcMultiLineTextBox'), {
    IMEReadingStringAppend: false
});

const furigana = document.getElementById('furigana');
gcMultiLineTextBox.onIMEReadingStringOutput((sender, eArgs) => {
    furigana.innerText += eArgs.readingString;
});

document.getElementById('clearText').addEventListener('click', () => {
    gcMultiLineTextBox.setText('');
    furigana.innerText = '';
});

document.getElementById('setReadingImeStringKanaMode').addEventListener('change', (e) => {
    gcMultiLineTextBox.setReadingImeStringKanaMode(kanaModes[e.target.selectedIndex]);
});

const kanaModes = [
    InputMan.KanaMode.KatakanaHalf,
    InputMan.KanaMode.Katakana,
    InputMan.KanaMode.Hiragana
];