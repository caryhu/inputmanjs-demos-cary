﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcMultiLineTextBox = new InputMan.GcMultiLineTextBox(document.getElementById('gcMultiLineTextBox'));

document.getElementById('setMaxLength').addEventListener('change', (e) => {
    gcMultiLineTextBox.setMaxLength(e.target.value);
});

document.getElementById('setLengthNotAsByte').addEventListener('change', (e) => {
    gcMultiLineTextBox.setLengthAsByte(!e.target.checked);
});

document.getElementById('setLengthAsByte').addEventListener('change', (e) => {
    gcMultiLineTextBox.setLengthAsByte(e.target.checked);
});

document.getElementById('setAcceptsCrlf').addEventListener('change', (e) => {
    gcMultiLineTextBox.setAcceptsCrlf(crLfModes[e.target.selectedIndex]);
});

const crLfModes = [
    InputMan.CrLfMode.NoControl,
    InputMan.CrLfMode.Filter,
    InputMan.CrLfMode.Cut
];