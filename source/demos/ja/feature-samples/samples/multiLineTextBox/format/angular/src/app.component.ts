import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public format: string;
    public autoConvert: boolean;
    public combobox_items: Array<CrLfModeComboItem> = [
        { value: 'Ａ', text: '全角大文字のアルファベット' },
        { value: 'A', text: '半角大文字のアルファベット' },
        { value: 'ａ', text: '全角小文字のアルファベット' },
        { value: 'a', text: '半角小文字のアルファベット' },
        { value: 'Ｋ', text: '全角カタカナ（促音・拗音の小書き表記あり）' },
        { value: 'K', text: '半角カタカナ（促音・拗音の小書き表記あり）' },
        { value: 'Ｎ', text: '全角カタカナ（促音・拗音の小書き表記なし）' },
        { value: 'N', text: '半角カタカナ（促音・拗音の小書き表記なし）' },
        { value: '９', text: '全角数字' },
        { value: '	9', text: '半角数字' },
        { value: '＃', text: '全角数字および数字関連記号' },
        { value: '#', text: '半角数字および数字関連記号' },
        { value: '＠', text: '全角記号' },
        { value: '@', text: '半角記号' },
        { value: 'Ｂ', text: '全角２進数' },
        { value: 'B', text: '半角２進数' },
        { value: 'Ｘ', text: '全角16進数' },
        { value: 'X', text: '半角16進数' },
        { value: 'Ｓ', text: '全角空白文字' },
        { value: 'S', text: '半角空白文字' },
        { value: 'Ｊ', text: 'ひらがな（促音・拗音の小書き表記あり）' },
        { value: 'Ｇ', text: 'ひらがな（促音・拗音の小書き表記なし）' },
        { value: 'Ｚ', text: '空白文字以外のすべての全角文字' },
        { value: '^Ｔ', text: 'サロゲートペア文字以外' },
        { value: 'Ｉ', text: 'JIS X 0208で構成された文字' },
        { value: 'Ｍ', text: 'Shift JISで構成された文字' },
        { value: '^Ｖ', text: 'IVS文字以外' },
        { value: 'Ｄ', text: '空白文字以外の２バイト文字' },
        { value: 'H', text: '空白文字以外のすべての半角文字' }
    ];
}

export interface CrLfModeComboItem {
    value: string;
    text: string
}

enableProdMode();