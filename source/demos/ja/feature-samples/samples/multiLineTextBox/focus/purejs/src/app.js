﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

// コントロールを初期化
const gcMultiLineTextBox1 = new InputMan.GcMultiLineTextBox(document.getElementById('gcMultiLineTextBox1'), {
    maxLength: 3,
    acceptsReturn: false
});
const gcMultiLineTextBox2 = new InputMan.GcMultiLineTextBox(document.getElementById('gcMultiLineTextBox2'), {
    maxLength: 3,
    acceptsReturn: false
});
const gcMultiLineTextBox3 = new InputMan.GcMultiLineTextBox(document.getElementById('gcMultiLineTextBox3'), {
    maxLength: 3,
    acceptsReturn: false
});

// 自動フォーカス移動
document.getElementById('setExitOnLastChar').addEventListener('change', (e) => {
    const isExitOnLastChar = e.target.checked;
    gcMultiLineTextBox1.setExitOnLastChar(isExitOnLastChar);
    gcMultiLineTextBox2.setExitOnLastChar(isExitOnLastChar);
    gcMultiLineTextBox3.setExitOnLastChar(isExitOnLastChar);
});

// 矢印キーによるフォーカス移動
const ExitOnLeftRightKeys = [
    InputMan.ExitOnLeftRightKey.None,
    InputMan.ExitOnLeftRightKey.Both,
    InputMan.ExitOnLeftRightKey.Left,
    InputMan.ExitOnLeftRightKey.Right
];

document.getElementById('setExitOnLeftRightKey').addEventListener('change', (e) => {
    gcMultiLineTextBox1.setExitOnLeftRightKey(ExitOnLeftRightKeys[e.target.selectedIndex]);
    gcMultiLineTextBox2.setExitOnLeftRightKey(ExitOnLeftRightKeys[e.target.selectedIndex]);
    gcMultiLineTextBox3.setExitOnLeftRightKey(ExitOnLeftRightKeys[e.target.selectedIndex]);
});

// Enterキーによるフォーカス移動
const ExitKeys = [
    InputMan.ExitKey.None,
    InputMan.ExitKey.Both,
    InputMan.ExitKey.Enter,
    InputMan.ExitKey.ShiftEnter
];
document.getElementById('setExitOnEnterKey').addEventListener('change', (e) => {
    gcMultiLineTextBox1.setExitOnEnterKey(ExitKeys[e.target.selectedIndex]);
    gcMultiLineTextBox2.setExitOnEnterKey(ExitKeys[e.target.selectedIndex]);
    gcMultiLineTextBox3.setExitOnEnterKey(ExitKeys[e.target.selectedIndex]);
});