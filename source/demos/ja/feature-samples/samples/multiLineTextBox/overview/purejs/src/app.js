import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcMultiLineTextBox1 = new InputMan.GcMultiLineTextBox(document.getElementById('gcMultiLineTextBox1'), {
    format: 'N',
    autoConvert: true
});
const gcMultiLineTextBox2 = new InputMan.GcMultiLineTextBox(document.getElementById('gcMultiLineTextBox2'), {
    format: 'Ｚ',
    autoConvert: true
});
const gcMultiLineTextBox3 = new InputMan.GcMultiLineTextBox(document.getElementById('gcMultiLineTextBox3'), {
    IMEReadingStringAppend: false
});

gcMultiLineTextBox3.onIMEReadingStringOutput((sender, eArgs) => {
    document.getElementById('furigana').innerText += eArgs.readingString;
});