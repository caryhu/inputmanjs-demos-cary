import * as React from "react";
import * as ReactDom from "react-dom";
import { GcMultiLineTextBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

class App extends React.Component{

    render(){
        return (
            <div class="flexbox">
                <div>
                    半角カタカナを入力<br/>
                    <GcMultiLineTextBox format= {'N'} autoConvert= {true}></GcMultiLineTextBox>
                </div>
                <div>
                    全角文字を入力<br/>
                    <GcMultiLineTextBox format= {'Ｚ'} autoConvert= {true}></GcMultiLineTextBox>
                </div>
                <div>
                    ふりがなの取得
                    <GcMultiLineTextBox 
                        IMEReadingStringAppend= {false} 
                        onIMEReadingStringOutput={(eArgs) => {
                            var lbl = document.getElementById("furigana");
                            lbl.innerText = eArgs.getIMEReadingString();
                        }}>
                    </GcMultiLineTextBox>
                    ｶﾅ: <span id="furigana"></span>
                </div>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));