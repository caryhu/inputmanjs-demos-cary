import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {

    public initTextBoxDropDown(gcTextBox: GC.InputMan.GcTextBox): void {
        const gcDropDown = gcTextBox.createDropDown();

        const dropdownContent = document.getElementById('prefecture');
        document.getElementById('prefecture').addEventListener('click', (e) => {
            if ((e.srcElement as HTMLElement).tagName.toLowerCase() == 'span') {
                gcTextBox.setText((e.srcElement as HTMLElement).innerText);
                gcDropDown.close();
            }
        });

        gcDropDown.getElement().appendChild(dropdownContent);
    }
}


enableProdMode();