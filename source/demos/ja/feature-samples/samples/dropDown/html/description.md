ドロップダウンプラグイン（GcDropDown）を使用して、HTML要素をドロップダウン表示させる方法について解説します。

## ドロップダウンオブジェクトの追加

ドロップダウンプラグインを使用する場合、まずcreateDropDownメソッドを使用して、ドロップダウンオブジェクトをInputManJSのコントロールに追加する必要があります。

テキストコントロールにドロップダウンオブジェクトを追加する場合、以下のようなコードになります。

```
var gcTextBox = new GC.InputMan.GcTextBox(document.getElementById('gcTextBox'));
var gcDropDown = gcTextBox.createDropDown();
```

なお、createDropDownメソッドでは、ドロップダウンボタンを配置する位置をパラメータとして指定することが可能です。設定可能な値（DropDownButtonAlignment列挙体）は、以下の通りです。

| 値                                | 説明                               |
| -------------------------------- | -------------------------------- |
| LeftSide                         | ドロップダウンボタンをコントロールの左側に配置します。      |
| RightSide                        | ドロップダウンボタンをコントロールの右側に配置します。（既定値） |

## HTML要素の表示

HTML要素をドロップダウン表示したい場合、ドロップダウンオブジェクトのgetElementメソッドを使用して、ドロップダウンのコンテナになるHTML要素を取得し、appendChildメソッドを使用して、そのHTML要素に表示したいHTML要素を追加します。

上記サンプルでは、"prefecture"というidのHTML要素をドロップダウンのコンテナに追加しています。

```
gcDropDown.getElement().appendChild(document.getElementById('prefecture'));
```

## 選択時の処理

上記のサンプルでは、"prefecture"要素内のspanタグがクリックされた時に、テキストコントロールにそのspanタグ内の文字（innerText）を表示し、closeメソッドでドロップダウンを閉じています。clickイベントを設定する箇所は、以下のように記述することも可能です。

```
document.getElementById('prefecture').addEventListener('click', selPref);

function selPref(e) {
  if (e.srcElement.tagName.toLowerCase() == 'span') {
    gcTextBox.setText(e.srcElement.innerText);
    gcDropDown.close();
  }
}
```

