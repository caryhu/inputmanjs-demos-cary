import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';

const gcTextBox = new InputMan.GcTextBox(document.getElementById('gcTextBox'));
const gcDropDown = gcTextBox.createDropDown();

const dropdownContent = document.getElementById('prefecture');
document.getElementById('prefecture').addEventListener('click', (e) => {
    if (e.srcElement.tagName.toLowerCase() == 'span') {
        gcTextBox.setText(e.srcElement.innerText);
        gcDropDown.close();
    }
});

gcDropDown.getElement().appendChild(dropdownContent);