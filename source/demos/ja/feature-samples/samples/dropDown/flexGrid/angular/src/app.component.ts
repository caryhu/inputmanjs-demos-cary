import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";
import '@grapecity/wijmo.styles/wijmo.css';
import './license';
import { FlexGrid, SelectionMode, HeadersVisibility } from '@grapecity/wijmo.grid';
import employees from './data';

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {

    public gcTextBoxSelectedIndex: number = -1;
    public gcMaskSelectedIndex: number = -1;

    public initGcTextBoxDropDown(gcTextBox: GC.InputMan.GcTextBox): void {
        // ドロップダウンプラグイン
        const gcDropDown1 = gcTextBox.createDropDown() as FlexGridDropDown;

        // ドロップダウンを開いたときに再描画します
        gcDropDown1.onOpen(() => {
            if (!gcDropDown1.flexGrid) {
                // Wijmo FlexGridコントロール
                gcDropDown1.flexGrid = new FlexGrid(gcDropDown1.getElement().appendChild(document.createElement('div')), {
                    itemsSource: employees,
                    columns: [
                        { binding: 'id', header: '社員ID', width: 60 },
                        { binding: 'department', header: '部署', width: 70 },
                        { binding: 'name', header: '名前', width: 100 },
                        { binding: 'tel', header: '電話番号', width: 120 },
                    ],
                    selectionMode: SelectionMode.Row,
                    headersVisibility: HeadersVisibility.Column,
                    selectionChanged: (sender: any, args: any) => {
                        if (gcDropDown1.isClosed()) {
                            return;
                        }
                        if (args.row === this.gcTextBoxSelectedIndex) {
                            return;
                        }
                        this.gcTextBoxSelectedIndex = args.row;
                        gcTextBox.setText(employees[args.row].name);
                        gcDropDown1.close();
                    }
                });
                gcDropDown1.flexGrid.hostElement.style.height = '140px';
            }
            gcDropDown1.flexGrid.select(this.gcTextBoxSelectedIndex, 0);
        });
    }

    public initGcMaskDropDown(gcMask: GC.InputMan.GcMask): void {
        // ドロップダウンプラグイン
        const gcDropDown2 = gcMask.createDropDown() as FlexGridDropDown;
        // ドロップダウンを開いたときに再描画します
        gcDropDown2.onOpen(() => {
            if (!gcDropDown2.flexGrid) {
                // Wijmo FlexGridコントロール
                gcDropDown2.flexGrid = new FlexGrid(gcDropDown2.getElement().appendChild(document.createElement('div')), {
                    itemsSource: employees,
                    columns: [
                        { binding: 'id', header: '社員ID', width: 60 },
                        { binding: 'department', header: '部署', width: 70 },
                        { binding: 'name', header: '名前', width: 100 },
                        { binding: 'tel', header: '電話番号', width: 120 },
                    ],
                    selectionMode: SelectionMode.Row,
                    headersVisibility: HeadersVisibility.Column,
                    selectionChanged: (sender: any, args: any) => {
                        if (gcDropDown2.isClosed()) {
                            return;
                        }
                        if (args.row === this.gcMaskSelectedIndex) {
                            return;
                        }
                        this.gcMaskSelectedIndex = args.row;
                        gcMask.setValue(employees[args.row].tel);
                        gcDropDown2.close();
                    }
                });
                gcDropDown2.flexGrid.hostElement.style.height = '140px';
            }
            gcDropDown2.flexGrid.select(this.gcMaskSelectedIndex, 0);
        });
    }
}


enableProdMode();

export interface FlexGridDropDown extends GC.InputMan.IDropDownContainer {
    flexGrid: FlexGrid;
}