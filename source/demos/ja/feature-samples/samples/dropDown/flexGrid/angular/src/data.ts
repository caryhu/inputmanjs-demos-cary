const items = [
    { id: 105, department: '第一営業', name: '森上 偉久馬', tel: '09011111111' },
    { id: 107, department: '第二営業', name: '葛城 孝史', tel: '09022222222' },
    { id: 110, department: '第一営業', name: '加藤 泰江', tel: '09033333333' },
    { id: 204, department: '営業開発', name: '川村 匡', tel: '09044444444' },
    { id: 207, department: '営業開発', name: '松沢 誠一', tel: '09055555555' },
    { id: 210, department: '営業一', name: '成宮 真紀', tel: '09066666666' },
];

export default items;