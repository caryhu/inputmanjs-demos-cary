ドロップダウンプラグインを使用して、[Wijmo](<https://www.grapecity.co.jp/developer/wijmo>)のFlexGridコントロールをドロップダウン表示させる方法について解説します。

## ドロップダウンオブジェクトの追加

ドロップダウンプラグインを使用する場合、まずcreateDropDownメソッドを使用して、ドロップダウンオブジェクトをInputManJSのコントロールに追加する必要があります。

テキストコントロールにドロップダウンオブジェクトを追加する場合、以下のようなコードになります。

```
var gcTextBox = new GC.InputMan.GcTextBox(document.getElementById('gcTextBox'));
var gcDropDown1 = gcTextBox.createDropDown();
```

なお、createDropDownメソッドでは、ドロップダウンボタンを配置する位置をパラメータとして、指定することが可能です。設定可能な値（DropDownButtonAlignment列挙体）は、以下の通りです。

| 値                                | 説明                               |
| -------------------------------- | -------------------------------- |
| LeftSide                         | ドロップダウンボタンをコントロールの左側に配置します。      |
| RightSide                        | ドロップダウンボタンをコントロールの右側に配置します。（既定値） |

## FlexGridの表示

FlexGridをドロップダウン表示したい場合、FlexGridをドロップダウンオブジェクトの子ノードとして追加します。なお、FlexGridをドロップダウンに表示する場合には、onOpenメソッドを使用して、ドロップダウンが開いた時にFlexGridを再描画する必要があります。

たとえば、以下のようなコードになります。

```
// Wijmo FlexGridコントロール
var flexGrid1 = new wijmo.grid.FlexGrid(gcDropDown1.getElement().appendChild(document.createElement('div')), {
  itemsSource: employees,
  columns: [
    { binding: 'id', header: '社員ID', width: 60 },
    { binding: 'department', header: '部署', width: 70 },
    { binding: 'name', header: '名前', width: 100 },
    { binding: 'tel', header: '電話番号', width: 120 },
  ],
  selectionMode: wijmo.grid.SelectionMode.Row,
  headersVisibility: wijmo.grid.HeadersVisibility.Column,
  selectionChanged: function (sender, args) {
    gcTextBox.setText(employees[args.row].name);
    gcDropDown1.close();
  }
});

// ドロップダウンを開いたときに再描画します
gcDropDown.onOpen(function () {
  flexGrid1.refresh();
});
```

## 選択時の処理

選択時の処理は、FlexGrid側のイベントを使用して行うことになります。具体的には、行が選択されたことを検知（selectionChangedイベント）し、テキストコントロールに選択された値を表示した上でcloseメソッドでドロップダウンを閉じるような処理を実装する必要があります。

たとえば、上記サンプルの場合、以下のコードがFlexGrid側で行が選択された時の処理になります。

```
selectionChanged: function (sender, args) {
    gcTextBox.setText(employees[args.row].name);
    gcDropDown1.close();
  }
```

