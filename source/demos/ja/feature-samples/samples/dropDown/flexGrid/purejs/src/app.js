import '@grapecity/inputman/CSS/gc.inputman-js.css';
import '@grapecity/wijmo.styles/wijmo.css';
import './styles.css';
import './license';
import { InputMan } from '@grapecity/inputman';
import { FlexGrid, SelectionMode, HeadersVisibility } from '@grapecity/wijmo.grid';
import employees from './data';

// テキストコントロール
const gcTextBox = new InputMan.GcTextBox(document.getElementById('gcTextBox'));
gcTextBox.selectedIndex = -1;

// ドロップダウンプラグイン
const gcDropDown1 = gcTextBox.createDropDown();

// ドロップダウンを開いたときに再描画します
gcDropDown1.onOpen(() => {
    if (!gcDropDown1.flexGrid) {
        // Wijmo FlexGridコントロール
        gcDropDown1.flexGrid = new FlexGrid(gcDropDown1.getElement().appendChild(document.createElement('div')), {
            itemsSource: employees,
            columns: [
                { binding: 'id', header: '社員ID', width: 60 },
                { binding: 'department', header: '部署', width: 70 },
                { binding: 'name', header: '名前', width: 100 },
                { binding: 'tel', header: '電話番号', width: 120 },
            ],
            selectionMode: SelectionMode.Row,
            headersVisibility: HeadersVisibility.Column,
            selectionChanged: (sender, args) => {
                if (gcDropDown1.isClosed()) {
                    return;
                }
                if (args.row === gcTextBox.selectedIndex) {
                    return;
                }
                gcTextBox.selectedIndex = args.row;
                gcTextBox.setText(employees[args.row].name);
                gcDropDown1.close();
            }
        });
        gcDropDown1.flexGrid.hostElement.style.height = '140px';
    }
    gcDropDown1.flexGrid.select(gcTextBox.selectedIndex, 0);
});

// マスクコントロール
const gcMask = new InputMan.GcMask(document.getElementById('gcMask'));
gcMask.selectedIndex = -1;
gcMask.setFormatPattern('\\D{3}-\\D{4}-\\D{4}');

// ドロップダウンプラグイン
const gcDropDown2 = gcMask.createDropDown();

// ドロップダウンを開いたときに再描画します
gcDropDown2.onOpen(() => {
    if (!gcDropDown2.flexGrid) {
        // Wijmo FlexGridコントロール
        gcDropDown2.flexGrid = new FlexGrid(gcDropDown2.getElement().appendChild(document.createElement('div')), {
            itemsSource: employees,
            columns: [
                { binding: 'id', header: '社員ID', width: 60 },
                { binding: 'department', header: '部署', width: 70 },
                { binding: 'name', header: '名前', width: 100 },
                { binding: 'tel', header: '電話番号', width: 120 },
            ],
            selectionMode: SelectionMode.Row,
            headersVisibility: HeadersVisibility.Column,
            selectionChanged: (sender, args) => {
                if (gcDropDown2.isClosed()) {
                    return;
                }
                if (args.row === gcMask.selectedIndex) {
                    return;
                }
                gcMask.selectedIndex = args.row;
                gcMask.setValue(employees[args.row].tel);
                gcDropDown2.close();
            }
        });
        gcDropDown2.flexGrid.hostElement.style.height = '140px';
    } 
    gcDropDown2.flexGrid.select(gcMask.selectedIndex, 0);
});