import * as React from "react";
import * as ReactDom from "react-dom";
import { GcTextBox, GcMask } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

import '@grapecity/wijmo.styles/wijmo.css';
import './styles.css';
import './license';
import { FlexGrid, SelectionMode, HeadersVisibility } from '@grapecity/wijmo.grid';
import employees from './data';


class App extends React.Component{
    gcDropDown1 = null;
    gcDropDown2 = null;
    gcTextBox = null;
    gcMask = null;

    componentDidMount(){
        this.gcTextBox.selectedIndex = -1;
        this.gcMask.selectedIndex = -1;
        this.gcDropDown1.onOpen(() => {
            if (!this.gcDropDown1.flexGrid) {
                // Wijmo FlexGridコントロール
                this.gcDropDown1.flexGrid = new FlexGrid(this.gcDropDown1.getElement().appendChild(document.createElement('div')), {
                    itemsSource: employees,
                    columns: [
                        { binding: 'id', header: '社員ID', width: 60 },
                        { binding: 'department', header: '部署', width: 70 },
                        { binding: 'name', header: '名前', width: 100 },
                        { binding: 'tel', header: '電話番号', width: 120 },
                    ],
                    selectionMode: SelectionMode.Row,
                    headersVisibility: HeadersVisibility.Column,
                    selectionChanged: (sender, args) => {
                        if (this.gcDropDown1.isClosed()) {
                            return;
                        }
                        if (args.row === this.gcTextBox.selectedIndex) {
                            return;
                        }
                        this.gcTextBox.selectedIndex = args.row;
                        this.gcTextBox.setText(employees[args.row].name);
                        this.gcDropDown1.close();
                    }
                });
                this.gcDropDown1.flexGrid.hostElement.style.height = '140px';
            }
            this.gcDropDown1.flexGrid.select(this.gcTextBox.selectedIndex, 0);
        });


        this.gcDropDown2.onOpen(() => {
            if (!this.gcDropDown2.flexGrid) {
                // Wijmo FlexGridコントロール
                this.gcDropDown2.flexGrid = new FlexGrid(this.gcDropDown2.getElement().appendChild(document.createElement('div')), {
                    itemsSource: employees,
                    columns: [
                        { binding: 'id', header: '社員ID', width: 60 },
                        { binding: 'department', header: '部署', width: 70 },
                        { binding: 'name', header: '名前', width: 100 },
                        { binding: 'tel', header: '電話番号', width: 120 },
                    ],
                    selectionMode: SelectionMode.Row,
                    headersVisibility: HeadersVisibility.Column,
                    selectionChanged: (sender, args) => {
                        if (this.gcDropDown2.isClosed()) {
                            return;
                        }
                        if (args.row === this.gcMask.selectedIndex) {
                            return;
                        }
                        this.gcMask.selectedIndex = args.row;
                        this.gcMask.setValue(employees[args.row].tel);
                        this.gcDropDown2.close();
                    }
                });
                this.gcDropDown2.flexGrid.hostElement.style.height = '140px';
            } 
            this.gcDropDown2.flexGrid.select(this.gcMask.selectedIndex, 0);
        });
    }
    render(){
        return (
            <div class="flexbox">
                <div>
                    名前
                    <GcTextBox
                    onInitialized={(imCtrl) => { 
                        this.gcTextBox = imCtrl;
                        this.gcDropDown1 = imCtrl.createDropDown();
                     }}></GcTextBox>
                </div>
                <div>
                    電話番号
                   <GcMask formatPattern={'\\D{3}-\\D{4}-\\D{4}'}
                    onInitialized={(imCtrl) => { 
                        this.gcMask = imCtrl;
                        this.gcDropDown2 = imCtrl.createDropDown();
                     }}></GcMask>
                </div>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));