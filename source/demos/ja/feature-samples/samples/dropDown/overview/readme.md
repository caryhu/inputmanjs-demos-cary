## 概要

ドロップダウンプラグインは、InputManJSのコントロールにドロップダウン機能を追加できる機能です。ドロップダウンプラグインを使用することで、HTML要素やJavaScriptコントロールをドロップダウン表示させることが可能です。

例）テキストコントロールにWijmo FlexGridをドロップダウン表示

![null](</inputmanjs/demos/ja/images/dropdown_sample.png> =360x214)

以下の図は、ドロップダウンプラグインで組み合わせることができるコントロールの例です。

![null](</inputmanjs/demos/ja/images/dropdown_combine.png> =664x277)

## サンプルについて

以下のサンプルでは、HTML要素や弊社製品[Wijmo](<https://www.grapecity.co.jp/developer/wijmo>)および[SpreadJS](<https://www.grapecity.co.jp/developer/spreadjs>)のコントロールをドロップダウン表示する方法を解説します。

- [HTML要素の表示](@Demo/dropDown/html/purejs>)
- [Wijmo FlexGridの表示](@Demo/dropDown/flexGrid/purejs>)
- [Wijmo TreeViewの表示](@Demo/dropDown/treeView/purejs>)
- [SpreadJSの表示](@Demo/dropDown/spreadSheets/purejs>)
