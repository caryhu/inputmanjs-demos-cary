import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";
import GC2 from "@grapecity/spread-sheets";
import '@grapecity/spread-sheets/styles/gc.spread.sheets.css';
import './license';
import employees from './data';

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {

    public initGcTextBoxDropDown(gcTextBox: GC.InputMan.GcTextBox): void {
        // ドロップダウンプラグイン
        const gcDropDown1 = gcTextBox.createDropDown();
        gcDropDown1.setWidth(350);

        // GC2.Spread.Sheetsコントロール
        const hostElement1 = gcDropDown1.getElement();
        hostElement1.style.height = '200px';
        const workbook1 = new GC2.Spread.Sheets.Workbook(hostElement1, {
            tabStripVisible: false,
            tabNavigationVisible: false,
            showHorizontalScrollbar: false
        });
        const sheet1 = workbook1.getSheet(0);
        sheet1.selectionPolicy(GC2.Spread.Sheets.SelectionPolicy.single);
        sheet1.selectionUnit(GC2.Spread.Sheets.SelectionUnit.row);
        sheet1.setDataSource(employees);
        sheet1.bindColumns([
            { name: 'id', displayName: '社員ID', size: 60 },
            { name: 'department', displayName: '部署', size: 70 },
            { name: 'name', displayName: '名前', size: 100 },
            { name: 'tel', displayName: '電話番号', size: 110 },
        ]);
        sheet1.options.isProtected = true;
        sheet1.options.rowHeaderVisible = false;
        sheet1.bind(GC2.Spread.Sheets.Events.SelectionChanged, (e: any, args: any) => {
            gcTextBox.setText(employees[args.newSelections[0].row].name);
            gcDropDown1.close();
        });
    }

    public initGcMaskDropDown(gcMask: GC.InputMan.GcMask): void {
        // ドロップダウンプラグイン
        const gcDropDown2 = gcMask.createDropDown();
        gcDropDown2.setWidth(350);

        // GC2.Spread.Sheetsコントロール
        const hostElement2 = gcDropDown2.getElement();
        hostElement2.style.height = '200px';
        const workbook2 = new GC2.Spread.Sheets.Workbook(hostElement2, {
            tabStripVisible: false,
            tabNavigationVisible: false,
            showHorizontalScrollbar: false
        });
        const sheet2 = workbook2.getSheet(0);
        sheet2.selectionPolicy(GC2.Spread.Sheets.SelectionPolicy.single);
        sheet2.selectionUnit(GC2.Spread.Sheets.SelectionUnit.row);
        sheet2.setDataSource(employees);
        sheet2.bindColumns([
            { name: 'id', displayName: '社員ID', size: 60 },
            { name: 'department', displayName: '部署', size: 70 },
            { name: 'name', displayName: '名前', size: 100 },
            { name: 'tel', displayName: '電話番号', size: 110 },
        ]);
        sheet2.options.isProtected = true;
        sheet2.options.rowHeaderVisible = false;
        sheet2.bind(GC2.Spread.Sheets.Events.SelectionChanged, (e:any, args:any) => {
            gcMask.setValue(employees[args.newSelections[0].row].tel);
            gcDropDown2.close();
        });
    }

}


enableProdMode();
