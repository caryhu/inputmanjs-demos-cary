ドロップダウンプラグインを使用して、[SpreadJS](<https://www.grapecity.co.jp/developer/spreadjs>)をドロップダウン表示させる方法について解説します。

## ドロップダウンオブジェクトの追加

ドロップダウンプラグインを使用する場合、まずcreateDropDownメソッドを使用して、ドロップダウンオブジェクトをInputManJSのコントロールに追加する必要があります。

テキストコントロールにドロップダウンオブジェクトを追加する場合、以下のようなコードになります。

```
var gcTextBox = new GC.InputMan.GcTextBox(document.getElementById('gcTextBox'));
var gcDropDown1 = gcTextBox.createDropDown();
```

なお、createDropDownメソッドでは、ドロップダウンボタンを配置する位置をパラメータとして、指定することが可能です。設定可能な値（DropDownButtonAlignment列挙体）は、以下の通りです。

| 値                                | 説明                               |
| -------------------------------- | -------------------------------- |
| LeftSide                         | ドロップダウンボタンをコントロールの左側に配置します。      |
| RightSide                        | ドロップダウンボタンをコントロールの右側に配置します。（既定値） |

## SoreadJSの表示

SoreadJSをドロップダウン表示したい場合、スプレッドシート（GC.Spread.Sheets.Workbook）をドロップダウンオブジェクトの子ノードとして追加し、ワークシートを設定します。

たとえば、以下のようなコードになります。

```
var hostElement1 = gcDropDown1.getElement();
hostElement1.style.height = '200px';

// ドロップダウンオブジェクトの要素としてスプレッドシートを追加します。
var workbook1 = new GC.Spread.Sheets.Workbook(hostElement1, {
  tabStripVisible: false,
  tabNavigationVisible: false,
  showHorizontalScrollbar: false
});

// ワークシートを設定します。
var sheet1 = workbook1.getSheet(0);
sheet1.selectionPolicy(GC.Spread.Sheets.SelectionPolicy.single);
sheet1.selectionUnit(GC.Spread.Sheets.SelectionUnit.row);
sheet1.setDataSource(employees);
sheet1.bindColumns([
  { name: 'id', displayName: '社員ID', size: 60 },
  { name: 'department', displayName: '部署', size: 70 },
  { name: 'name', displayName: '名前', size: 100 },
  { name: 'tel', displayName: '電話番号', size: 110 },
]);
sheet1.options.isProtected = true;
sheet1.options.rowHeaderVisible = false;
```

## 選択時の処理

選択時の処理は、SoreadJS側のイベントを使用して行うことになります。具体的には、ワークシート上でアイテムが選択されたことを検知（SelectionChangedイベント）し、テキストコントロールに選択された値を表示した上でcloseメソッドでドロップダウンを閉じるような処理を実装する必要があります。

たとえば、以下のようなコードになります。

```
sheet1.bind(GC.Spread.Sheets.Events.SelectionChanged, function (e, args) {
  gcTextBox.setText(employees[args.newSelections[0].row].name);
  gcDropDown1.close();
});
```

