import '@grapecity/inputman/CSS/gc.inputman-js.css';
import '@grapecity/spread-sheets/styles/gc.spread.sheets.css';
import './styles.css';
import './license';
import { InputMan } from '@grapecity/inputman';
import { Spread } from '@grapecity/spread-sheets';
import employees from './data';

// テキストコントロール
const gcTextBox = new InputMan.GcTextBox(document.getElementById('gcTextBox'));

// ドロップダウンプラグイン
const gcDropDown1 = gcTextBox.createDropDown();
gcDropDown1.setWidth(350);

// Spread.Sheetsコントロール
const hostElement1 = gcDropDown1.getElement();
hostElement1.style.height = '200px';
const workbook1 = new Spread.Sheets.Workbook(hostElement1, {
    tabStripVisible: false,
    tabNavigationVisible: false,
    showHorizontalScrollbar: false
});
const sheet1 = workbook1.getSheet(0);
sheet1.selectionPolicy(Spread.Sheets.SelectionPolicy.single);
sheet1.selectionUnit(Spread.Sheets.SelectionUnit.row);
sheet1.setDataSource(employees);
sheet1.bindColumns([
    { name: 'id', displayName: '社員ID', size: 60 },
    { name: 'department', displayName: '部署', size: 70 },
    { name: 'name', displayName: '名前', size: 100 },
    { name: 'tel', displayName: '電話番号', size: 110 },
]);
sheet1.options.isProtected = true;
sheet1.options.rowHeaderVisible = false;
sheet1.bind(Spread.Sheets.Events.SelectionChanged, (e, args) => {
    gcTextBox.setText(employees[args.newSelections[0].row].name);
    gcDropDown1.close();
});

// マスクコントロール
const gcMask = new InputMan.GcMask(document.getElementById('gcMask'));
gcMask.setFormatPattern('\\D{3}-\\D{4}-\\D{4}');

// ドロップダウンプラグイン
const gcDropDown2 = gcMask.createDropDown();
gcDropDown2.setWidth(350);

// Spread.Sheetsコントロール
const hostElement2 = gcDropDown2.getElement();
hostElement2.style.height = '200px';
const workbook2 = new Spread.Sheets.Workbook(hostElement2, {
    tabStripVisible: false,
    tabNavigationVisible: false,
    showHorizontalScrollbar: false
});
const sheet2 = workbook2.getSheet(0);
sheet2.selectionPolicy(Spread.Sheets.SelectionPolicy.single);
sheet2.selectionUnit(Spread.Sheets.SelectionUnit.row);
sheet2.setDataSource(employees);
sheet2.bindColumns([
    { name: 'id', displayName: '社員ID', size: 60 },
    { name: 'department', displayName: '部署', size: 70 },
    { name: 'name', displayName: '名前', size: 100 },
    { name: 'tel', displayName: '電話番号', size: 110 },
]);
sheet2.options.isProtected = true;
sheet2.options.rowHeaderVisible = false;
sheet2.bind(Spread.Sheets.Events.SelectionChanged, (e, args) => {
    gcMask.setValue(employees[args.newSelections[0].row].tel);
    gcDropDown2.close();
});