import * as React from "react";
import * as ReactDom from "react-dom";
import { GcTextBox, GcMask } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

import '@grapecity/spread-sheets/styles/gc.spread.sheets.css';
import './styles.css';
import './license';
import { Spread } from '@grapecity/spread-sheets';
import employees from './data';

class App extends React.Component{

    gcDropDown1 = null;
    gcDropDown2 = null;
    gcTextBox = null;
    gcMask = null;

    componentDidMount(){
        // Spread.Sheetsコントロール
        const hostElement1 = this.gcDropDown1.getElement();
        hostElement1.style.height = '200px';
        const workbook1 = new Spread.Sheets.Workbook(hostElement1, {
            tabStripVisible: false,
            tabNavigationVisible: false,
            showHorizontalScrollbar: false
        });
        const sheet1 = workbook1.getSheet(0);
        sheet1.selectionPolicy(Spread.Sheets.SelectionPolicy.single);
        sheet1.selectionUnit(Spread.Sheets.SelectionUnit.row);
        sheet1.setDataSource(employees);
        sheet1.bindColumns([
            { name: 'id', displayName: '社員ID', size: 60 },
            { name: 'department', displayName: '部署', size: 70 },
            { name: 'name', displayName: '名前', size: 100 },
            { name: 'tel', displayName: '電話番号', size: 110 },
        ]);
        sheet1.options.isProtected = true;
        sheet1.options.rowHeaderVisible = false;
        sheet1.bind(Spread.Sheets.Events.SelectionChanged, (e, args) => {
            this.gcTextBox.setText(employees[args.newSelections[0].row].name);
            this.gcDropDown1.close();
        });


        // Spread.Sheetsコントロール
        const hostElement2 = this.gcDropDown2.getElement();
        hostElement2.style.height = '200px';
        const workbook2 = new Spread.Sheets.Workbook(hostElement2, {
            tabStripVisible: false,
            tabNavigationVisible: false,
            showHorizontalScrollbar: false
        });
        const sheet2 = workbook2.getSheet(0);
        sheet2.selectionPolicy(Spread.Sheets.SelectionPolicy.single);
        sheet2.selectionUnit(Spread.Sheets.SelectionUnit.row);
        sheet2.setDataSource(employees);
        sheet2.bindColumns([
            { name: 'id', displayName: '社員ID', size: 60 },
            { name: 'department', displayName: '部署', size: 70 },
            { name: 'name', displayName: '名前', size: 100 },
            { name: 'tel', displayName: '電話番号', size: 110 },
        ]);
        sheet2.options.isProtected = true;
        sheet2.options.rowHeaderVisible = false;
        sheet2.bind(Spread.Sheets.Events.SelectionChanged, (e, args) => {
            this.gcMask.setValue(employees[args.newSelections[0].row].tel);
            this.gcDropDown2.close();
        });
    }

    render(){
        return (
            <div class="flexbox">
                <div>
                    名前
                    <GcTextBox onInitialized={(imCtrl) => { 
                        this.gcTextBox = imCtrl;
                        this.gcDropDown1 = imCtrl.createDropDown();
                        this.gcDropDown1.setWidth(350);
                     }}></GcTextBox>
                </div>
                <div>
                    電話番号
                    <GcMask formatPattern={'\\D{3}-\\D{4}-\\D{4}'}
                     onInitialized={(imCtrl) => { 
                        this.gcMask = imCtrl;
                        this.gcDropDown2 = imCtrl.createDropDown();
                        this.gcDropDown2.setWidth(350);
                     }}></GcMask>
                </div>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));