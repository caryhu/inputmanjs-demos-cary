ドロップダウンプラグインを使用して、[Wijmo](<https://www.grapecity.co.jp/developer/wijmo>)のTreeViewコントロールをドロップダウン表示させる方法について解説します。

## ドロップダウンオブジェクトの追加

ドロップダウンプラグインを使用する場合、まずcreateDropDownメソッドを使用して、ドロップダウンオブジェクトをInputManJSのコントロールに追加する必要があります。

テキストコントロールにドロップダウンオブジェクトを追加する場合、以下のようなコードになります。

```
var gcTextBox = new GC.InputMan.GcTextBox(document.getElementById('gcTextBox'));
var gcDropDown = gcTextBox.createDropDown();
```

なお、createDropDownメソッドでは、ドロップダウンボタンを配置する位置をパラメータとして、指定することが可能です。設定可能な値（DropDownButtonAlignment列挙体）は、以下の通りです。

| 値                                | 説明                               |
| -------------------------------- | -------------------------------- |
| LeftSide                         | ドロップダウンボタンをコントロールの左側に配置します。      |
| RightSide                        | ドロップダウンボタンをコントロールの右側に配置します。（既定値） |

## TreeViewの表示

TreeViewをドロップダウン表示したい場合、TreeViewをドロップダウンオブジェクトの子ノードとして追加します。

たとえば、以下のようなコードになります。

```
var treeView = new wijmo.nav.TreeView(gcDropDown.getElement().appendChild(document.createElement('div')), {
  itemsSource: prefectures,
  displayMemberPath: 'name',
  isAnimated: false,
  selectedItemChanged: function (s, e) {
    if (!s.selectedNode.hasChildren) {
      // （選択したノードが子ノードを持たない場合）
      gcTextBox.setText(s.selectedItem.name);
      gcDropDown.close();
    }
  }
});
```

## 選択時の処理

選択時の処理は、TreeView側のイベントを使用して行うことになります。具体的には、アイテムが選択されたことを検知（selectedItemChangedイベント）し、テキストコントロールに選択された値を表示した上でcloseメソッドでドロップダウンを閉じるような処理を実装する必要があります。

たとえば、上記サンプルの場合、以下のコードがTreeView側で行が選択された時の処理になります。

```
selectedItemChanged: function (s, e) {
    if (!s.selectedNode.hasChildren) {
      // （選択したノードが子ノードを持たない場合）
      gcTextBox.setText(s.selectedItem.name);
      gcDropDown.close();
    }
  }
```

