import '@grapecity/inputman/CSS/gc.inputman-js.css';
import '@grapecity/wijmo.styles/wijmo.css';
import './styles.css';
import './license';
import { InputMan } from '@grapecity/inputman';
import { TreeView } from '@grapecity/wijmo.nav'
import prefectures from './data';

// テキストコントロール
const gcTextBox = new InputMan.GcTextBox(document.getElementById('gcTextBox'));

// ドロップダウンプラグイン
const gcDropDown = gcTextBox.createDropDown();

// Wijmo TreeViewコントロール
const treeView = new TreeView(gcDropDown.getElement().appendChild(document.createElement('div')), {
    itemsSource: prefectures,
    displayMemberPath: 'name',
    isAnimated: false,
    selectedItemChanged: (s, e) => {
        if (!s.selectedNode.hasChildren) {
            gcTextBox.setText(s.selectedItem.name);
            gcDropDown.close();
        }
    }
});