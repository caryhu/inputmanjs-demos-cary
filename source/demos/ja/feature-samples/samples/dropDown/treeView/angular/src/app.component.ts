import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import '@grapecity/wijmo.styles/wijmo.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";
import { TreeView } from '@grapecity/wijmo.nav'
import prefectures from './data';
import "./license";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {

    public initTextBoxDropDown(gcTextBox: GC.InputMan.GcTextBox): void {
        // ドロップダウンプラグイン
        const gcDropDown = gcTextBox.createDropDown();

        // Wijmo TreeViewコントロール
        const treeView = new TreeView(gcDropDown.getElement().appendChild(document.createElement('div')), {
            itemsSource: prefectures,
            displayMemberPath: 'name',
            isAnimated: false,
            selectedItemChanged: (s:any, e:any) => {
                if (!s.selectedNode.hasChildren) {
                    gcTextBox.setText(s.selectedItem.name);
                    gcDropDown.close();
                }
            }
        });
    }
}


enableProdMode();