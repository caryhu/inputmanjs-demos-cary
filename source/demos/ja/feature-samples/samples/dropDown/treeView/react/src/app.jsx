import * as React from "react";
import * as ReactDom from "react-dom";
import { GcTextBox } from "@grapecity/inputman.react";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import '@grapecity/wijmo.styles/wijmo.css';
import './styles.css';
import './license';
import { TreeView } from '@grapecity/wijmo.nav'
import prefectures from './data';

class App extends React.Component{

    gcDropDown = null;
    gcTextBox = null;

    createTreeView(){
        // Wijmo TreeViewコントロール
        const treeView = new TreeView(this.gcDropDown.getElement().appendChild(document.createElement('div')), {
            itemsSource: prefectures,
            displayMemberPath: 'name',
            isAnimated: false,
            selectedItemChanged: (s, e) => {
                if (!s.selectedNode.hasChildren) {
                    this.gcTextBox.setText(s.selectedItem.name);
                    this.gcDropDown.close();
                }
            }
        });
    }

    render(){
        return (
            <div>
                都道府県
                <GcTextBox onInitialized={(imCtrl) => { 
                        this.gcTextBox = imCtrl;
                        this.gcDropDown = imCtrl.createDropDown();
                        this.createTreeView();
                     }}></GcTextBox>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));