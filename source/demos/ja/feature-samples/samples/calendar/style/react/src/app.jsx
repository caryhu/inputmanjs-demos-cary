import * as React from "react";
import * as ReactDom from "react-dom";
import { GcCalendar } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{
    gcCalendar = null;
    render(){
        return <GcCalendar 
                    showRokuyou= {InputMan.Rokuyou.All} 
                    onInitialized={(imCtrl)=>{ 
                        this.gcCalendar = imCtrl; 
                        this.addHolidayGroup();
                }}/>
    }

    addHolidayGroup(){
        const holidayGroup = new InputMan.HolidayGroup('holiday', true);
        holidayGroup.addHoliday(new InputMan.Holiday('元旦', 1, 1));
        holidayGroup.addHoliday(new InputMan.DayOfWeekHoliday('成人の日', InputMan.MonthFlags.January, InputMan.WeekFlags.Second, InputMan.DayOfWeek.Monday));
        this.gcCalendar.addHolidayGroup(holidayGroup);

        // 休業日を設定します。
        this.gcCalendar.getWeekday('sunday').setWeekFlags(InputMan.WeekFlags.All);
        this.gcCalendar.getWeekday('monday').setWeekFlags(InputMan.WeekFlags.All);
        this.gcCalendar.getWeekday('tuesday').setWeekFlags(InputMan.WeekFlags.All);
        this.gcCalendar.getWeekday('wednesday').setWeekFlags(InputMan.WeekFlags.All);
        this.gcCalendar.getWeekday('thursday').setWeekFlags(InputMan.WeekFlags.All);
        this.gcCalendar.getWeekday('friday').setWeekFlags(InputMan.WeekFlags.All);
        this.gcCalendar.getWeekday('saturday').setWeekFlags(InputMan.WeekFlags.All);
    }
}

ReactDom.render(<App />, document.getElementById("app"));