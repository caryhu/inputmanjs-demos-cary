import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';


const gcCalendar = new InputMan.GcCalendar(document.getElementById('gcCalendar'), {
    // 六曜を表示します。
    showRokuyou: InputMan.Rokuyou.All
});

// 休日を設定します
const holidayGroup = new InputMan.HolidayGroup('holiday', true);
holidayGroup.addHoliday(new InputMan.Holiday('元旦', 1, 1));
holidayGroup.addHoliday(new InputMan.DayOfWeekHoliday('成人の日', InputMan.MonthFlags.January, InputMan.WeekFlags.Second, InputMan.DayOfWeek.Monday));
gcCalendar.addHolidayGroup(holidayGroup);

// 休業日を設定します。
gcCalendar.getWeekday('sunday').setWeekFlags(InputMan.WeekFlags.All);
gcCalendar.getWeekday('monday').setWeekFlags(InputMan.WeekFlags.All);
gcCalendar.getWeekday('tuesday').setWeekFlags(InputMan.WeekFlags.All);
gcCalendar.getWeekday('wednesday').setWeekFlags(InputMan.WeekFlags.All);
gcCalendar.getWeekday('thursday').setWeekFlags(InputMan.WeekFlags.All);
gcCalendar.getWeekday('friday').setWeekFlags(InputMan.WeekFlags.All);
gcCalendar.getWeekday('saturday').setWeekFlags(InputMan.WeekFlags.All);
