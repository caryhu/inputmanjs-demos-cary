カレンダーコントロールで使用されている表示スタイルについて解説します。

## スタイルの設定方法

カレンダーコントロールのスタイルは、CSSで設定します。カレンダーを構成する要素のクラス名を指定して、そのクラスのスタイルを設定します。

次のサンプルコードは、「今日の日付ボタン」要素のスタイルを設定する方法を示します。

```
/* 今日の日付ボタンのスタイル */
.gcim-calendar__today-button {
  color: green;
}
```

.gcim-calendar\_\_today-buttonは、「今日の日付」の要素に割り当てられるクラス名です。ここではcolorプロパティで文字色を設定していますが、その他にも、背景色、フォント、余白、境界線などの様々なスタイルを設定できます。CSSにおけるスタイル設定方法については、CSSのリファレンスを参照してください。

## クラス名

カレンダーを構成する要素のクラス名は次の通りです。

| 要素                                                                                      | クラス名                                                                                    |
| --------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------- |
| コントロール                                                                                  | gcim-calendar\_\_outter-container                                                       |
| ヘッダ                                                                                     | gcim-calendar\_\_header                                                                 |
| 曜日タイトル                                                                                  | gcim-calendar\_\_month-body\_\_week-title<br>gcim-calendar\_\_month-body\_\_corner-cell |
| 今日の日付ボタン                                                                                | gcim-calendar\_\_today-button                                                           |
| 週番号                                                                                     | gcim-calendar\_\_month-body\_\_week-number                                              |
| 日曜                                                                                      | gcim-calendar\_\_weekday\_sunday                                                        |
| 月曜                                                                                      | gcim-calendar\_\_weekday\_monday                                                        |
| 火曜                                                                                      | gcim-calendar\_\_weekday\_tuesday                                                       |
| 水曜                                                                                      | gcim-calendar\_\_weekday\_wednesday                                                     |
| 木曜                                                                                      | gcim-calendar\_\_weekday\_thursday                                                      |
| 金曜                                                                                      | gcim-calendar\_\_weekday\_friday                                                        |
| 土曜                                                                                      | gcim-calendar\_\_weekday\_saturday                                                      |
| 休日                                                                                      | calendar-holiday-type属性<br>※休日はスタイルでなく属性名で指定する必要があります。                                  |
| 隣接日                                                                                     | gcim-calendar\_\_month-body\_\_trailing-day                                             |
| 選択日                                                                                     | gcim-calendar\_\_month-body\_\_selected-day                                             |
| フォーカス日                                                                                  | gcim-calendar\_\_month-body\_\_focusd-day                                               |
| 六曜                                                                                      | gcim-calendar\_\_month-body\_\_rokuyou                                                  |
| 今日                                                                                      | gcim-calendar\_\_month-body\_\_today                                                    |

