import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public showRokuyou: GC.InputMan.Rokuyou = GC.InputMan.Rokuyou.All;

    public onInitialized(gcCalendar: GC.InputMan.GcCalendar): void {
        this.addHolidayGroup(gcCalendar);
    }

    public addHolidayGroup(gcCalendar: GC.InputMan.GcCalendar): void {
        // 休日を設定します
        const holidayGroup = new GC.InputMan.HolidayGroup('holiday', true);
        holidayGroup.addHoliday(new GC.InputMan.Holiday('元旦', 1, 1));
        holidayGroup.addHoliday(new GC.InputMan.DayOfWeekHoliday('成人の日', GC.InputMan.MonthFlags.January, GC.InputMan.WeekFlags.Second, GC.InputMan.DayOfWeek.Monday));
        gcCalendar.addHolidayGroup(holidayGroup);

        // 休業日を設定します。
        gcCalendar.getWeekday('sunday').setWeekFlags(GC.InputMan.WeekFlags.All);
        gcCalendar.getWeekday('monday').setWeekFlags(GC.InputMan.WeekFlags.All);
        gcCalendar.getWeekday('tuesday').setWeekFlags(GC.InputMan.WeekFlags.All);
        gcCalendar.getWeekday('wednesday').setWeekFlags(GC.InputMan.WeekFlags.All);
        gcCalendar.getWeekday('thursday').setWeekFlags(GC.InputMan.WeekFlags.All);
        gcCalendar.getWeekday('friday').setWeekFlags(GC.InputMan.WeekFlags.All);
        gcCalendar.getWeekday('saturday').setWeekFlags(GC.InputMan.WeekFlags.All);
    }
}

enableProdMode();