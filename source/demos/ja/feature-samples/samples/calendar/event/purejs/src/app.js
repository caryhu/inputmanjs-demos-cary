import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';


const gcCalendar = new InputMan.GcCalendar(document.getElementById('gcCalendar'));

// イベントハンドラ
gcCalendar.onClickDate((sender, eArgs) => {
    log('onClickDate');
});
gcCalendar.onFocusDateChanged((sender, eArgs) => {
    log('onFocusDateChanged');
});
gcCalendar.onScrolled((sender, eArgs) => {
    log('onScrolled');
});
gcCalendar.onSelectedDateChanged((sender, eArgs) => {
    log('onSelectedDateChanged');
});

// テキストボックスにログを出力
const log = (message) => {
    var textarea = document.getElementById('log');
    textarea.value += message + '\n';
    textarea.scrollTop = textarea.scrollHeight;
}