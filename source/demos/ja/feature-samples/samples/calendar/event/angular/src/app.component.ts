import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, ViewChild, ElementRef } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public message: string = "";
    @ViewChild('logPanel')
    logPanel: ElementRef;

    // イベントハンドラ
    public onClickDate() {
        this.log('onClickDate');
    };
    public onFocusDateChanged() {
        this.log('onFocusDateChanged');
    };
    public onScrolled() {
        this.log('onScrolled');
    };
    public onSelectedDateChanged() {
        this.log('onSelectedDateChanged');
    };

    // テキストボックスにログを出力
    public log(message: string) {
        this.message += message + '\n';
        this.logPanel.nativeElement.scrollTop = this.logPanel.nativeElement.scrollHeight;
    }
}

enableProdMode();