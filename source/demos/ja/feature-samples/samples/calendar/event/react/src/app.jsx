import * as React from "react";
import * as ReactDom from "react-dom";
import { GcCalendar } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            message: ''
        };
    }

    log(msg){
        var newMessage = this.state.message + msg + '\n';
        this.refs.log.scrollTop = this.refs.log.scrollHeight;
        this.setState({message: newMessage});
    }

    render(){
        return (
            <div>
                <GcCalendar
                onClickDate = {() => {
                    this.log('onClickDate');
                }}
                onFocusDateChanged = {() =>{
                    this.log('onFocusDateChanged');
                }}
                onScrolled = {()=>{
                    this.log('onScrolled');
                }}
                onSelectedDateChanged = {()=>{
                    this.log('onSelectedDateChanged');
                }}/>
                <br/><br/>
                イベント<br/>
                <textarea ref='log' id="log" rows="10" value={this.state.message} cols="30"></textarea>
            </div>
        );
    }
}

ReactDom.render(<App />, document.getElementById("app"));