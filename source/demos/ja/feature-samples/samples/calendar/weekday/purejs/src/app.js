import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';


const gcCalendar = new InputMan.GcCalendar(document.getElementById('gcCalendar'));

// 土日と第2／4水曜を休業日に設定します。
gcCalendar.getWeekday('sunday').setWeekFlags(InputMan.WeekFlags.All);
gcCalendar.getWeekday('saturday').setWeekFlags(InputMan.WeekFlags.All);
gcCalendar.getWeekday('wednesday').setWeekFlags(InputMan.WeekFlags.Second | InputMan.WeekFlags.Fourth);