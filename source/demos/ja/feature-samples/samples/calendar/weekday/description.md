休業日（毎週の定期的な休み）を設定する方法について解説します。

※曜日の色を変更するには、休業日を設定する必要があります。

## 休業日の設定

休業日は、次の手順で設定します。

1. GcCalendar.getWeekdayメソッドの引数に曜日名（'sunday'など）を指定して、指定した曜日のWeekDayオブジェクトを取得します。
2. WeekDay.setWeekFlagsメソッドで休業日にする週（例：第2週と第4週）を指定します。
3. CSSの'.gcim-calendar\_\_weekday\_曜日名'セレクタで休業日のスタイルを設定して、休業日を識別できるようにします。

<!-- -->

次のサンプルコードは、土日と第2／4水曜を休業日に設定します。

```
// 土日と第2／4水曜を休業日に設定します。
gcCalendar.getWeekday('sunday').setWeekFlags(GC.InputMan.WeekFlags.All);
gcCalendar.getWeekday('saturday').setWeekFlags(GC.InputMan.WeekFlags.All);
gcCalendar.getWeekday('wednesday').setWeekFlags(GC.InputMan.WeekFlags.Second | GC.InputMan.WeekFlags.Fourth);
```

```
/* 日曜と休業日のスタイル */
.gcim-calendar__weekday_sunday, .gcim-calendar__weekday_wednesday {
  color: red;
}

/* 土曜のスタイル */
.gcim-calendar__weekday_saturday {
  color: blue;
}
```

