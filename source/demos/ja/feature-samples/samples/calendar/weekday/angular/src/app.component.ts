import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public onInitialized(sender: GC.InputMan.GcCalendar): void {
        var gcCalendar = sender;
        // 土日と第2／4水曜を休業日に設定します。
        gcCalendar.getWeekday('sunday').setWeekFlags(GC.InputMan.WeekFlags.All);
        gcCalendar.getWeekday('saturday').setWeekFlags(GC.InputMan.WeekFlags.All);
        gcCalendar.getWeekday('wednesday').setWeekFlags(GC.InputMan.WeekFlags.Second | GC.InputMan.WeekFlags.Fourth);
    }
}

enableProdMode();