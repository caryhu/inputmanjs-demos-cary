import * as React from "react";
import * as ReactDom from "react-dom";
import { GcCalendar } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{
    gcCalendar = null;
    render(){
        return <GcCalendar 
            onInitialized={(imCtrl)=>{ 
                this.gcCalendar = imCtrl; 
                this.addWeekFlag();
        }}></GcCalendar>
    }

    addWeekFlag(){
        // 土日と第2／4水曜を休業日に設定します。
        this.gcCalendar.getWeekday('sunday').setWeekFlags(InputMan.WeekFlags.All);
        this.gcCalendar.getWeekday('saturday').setWeekFlags(InputMan.WeekFlags.All);
        this.gcCalendar.getWeekday('wednesday').setWeekFlags(InputMan.WeekFlags.Second | InputMan.WeekFlags.Fourth);
    }
}

ReactDom.render(<App />, document.getElementById("app"));