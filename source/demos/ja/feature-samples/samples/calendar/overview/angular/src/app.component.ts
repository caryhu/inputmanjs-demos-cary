import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public showRokuyou: GC.InputMan.Rokuyou = GC.InputMan.Rokuyou.All;

    public setWeekFlag(sender: GC.InputMan.GcCalendar) {
        var gcCalendar1 = sender;
        gcCalendar1.getWeekday('sunday').setWeekFlags(GC.InputMan.WeekFlags.All);
        gcCalendar1.getWeekday('saturday').setWeekFlags(GC.InputMan.WeekFlags.All);
    }
}

enableProdMode();