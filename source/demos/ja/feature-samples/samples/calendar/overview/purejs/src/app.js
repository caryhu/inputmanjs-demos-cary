import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';
import "./styles.css";

const gcCalendar1 = new InputMan.GcCalendar(document.getElementById('gcCalendar1'));
gcCalendar1.getWeekday('sunday').setWeekFlags(InputMan.WeekFlags.All);
gcCalendar1.getWeekday('saturday').setWeekFlags(InputMan.WeekFlags.All);

const gcCalendar2 = new InputMan.GcCalendar(document.getElementById('gcCalendar2'), {
    headerFormat: 'gggE年 M月',
    yearMonthFormat: 'gggE年,M月'
});

const gcCalendar3 = new InputMan.GcCalendar(document.getElementById('gcCalendar3'), {
    showRokuyou: InputMan.Rokuyou.All
});

const gcCalendar4 = new InputMan.GcCalendar(document.getElementById('gcCalendar4'), {
    calendarDimensions: {
        width: 3,
        height: 2
    }
});