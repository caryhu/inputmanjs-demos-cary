import * as React from "react";
import * as ReactDom from "react-dom";
import { GcCalendar } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{
    gcCalendar = null;
    render(){
        return (
            <div>
                休業日<br/>
                <GcCalendar onInitialized={(imCtrl)=>{ this.gcCalendar = imCtrl; }}/><br/>
                和暦<br/>
                <GcCalendar headerFormat= {'gggE年 M月'} yearMonthFormat= {'gggE年,M月'}/><br/>
                六曜<br/>
                <GcCalendar showRokuyou= {InputMan.Rokuyou.All}/><br/>
                複数月<br/>
                <GcCalendar dimensions= {{width: 3, height: 2}}/>    
            </div>
        )
    }

    componentDidMount(){
        this.gcCalendar.getWeekday('sunday').setWeekFlags(InputMan.WeekFlags.All);
        this.gcCalendar.getWeekday('saturday').setWeekFlags(InputMan.WeekFlags.All);
    }
}

ReactDom.render(<App />, document.getElementById("app"));