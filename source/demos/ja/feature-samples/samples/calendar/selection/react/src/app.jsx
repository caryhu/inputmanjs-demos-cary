import * as React from "react";
import * as ReactDom from "react-dom";
import { GcCalendar } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

const calendarSelectionMode = [
    InputMan.CalendarSelectionMode.One,
    InputMan.CalendarSelectionMode.MultiSimple,
    InputMan.CalendarSelectionMode.MultiRich
];
const allowSelections = [
    InputMan.AllowSelection.Anyday,
    InputMan.AllowSelection.Workday,
    InputMan.AllowSelection.Holiday
];

class App extends React.Component{
    gcCalendar = null;

    addHolidayGroup(){
        const holidayGroup = new InputMan.HolidayGroup('holiday', true);
        const workdayGroup = new InputMan.HolidayGroup('workday', true);

        // 振替休日を有効にします
        holidayGroup.setWeekFlags('sunday', InputMan.WeekFlags.All);
        holidayGroup.setOverride('sunday', InputMan.HolidayOverride.NextWorkDay);
        // 振替休日用のツールチップテキスト
        this.gcCalendar.setOverrideTipText('振替休日');

        // 固定の祝日
        holidayGroup.addHoliday(new InputMan.Holiday('元旦', 1, 1));
        holidayGroup.addHoliday(new InputMan.Holiday('建国記念の日', 2, 11));
        holidayGroup.addHoliday(new InputMan.Holiday('昭和の日', 4, 29));
        holidayGroup.addHoliday(new InputMan.Holiday('憲法記念日', 5, 3));
        holidayGroup.addHoliday(new InputMan.Holiday('みどりの日', 5, 4));
        holidayGroup.addHoliday(new InputMan.Holiday('こどもの日', 5, 5));
        holidayGroup.addHoliday(new InputMan.Holiday('文化の日', 11, 3));
        holidayGroup.addHoliday(new InputMan.Holiday('勤労感謝の日', 11, 23));

        // ハッピーマンデー
        holidayGroup.addHoliday(new InputMan.DayOfWeekHoliday('成人の日', InputMan.MonthFlags.January, InputMan.WeekFlags.Second, InputMan.DayOfWeek.Monday));
        holidayGroup.addHoliday(new InputMan.DayOfWeekHoliday('海の日', InputMan.MonthFlags.July, InputMan.WeekFlags.Third, InputMan.DayOfWeek.Monday));
        holidayGroup.addHoliday(new InputMan.DayOfWeekHoliday('敬老の日', InputMan.MonthFlags.September, InputMan.WeekFlags.Third, InputMan.DayOfWeek.Monday));
        holidayGroup.addHoliday(new InputMan.DayOfWeekHoliday('体育の日', InputMan.MonthFlags.October, InputMan.WeekFlags.Second, InputMan.DayOfWeek.Monday));

        // 2019年のみの祝日
        holidayGroup.addHoliday(new InputMan.ForceHoliday('国民の休日', '2019/4/30'));
        holidayGroup.addHoliday(new InputMan.ForceHoliday('天皇の即位の日', '2019/5/1'));
        holidayGroup.addHoliday(new InputMan.ForceHoliday('国民の休日', '2019/5/2'));
        holidayGroup.addHoliday(new InputMan.ForceHoliday('即位礼正殿の儀', '2019/10/22'));
        // 2020年のみの祝日
        holidayGroup.addHoliday(new InputMan.ForceHoliday('海の日', '2020/7/23'));
        holidayGroup.addHoliday(new InputMan.ForceHoliday('スポーツの日', '2020/7/24'));
        holidayGroup.addHoliday(new InputMan.ForceHoliday('山の日', '2020/8/10'));
        // 2020年のみ平日扱い
        workdayGroup.addHoliday(new InputMan.ForceWorkday('', '2020/7/20'));
        workdayGroup.addHoliday(new InputMan.ForceWorkday('', '2020/8/11'));
        workdayGroup.addHoliday(new InputMan.ForceWorkday('', '2020/10/12'));

        // 年により変動のある祝日を設定します。
        // 2000年から2050年までを対象としています。
        for (var currentYear = 2000; currentYear <= 2050; currentYear++) {
            var syunbun;
            var syubun;
            switch (currentYear % 4) {
                case 0:
                    // 春分の日を設定します。
                    if (currentYear < 1960) syunbun = currentYear + '/3/21';
                    else if (currentYear >= 1960 && currentYear < 2092) syunbun = currentYear + '/3/20';
                    else syunbun = currentYear + '/3/19';

                    // 秋分の日を設定します。
                    if (currentYear < 2012) syubun = currentYear + '/9/23';
                    else syubun = currentYear + '/9/22';

                    break;
                case 1:
                    // 春分の日を設定します。
                    if (currentYear < 1993) syunbun = currentYear + '/3/21';
                    else syunbun = currentYear + '/3/20';

                    // 秋分の日を設定します。
                    if (currentYear < 1921) syubun = currentYear + '/9/24';
                    else if (currentYear >= 1921 && currentYear < 2045) syubun = currentYear + '/9/23';
                    else syubun = currentYear + '/9/22';

                    break;
                case 2:
                    // 春分の日を設定します。
                    if (currentYear < 2026) syunbun = currentYear + '/3/21';
                    else syunbun = currentYear + '/3/20';

                    // 秋分の日を設定します。
                    if (currentYear < 1950) syubun = currentYear + '/9/24';
                    else if (currentYear >= 1950 && currentYear < 2078) syubun = currentYear + '/9/23';
                    else syubun = currentYear + '/9/22';

                    break;
                case 3:
                    // 春分の日を設定します。
                    if (currentYear < 1927) syunbun = currentYear + '/3/22';
                    else if (currentYear >= 1927 && currentYear < 2059) syunbun = currentYear + '/3/21';
                    else syunbun = currentYear + '/3/20';

                    // 秋分の日を設定します。
                    if (currentYear < 1983) syubun = currentYear + '/9/24';
                    else syubun = currentYear + '/9/23';

                    break;
            }
            // 春分の日、秋分の日を追加します。
            holidayGroup.addHoliday(new InputMan.ForceHoliday('春分の日', syunbun));
            holidayGroup.addHoliday(new InputMan.ForceHoliday('秋分の日', syubun));
            // 2016年以降「山の日」を追加します。
            if (currentYear > 2016) {
                holidayGroup.addHoliday(new InputMan.ForceHoliday('山の日', currentYear + '/8/11'));
            }
            // 天皇誕生日を追加します。
            if (currentYear < 2019) {
                holidayGroup.addHoliday(new InputMan.ForceHoliday('天皇誕生日', currentYear + '/12/23'));
            }
            else if (currentYear > 2019) {
                holidayGroup.addHoliday(new InputMan.ForceHoliday('天皇誕生日', currentYear + '/2/23'));
            }
            // 2つの祝日に挟まれた営業日を休日に設定します。
            if (currentYear > 2003) {
                const syubunDate = new Date(syubun);
                if (syubunDate.getDay() == 3) {
                    syubunDate.setDate(syubunDate.getDate() - 1);
                    holidayGroup.addHoliday(new InputMan.ForceHoliday('国民の休日', syubunDate));
                }
            }
        }
        this.gcCalendar.addHolidayGroup(holidayGroup);
        this.gcCalendar.addHolidayGroup(workdayGroup);
    }

    constructor(props, context) {
        super(props, context);
        this.state = {
            selectionMode: InputMan.CalendarSelectionMode.One,
            weekNumberSelect: false,
            WeekTitleSelect: false,
            allowSelection: 0,
            maxSelectionCount: 14
        };
    }

    render(){
        return (
            <div>
                <table>
                    <tr>
                        <td>
                            <GcCalendar 
                            maxSelectionCount= {this.state.maxSelectionCount}
                            selectionMode= {this.state.selectionMode}
                            weekNumberSelect= {this.state.weekNumberSelect}
                            weekTitleSelect= {this.state.weekTitleSelect}
                            allowSelection= {this.state.allowSelection}
                            onInitialized={(imCtrl)=>{ 
                                this.gcCalendar = imCtrl; 
                                this.addHolidayGroup();
                            }}
                            onSelectedDateChanged={(sender) => {
                                const selections = document.getElementById('selections');
                                const dates = this.gcCalendar.getSelections();
                                if (dates.length > 0) {
                                    selections.innerText = '';
                                    for (var i = 0; i < dates.length; i++) {
                                        selections.innerText += dates[i].toLocaleDateString() + '\n';
                                    }
                                } 
                                else {
                                    selections.innerText = this.gcCalendar.getSelectedDate().toLocaleDateString();
                                }
                            }}></GcCalendar>
                        </td>
                        <td style={{verticalAlign:top, paddingLeft: '2rem'}}>
                            <div>選択日付</div>
                            <div id="selections" style={{height: '15rem', overflow: 'auto'}}></div>
                        </td>
                    </tr>
                </table>
                <table class="sample">
                    <tr>
                        <th>選択方法</th>
                        <td>
                            <select id="setSelectionMode" onChange={(e)=>this.setState({selectionMode :calendarSelectionMode[e.target.selectedIndex]})}>
                                <option>単一の日付</option>
                                <option>複数の日付</option>
                                <option>複数の日付（Ctrl／Shiftキーで選択可）</option>
                            </select><br/>
                            <label><input type="checkbox" onChange={(e)=>this.setState({weekNumberSelect :e.target.checked})} id="setWeekNumberSelect"/>週番号のクリックで週全体を選択</label><br/>
                            <label><input type="checkbox" onChange={(e)=>this.setState({weekTitleSelect :e.target.checked})} id="setWeekTitleSelect"/>曜日のクリックで曜日全体を選択</label>
                        </td>
                    </tr>
                    <tr>
                        <th>選択可能な日付</th>
                        <td>
                            <select id="setAllowSelection" onChange={(e)=>this.setState({allowSelection: allowSelections[e.target.selectedIndex]})}>
                                <option>すべて</option>
                                <option>営業日</option>
                                <option>休日と休業日</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>最大選択日数</th>
                        <td><input type="number" value={this.state.maxSelectionCount} id="setMaxSelectionCount" onChange={(e)=>this.setState({maxSelectionCount: e.target.value})}/></td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));