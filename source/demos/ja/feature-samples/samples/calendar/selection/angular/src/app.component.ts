import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public calendarSelectionMode = [
        GC.InputMan.CalendarSelectionMode.One,
        GC.InputMan.CalendarSelectionMode.MultiSimple,
        GC.InputMan.CalendarSelectionMode.MultiRich
    ];
    public allowSelections = [
        GC.InputMan.AllowSelection.Anyday,
        GC.InputMan.AllowSelection.Workday,
        GC.InputMan.AllowSelection.Holiday
    ];

    public selectionInnerText: string = "";
    public selectionMode: GC.InputMan.CalendarSelectionMode;
    public allowSelection: GC.InputMan.AllowSelection;
    public maxSelectionCount: number = 14;
    public weekNumberSelect: boolean;
    public weekTitleSelect: number;

    public onSelectedDateChanged(sender: GC.InputMan.GcCalendar): void {
        const dates = sender.getSelections();
        if (dates.length > 0) {
            this.selectionInnerText = '';
            for (var i = 0; i < dates.length; i++) {
                this.selectionInnerText += dates[i].toLocaleDateString() + '\n';
            }
        } else {
            this.selectionInnerText = sender.getSelectedDate().toLocaleDateString();
        }
    }

    public addHolidayGroup(gcCalendar: GC.InputMan.GcCalendar): void {
        const holidayGroup = new GC.InputMan.HolidayGroup('holiday', true);
        const workdayGroup = new GC.InputMan.HolidayGroup('workday', true);

        // 振替休日を有効にします
        holidayGroup.setWeekFlags('sunday', GC.InputMan.WeekFlags.All);
        holidayGroup.setOverride('sunday', GC.InputMan.HolidayOverride.NextWorkDay);
        // 振替休日用のツールチップテキスト
        gcCalendar.setOverrideTipText('振替休日');

        // 固定の祝日
        holidayGroup.addHoliday(new GC.InputMan.Holiday('元旦', 1, 1));
        holidayGroup.addHoliday(new GC.InputMan.Holiday('建国記念の日', 2, 11));
        holidayGroup.addHoliday(new GC.InputMan.Holiday('昭和の日', 4, 29));
        holidayGroup.addHoliday(new GC.InputMan.Holiday('憲法記念日', 5, 3));
        holidayGroup.addHoliday(new GC.InputMan.Holiday('みどりの日', 5, 4));
        holidayGroup.addHoliday(new GC.InputMan.Holiday('こどもの日', 5, 5));
        holidayGroup.addHoliday(new GC.InputMan.Holiday('文化の日', 11, 3));
        holidayGroup.addHoliday(new GC.InputMan.Holiday('勤労感謝の日', 11, 23));

        // ハッピーマンデー
        holidayGroup.addHoliday(new GC.InputMan.DayOfWeekHoliday('成人の日', GC.InputMan.MonthFlags.January, GC.InputMan.WeekFlags.Second, GC.InputMan.DayOfWeek.Monday));
        holidayGroup.addHoliday(new GC.InputMan.DayOfWeekHoliday('海の日', GC.InputMan.MonthFlags.July, GC.InputMan.WeekFlags.Third, GC.InputMan.DayOfWeek.Monday));
        holidayGroup.addHoliday(new GC.InputMan.DayOfWeekHoliday('敬老の日', GC.InputMan.MonthFlags.September, GC.InputMan.WeekFlags.Third, GC.InputMan.DayOfWeek.Monday));
        holidayGroup.addHoliday(new GC.InputMan.DayOfWeekHoliday('体育の日', GC.InputMan.MonthFlags.October, GC.InputMan.WeekFlags.Second, GC.InputMan.DayOfWeek.Monday));

        // 2019年のみの祝日
        holidayGroup.addHoliday(new GC.InputMan.ForceHoliday('国民の休日', new Date('2019/4/30')));
        holidayGroup.addHoliday(new GC.InputMan.ForceHoliday('天皇の即位の日', new Date('2019/5/1')));
        holidayGroup.addHoliday(new GC.InputMan.ForceHoliday('国民の休日', new Date('2019/5/2')));
        holidayGroup.addHoliday(new GC.InputMan.ForceHoliday('即位礼正殿の儀', new Date('2019/10/22')));
        // 2020年のみの祝日
        holidayGroup.addHoliday(new GC.InputMan.ForceHoliday('海の日', new Date('2020/7/23')));
        holidayGroup.addHoliday(new GC.InputMan.ForceHoliday('スポーツの日', new Date('2020/7/24')));
        holidayGroup.addHoliday(new GC.InputMan.ForceHoliday('山の日', new Date('2020/8/10')));
        // 2020年のみ平日扱い
        workdayGroup.addHoliday(new GC.InputMan.ForceWorkday('', new Date('2020/7/20')));
        workdayGroup.addHoliday(new GC.InputMan.ForceWorkday('', new Date('2020/8/11')));
        workdayGroup.addHoliday(new GC.InputMan.ForceWorkday('', new Date('2020/10/12')));

        // 年により変動のある祝日を設定します。
        // 2000年から2050年までを対象としています。
        for (var currentYear = 2000; currentYear <= 2050; currentYear++) {
            var syunbun: string;
            var syubun;
            switch (currentYear % 4) {
                case 0:
                    // 春分の日を設定します。
                    if (currentYear < 1960) syunbun = currentYear + '/3/21';
                    else if (currentYear >= 1960 && currentYear < 2092) syunbun = currentYear + '/3/20';
                    else syunbun = currentYear + '/3/19';

                    // 秋分の日を設定します。
                    if (currentYear < 2012) syubun = currentYear + '/9/23';
                    else syubun = currentYear + '/9/22';

                    break;
                case 1:
                    // 春分の日を設定します。
                    if (currentYear < 1993) syunbun = currentYear + '/3/21';
                    else syunbun = currentYear + '/3/20';

                    // 秋分の日を設定します。
                    if (currentYear < 1921) syubun = currentYear + '/9/24';
                    else if (currentYear >= 1921 && currentYear < 2045) syubun = currentYear + '/9/23';
                    else syubun = currentYear + '/9/22';

                    break;
                case 2:
                    // 春分の日を設定します。
                    if (currentYear < 2026) syunbun = currentYear + '/3/21';
                    else syunbun = currentYear + '/3/20';

                    // 秋分の日を設定します。
                    if (currentYear < 1950) syubun = currentYear + '/9/24';
                    else if (currentYear >= 1950 && currentYear < 2078) syubun = currentYear + '/9/23';
                    else syubun = currentYear + '/9/22';

                    break;
                case 3:
                    // 春分の日を設定します。
                    if (currentYear < 1927) syunbun = currentYear + '/3/22';
                    else if (currentYear >= 1927 && currentYear < 2059) syunbun = currentYear + '/3/21';
                    else syunbun = currentYear + '/3/20';

                    // 秋分の日を設定します。
                    if (currentYear < 1983) syubun = currentYear + '/9/24';
                    else syubun = currentYear + '/9/23';

                    break;
            }
            // 春分の日、秋分の日を追加します。
            holidayGroup.addHoliday(new GC.InputMan.ForceHoliday('春分の日', new Date(syunbun)));
            holidayGroup.addHoliday(new GC.InputMan.ForceHoliday('秋分の日', new Date(syubun)));
            // 2016年以降「山の日」を追加します。
            if (currentYear > 2016) {
                holidayGroup.addHoliday(new GC.InputMan.ForceHoliday('山の日', new Date(currentYear + '/8/11')));
            }
            // 天皇誕生日を追加します。
            if (currentYear < 2019) {
                holidayGroup.addHoliday(new GC.InputMan.ForceHoliday('天皇誕生日', new Date(currentYear + '/12/23')));
            }
            else if (currentYear > 2019) {
                holidayGroup.addHoliday(new GC.InputMan.ForceHoliday('天皇誕生日', new Date(currentYear + '/2/23')));
            }
            // 2つの祝日に挟まれた営業日を休日に設定します。
            if (currentYear > 2003) {
                const syubunDate = new Date(syubun);
                if (syubunDate.getDay() == 3) {
                    syubunDate.setDate(syubunDate.getDate() - 1);
                    holidayGroup.addHoliday(new GC.InputMan.ForceHoliday('国民の休日', syubunDate));
                }
            }
        }
        gcCalendar.addHolidayGroup(holidayGroup);
        gcCalendar.addHolidayGroup(workdayGroup);
    }
}


enableProdMode();