日付の選択方法を設定する方法と、選択された日付を取得する方法について解説します。

## 最大値と最小値

カレンダーに表示する日付の最小値と最大値は、setMinDateメソッドとsetMaxDateメソッドで設定します。この最小値と最大値を超える日付には、フォーカスを移動することはできません。

## 選択方法の指定

カレンダーコントロールには、日付の選択方法として、1つの日付だけを選択する単一選択と、複数の日付を選択する複数選択の2種類があります。

これらの選択方法は、setSelectionModeメソッドで指定します。また、下記に示した選択可能な日付の種類は、setAllowSelectionメソッドで設定します。

- すべての日付 （AllowSellection.AnyDay）
- 営業日 （AllowSellection.Workday）
- 休日と休業日 （AllowSellection.Holiday）

<!-- -->

日付選択の操作は、目的の日付をマウスでクリックするか、または矢印キーでフォーカス枠を移動し、［Space］または［Enter］キーを押して行います。また、setSelectionModeメソッドをSelectionMode.MultiRichに設定した場合は、CtrlキーやShiftキーを使った日付選択ができます。

なお、複数選択の場合は、選択できる最大日数をsetMaxSelectionCountメソッドで設定できます。

また、setWeekNumberSelectメソッドをtrueにすると、週番号（setShowWeekNumberメソッドで表示を設定）をクリックすると、その週全体を選択し、setWeekTitleSelectメソッドをtrueにすると、曜日タイトルのクリックでその曜日全部を選択します。

## 選択された日付の取得

マウスやキーボードの操作で選択された日付は、選択モードの違いによって2つのメソッドを使い分けて取得します。

単一選択された日付の取得には、getSelectedDateメソッドを使用し、複数選択された日付はgetSelectionsメソッドを使用します。getSelectionsメソッドは、選択された日付の配列を返します。

## コードによる日付選択

日付の選択は、通常、マウスやキーボードの操作によって行われますが、コードで選択することも可能です。

日付選択は、単一選択モードの場合は、setSelectedDateメソッド、複数選択モードの場合は、setSelectionsメソッドを使用します。

## 選択時のイベント

直前に選択されていた日付とは異なる日付が選択されたときには、単一選択モードと複数選択モードの両方でonSelectedDateChangedイベントが発生します。また、フォーカス枠を移動したときには、onFocusDateChangedイベントが発生します。

カレンダー上の日付をクリックしたときには、onClickDateイベントが発生します。

カレンダー上の日付をクリックして日付を選択したときには、次の順序でイベントが発生します。

1. onFocusDateChanged
2. onClickDate
3. onSelectedDateChanged

<!-- -->

## サンプルコード

次のサンプルコードは、複数選択モードで選択された日付をテキストボックスに表示する方法を示します。

```
var gcCalendar = new GC.InputMan.GcCalendar(document.getElementById('gcCalendar'));

gcCalendar.onSelectedDateChanged(function () {
  var selections = document.getElementById('selections');
  var dates = gcCalendar.getSelections();
  if (dates.length > 0) {
    selections.innerText = '';
    for (var i = 0; i < dates.length; i++) {
      selections.innerText += dates[i].toLocaleDateString() + '\n';
    }
  }
});
```

