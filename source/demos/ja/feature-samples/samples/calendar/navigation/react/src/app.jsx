import * as React from "react";
import * as ReactDom from "react-dom";
import { GcCalendar } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

const calendarNavigators = [
    InputMan.CalendarNavigators.None,
    InputMan.CalendarNavigators.Buttons,
    InputMan.CalendarNavigators.ScrollBar,
    InputMan.CalendarNavigators.Outlook
];
const navigatorOrientations = [
    InputMan.NavigatorOrientation.Top,
    InputMan.NavigatorOrientation.Bottom,
    InputMan.NavigatorOrientation.Left,
    InputMan.NavigatorOrientation.Right
];

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            showZoomButton: true,
            showNavigator: InputMan.CalendarNavigators.Outlook,
            navigatorOrientation: InputMan.NavigatorOrientation.Right,
            scrollRate: 1
        };
    }

    render(){
        return(
        <div>
            <GcCalendar
                scrollRate={this.state.scrollRate}
                showZoomButton= {this.state.showZoomButton}
                showNavigator= {this.state.showNavigator}
                navigatorOrientation= {this.state.navigatorOrientation}/>
             <table class="sample">
                <tr>
                    <th>ナビゲータ</th>
                    <td>
                        種類：
                        <select id="setShowNavigator" onChange={(e) => this.setState({showNavigator: calendarNavigators[e.target.selectedIndex]})}>
                            <option>なし</option>
                            <option>ボタンスタイル</option>
                            <option>スクロールバースタイル</option>
                            <option selected>Outlookスタイル</option>
                        </select><br/>
                        位置：
                        <select id="setNavigatorOrientation" onChange={(e) => this.setState({navigatorOrientation: navigatorOrientations[e.target.selectedIndex]})}>
                            <option>上</option>
                            <option>下</option>
                            <option>左</option>
                            <option selected>右</option>
                        </select><br/>
                        <input type="checkbox" checked={this.state.showZoomButton} onChange={(e) => this.setState({ showZoomButton: e.target.checked })}
                            id="setShowZoomButton"></input><label>ズームボタンを表示する</label><br />
                        月ナビケータで移動する月数：<input type="number" min ="1" max="11" value={this.state.scrollRate} onChange={(e) => this.setState({scrollRate: Number(e.target.value)})}></input>
                    </td>
                </tr>
            </table>
        </div>)
    }
}

ReactDom.render(<App />, document.getElementById("app"));