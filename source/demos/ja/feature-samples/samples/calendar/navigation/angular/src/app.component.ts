import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public showNavigator: GC.InputMan.CalendarNavigators;
    public navigatorOrientation: GC.InputMan.NavigatorOrientation;
    public showZoomButton: boolean = true;
    public skipMonth: number = 1;

    public setShowNavigator(selectedIndex: number) {
        this.showNavigator = calendarNavigators[selectedIndex];
    }

    public setNavigatorOrientation(selectedIndex: number) {
        this.navigatorOrientation = navigatorOrientations[selectedIndex];
    }
}

const calendarNavigators = [
    GC.InputMan.CalendarNavigators.None,
    GC.InputMan.CalendarNavigators.Buttons,
    GC.InputMan.CalendarNavigators.ScrollBar,
    GC.InputMan.CalendarNavigators.Outlook
];
const navigatorOrientations = [
    GC.InputMan.NavigatorOrientation.Top,
    GC.InputMan.NavigatorOrientation.Bottom,
    GC.InputMan.NavigatorOrientation.Left,
    GC.InputMan.NavigatorOrientation.Right
];

enableProdMode();