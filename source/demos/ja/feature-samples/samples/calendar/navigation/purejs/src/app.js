import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcCalendar = new InputMan.GcCalendar(document.getElementById('gcCalendar'));

document.getElementById('setShowNavigator').addEventListener('change', (e) => {
    gcCalendar.setShowNavigator(calendarNavigators[e.target.selectedIndex]);
});

document.getElementById('setNavigatorOrientation').addEventListener('change', (e) => {
    gcCalendar.setNavigatorOrientation(navigatorOrientations[e.target.selectedIndex]);
});

document.getElementById('setShowZoomButton').addEventListener('change', (e) => {
    gcCalendar.setShowZoomButton(e.target.checked);
});

document.getElementById('skipMonth').addEventListener('change', (e) => {
    gcCalendar.scrollRate = Number(e.target.value);
});

const calendarNavigators = [
    InputMan.CalendarNavigators.None,
    InputMan.CalendarNavigators.Buttons,
    InputMan.CalendarNavigators.ScrollBar,
    InputMan.CalendarNavigators.Outlook
];
const navigatorOrientations = [
    InputMan.NavigatorOrientation.Top,
    InputMan.NavigatorOrientation.Bottom,
    InputMan.NavigatorOrientation.Left,
    InputMan.NavigatorOrientation.Right
];