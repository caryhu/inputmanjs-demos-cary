import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public months = [
        GC.InputMan.Months.Default,
        GC.InputMan.Months.January,
        GC.InputMan.Months.February,
        GC.InputMan.Months.March,
        GC.InputMan.Months.April,
        GC.InputMan.Months.May,
        GC.InputMan.Months.June,
        GC.InputMan.Months.July,
        GC.InputMan.Months.August,
        GC.InputMan.Months.September,
        GC.InputMan.Months.October,
        GC.InputMan.Months.November,
        GC.InputMan.Months.December
    ];
    // 月-日カレンダーの書式を設定します。
    public headerFormat: string = 'yyy年 M月';
    public setHeaderFormat(sender: GC.InputMan.GcComboBox) {
        if (sender.getSelectedIndex() === -1) {
            this.headerFormat = sender.getDisplayText();
        } else {
            this.headerFormat = sender.getSelectedValue();
        }
    }

    // 年-月カレンダーの書式を設定します。
    public yearMonthFormat: string = 'yyy年,M月';
    public setYearMonthFormat(sender: GC.InputMan.GcComboBox) {
        if (sender.getSelectedIndex() === -1) {
            this.yearMonthFormat = sender.getDisplayText();
        } else {
            this.yearMonthFormat = sender.getSelectedValue();
        }
    }

    // カレンダーを会計年度の形式で表示しません。
    public calendarYear: GC.InputMan.CalendarYear = GC.InputMan.CalendarYear.Normal;
    // 会計年度の最初の月を設定します。
    public firstFiscalMonth: GC.InputMan.Months = GC.InputMan.Months.April;


    public headerFormatItems: Array<ComboItem<string, string>> = [
        {
            text: "2018年 1月",
            value: 'yyy年 M月'
        },
        {
            text: "2018/1",
            value: 'yyy/M'
        },
        {
            text: "平成元年1月",
            value: 'gggE年 M月'
        },
        {
            text: "H.1/1",
            value: 'g.e/M'
        },
        {
            text: "2018年度 1月",
            value: 'yyy年度 M月'
        },
        {
            text: "FY2018/1",
            value: 'FYyyy/M'
        }
    ];

    public yearMonthFormatItems: Array<ComboItem<string, string>> = [
        {
            text: "2018年、1月",
            value: 'yyy年,M月'
        },
        {
            text: "2018、1",
            value: 'yyy,M'
        },
        {
            text: "平成元年、1月",
            value: 'gggE年,M月'
        },
        {
            text: "H.1、1",
            value: 'g.e,M'
        },
        {
            text: "2018年度、1月",
            value: 'yyy年度,M月'
        },
        {
            text: "FY2018、1",
            value: 'FYyyy,M'
        }
    ];

    public setCalendarYear(event: Event): void {
        this.calendarYear = (event.target as HTMLInputElement).checked ? GC.InputMan.CalendarYear.Fiscal : GC.InputMan.CalendarYear.Normal;
    }
}

export interface ComboItem<T1, T2> {
    value: T1;
    text: T2
}

enableProdMode();