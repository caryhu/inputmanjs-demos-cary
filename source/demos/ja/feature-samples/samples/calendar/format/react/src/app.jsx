import * as React from "react";
import * as ReactDom from "react-dom";
import { GcCalendar, GcComboBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            headerFormat: 'yyy年 M月',
            yearMonthFormat: 'yyy年,M月',
            calendarYear: InputMan.CalendarYear.Normal,
            firstFiscalMonth: InputMan.Months.April
        };
    }

    componentDidMount() {
        document.body.style.paddingBottom = '10rem';
    }

    render() {
        return (
            <div>
                <GcCalendar
                    // 月-日カレンダーの書式を設定します。
                    headerFormat={this.state.headerFormat}
                    // 年-月カレンダーの書式を設定します。
                    yearMonthFormat={this.state.yearMonthFormat}
                    // カレンダーを会計年度の形式で表示しません。
                    calendarYear={this.state.calendarYear}
                    // 会計年度の最初の月を設定します。
                    firstFiscalMonth={this.state.firstFiscalMonth} />
                <table class="sample">
                    <tr>
                        <th>月-日カレンダーの書式</th>
                        <td>
                            <GcComboBox
                                items={[
                                    { value: 'yyy年 M月', desc: '2018年 1月' },
                                    { value: 'yyy/M', desc: '2018/1' },
                                    { value: 'gggE年 M月', desc: '平成元年1月' },
                                    { value: 'g.e/M', desc: 'H.1/1' },
                                    { value: 'yyy年度 M月', desc: '2018年度 1月' },
                                    { value: 'FYyyy/M', desc: 'FY2018/1' },
                                ]}
                                displayMemberPath={'value'}
                                valueMemberPath={'value'}
                                dropDownWidth={'auto'}
                                columns={[
                                    { name: 'value', label: '書式', width: 120 },
                                    { name: 'desc', label: '説明', width: 120 },
                                ]}
                                selectedChanged={(sender) => {
                                    this.setState({ headerFormat: sender.getSelectedValue() });
                                }}></GcComboBox>
                        </td>
                    </tr>
                    <tr>
                        <th>年-月カレンダーの書式</th>
                        <td>
                            <GcComboBox
                                items={[
                                    { value: 'yyy年,M月', desc: '2018年、1月' },
                                    { value: 'yyy,M', desc: '2018、1' },
                                    { value: 'gggE年,M月', desc: '平成元年、1月' },
                                    { value: 'g.e,M', desc: 'H.1、1' },
                                    { value: 'yyy年度,M月', desc: '2018年度、1月' },
                                    { value: 'FYyyy,M', desc: 'FY2018、1' },
                                ]}
                                displayMemberPath={'value'}
                                valueMemberPath={'value'}
                                dropDownWidth={'auto'}
                                columns={[
                                    { name: 'value', label: '書式', width: 120 },
                                    { name: 'desc', label: '説明', width: 130 },
                                ]}
                                selectedChanged={(sender) => {
                                    this.setState({ yearMonthFormat: sender.getSelectedValue() });
                                }}></GcComboBox>
                        </td>
                    </tr>
                    <tr>
                        <th>会計年度表示</th>
                        <td>
                            <input type="checkbox" id="setCalendarYear" onChange={(event) => {
                                this.setState({ calendarYear: event.target.checked ? InputMan.CalendarYear.Fiscal : InputMan.CalendarYear.Normal });
                            }}>
                            </input>
                            会計年度で表示<br />
                            <label>最初の月
                                <select id="setFirstFiscalMonth" onChange={(event) => { this.setState({ firstFiscalMonth: event.target.selectedIndex }) }}>
                                    <option>指定なし</option>
                                    <option>1月</option>
                                    <option>2月</option>
                                    <option>3月</option>
                                    <option selected>4月</option>
                                    <option>5月</option>
                                    <option>6月</option>
                                    <option>7月</option>
                                    <option>8月</option>
                                    <option>9月</option>
                                    <option>10月</option>
                                    <option>11月</option>
                                    <option>12月</option>
                                </select>
                            </label>
                        </td>
                    </tr>
                </table>
            </div>
        );
    }
}

ReactDom.render(<App />, document.getElementById("app"));