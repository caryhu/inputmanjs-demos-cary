import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';

const gcCalendar = new InputMan.GcCalendar(document.getElementById('gcCalendar'), {
    // 月-日カレンダーの書式を設定します。
    headerFormat: 'yyy年 M月',
    // 年-月カレンダーの書式を設定します。
    yearMonthFormat: 'yyy年,M月',
    // カレンダーを会計年度の形式で表示しません。
    calendarYear: InputMan.CalendarYear.Normal,
    // 会計年度の最初の月を設定します。
    firstFiscalMonth: InputMan.Months.April
});

const headerFormat = new InputMan.GcComboBox(document.getElementById('headerFormat'), {
    items: [
        { value: 'yyy年 M月', desc: '2018年 1月' },
        { value: 'yyy/M', desc: '2018/1' },
        { value: 'gggE年 M月', desc: '平成元年1月' },
        { value: 'g.e/M', desc: 'H.1/1' },
        { value: 'yyy年度 M月', desc: '2018年度 1月' },
        { value: 'FYyyy/M', desc: 'FY2018/1' },
    ],
    displayMemberPath: 'value',
    valueMemberPath: 'value',
    dropDownWidth: 'auto',
    columns: [
        { name: 'value', label: '書式', width: 120 },
        { name: 'desc', label: '説明', width: 120 },
    ]
});
headerFormat.addEventListener(InputMan.GcComboBoxEvent.SelectedChanged, (control, args) => {
    console.log(headerFormat.getSelectedValue());
    if (headerFormat.getSelectedValue()) {
        gcCalendar.setHeaderFormat(headerFormat.getSelectedValue());
    }
});
headerFormat.addEventListener(InputMan.GcComboBoxEvent.TextChanged, (control, args) => {
    console.log(headerFormat.getDisplayText());
    gcCalendar.setHeaderFormat(headerFormat.getDisplayText());
});

const yearMonthFormat = new InputMan.GcComboBox(document.getElementById('yearMonthFormat'), {
    items: [
        { value: 'yyy年,M月', desc: '2018年、1月' },
        { value: 'yyy,M', desc: '2018、1' },
        { value: 'gggE年,M月', desc: '平成元年、1月' },
        { value: 'g.e,M', desc: 'H.1、1' },
        { value: 'yyy年度,M月', desc: '2018年度、1月' },
        { value: 'FYyyy,M', desc: 'FY2018、1' },
    ],
    displayMemberPath: 'value',
    valueMemberPath: 'value',
    dropDownWidth: 'auto',
    columns: [
        { name: 'value', label: '書式', width: 120 },
        { name: 'desc', label: '説明', width: 130 },
    ]
});
yearMonthFormat.addEventListener(InputMan.GcComboBoxEvent.SelectedChanged, (control, args) => {
    if (yearMonthFormat.getSelectedValue()) {
        gcCalendar.setYearMonthFormat(yearMonthFormat.getSelectedValue());
    }
});
yearMonthFormat.addEventListener(InputMan.GcComboBoxEvent.TextChanged, (control, args) => {
    gcCalendar.setYearMonthFormat(yearMonthFormat.getDisplayText());
});

document.getElementById('setCalendarYear').addEventListener('change', (e) => {
    const isFiscal = e.target.checked;
    gcCalendar.setCalendarYear(isFiscal ? InputMan.CalendarYear.Fiscal : InputMan.CalendarYear.Normal);
});

document.getElementById('setFirstFiscalMonth').addEventListener('change', (e) => {
    gcCalendar.setFirstFiscalMonth(months[e.target.selectedIndex]);
});

const months = [
    InputMan.Months.Default,
    InputMan.Months.January,
    InputMan.Months.February,
    InputMan.Months.March,
    InputMan.Months.April,
    InputMan.Months.May,
    InputMan.Months.June,
    InputMan.Months.July,
    InputMan.Months.August,
    InputMan.Months.September,
    InputMan.Months.October,
    InputMan.Months.November,
    InputMan.Months.December
];