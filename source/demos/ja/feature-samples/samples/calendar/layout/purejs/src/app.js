import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';


const gcCalendar = new InputMan.GcCalendar(document.getElementById('gcCalendar'), {
    // 最初の列の曜日を設定します。
    firstDayOfWeek: InputMan.DayOfWeek.NotSet,
    // 表示する月数を設定します。
    calendarDimensions: { width: 1, height: 1 },
    // 12ヶ月表示時に左上に配置する最初の月を設定します。
    firstMonthInView: InputMan.Months.Default,
    // ヘッダを表示します。
    showHeader: true,
    // 表示する六曜を設定します。
    showRokuyou: InputMan.Rokuyou.None
});

document.getElementById('setFirstDayOfWeek').addEventListener('change', (e) => {
    gcCalendar.setFirstDayOfWeek(dayOfWeeks[e.target.selectedIndex]);
});

document.getElementById('width').addEventListener('change', (e) => {
    gcCalendar.setCalendarDimensions(Number(document.getElementById('width').value), Number(document.getElementById('height').value));
});

document.getElementById('height').addEventListener('change', (e) => {
    gcCalendar.setCalendarDimensions(Number(document.getElementById('width').value), Number(document.getElementById('height').value));
});

document.getElementById('setFirstMonthInView').addEventListener('change', (e) => {
    gcCalendar.setFirstMonthInView(months[e.target.selectedIndex]);
});

document.getElementById('setShowHeader').addEventListener('change', (e) => {
    gcCalendar.setShowHeader(e.target.checked);
});

document.getElementById('setShowTrailing').addEventListener('change', (e) => {
    gcCalendar.setShowTrailing(e.target.checked);
});

document.getElementById('setEmptyRows').addEventListener('change', (e) => {
    gcCalendar.setEmptyRows(e.target.checked ? InputMan.EmptyRows.StartEmpty : InputMan.EmptyRows.AllAtEnd);
    gcCalendar.setFocusDate(new Date(2015, 1, 1));
});

document.getElementById('setShowToday').addEventListener('change', (e) => {
    gcCalendar.setShowToday(e.target.checked);
});

document.getElementById('setShowWeekNumber').addEventListener('change', (e) => {
    gcCalendar.setShowWeekNumber(e.target.checked);
});

document.getElementById('showAllRokuyouBtn').addEventListener('click', () => {
    showAllRokuyou(true);
});

document.getElementById('hideAllRokuyouBtn').addEventListener('click', () => {
    showAllRokuyou(false);
});

document.getElementById('rokuyouList').addEventListener('click', (e) => {
    const target = e.currentTarget;
    var rokuyou = InputMan.Rokuyou.None;
    for (var i = 0; i < target.children.length; i++) {
        const checkbox = target.children[i].firstChild;
        rokuyou |= (checkbox && checkbox.checked) ? rokuyous[i] : InputMan.Rokuyou.None;
    }
    gcCalendar.setShowRokuyou(rokuyou);
});

const showAllRokuyou = (show) => {
    const rokuyousList = document.getElementById('rokuyouList');
    for (var i = 0; i < rokuyousList.children.length; i++) {
        const checkbox = rokuyousList.children[i].firstChild;
        if (checkbox) {
            checkbox.checked = show;
        }
    }
    gcCalendar.setShowRokuyou(show ? InputMan.Rokuyou.All : InputMan.Rokuyou.None);
}

const dayOfWeeks = [
    InputMan.DayOfWeek.NotSet,
    InputMan.DayOfWeek.Sunday,
    InputMan.DayOfWeek.Monday,
    InputMan.DayOfWeek.Tuesday,
    InputMan.DayOfWeek.Wednesday,
    InputMan.DayOfWeek.Thursday,
    InputMan.DayOfWeek.Friday,
    InputMan.DayOfWeek.Saturday
];
const months = [
    InputMan.Months.Default,
    InputMan.Months.January,
    InputMan.Months.February,
    InputMan.Months.March,
    InputMan.Months.April,
    InputMan.Months.May,
    InputMan.Months.June,
    InputMan.Months.July,
    InputMan.Months.August,
    InputMan.Months.September,
    InputMan.Months.October,
    InputMan.Months.November,
    InputMan.Months.December
];
const rokuyous = [
    InputMan.Rokuyou.Senshou,
    InputMan.Rokuyou.Tomobiki,
    InputMan.Rokuyou.Senbu,
    InputMan.Rokuyou.Butsumetsu,
    InputMan.Rokuyou.Taian,
    InputMan.Rokuyou.Shakkou
];