import * as React from "react";
import * as ReactDom from "react-dom";
import { GcCalendar } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

const calendarDimensionStyle={ width: '3rem'};

const dayOfWeeks = [
    InputMan.DayOfWeek.NotSet,
    InputMan.DayOfWeek.Sunday,
    InputMan.DayOfWeek.Monday,
    InputMan.DayOfWeek.Tuesday,
    InputMan.DayOfWeek.Wednesday,
    InputMan.DayOfWeek.Thursday,
    InputMan.DayOfWeek.Friday,
    InputMan.DayOfWeek.Saturday
];
const rokuyous = [
    InputMan.Rokuyou.Senshou,
    InputMan.Rokuyou.Tomobiki,
    InputMan.Rokuyou.Senbu,
    InputMan.Rokuyou.Butsumetsu,
    InputMan.Rokuyou.Taian,
    InputMan.Rokuyou.Shakkou
];
class App extends React.Component{
    gcCalendar = null;
    constructor(props, context) {
        super(props, context);
        this.state = {
            firstDayOfWeek: InputMan.DayOfWeek.NotSet,
            dimensions: {width: 1, height: 1},
            firstMonthInView: InputMan.Months.Default,
            showHeader: true,
            showTrailing: true,
            emptyRows: false,
            showToday: true,
            showWeekNumber: true,
            showRokuyou: InputMan.Rokuyou.None
        };
    }

    componentDidMount(){
        document.getElementById('rokuyouList').addEventListener('click', (e) => {
            const target = e.currentTarget;
            var rokuyou = InputMan.Rokuyou.None;
            for (var i = 0; i < target.children.length; i++) {
                const checkbox = target.children[i].firstChild;
                rokuyou |= (checkbox && checkbox.checked) ? rokuyous[i] : InputMan.Rokuyou.None;
            }
            this.gcCalendar.setShowRokuyou(rokuyou);
        });
    }

    showAllRokuyou(show){
        const rokuyousList = document.getElementById('rokuyouList');
        for (var i = 0; i < rokuyousList.children.length; i++) {
            const checkbox = rokuyousList.children[i].firstChild;
            if (checkbox) {
                checkbox.checked = show;
            }
        }
        this.gcCalendar.setShowRokuyou(show ? InputMan.Rokuyou.All : InputMan.Rokuyou.None);
    }

    render(){
        return (
        <div>
        <GcCalendar 
                // 最初の列の曜日を設定します。
                firstDayOfWeek= {this.state.firstDayOfWeek}
                // 表示する月数を設定します。
                dimensions= {this.state.dimensions}
                // 12ヶ月表示時に左上に配置する最初の月を設定します。
                firstMonthInView= {this.state.firstMonthInView}
                // ヘッダを表示します。
                showHeader= {this.state.showHeader}
                showTrailing= {this.state.showTrailing}
                emptyRows= {this.state.emptyRows}
                showToday= {this.state.showToday}
                showWeekNumber ={this.state.showWeekNumber}
                // 表示する六曜を設定します。
                showRokuyou= {this.state.showRokuyou}
                onInitialized={(imCtrl)=>{ this.gcCalendar = imCtrl; }}
        />
        <table class="sample">
            <tr>
                <th>最初の列の曜日</th>
                <td>
                    <select id="setFirstDayOfWeek" onChange={(e) => { this.setState({ firstDayOfWeek: dayOfWeeks[e.target.selectedIndex] }) }}>
                        <option>システムの設定</option>
                        <option>日曜</option>
                        <option>月曜</option>
                        <option>火曜</option>
                        <option>水曜</option>
                        <option>木曜</option>
                        <option>金曜</option>
                        <option>土曜</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th>複数月の表示</th>
                <td>
                    <label>列数：</label><input type="number" id="width" value={this.state.dimensions.width} style={calendarDimensionStyle} onChange={(e)=>{
                         this.setState({ dimensions: {
                             width: Number(e.target.value),
                             height: this.state.dimensions.height
                         }});
                    }}/>
                    <label> 行数：</label><input type="number" id="height" value={this.state.dimensions.height} style={calendarDimensionStyle} onChange={(e)=>{
                         this.setState({ dimensions: {
                            width: this.state.dimensions.width,
                            height: Number(e.target.value)
                        }});
                    }}/>
                </td>
            </tr>
            <tr>
                <th>最初の月</th>
                <td>
                    <select id="setFirstMonthInView" onChange={(e) => this.setState({ firstMonthInView: e.target.selectedIndex })}>
                        <option>指定なし</option>
                        <option>1月</option>
                        <option>2月</option>
                        <option>3月</option>
                        <option>4月</option>
                        <option>5月</option>
                        <option>6月</option>
                        <option>7月</option>
                        <option>8月</option>
                        <option>9月</option>
                        <option>10月</option>
                        <option>11月</option>
                        <option>12月</option>
                    </select>
                    <label> （12ヶ月表示時のみ）</label>
                </td>
            </tr>
            <tr>
                <th>ヘッダの表示</th>
                <td><label><input type="checkbox" checked={this.state.showHeader}
                onChange={(e) => this.setState({ showHeader: e.target.checked })} id="setShowHeader"/>表示する</label></td>
            </tr>
            <tr>
                <th>隣接日の表示</th>
                <td><label><input type="checkbox" checked={this.state.showTrailing}
                onChange={(e) => this.setState({ showTrailing: e.target.checked })} id="setShowTrailing"/>表示する</label></td>
            </tr>
            <tr>
                <th>空白行の表示</th>
                <td><label><input type="checkbox" id="setEmptyRows" checked={this.state.emptyRows} onChange={(e)=>{
                   this.setState({emptyRows: e.target.checked ? InputMan.EmptyRows.AllAtEnd : InputMan.EmptyRows.StartEmpty});
                   this.gcCalendar.setFocusDate(new Date(2015, 1, 1));
                }}/>表示する</label></td>
            </tr>
            <tr>
                <th>今日の日付の表示</th>
                <td><label><input type="checkbox" checked={this.state.showToday}
                onChange={(e) => this.setState({ showToday: e.target.checked })} id="setShowToday"/>表示する</label></td>
            </tr>
            <tr>
                <th>週番号</th>
                <td><label><input type="checkbox"  checked={this.state.showWeekNumber}
                onChange={(e) => this.setState({ showWeekNumber: e.target.checked })} id="setShowWeekNumber"/>表示する</label></td>
            </tr>
            <tr>
                <th>六曜</th>
                <td>
                    <button id="showAllRokuyouBtn" onClick={()=>this.showAllRokuyou(true)}>すべて表示</button>&nbsp;
                    <button id="hideAllRokuyouBtn" onClick={()=>this.showAllRokuyou(false)}>すべて非表示</button><br/>
                    <div id="rokuyouList">
                        <label><input type="checkbox" value="先勝"/>先勝</label>&nbsp;
                        <label><input type="checkbox" value="友引"/>友引</label>&nbsp;
                        <label><input type="checkbox" value="先負"/>先負</label>&nbsp;
                        <label><input type="checkbox" value="仏滅"/>仏滅</label>&nbsp;
                        <label><input type="checkbox" value="大安"/>大安</label>&nbsp;
                        <label><input type="checkbox" value="赤口"/>赤口</label>
                    </div>
                </td>
            </tr>
        </table>
    </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));