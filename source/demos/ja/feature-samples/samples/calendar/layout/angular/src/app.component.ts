import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    // 最初の列の曜日を設定します。
    public firstDayOfWeek: GC.InputMan.DayOfWeek = GC.InputMan.DayOfWeek.NotSet;
    // 表示する月数を設定します。
    public calendarDimensions: Array<number> = [1, 1];
    // 12ヶ月表示時に左上に配置する最初の月を設定します。
    public firstMonthInView: GC.InputMan.Months = GC.InputMan.Months.Default;
    // ヘッダを表示します。
    public showHeader: boolean;
    // 表示する六曜を設定します。
    public showRokuyou: GC.InputMan.Rokuyou;
    public showTrailing: boolean;
    public emptyRows: GC.InputMan.EmptyRows;
    public focusDate: Date;
    public showToday: boolean;
    public showWeekNumber: boolean;

    public setEmptyRows(emptyRows: boolean): void {
        if (emptyRows) {
            this.emptyRows = GC.InputMan.EmptyRows.StartEmpty;
        } else {
            this.emptyRows = GC.InputMan.EmptyRows.AllAtEnd;
        }
        this.focusDate = new Date(2015, 1, 1);
    }

    public toggleRokuyou(showAll: boolean): void {
        const rokuyousList = document.getElementById('rokuyouList');
        for (var i = 0; i < rokuyousList.children.length; i++) {
            const checkbox = <HTMLInputElement>(rokuyousList.children[i].firstChild);
            if (checkbox) {
                checkbox.checked = showAll;
            }
        }
        if (showAll) {
            this.showRokuyou = GC.InputMan.Rokuyou.All;
        } else {
            this.showRokuyou = GC.InputMan.Rokuyou.None;
        }
    }

    public setRokuyou(checked: boolean, index: number): void {
        if (checked) {
            this.showRokuyou |= this.rokuyous[index];
        } else {
            this.showRokuyou &= (~this.rokuyous[index]);
        }
    }

    public dayOfWeeks = [
        GC.InputMan.DayOfWeek.NotSet,
        GC.InputMan.DayOfWeek.Sunday,
        GC.InputMan.DayOfWeek.Monday,
        GC.InputMan.DayOfWeek.Tuesday,
        GC.InputMan.DayOfWeek.Wednesday,
        GC.InputMan.DayOfWeek.Thursday,
        GC.InputMan.DayOfWeek.Friday,
        GC.InputMan.DayOfWeek.Saturday
    ];
    public months = [
        GC.InputMan.Months.Default,
        GC.InputMan.Months.January,
        GC.InputMan.Months.February,
        GC.InputMan.Months.March,
        GC.InputMan.Months.April,
        GC.InputMan.Months.May,
        GC.InputMan.Months.June,
        GC.InputMan.Months.July,
        GC.InputMan.Months.August,
        GC.InputMan.Months.September,
        GC.InputMan.Months.October,
        GC.InputMan.Months.November,
        GC.InputMan.Months.December
    ];
    public rokuyous = [
        GC.InputMan.Rokuyou.Senshou,
        GC.InputMan.Rokuyou.Tomobiki,
        GC.InputMan.Rokuyou.Senbu,
        GC.InputMan.Rokuyou.Butsumetsu,
        GC.InputMan.Rokuyou.Taian,
        GC.InputMan.Rokuyou.Shakkou,
    ];

    public setDimensions(property: string, size: string): void {
        var value = parseInt(size, 10);
        var width = this.calendarDimensions[0];
        var height = this.calendarDimensions[1];
        if (property === 'width') {
            this.calendarDimensions = [value, height];
        } else {
            this.calendarDimensions = [width, value];
        }
    }
}

enableProdMode();