休日を設定する方法について解説します。

## 休日スタイルの設定

休日スタイルは、CSSの[calendar-holiday-type]セレクタで設定します。日付の文字色、背景色、太字、下線などのスタイルを指定できます。

```
/* 休日のスタイル */
[calendar-holiday-type] {
  color: red;
}
```

## 休日の追加

休日を追加するには、以下の手順を実行します。

1. HolidayGroupオブジェクトを作成します。
2. Holidayオブジェクトを作成します。
3. HolidayGroupオブジェクトのaddHolidayメソッドでHolidayオブジェクトを追加します。
4. GcCalendarコントロールのaddHolidayGroupメソッドでHolidayGroupオブジェクトを追加します。

<!-- -->

```
var holidayGroup = new GC.InputMan.HolidayGroup('holiday', true);
// 休日を追加します。
holidayGroup.addHoliday(new GC.InputMan.Holiday('元旦', 1, 1));
gcCalendar.addHolidayGroup(holidayGroup);
```

休日は、Holidayオブジェクトで設定します。このHolidayオブジェクトは、次の５つの設定メソッドを持っています。

- setName
- setStartMonth
- setStartDay
- setEndMonth
- setEndDay

<!-- -->

setNameメソッドには、休日を識別するための名前を設定し、setStartMonthとsetStartDayの２つのメソッドには休日の開始日を、また、setEndMonthとsetEndDayの各メソッドには休日の終了日を指定します。コンストラクタの引数でこれらの値を指定することもできます。

```
// 休日を追加します。
holidayGroup.addHoliday(new GC.InputMan.Holiday('元旦', 1, 1));
```

また、ハッピーマンデーと呼ばれる祝日など、特定の月、週、曜日を指定して設定する休日は、DayOfWeekHolidayオブジェクトで設定します。このDayOfWeekHolidayオブジェクトは、次の４つの設定メソッドを持っています。

- setDayOfWeek
- setDayOfWeekInMonth
- setMonth
- setName

<!-- -->

setNameメソッドには、休日を識別するための名前を設定し、setMonthには休日を設定する月、setDayOfWeekInMonthには週、setDayOfWeekには曜日を指定します。コンストラクタの引数でこれらの値を指定することもできます。

```
// 休日を追加します。
holidayGroup.addHoliday(new GC.InputMan.DayOfWeekHoliday('成人の日', GC.InputMan.MonthFlags.January, GC.InputMan.WeekFlags.Second, GC.InputMan.DayOfWeek.Monday));
```

## 臨時の休日と営業日

上記で使用したHolidayおよびDayOfWeekHolidayオブジェクトでは、休日が毎年繰り返されます。一方で、特定の年だけに行われる休日もあります。また、春分の日と秋分の日は、年によってその日付が異なります。

このように、特定の年にしか適用できないものや、年によって日付が異なるものについては、HolidayオブジェクトではなくForceHolidayオブジェクトを使用します。

また、Holidayオブジェクトで設定された休日にも関わらず、その年だけは営業日になるということもあります。このような場合、ForceWorkdayオブジェクトを使うことで、Holidayオブジェクトを削除することなく、臨時に営業日に設定することが可能です。

以下は、「休日スタイルの生成」で設定した誕生日（3月6日）を2015年に限って臨時営業日（仕事）にし、翌日を臨時休日（代休）に設定する例です。

```
// 休日出勤日と代休日を設定します。
holidayGroup.addHoliday(new GC.InputMan.ForceWorkday('休日出勤', '2018/1/8'));
holidayGroup.addHoliday(new GC.InputMan.ForceHoliday('代休', '2018/1/9'));
```

## 振替休日

国民の祝日が日曜日に重なったとき、「翌日（休日か営業日か問わず）」もしくは「次の営業日」を休日にするというシステムに対応して、カレンダーコントロールには、この振替休日を自動的に設定する機能があります。

振替休日は、HolidayCollectionのWeekdaysプロパティを経由してDayOfWeekHolidayPolicyオブジェクトにアクセスすることで、設定できます。以下にその例を示します。

```
// 振替休日を有効にします
holidayGroup.setWeekFlags('sunday', GC.InputMan.WeekFlags.All);
holidayGroup.setOverride('sunday', GC.InputMan.HolidayOverride.NextWorkDay);
```

## 休日の確認

HolidayオブジェクトのIsHolidayメソッドを使うと、特定の日付が休日（営業日でない日）に設定されているかどうかを確認することができます。IsHolidayメソッドがTrueを戻すのは、以下の３種類の日です。

- 臨時営業日でない休日
- 臨時休日
- 休業日

<!-- -->

```
// 休日かどうか確認します。
var holiday = new GC.InputMan.Holiday();
console.log(holiday.isHoliday(new Date('2018/1/8')));
```

