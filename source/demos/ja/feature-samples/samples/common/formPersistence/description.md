ImputManJSでは、フォームデータを永続化して、再読み込みしてもフォームデータが失われないようにするクラスGcFormPersistenceを提供しています。

## フォームの永続化
GcFormPersistenceクラスに永続化したいフォーム要素を指定し、persistメソッドを実行します。

```html
<body>
  <form id="testform">
    InputManテキストコントロール:<input id="gcTextBox"><br>
    通常のテキスト要素<input id="sttext" type="text">
  </form>

  <script>
    gcTextBox1 = new GC.InputMan.GcTextBox(document.getElementById('gcTextBox'), {
    });

    //保存したいフォーム要素を引数に指定する
    fp = new GC.InputMan.GcFormPersistence(document.getElementById('testform'));
    //persistメソッドを実行する
    fp.persist();

  </script>
</body>
```

GcFormPersistenceクラスではInputManJSが提供しているコントロールに加えて、以下のHTML標準の要素をサポートしています。

| 要素名                                               | 備考                                                        |
| --------------------------------------------------------- | --------------------------------------------------------- |
| input要素        |   type属性が以下の要素は保存されません<br>  button、file、password、reset、submit      |
| select要素        |    |
| textarea要素        |    |

またInputManJSのテキストコントロール(GcTextBox)でパスワード表示となっている時、データは保存されません。

## 保存のタイミングと保存先の設定
GcFormPersistenceクラスでは、[saveModeプロパティ](/inputmanjs/api/classes/inputman.gcformpersistence.html#savemode>)を指定することで、データを保存するタイミングを設定することができます。

| 値 | 説明 |
| -- | -- |
| SaveMode.SaveOnRefresh | ブラウザ更新時に保存します。 |
| SaveMode.SaveOnSubmit | ブラウザ更新時とデータ送信時に保存します。 |

また、[storageModeプロパティ](/inputmanjs/api/classes/inputman.gcformpersistence.html#storagemode>)を指定することで、データの保存先を設定することができます。

| 値 | 説明 |
| -- | -- |
| StorageMode.LocalStorage | ローカルストレージにデータを保存します。 |
| StorageMode.SessionStorage | セッションストレージにデータを保存します。 |

## サードパーティ製ライブラリのデータの永続化
GcFormPersistenceクラスでは[IThirdpartyEditor](</inputmanjs/api/interfaces/inputman.ithirdpartyeditor.html>)を設定することで、サードパーティ製ライブラリのデータを永続化することができます。

このデモでは一例としてWijmoの[InputNumber](</wijmo/api/classes/wijmo_input.inputnumber.html>)のデータを永続化する方法を紹介しています。

またはサードパーティ製のライブラリのデータを保存したくない・保存する必要がない場合は、明示的にskipメソッドを実行する必要があります。
```javascript
fp = new GC.InputMan.GcFormPersistence(document.getElementById('testform'));
//Wijmo要素をスキップする
fp.skip(function(el){
    return el.classList.contains("wj-control");
})
```