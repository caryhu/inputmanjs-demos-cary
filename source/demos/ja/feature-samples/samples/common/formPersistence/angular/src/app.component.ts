import '@grapecity/wijmo.styles/wijmo.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, AfterViewInit } from '@angular/core';
import * as wjcInput from '@grapecity/wijmo.input';
import GC from "@grapecity/inputman";
import "./license";
import "./wijmoRule";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent implements AfterViewInit{
    fp: GC.InputMan.GcFormPersistence;
    wjfp: GC.InputMan.GcFormPersistence;
    htmlfp: GC.InputMan.GcFormPersistence;

    set saveMode(newVal: GC.InputMan.SaveMode){
        this.fp.saveMode = newVal;
        this.htmlfp.saveMode = newVal;
        this.wjfp.saveMode = newVal;
    } 

    set storageMode(newVal: GC.InputMan.StorageMode){
        this.fp.storageMode = newVal;
        this.htmlfp.storageMode = newVal;
        this.wjfp.storageMode = newVal;
    }

    ngAfterViewInit(){
        const theNumber = new wjcInput.InputNumber('#theNumber');

        //オプション要素の永続化を実行する
        let op = new GC.InputMan.GcFormPersistence(document.getElementById('op'));
        op.persist();

        let _saveMode = (document.getElementById('onSubmit') as any).checked ? GC.InputMan.SaveMode.SaveOnSubmit : GC.InputMan.SaveMode.SaveOnRefresh;
        let _storageMode = (document.getElementById('session') as any).checked ? GC.InputMan.StorageMode.SessionStorage : GC.InputMan.StorageMode.LocalStorage;

        //InputManJSフォームの永続化を実行する
        this.fp = new GC.InputMan.GcFormPersistence(document.getElementById('imfm'), {
            saveMode: _saveMode,
            storageMode: _storageMode
        });
        this.fp.persist();

        //wijmoフォームの永続化を実行する
        this.wjfp = new GC.InputMan.GcFormPersistence(document.getElementById('wjfm'), {
            saveMode: _saveMode,
            storageMode: _storageMode
        });
        this.wjfp.persist();

        //標準のHTML要素の永続化を実行する
        this.htmlfp = new GC.InputMan.GcFormPersistence(document.getElementById('htmlfm'), {
            saveMode: _saveMode,
            storageMode: _storageMode
        });
        this.htmlfp.persist();
    }

    reload(){
        window.parent.location.reload(true);
    }
}


enableProdMode();