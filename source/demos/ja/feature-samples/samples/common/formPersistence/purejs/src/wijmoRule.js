//Wijmoの永続化を行うためのルール(IThirdpartyEditor)を設定します。
import * as wjcCore from '@grapecity/wijmo';
import { InputMan } from '@grapecity/inputman';

function WijmoRule() { }
WijmoRule.prototype = {
    /**
    * 要素をスキップする際の処理を設定します。
    * @param {HTMLElement} element ホスト要素からのHtml要素
    */
    skip: function (ele) {
        //今回はスキップしたい要素はないのでfalseを設定します。
        return false;
    },

    /**
    * 要素がサードパーティのコントロールかどうかを判断し、
    * そうである場合にストレージキーの利用するnameを設定する処理を定義します。
    * @param {HTMLElement} element ホスト要素からのHtml要素
    */
    checker: function (ele) {
        //wijmoは常に最も外側の要素にクラスに「wj-control」を追加するため、
        //以下の条件で要素がwijmoコントロールかどうかを確認できます。 
        if (ele.classList.contains("wj-control")) {
            return ele.getAttribute("name");
        }
        return "";
    },

    /**
    * ページの更新やフォームの送信時にコントロールの値をストレージに保存する際の処理を定義します。
    * @param {ElementContext} context 現在のGcFormPersistence、現在の要素、要素名の情報
    * interface ElementContext {
    *     formPersistence: GcFormPersistence;
    *     element: HTMLElement;
    *     name: string;
    * }
    */
    save: function (context) {
        //最初にcontent.elementによってコントロールインスタンスを取得します。
        var wijmo_control = wjcCore.Control.getControl(context.element);

        //次にGcFormPersistenceのインスタンスを利用して、setValueメソッドで値を保存します。
        //setValueの第1引数にはcheckerメソッドで設定したストレージキー名、
        //第2引数はストレージに保存する値をそれぞれ指定します。
        context.formPersistence.setValue(context.name, wijmo_control.value);
    },

    /**
    * ページがロードされたときにストレージからコントロールの値を復元する際の処理を定義します。
    * @param {ElementContext} context 現在のGcFormPersistence、現在の要素、要素名の情報
    * interface ElementContext {
    *     formPersistence: GcFormPersistence;
    *     element: HTMLElement;
    *     name: string;
    * }
    */
    restore: function (context) {
        //最初にcontent.elementによってコントロールインスタンスを取得します。
        var wijmo_control = wjcCore.Control.getControl(context.element);
        //次にGcFormPersistenceのインスタンスを利用して、getValueメソッドで値を取得します。
        //getValueメソッドの引数は保存時に設定したストレージキー名を指定し、
        //戻り値を復元したいコントロールインスタンスのプロパティに設定します。
        wijmo_control.value = Number(context.formPersistence.getValue(context.name));
    },
};

 //registerEditorメソッドにWijmo永続化のルールを設定する
 InputMan.GcFormPersistence.registerEditor(new WijmoRule());