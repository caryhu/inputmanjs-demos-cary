import '@grapecity/inputman/CSS/gc.inputman-js.css';
import '@grapecity/wijmo.styles/wijmo.css';
import './license';
import './wijmoRule';
import { InputMan } from '@grapecity/inputman';
import * as wjcInput from '@grapecity/wijmo.input';

//オプション要素の永続化を実行する
let op = new GC.InputMan.GcFormPersistence(document.getElementById('op'));
op.persist();

let save_mode = document.getElementById('onSubmit').checked ? InputMan.SaveMode.SaveOnSubmit : InputMan.SaveMode.SaveOnRefresh;
let storage_mode = document.getElementById('session').checked ? InputMan.StorageMode.SessionStorage : InputMan.StorageMode.LocalStorage;

// テキストコントロール
const gcTextBox = new InputMan.GcTextBox(document.getElementById('gcTextBox'));
// マスクコントロール
const gcMask = new InputMan.GcMask(document.getElementById('gcMask'), {
    formatPattern: '〒\\D{3}-\\D{4}'
});
// 数値コントロール
const gcNumber = new InputMan.GcNumber(document.getElementById('gcNumber'));

// 日付時刻コントロール
const gcDateTime = new InputMan.GcDateTime(document.getElementById('gcDateTime'));

let fp = new GC.InputMan.GcFormPersistence(document.getElementById('imfm'), {
    saveMode: save_mode,
    storageMode: storage_mode
});
//InputManJSフォームの永続化を実行する
fp.persist();

//WijmoInputNumberコントロール
const theNumber = new wjcInput.InputNumber('#theNumber');


//wijmoフォームの永続化を実行する
let wjfp = new GC.InputMan.GcFormPersistence(document.getElementById('wjfm'), {
    saveMode: save_mode,
    storageMode: storage_mode
});
wjfp.persist();

//標準のHTML要素の永続化を実行する
let htmlfp = new GC.InputMan.GcFormPersistence(document.getElementById('htmlfm'), {
    saveMode: save_mode,
    storageMode: storage_mode
});
htmlfp.persist();

document.getElementById('refresh').addEventListener('click', () => {
    window.parent.location.reload();
});

document.getElementById('onReload').addEventListener('click', () => {
    fp.saveMode = GC.InputMan.SaveMode.SaveOnRefresh;
    wjfp.saveMode = GC.InputMan.SaveMode.SaveOnRefresh;
    htmlfp.saveMode = GC.InputMan.SaveMode.SaveOnRefresh;
    op.saveMode = GC.InputMan.SaveMode.SaveOnRefresh;
});

document.getElementById('onSubmit').addEventListener('click', () => {
    fp.saveMode = GC.InputMan.SaveMode.SaveOnSubmit;
    wjfp.saveMode = GC.InputMan.SaveMode.SaveOnSubmit;
    htmlfp.saveMode = GC.InputMan.SaveMode.SaveOnSubmit;
    op.saveMode = GC.InputMan.SaveMode.SaveOnSubmit;
});

document.getElementById('local').addEventListener('click', () => {
    fp.storageMode = GC.InputMan.StorageMode.LocalStorage;
    wjfp.storageMode = GC.InputMan.StorageMode.LocalStorage;
    htmlfp.storageMode = GC.InputMan.StorageMode.LocalStorage;
});

document.getElementById('session').addEventListener('click', () => {
    fp.storageMode = GC.InputMan.StorageMode.SessionStorage;
    wjfp.storageMode = GC.InputMan.StorageMode.SessionStorage;
    htmlfp.storageMode = GC.InputMan.StorageMode.SessionStorage;
});