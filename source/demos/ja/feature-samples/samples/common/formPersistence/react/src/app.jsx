import * as React from "react";
import * as ReactDom from "react-dom";
import { GcDateTime, GcMask, GcNumber, GcTextBox} from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import * as wjInput from '@grapecity/wijmo.input';
 import '@grapecity/wijmo.styles/wijmo.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './license';
import './wijmoRule';

class App extends React.Component{

    get saveMode(){return this._saveMode}
    set saveMode(newVal){
        this._saveMode = newVal;
        this.fp.saveMode = newVal;
        this.htmlfp.saveMode = newVal;
        this.wjfp.saveMode = newVal;
    } 

    get storageMode() {return this._storageMode}
    set storageMode(newVal){
        this._storageMode = newVal;
        this.fp.storageMode = newVal;
        this.htmlfp.storageMode = newVal;
        this.wjfp.storageMode = newVal;
    }

    componentDidMount(){
        const theNumber = new wjInput.InputNumber('#theNumber');

        //オプション要素の永続化を実行する
        let op = new GC.InputMan.GcFormPersistence(document.getElementById('op'));
        op.persist();

        this._saveMode = document.getElementById('onSubmit').checked ? InputMan.SaveMode.SaveOnSubmit : InputMan.SaveMode.SaveOnRefresh;
        this._storageMode = document.getElementById('session').checked ? InputMan.StorageMode.SessionStorage : InputMan.StorageMode.LocalStorage;

        //InputManJSフォームの永続化を実行する
        this.fp = new GC.InputMan.GcFormPersistence(document.getElementById('imfm'), {
            saveMode: this.saveMode,
            storageMode: this.storageMode
        });
        this.fp.persist();

        //wijmoフォームの永続化を実行する
        this.wjfp = new GC.InputMan.GcFormPersistence(document.getElementById('wjfm'), {
            saveMode: this.saveMode,
            storageMode: this.storageMode
        });
        this.wjfp.persist();

        //標準のHTML要素の永続化を実行する
        this.htmlfp = new GC.InputMan.GcFormPersistence(document.getElementById('htmlfm'), {
            saveMode: this.saveMode,
            storageMode: this.storageMode
        });
        this.htmlfp.persist();
    }



    render(){
        return <React.Fragment>
            InputManJSコントロール：
            <form id="imfm" action="" target="_parent">
                <div class="flexbox">
                    <div>
                        テキストコントロール<br/>
                        <GcTextBox />
                    </div>
                    <div>
                        マスクコントロール<br/>
                        <GcMask formatPattern={"〒\\D{3}-\\D{4}"} />
                    </div>
                    <div>
                        数値コントロール<br/>
                        <GcNumber />
                    </div>
                    <div>
                        日付時刻コントロール<br/>
                        <GcDateTime />
                    </div>
                </div>
            </form>

            Wijmoコントロール：
            <form id="wjfm">
                InputNumber<br/>
                <div id="theNumber" name="number"></div>
            </form>

            <br/>HTML入力要素：
            <form id="htmlfm">
                <input id="input"/>
            </form>

            <form id="op">
                <table class="sample">
                    <tr>
                        <th>保存するタイミング</th>
                        <td>
                            <label><input type="radio" id="onReload" value="0" name="saveMode" checked onChange={()=> this.saveMode=InputMan.SaveMode.SaveOnRefresh}/>ブラウザ更新時に保存する</label>
                            <label><input type="radio" id="onSubmit" value="1" name="saveMode" onChange={()=> this.saveMode=InputMan.SaveMode.SaveOnSubmit}/>データ送信時にも保存する</label>
                        </td>
                    </tr>
                    <tr>
                        <th>データの保存先</th>
                        <td>
                            <label><input type="radio" id="local" value="0" name="storageMode" checked onChange={()=> this.storageMode=InputMan.StorageMode.LocalStorage} />ローカルストレージ</label>
                            <label><input type="radio" id="session" value="1" name="storageMode" onChange={()=>this.storageMode=InputMan.StorageMode.SessionStorage} />セッションストレージ</label>
                        </td>
                    </tr>
                </table>
            </form>
            <br/>
            <button type="button" onClick={() => window.parent.location.reload()}>ブラウザを更新する</button>
            <input type="submit" form="imfm" value="フォームデータを送信する"></input>
        </React.Fragment>
    }
}

ReactDom.render(<App />, document.getElementById("app"));