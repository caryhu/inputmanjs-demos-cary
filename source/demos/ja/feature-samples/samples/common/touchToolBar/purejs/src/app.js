import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';
import products from './data';

const gcTextBox = new InputMan.GcTextBox(document.getElementById('gcTextBox'), {
    text: 'テキスト'
});
const gcMultiLineTextBox = new InputMan.GcMultiLineTextBox(document.getElementById('gcMultiLineTextBox'), {
    text: '宮城県仙台市泉区紫山３－１－４'
});
const gcMask = new InputMan.GcMask(document.getElementById('gcMask'), {
    formatPattern: '〒\\D{3}-\\D{4}'
});
const gcDateTime = new InputMan.GcDateTime(document.getElementById('gcDateTime'));
const gcNumber = new InputMan.GcNumber(document.getElementById('gcNumber'));
const gcComboBox = new InputMan.GcComboBox(document.getElementById('gcComboBox'), {
    items: products
});