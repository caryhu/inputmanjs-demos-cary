import * as React from "react";
import * as ReactDom from "react-dom";
import { GcTextBox, GcMultiLineTextBox, GcMask, GcNumber, GcDateTime, GcComboBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import products from './data';

class App extends React.Component{

    render(){
        return (
            <div>
                <p>タッチデバイスで各コントロールを長押ししてください。</p>
                <div class="flexbox">
                    <div>
                    テキストコントロール
                    <GcTextBox text= {'テキスト'}></GcTextBox>
                    </div>
                    <div>
                    複数行テキストコントロール
                    <GcMultiLineTextBox text= {'宮城県仙台市泉区紫山３－１－４'}></GcMultiLineTextBox>
                    </div>
                    <div>
                    マスクコントロール
                    <GcMask formatPattern= {'〒\\D{3}-\\D{4}'}></GcMask>
                    </div>
                    <div>
                    日付時刻コントロール
                    <GcDateTime></GcDateTime>
                    </div>
                    <div>
                    数値コントロール
                    <GcNumber></GcNumber>
                    </div>
                    <div>
                    コンボコントロール
                    <GcComboBox items={products}></GcComboBox>
                    </div>
                </div>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));