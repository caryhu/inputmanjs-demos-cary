import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";
import products from './data';


@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {

    public products: Array<string> = products;
}



enableProdMode();