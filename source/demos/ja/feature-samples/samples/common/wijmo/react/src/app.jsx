import * as React from "react";
import * as ReactDom from "react-dom";
import { GcMask, GcTextBox, GcComboBox, GcNumber, GcDateTime } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { CollectionView } from '@grapecity/wijmo';
import {getProducts, categoryNames} from './data';
//import { FlexGrid } from '@grapecity/wijmo.grid';

import * as grid from '@grapecity/wijmo.grid';
import * as wjGrid from '@grapecity/wijmo.react.grid';

import '@grapecity/wijmo.styles/wijmo.css';
import './styles.css';
import './license';

class App extends React.Component{
    controls = null;
    cv = new CollectionView(getProducts(), {
        trackChanges: true,
        currentChanged: () => this.setState({currentItem: this.cv.currentItem})
    });

    constructor(props, context) {
        super(props, context);
        this.state = {
            enabled: false,
            currentItem: this.cv.currentItem
        };
    }


    componentDidMount(){
        this.controls={
            商品コード: this.refs.gcMask._imCtrl,
            商品名: this.refs.gcTextBox._imCtrl,
            分類: this.refs.gcComboBox._imCtrl,
            価格: this.refs.gcNumber._imCtrl,
            発売日: this.refs.gcDateTime._imCtrl
        }
        const btn_contanier = document.getElementById('btn_container');
        btn_contanier.addEventListener('click', (e) => {
            let name = e.target.getAttribute('name');
            if(!name){
                return;
            }
            this.handler[name]();
        });
    }

    switchForms = ()=> {
        const editForm = document.getElementById('editForm');
        const commitForm = document.getElementById('commitForm');
        editForm.style.display = editForm.style.display == 'block' ? 'none' : 'block';
        commitForm.style.display = commitForm.style.display == 'block' ? 'none' : 'block';
    }

    handler = {
        edit: () => {
            this.cv.editItem(this.cv.currentItem);
            this.setState({enabled: true});
            this.switchForms();
        },
        add: () => {
            this.cv.addNew();
            this.setState({enabled: true, currentItem: this.cv.currentItem});
            this.switchForms();
        },
        remove: () => {
            this.cv.remove(this.cv.currentItem);
        },
        commit: () => {
            for (var fieldName in this.controls) {
                this.cv.currentItem[fieldName] = fieldName == '分類' ? categoryNames[this.controls[fieldName].getSelectedIndex()] : this.controls[fieldName].getValue();
            }
            this.cv.commitEdit();
            this.cv.commitNew();
            this.setState({enabled: false});
            this.switchForms();
        },
        cancel: () => {
            cv.cancelEdit();
            cv.cancelNew();
            this.setState({enabled: false});
            this.switchForms();
        },
        moveCurrentToFirst: () => this.cv.moveCurrentToFirst(), 
        moveCurrentToPrevious: () => this.cv.moveCurrentToPrevious(),
        moveCurrentToNext: () => this.cv.moveCurrentToNext(),
        moveCurrentToLast: () => this.cv.moveCurrentToLast(),
    }

    render(){
        return (
            <div>
                <table id="btn_container">
                    <tr>
                        <td style={{minWidth: '300px'}}>
                            <table>
                                <tr>
                                    <td>商品コード</td>
                                    <td><GcMask ref="gcMask" formatPattern={'\\A\\D{3}'} enabled={this.state.enabled} value={this.state.currentItem.商品コード === undefined ? null : this.state.currentItem.商品コード}></GcMask></td>
                                </tr>
                                <tr>
                                    <td>商品名</td>
                                    <td><GcTextBox ref="gcTextBox" enabled={this.state.enabled} text={this.state.currentItem.商品名 === undefined? null: this.state.currentItem.商品名}></GcTextBox></td>
                                </tr>
                                <tr>
                                    <td>分類</td>
                                    <td><GcComboBox ref="gcComboBox" items= {categoryNames} enabled={this.state.enabled} selectedValue={this.state.currentItem.分類 === undefined? -1 : this.state.currentItem.分類}></GcComboBox></td>
                                </tr>
                                <tr>
                                    <td>価格</td>
                                    <td><GcNumber ref="gcNumber"
                                            displayPositivePrefix= {'$'}
                                            displayNegativePrefix= {'-$'}
                                            displayFormatDigit= {'##,##0'}
                                            showNumericPad= {true}
                                            enabled={this.state.enabled}
                                            value={this.state.currentItem.価格 === undefined? null: this.state.currentItem.価格}></GcNumber>
                                    </td>
                                </tr>
                                <tr>
                                    <td>発売日</td>
                                    <td><GcDateTime ref="gcDateTime"
                                            formatPattern= {'yyy/M/d'}
                                            displayFormatPattern= {''}
                                            showDropDownButton= {true}
                                            dropDownConfig= {{
                                                dropDownType: InputMan.DateDropDownType.Calendar
                                            }}
                                            enabled={this.state.enabled}
                                            value={this.state.currentItem.発売日 === undefined? null :this.state.currentItem.発売日}></GcDateTime></td>
                                </tr>
                            </table><br/>
                            <div id="editForm" style={{display: 'block'}}>
                                <button name="edit">編集</button>&nbsp;
                                <button name="add">追加</button>&nbsp;
                                <button name="remove">削除</button>
                            </div>
                            <div id="commitForm" style={{display: 'none'}}>
                                <button name="commit">確定</button>&nbsp;
                                <button name="cancel">キャンセル</button>
                            </div>
                            <div>
                                <button name="moveCurrentToFirst">&lt;&lt;</button>&nbsp;
                                <button name="moveCurrentToPrevious">&lt;</button>&nbsp;
                                <button name="moveCurrentToNext">&gt;</button>&nbsp;
                                <button name="moveCurrentToLast">&gt;&gt;</button>
                            </div>
                        </td>
                        <td>
                            <div id="grid">
                                <wjGrid.FlexGrid
                                    itemsSource= {this.cv}
                                    isReadOnly= {true}
                                    columns= {[
                                        { header: '商品コード', binding: '商品コード', width: 100 },
                                        { header: '商品名', binding: '商品名', width: 200 },
                                        { header: '分類', binding: '分類', width: 120 },
                                        { header: '価格', binding: '価格', format: 'c', width: 80 },
                                        { header: '発売日', binding: '発売日', width: 120 }
                                    ]}>
                                </wjGrid.FlexGrid>
                            </div>
                        </td>
                    </tr>
                </table><br/>
                <table id='edit' style={{tableLayout: 'fixed',width:'100%'}}>
                    <tr>
                        <td>
                            <label>編集された項目:</label>
                            <wjGrid.FlexGrid
                                itemsSource= {this.cv.itemsEdited}
                                isReadOnly= {true}>
                            </wjGrid.FlexGrid>
                        </td>
                        <td>
                            <label>追加された項目:</label>
                            <wjGrid.FlexGrid
                                itemsSource= {this.cv.itemsAdded}
                                isReadOnly= {true}>
                            </wjGrid.FlexGrid>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>削除された項目：</label>
                            <wjGrid.FlexGrid
                                itemsSource= {this.cv.itemsRemoved}
                                isReadOnly= {true}>
                            </wjGrid.FlexGrid>
                        </td>
                        <td>

                        </td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));