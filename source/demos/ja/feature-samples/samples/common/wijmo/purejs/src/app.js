import '@grapecity/inputman/CSS/gc.inputman-js.css';
import '@grapecity/wijmo.styles/wijmo.css';
import './styles.css';
import './license';
import {InputMan} from '@grapecity/inputman';
import { FlexGrid } from '@grapecity/wijmo.grid';
import { CollectionView } from '@grapecity/wijmo';
import {getProducts, categoryNames} from './data';

const cv = new CollectionView(getProducts(), {
    trackChanges: true,
    currentChanged: () => update()
});
const grid = new FlexGrid('#grid', {
    itemsSource: cv,
    isReadOnly: true,
    columns: [
        { header: '商品コード', binding: '商品コード', width: 100 },
        { header: '商品名', binding: '商品名', width: 200 },
        { header: '分類', binding: '分類', width: 120 },
        { header: '価格', binding: '価格', format: 'c', width: 80 },
        { header: '発売日', binding: '発売日', width: 120 }
    ]
});
const gridEdited = new FlexGrid('#gridEdited', {
    itemsSource: cv.itemsEdited,
    isReadOnly: true
});
const gridAdded = new FlexGrid('#gridAdded', {
    itemsSource: cv.itemsAdded,
    isReadOnly: true
});
const gridRemoved = new FlexGrid('#gridRemoved', {
    itemsSource: cv.itemsRemoved,
    isReadOnly: true
});

const controls = {
    商品コード: new InputMan.GcMask(document.getElementById('商品コード'), {
        formatPattern: '\\A\\D{3}'
    }),
    商品名: new InputMan.GcTextBox(document.getElementById('商品名')),
    分類: new InputMan.GcComboBox(document.getElementById('分類'), {
        items: categoryNames
    }),
    価格: new InputMan.GcNumber(document.getElementById('価格'), {
        displayPositivePrefix: '$',
        displayNegativePrefix: '-$',
        displayFormatDigit: '##,##0',
        showNumericPad: true
    }),
    発売日: new InputMan.GcDateTime(document.getElementById('発売日'), {
        formatPattern: 'yyy/M/d',
        displayFormatPattern: '',
        showDropDownButton: true,
        dropDownConfig: {
            dropDownType: InputMan.DateDropDownType.Calendar
        }
    })
};

const update = () => {
    for (var fieldName in controls) {
        if (fieldName == '商品名') {
            controls[fieldName].setText(cv.currentItem[fieldName]);
        } else if (fieldName == '分類') {
            controls[fieldName].setSelectedIndex(categoryNames.indexOf(cv.currentItem[fieldName]));
        } else {
            controls[fieldName].setValue(cv.currentItem[fieldName]);
        }
    }
}
const setEnabled = (isEnabled) => {
    for (var fieldName in controls) {
        controls[fieldName].setEnabled(isEnabled);
    }
}
const switchForms = ()=> {
    const editForm = document.getElementById('editForm');
    const commitForm = document.getElementById('commitForm');
    editForm.style.display = editForm.style.display == 'block' ? 'none' : 'block';
    commitForm.style.display = commitForm.style.display == 'block' ? 'none' : 'block';
}

const btn_contanier = document.getElementById('btn_container');
btn_contanier.addEventListener('click', (e) => {
    let name = e.target.getAttribute('name');
    if(!name){
        return;
    }
    handler[name]();
});

const handler = {
    edit: () => {
        cv.editItem(cv.currentItem);
        setEnabled(true);
        switchForms();
    },
    add: () => {
        cv.addNew();
        setEnabled(true);
        switchForms();
    },
    remove: () => {
        cv.remove(cv.currentItem);
    },
    commit: () => {
        for (var fieldName in controls) {
            cv.currentItem[fieldName] = fieldName == '分類' ? categoryNames[controls[fieldName].getSelectedIndex()] : controls[fieldName].getValue();
        }
        cv.commitEdit();
        cv.commitNew();
        setEnabled(false);
        switchForms();
    },
    cancel: () => {
        cv.cancelEdit();
        cv.cancelNew();
        setEnabled(false);
        switchForms();
    },
    moveCurrentToFirst: () => cv.moveCurrentToFirst(), 
    moveCurrentToPrevious: () => cv.moveCurrentToPrevious(),
    moveCurrentToNext: () => cv.moveCurrentToNext(),
    moveCurrentToLast: () => cv.moveCurrentToLast(),
}

update();
setEnabled(false);