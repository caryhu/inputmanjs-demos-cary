import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, OnInit } from '@angular/core';
import GC from "@grapecity/inputman";
import { FlexGrid } from '@grapecity/wijmo.grid';
import { CollectionView } from '@grapecity/wijmo';
import { getProducts, categoryNames } from './data';
import './license';
import '@grapecity/wijmo.styles/wijmo.css';


@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent implements OnInit {
    public categoryNames = categoryNames;
    public gcDateTimeDropDownConfig = {
        dropDownType: GC.InputMan.DateDropDownType.Calendar
    }
    public cv: CollectionView;
    public controlDic: object = {
        "商品コード": undefined,
        "商品名": undefined,
        "分類": undefined,
        "価格": undefined,
        "発売日": undefined,
    }
    public enabled: boolean = false;

    public ngOnInit() {
        this.cv = new CollectionView(getProducts(), {
            trackChanges: true,
            currentChanged: () => this.update()
        });
        const grid = new FlexGrid('#grid', {
            itemsSource: this.cv,
            isReadOnly: true,
            columns: [
                { header: '商品コード', binding: '商品コード', width: 100 },
                { header: '商品名', binding: '商品名', width: 200 },
                { header: '分類', binding: '分類', width: 120 },
                { header: '価格', binding: '価格', format: 'c', width: 80 },
                { header: '発売日', binding: '発売日', width: 120 }
            ]
        });
        const gridEdited = new FlexGrid('#gridEdited', {
            itemsSource: this.cv.itemsEdited,
            isReadOnly: true
        });
        const gridAdded = new FlexGrid('#gridAdded', {
            itemsSource: this.cv.itemsAdded,
            isReadOnly: true
        });
        const gridRemoved = new FlexGrid('#gridRemoved', {
            itemsSource: this.cv.itemsRemoved,
            isReadOnly: true
        });
        this.update();
    }

    public switchForms() {
        const editForm = document.getElementById('editForm');
        const commitForm = document.getElementById('commitForm');
        editForm.style.display = editForm.style.display == 'block' ? 'none' : 'block';
        commitForm.style.display = commitForm.style.display == 'block' ? 'none' : 'block';
    }

    public edit() {
        this.cv.editItem(this.cv.currentItem);
        this.enabled = true;
        this.switchForms();
    }

    public add() {
        this.cv.addNew();
        this.enabled = true;
        this.switchForms();
    }

    public remove() {
        this.cv.remove(this.cv.currentItem);
    }

    public commit() {
        for (var fieldName in this.controlDic) {
            this.cv.currentItem[fieldName] = this.controlDic[fieldName];
        }
        this.cv.commitEdit();
        this.cv.commitNew();
        this.enabled = false;
        this.switchForms();
    }

    public cancel() {
        this.cv.cancelEdit();
        this.cv.cancelNew();
        this.enabled = false;
        this.switchForms();
    }

    public moveCurrentToFirst() {
        this.cv.moveCurrentToFirst();
    }

    public moveCurrentToPrevious() {
        this.cv.moveCurrentToPrevious();
    }

    public moveCurrentToNext() {
        this.cv.moveCurrentToNext();
    }

    public moveCurrentToLast() {
        this.cv.moveCurrentToLast();
    }

    public update() {
        for (var fieldName in this.controlDic) {
            if (this.cv.currentItem[fieldName] === undefined) {
                this.controlDic[fieldName] = null;
            } else {
                this.controlDic[fieldName] = this.cv.currentItem[fieldName];
            }
        }
    }

    public handler(event: Event): void {
        let name = (event as any).target.getAttribute('name');
        if (!name) {
            return;
        }
        this[name]();
    }

}


enableProdMode();