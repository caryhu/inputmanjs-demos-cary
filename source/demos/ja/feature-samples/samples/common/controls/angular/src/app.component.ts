import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, ViewChild } from '@angular/core';
import GC from "@grapecity/inputman";
import products from './data';
import { GcSoftKeyboardComponent } from '@grapecity/inputman.angular';

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public products: Array<string> = products;
    public InputMan = GC.InputMan;
    public SF9 = GC.InputMan.FunctionKey.Shift | GC.InputMan.FunctionKey.F9;
    public SF10 = GC.InputMan.FunctionKey.Shift | GC.InputMan.FunctionKey.F10;
    public SF11 = GC.InputMan.FunctionKey.Shift | GC.InputMan.FunctionKey.F11;
    public SF12 = GC.InputMan.FunctionKey.Shift | GC.InputMan.FunctionKey.F12;
    public CFH = GC.InputMan.FunctionKey.Ctrl | GC.InputMan.FunctionKey.Home;
    public CFE = GC.InputMan.FunctionKey.Ctrl | GC.InputMan.FunctionKey.End;
    public AFPU = GC.InputMan.FunctionKey.Alt | GC.InputMan.FunctionKey.PageUp;
    public AFPD = GC.InputMan.FunctionKey.Alt | GC.InputMan.FunctionKey.PageDown;
    @ViewChild("keyboard") keyboard: GcSoftKeyboardComponent;

    public initGcTextBoxDropDown(gcTextBoxDropDown: GC.InputMan.GcTextBox): void {
        // ドロップダウンプラグイン
        const gcDropDown = gcTextBoxDropDown.createDropDown();
        gcDropDown.getElement().appendChild(document.getElementById('products'));
        document.getElementById('products').addEventListener('click', (e) => {
            if ((e.srcElement as HTMLElement).className.indexOf('product') >= 0) {
                gcTextBoxDropDown.setText((e.srcElement as HTMLElement).innerText);
                gcDropDown.close();
            }
        });
    }

    public initGcTextBoxValidator(gcTextBoxValidator: GC.InputMan.GcTextBox): void {
        // 検証コントロール
        const validator: any = new GC.InputMan.GcValidator({
            items: [
                {
                    control: gcTextBoxValidator,
                    ruleSet: [
                        {
                            rule: GC.InputMan.ValidateType.Required
                        }
                    ],
                }
            ],
            defaultNotify: {
                fail: {
                    icon: {
                        direction: GC.InputMan.IconDirection.Inside
                    },
                    controlState: true
                }
            }
        });
        validator.validate();
    }

    onFunctionKeyActived(e: GC.InputMan.KeyActivateArgs){
        window.alert('「' + e.description + '」キーが押されました。');
    }

    onInputFocus(){
        this.keyboard.getNestedIMControl().open();
    }
}


enableProdMode();