import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';
import products  from './data';


// テキストコントロール
const gcTextBox = new InputMan.GcTextBox(document.getElementById('gcTextBox'), {
    text: 'テキスト'
});
// 複数行テキストコントロール
const gcMultiLineTextBox = new InputMan.GcMultiLineTextBox(document.getElementById('gcMultiLineTextBox'), {
    text: '宮城県仙台市泉区紫山３－１－４'
});
// マスクコントロール
const gcMask = new InputMan.GcMask(document.getElementById('gcMask'), {
    formatPattern: '〒\\D{3}-\\D{4}'
});
// 数値コントロール
const gcNumber = new InputMan.GcNumber(document.getElementById('gcNumber'));
// 日付時刻コントロール
const gcDateTime = new InputMan.GcDateTime(document.getElementById('gcDateTime'));
// カレンダーコントロール
const gcCalendar = new InputMan.GcCalendar(document.getElementById('gcCalendar'));
// コンボコントロール
const gcComboBox = new InputMan.GcComboBox(document.getElementById('gcComboBox'), {
    items: products
});
// リストコントロール
const gcListBox = new InputMan.GcListBox(document.getElementById('gcListBox'), {
    items: products
});
// ドロップダウンプラグイン
const gcTextBoxDropDown = new InputMan.GcTextBox(document.getElementById('gcTextBoxDropDown'));
const gcDropDown = gcTextBoxDropDown.createDropDown();
gcDropDown.getElement().appendChild(document.getElementById('products'));
document.getElementById('products').addEventListener('click', (e) => {
    if (e.srcElement.className.indexOf('product') >= 0) {
        gcTextBoxDropDown.setText(e.srcElement.innerText);
        gcDropDown.close();
    }
});
// 検証コントロール
const gcTextBoxValidator = new InputMan.GcTextBox(document.getElementById('gcTextBoxValidator'));
const validator = new GCIM.GcValidator({
    items: [
        {
            control: gcTextBoxValidator,
            ruleSet: [
                {
                    rule: InputMan.ValidateType.Required
                }
            ],
        }
    ],
    defaultNotify: {
        fail: {
            icon: {
                direction: InputMan.IconDirection.Inside
            },
            controlState: true
        }
    }
});
validator.validate();
// ソフトウェアキーボード
const gcSoftKeyboard = new InputMan.GcSoftKeyboard(document.getElementById('gcSoftKeyboard'), {
    target: document.getElementById('input')
});
document.getElementById('input').addEventListener('focus', () => {
    gcSoftKeyboard.open();
});
// ファンクションキーコントロール
const gcFunctionKey = new InputMan.GcFunctionKey(document.getElementById('gcFunctionKey'), {
    functionKeys: [
        { key: InputMan.FunctionKey.F1, description: 'F1' },
        { key: InputMan.FunctionKey.F2, description: 'F2' },
        { key: InputMan.FunctionKey.F3, description: 'F3' },
        { key: InputMan.FunctionKey.F4, description: 'F4' },
        { key: InputMan.FunctionKey.F5, description: 'F5' },
        { key: InputMan.FunctionKey.F6, description: 'F6' },
        { key: InputMan.FunctionKey.F7, description: 'F7' },
        { key: InputMan.FunctionKey.F8, description: 'F8' },
        { key: InputMan.FunctionKey.Shift | InputMan.FunctionKey.F9, description: 'Shift+F9' },
        { key: InputMan.FunctionKey.Shift | InputMan.FunctionKey.F10, description: 'Shift+F10' },
        { key: InputMan.FunctionKey.Shift | InputMan.FunctionKey.F11, description: 'Shift+F11' },
        { key: InputMan.FunctionKey.Shift | InputMan.FunctionKey.F12, description: 'Shift+F12' },
        { key: InputMan.FunctionKey.Ctrl | InputMan.FunctionKey.Home, description: 'Ctrl+Home' },
        { key: InputMan.FunctionKey.Ctrl | InputMan.FunctionKey.End, description: 'Ctrl+End' },
        { key: InputMan.FunctionKey.Alt | InputMan.FunctionKey.PageUp, description: 'Alt+PageUp' },
        { key: InputMan.FunctionKey.Alt | InputMan.FunctionKey.PageDown, description: 'Alt+PageDown' },
    ],
    onActived: function (s, e) {
        window.alert('「' + e.description + '」キーが押されました。');
    }
});
const controls = [gcTextBox, gcMultiLineTextBox, gcMask, gcNumber, gcDateTime, gcCalendar, gcComboBox, gcListBox, gcTextBoxDropDown, gcTextBoxValidator, document.getElementById('input')];
// ショートカットキーコントロール
var gcShortcut = new InputMan.GcShortcut();
for (let control of controls) {
    gcShortcut.add({ key: InputMan.GcShortcutKey.P, target: control, action: InputMan.GcShortcutAction.PreviousControl });
    gcShortcut.add({ key: InputMan.GcShortcutKey.N, target: control, action: InputMan.GcShortcutAction.NextControl });
}

// 電卓コントロール
let gcCalculator = new InputMan.GcCalculator(document.getElementById('calculator'));