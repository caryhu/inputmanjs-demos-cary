import * as React from "react";
import * as ReactDom from "react-dom";
import { GcTextBox, GcMultiLineTextBox, GcMask, GcNumber, GcDateTime,GcCalendar, GcComboBox, GcListBox, GcSoftKeyboard, GcFunctionKey, GcCalculator, GcFunctionKeyInfo, GcShortcut, GcShortcutInfo } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import products  from './data';

class App extends React.Component{

    constructor(){
        super();
        this.keyboard = React.createRef();
        this.textbox = React.createRef();
    }

    componentDidMount(){
        // 検証コントロール
        const gcTextBoxValidator = this.textbox.current.getNestedIMControl();
        const validator = new GCIM.GcValidator({
            items: [
                {
                    control: gcTextBoxValidator,
                    ruleSet: [
                        {
                            rule: InputMan.ValidateType.Required
                        }
                    ],
                }
            ],
            defaultNotify: {
                fail: {
                    icon: {
                        direction: InputMan.IconDirection.Inside
                    },
                    controlState: true
                }
            }
        });
        validator.validate();
    }

    render(){
        return (
            <div>
                <div class="flexbox">
                    <div>
                        テキストコントロール
                        <GcTextBox id='textbox' text= {'テキスト'}></GcTextBox>
                    </div>
                    <div>
                        複数行テキストコントロール
                        <GcMultiLineTextBox id='multitextbox' text= {'宮城県仙台市泉区紫山３－１－４'}></GcMultiLineTextBox>
                    </div>
                    <div>
                        マスクコントロール
                        <GcMask id='mask' formatPattern= {'〒\\D{3}-\\D{4}'}></GcMask>
                    </div>
                    <div>
                        数値コントロール
                        <GcNumber id='number'></GcNumber>
                    </div>
                    <div>
                        日付時刻コントロール
                        <GcDateTime id='datetime'></GcDateTime>
                    </div>
                    <div>
                        カレンダーコントロール
                        <GcCalendar id='calendar'></GcCalendar>
                    </div>
                    <div>
                        コンボコントロール
                       <GcComboBox id='combo' items= {products}></GcComboBox>
                    </div>
                    <div>
                        リストコントロール
                        <GcListBox id='list' items= {products}></GcListBox>
                    </div>
                    <div>
                        ドロップダウンプラグイン
                        <GcTextBox id='textbox-dropdown'
                        onInitialized={(imCtrl)=>{ 
                            const gcDropDown = imCtrl.createDropDown();
                            gcDropDown.getElement().appendChild(document.getElementById('products'));
                            document.getElementById('products').addEventListener('click', (e) => {
                                if (e.srcElement.className.indexOf('product') >= 0) {
                                    imCtrl.setText(e.srcElement.innerText);
                                    gcDropDown.close();
                                }
                            });
                        }}></GcTextBox>
                    </div>
                    <div>
                        検証コントロール<br/>
                        <GcTextBox ref={this.textbox} id='textbox-validator'/>
                    </div>
                    <div>
                        ソフトウェアキーボード<br/>
                        <input id="soft-key-input" onFocus={()=>this.keyboard.current.getNestedIMControl().open()} autoComplete="off"/>
                        <GcSoftKeyboard ref={this.keyboard} target='#soft-key-input'/>
                    </div>
                </div>

                <div>
                    ファンクションキーコントロール<br/>
                    <GcFunctionKey onActived={(s,e)=> window.alert('「' + e.description + '」キーが押されました。')}>
                        <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F1} description="F1" />
                        <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F2} description="F2" />
                        <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F3} description="F3" />
                        <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F4} description="F4" />
                        <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F5} description="F5" />
                        <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F6} description="F6" />
                        <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F7} description="F7" />
                        <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F8} description="F8" />
                        <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.Shift | InputMan.FunctionKey.F9} description="Shift+F9" />
                        <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.Shift | InputMan.FunctionKey.F10} description="Shift+F10" />
                        <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.Shift | InputMan.FunctionKey.F11} description="Shift+F11" />
                        <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.Shift | InputMan.FunctionKey.F12} description="Shift+F12" />
                        <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.Ctrl | InputMan.FunctionKey.Home} description="Ctrl+Home" />
                        <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.Ctrl | InputMan.FunctionKey.End} description="Ctrl+End" />
                        <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.Alt | InputMan.FunctionKey.PageUp} description="Alt+PageUp" />
                        <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.Alt | InputMan.FunctionKey.PageDown} description="Alt+PageDown" />
                    </GcFunctionKey>
                </div>
                <br/>
                <div>
                    電卓コントロール<br/>
                    <GcCalculator />
                </div>
                <GcShortcut>
                    <GcShortcutInfo shortcutKey={InputMan.GcShortcutKey.P} target="#textbox" action={InputMan.GcShortcutAction.PreviousControl} />
                    <GcShortcutInfo shortcutKey={InputMan.GcShortcutKey.P} target="#multitextbox" action={InputMan.GcShortcutAction.PreviousControl} />
                    <GcShortcutInfo shortcutKey={InputMan.GcShortcutKey.P} target="#mask" action={InputMan.GcShortcutAction.PreviousControl} />
                    <GcShortcutInfo shortcutKey={InputMan.GcShortcutKey.P} target="#number" action={InputMan.GcShortcutAction.PreviousControl} />
                    <GcShortcutInfo shortcutKey={InputMan.GcShortcutKey.P} target="#datetime" action={InputMan.GcShortcutAction.PreviousControl} />
                    <GcShortcutInfo shortcutKey={InputMan.GcShortcutKey.P} target="#calendar" action={InputMan.GcShortcutAction.PreviousControl} />
                    <GcShortcutInfo shortcutKey={InputMan.GcShortcutKey.P} target="#combo" action={InputMan.GcShortcutAction.PreviousControl} />
                    <GcShortcutInfo shortcutKey={InputMan.GcShortcutKey.P} target="#list" action={InputMan.GcShortcutAction.PreviousControl} />
                    <GcShortcutInfo shortcutKey={InputMan.GcShortcutKey.P} target="#textbox-dropdown" action={InputMan.GcShortcutAction.PreviousControl} />
                    <GcShortcutInfo shortcutKey={InputMan.GcShortcutKey.P} target="#textbox-validator" action={InputMan.GcShortcutAction.PreviousControl} />
                    <GcShortcutInfo shortcutKey={InputMan.GcShortcutKey.P} target="#soft-key-input" action={InputMan.GcShortcutAction.PreviousControl} />
                    
                    <GcShortcutInfo shortcutKey={InputMan.GcShortcutKey.N} target="#textbox" action={InputMan.GcShortcutAction.NextControl} />
                    <GcShortcutInfo shortcutKey={InputMan.GcShortcutKey.N} target="#multitextbox" action={InputMan.GcShortcutAction.NextControl} />
                    <GcShortcutInfo shortcutKey={InputMan.GcShortcutKey.N} target="#mask" action={InputMan.GcShortcutAction.NextControl} />
                    <GcShortcutInfo shortcutKey={InputMan.GcShortcutKey.N} target="#number" action={InputMan.GcShortcutAction.NextControl} />
                    <GcShortcutInfo shortcutKey={InputMan.GcShortcutKey.N} target="#datetime" action={InputMan.GcShortcutAction.NextControl} />
                    <GcShortcutInfo shortcutKey={InputMan.GcShortcutKey.N} target="#calendar" action={InputMan.GcShortcutAction.NextControl} />
                    <GcShortcutInfo shortcutKey={InputMan.GcShortcutKey.N} target="#combo" action={InputMan.GcShortcutAction.NextControl} />
                    <GcShortcutInfo shortcutKey={InputMan.GcShortcutKey.N} target="#list" action={InputMan.GcShortcutAction.NextControl} />
                    <GcShortcutInfo shortcutKey={InputMan.GcShortcutKey.N} target="#textbox-dropdown" action={InputMan.GcShortcutAction.NextControl} />
                    <GcShortcutInfo shortcutKey={InputMan.GcShortcutKey.N} target="#textbox-validator" action={InputMan.GcShortcutAction.NextControl} />
                    <GcShortcutInfo shortcutKey={InputMan.GcShortcutKey.N} target="#soft-key-input" action={InputMan.GcShortcutAction.NextControl} />
                </GcShortcut>
                <div id="products">
                <table class="form">
                    <tr>
                        <th>商品コード</th>
                        <th>商品名</th>
                        <th>価格</th>
                    </tr>
                    <tr>
                        <th colspan="3">飲料</th>
                    </tr>
                    <tr>
                        <td>11</td>
                        <td class="product">果汁100%オレンジ</td>
                        <td>200円</td>
                    </tr>
                    <tr>
                        <td>12</td>
                        <td class="product">コーヒーマイルド</td>
                        <td>190円</td>
                    </tr>
                    <tr>
                        <td>13</td>
                        <td class="product">ピリピリビール</td>
                        <td>280円</td>
                    </tr>
                    <tr>
                        <th colspan="3">調味料</th>
                    </tr>
                    <tr>
                        <td>21</td>
                        <td class="product">ホワイトソルト</td>
                        <td>260円</td>
                    </tr>
                    <tr>
                        <td>22</td>
                        <td class="product">ブラックペッパー</td>
                        <td>210円</td>
                    </tr>
                    <tr>
                        <td>23</td>
                        <td class="product">ピュアシュガー</td>
                        <td>280円</td>
                    </tr>
                </table>
                </div>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));