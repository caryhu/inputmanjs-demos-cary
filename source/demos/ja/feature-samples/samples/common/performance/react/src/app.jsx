import * as React from "react";
import * as ReactDom from "react-dom";
import { GcTextBox, GcMultiLineTextBox, GcMask, GcNumber, GcDateTime,GcCalendar, GcComboBox, GcListBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import products from './data';


class App extends React.Component{
    controlTypeIndex = 0;
    controlCount=0;
    items= [];
    constructor(props, context) {
        super(props, context);
        this.state = {
            key: (new Date()).valueOf()
        };
    }

    componentDidMount(){
        this.getGenerateInfo();
    }

    getGenerateInfo(){
        const controlType = document.getElementById('controlType');
        this.controlCount = Number(document.getElementById('controlCount').value);
        this.controlTypeIndex = controlType.selectedIndex;
    }

    generateItems(){
        this.items=[];
        this.getGenerateInfo();
        for(var i = 0; i < this.controlCount; i++){
            switch (this.controlTypeIndex) {
                case 0:
                    this.items.push(<GcTextBox className={'displayStyle'}></GcTextBox>);
                    break;
                case 1:
                    this.items.push(<GcMultiLineTextBox className={'displayStyle'}></GcMultiLineTextBox>);
                    break;
                case 2:
                    this.items.push(<GcMask className={'displayStyle'} formatPattern= {'〒\\D{3}-\\D{4}'}></GcMask>);
                    break;
                case 3:
                    this.items.push(<GcDateTime className={'displayStyle'}></GcDateTime>);
                    break;
                case 4:
                    this.items.push(<GcNumber className={'displayStyle'}></GcNumber>);
                    break;
                case 5:
                    this.items.push(<GcCalendar className={'displayStyle'}></GcCalendar>);
                    break;
                case 6:
                    this.items.push(<GcComboBox className={'displayStyle'} items={products}></GcComboBox>);
                    break;
                case 7:
                    this.items.push(<GcListBox className={'displayStyle'} items={products}></GcListBox>);
                    break;
            }
        }
        this.setState({key: (new Date()).valueOf()});
    }

    render(){
        return (
            <div>
                <button id="performance_btn" onClick={()=>{
                    this.generateItems();
                }}>コントロールを生成</button>
                <table class="sample">
                    <tr>
                        <th>コントロールの種類</th>
                        <td>
                            <select id="controlType">
                                <option>テキストコントロール</option>
                                <option>複数行テキストコントロール</option>
                                <option>マスクコントロール</option>
                                <option>日付時刻コントロール</option>
                                <option>数値コントロール</option>
                                <option>カレンダーコントロール</option>
                                <option>コンボコントロール</option>
                                <option>リストコントロール</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>コントロールの数</th>
                        <td>
                            <select id="controlCount">
                                <option>100</option>
                                <option>1000</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <div key={this.state.key}>
                    {this.items}
                </div>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));