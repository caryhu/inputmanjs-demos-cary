import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";
import products from './data';

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {

    public changeControl(): void {

        const controlType = document.getElementById('controlType') as HTMLSelectElement;
        const controlCount = document.getElementById('controlCount') as HTMLSelectElement;

        let container = document.getElementById('container');

        while (container.hasChildNodes()) {
            container.removeChild(container.lastChild);
        }

        let count = Number(controlCount.value);
        for (let i = 0; i < count; i++) {
            var tagName = controlType.selectedIndex == 1 ? 'textarea' : controlType.selectedIndex <= 4 ? 'input' : controlType.selectedIndex >= 6 ? 'select' : 'div';
            let element = document.createElement(tagName) as HTMLInputElement;
            container.appendChild(element);
            switch (controlType.selectedIndex) {
                case 0:
                    new GC.InputMan.GcTextBox(element);
                    break;
                case 1:
                    new GC.InputMan.GcMultiLineTextBox(element as any);
                    break;
                case 2:
                    let gcMask = new GC.InputMan.GcMask(element, {
                        formatPattern: '〒\\D{3}-\\D{4}'
                    });
                    break;
                case 3:
                    new GC.InputMan.GcDateTime(element);
                    break;
                case 4:
                    new GC.InputMan.GcNumber(element);
                    break;
                case 5:
                    new GC.InputMan.GcCalendar(element);
                    break;
                case 6:
                    new GC.InputMan.GcComboBox(element as any , {
                        items: products
                    });
                    break;
                case 7:
                    new GC.InputMan.GcListBox(element, {
                        items: products
                    });
                    break;
            }
        }
    }
}


enableProdMode();