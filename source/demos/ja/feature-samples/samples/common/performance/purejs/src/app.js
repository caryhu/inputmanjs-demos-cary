import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';
import products from './data';

const controlType = document.getElementById('controlType');
const controlCount = document.getElementById('controlCount');

document.getElementById('performance_btn').addEventListener('click', () => {
    let container = document.getElementById('container');

    while (container.hasChildNodes()) {
        container.removeChild(container.lastChild);
    }

    let count = Number(controlCount.value);
    for (let i = 0; i < count; i++) {
        var tagName = controlType.selectedIndex == 1 ? 'textarea' : controlType.selectedIndex <= 4 ? 'input' : controlType.selectedIndex >= 6 ? 'select' : 'div';
        let element = document.createElement(tagName);
        container.appendChild(element);
        switch (controlType.selectedIndex) {
            case 0:
                new InputMan.GcTextBox(element);
                break;
            case 1:
                new InputMan.GcMultiLineTextBox(element);
                break;
            case 2:
                let gcMask = new InputMan.GcMask(element, {
                    formatPattern: '〒\\D{3}-\\D{4}'
                });
                break;
            case 3:
                new InputMan.GcDateTime(element);
                break;
            case 4:
                new InputMan.GcNumber(element);
                break;
            case 5:
                new InputMan.GcCalendar(element);
                break;
            case 6:
                new InputMan.GcComboBox(element, {
                    items: products
                });
                break;
            case 7:
                new InputMan.GcListBox(element, {
                    items: products
                });
                break;
        }
    }
});