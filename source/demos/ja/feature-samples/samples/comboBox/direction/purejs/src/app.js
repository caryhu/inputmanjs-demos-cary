import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';
import { products } from './data';

const gcComboBox1 = new InputMan.GcComboBox(document.getElementById('gcComboBox1'), {
    items: products,
    container: document.getElementById('container1')
});
const gcComboBox2 = new InputMan.GcComboBox(document.getElementById('gcComboBox2'), {
    items: products,
    container: document.getElementById('container2')
});