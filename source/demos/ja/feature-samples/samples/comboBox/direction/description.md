コンボコントロールでは、ドロップダウンの表示スペースを判定して、自動的に適切な方向にドロップダウンを表示することができます。

## ドロップダウンの方向

ドロップダウンは通常コントロールの下に表示されますが、コントロールの下にドロップダウンを表示できる十分なスペースがない場合は、ドロップダウンがコントロールの上に表示されます。ドロップダウンの表示スペースを判定するとき、既定ではWebページ本体（body要素）をコンテナとして扱いますが、setContainerメソッドで任意の要素をコンテナとして指定することも可能です。

```js
gcComboBox.setContainer(document.getElementById('container1'));
```
