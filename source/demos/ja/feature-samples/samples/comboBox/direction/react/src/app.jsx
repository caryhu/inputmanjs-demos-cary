import * as React from "react";
import * as ReactDom from "react-dom";
import { GcComboBox } from "@grapecity/inputman.react";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { products } from './data';

class App extends React.Component{
    constructor(){
        super();
        this.gcCombo1 = React.createRef();
        this.gcCombo2 = React.createRef();
    }

    componentDidMount(){
        this.gcCombo1.current.getNestedIMControl().setContainer(this.refs.container1);
        this.gcCombo2.current.getNestedIMControl().setContainer(this.refs.container2);
    }

    render(){
        return (
            <div class="flexbox">
                <div id="container1" ref="container1">
                    <p>ドロップダウンが下に表示されます。</p>
                    <GcComboBox
                        ref={this.gcCombo1}
                        items= {products}>
                    </GcComboBox>
                </div>
                <div id="container2" ref="container2">
                    <p>ドロップダウンが上に表示されます。</p>
                    <GcComboBox
                        ref={this.gcCombo2}
                        items= {products}>
                    </GcComboBox>
                </div>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));