export const orders = [
    { id: 15, product: 'ピュアデミグラスソース', date: '2017/01/10', price: 200, amount: 30 },
    { id: 17, product: 'だしこんぶ', date: '2017/01/08', price: 290, amount: 50 },
    { id: 18, product: 'ピリカラタバスコ', date: '2017/01/12', price: 200, amount: 20 },
    { id: 84, product: 'なまわさび', date: '2017/01/21', price: 200, amount: 40 },
    { id: 85, product: 'なまからし', date: '2017/01/13', price: 200, amount: 40 },
    { id: 86, product: 'なましょうが', date: '2017/01/20', price: 200, amount: 40 },
];

export const styles = {
    '.gcim': {
        width: '180px',
        height: '40px',
        backgroundColor: '#ddffdd',
        borderColor: '#009900',
        borderWidth: '2px',
        borderStyle: 'dashed',
        borderRadius: '12px',
        boxShadow: '5px 5px 5px rgba(0,0,0,0.5)',
        color: '#009900'
    },
    '.gcim__input': {
        cursor: 'crosshair',
        fontSize: '20px',
        fontWeight: 'bold',
        fontStyle: 'italic',
        fontFamily: 'serif',
        textAlign: 'right',
        textShadow: '1px 1px 1px rgba(0,0,0,0.5)'
    },
    '.gcim__input:disabled': {
        backgroundColor: '#666666',
        color: '#cccccc',
        cursor: 'wait'
    },
    '.gcim_focused': {
        backgroundColor: '#ddddff',
        borderColor: '#0000ff',
        color: '#0000ff'
    },
    '.gcim_watermark_null': {
        backgroundColor: '#ffdddd',
        borderColor: '#ff0000',
        color: '#ff0000'
    },
    '.gcim_focused.gcim_watermark_null': {
        backgroundColor: '#ffddff',
        borderColor: '#990099',
        color: '#990099'
    },
    '.gcim__listbox': {
        backgroundColor: '#ddffdd',
        borderColor: '#009900',
        borderWidth: '2px',
        borderStyle: 'dashed',
        borderRadius: '12px',
        boxShadow: '5px 5px 5px rgba(0,0,0,0.5)',
        color: '#009900',
        fontSize: '20px',
        fontWeight: 'bold',
        fontStyle: 'italic',
        fontFamily: 'serif',
        textShadow: '1px 1px 1px rgba(0,0,0,0.5)'
    },
    '.gcim__listbox>.viewport': {
        cursor: 'crosshair'
    },
    '.gcim__listbox .column-header': {
        color: '#009900',
        fontSize: '20px',
        fontWeight: 'bold',
        fontStyle: 'italic',
        fontFamily: 'serif',
        textShadow: '1px 1px 1px rgba(0,0,0,0.5)'
    },
    '.gcim__listbox .footer_style': {
        color: '#009900',
        fontSize: '20px',
        fontWeight: 'bold',
        fontStyle: 'italic',
        fontFamily: 'serif',
        textAlign: 'right',
        textShadow: '1px 1px 1px rgba(0,0,0,0.5)'
    },
};

export const specRules = {
    'text_style_align': [
        {
            selector: '.gcim__listbox .viewport .list-item',
            style: 'text-align: right;'
        },
        {
            selector: '.gcim__listbox .text-content',
            style: 'text-align: right !important;'
        },
        {
            selector: '.gcim__listbox .footer',
            style: 'text-align: right;'
        }
    ]  
};