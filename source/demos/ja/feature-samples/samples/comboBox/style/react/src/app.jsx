import * as React from "react";
import * as ReactDom from "react-dom";
import { GcComboBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { orders, styles, specRules } from './data';

const SpecPrefix = "$spec:";

class OSpecCssStyleMap {
    map = {};

    constructor() { }

    addRules(key, rules) {
        if (!this.map[key]) {
            this.map[key] = [];
        }

        rules.forEach(rule => {
            this.map[key].push(rule);
        })
    }

    deleteAllRuleByKey(key) {
        this.map[key] = [];
    }

    getAllRule() {
        let arr = [];
        Object.keys(this.map).forEach(key => {
            this.map[key].forEach((rule) => {
                arr.push(rule);
            })
        }); 
        return arr;
    }
}

class App extends React.Component{

    styleSheet = null;
    specStyleSheet = null;
    specCssStyleMap = null;

    constructor(props) {
        super(props);
    }

    componentDidMount(){
        this.styleSheet = this.createStyleSheet();
        this.specStyleSheet = this.createStyleSheet();
        this.specCssStyleMap = new OSpecCssStyleMap();

        this.initStyleSheet(this.styleSheet, this.props.styles);
        this.attachEvent();
    }

    attachEvent() {
        document.getElementById('container').addEventListener('click', (e) => {
            if(e.target.tagName == 'INPUT'){
                this.inputBtnClickHandler(e);
            }else if(e.target.tagName == 'BUTTON'){
                this.copyStyle()
            }
        })
    }
    
    inputBtnClickHandler(evt) {
        let el = evt.target;
    
        if (this.startsWith(el.value, SpecPrefix)) {
            let key = el.value.slice(SpecPrefix.length, el.value.length);
            this.processSpecKey(key, el.checked);
        } else {
            this.processKey(el.value, el.checked);
        }
    
        this.showCssText([this.styleSheet, this.specStyleSheet]);
    }
    
    initStyleSheet(styleSheet, styles) {
        Object.keys(styles).forEach((styleName, index) => styleSheet.insertRule(`${styleName}{}`, index));
    }
    
    processKey(key, checked) {
        let values = key.split(','),
        selector = values[0],
        propName = values[1];
    
        let cssRules = this.findCssRule(this.styleSheet, selector);
        cssRules.style[propName] = checked ? styles[selector][propName] : '';
    }
    
    processSpecKey(key, checked) {
        let specStyle = this.props.specRules[key];
        if (checked) {
            this.specCssStyleMap.addRules(key, specStyle);
        } else {
            this.specCssStyleMap.deleteAllRuleByKey(key);
        }
    
        this.syncSpecStyleMapToStyleSheet(this.specCssStyleMap, this.specStyleSheet);
    }

    //#region helperMethod
    findCssRule(styleSheet, selector) {
        for(let i = 0; i < styleSheet.cssRules.length; i++){
            if(styleSheet.cssRules[i].selectorText == selector){
                return styleSheet.cssRules[i];
            }
        }
        return null;
    }

    syncSpecStyleMapToStyleSheet(styleMap, styleSheet) {
        this.clearStyleSheet(styleSheet);

        let specRules = styleMap.getAllRule();
        specRules.forEach((rule) => {
            styleSheet.insertRule(`${rule.selector} { ${rule.style} }`, styleSheet.cssRules.length);
        });
    }

    showCssText(styleSheets) {
        let cssTexts = [];
        styleSheets.forEach((styleSheet) => {
            this.appendCssText(styleSheet, cssTexts);
        });
        document.getElementById('style').value = cssTexts.join('\r\n');
    }

    appendCssText(styleSheet, cssTexts) {
        for(let i = 0; i < styleSheet.cssRules.length; i++){
            if(styleSheet.cssRules[i].style.length > 0){
                cssTexts.push(styleSheet.cssRules[i].cssText
                    .replace(/{/g, '{\r\n  ')
                    .replace(/;/g, ';\r\n  ')
                    .replace(/  }/g, '}'));
            }
        }
    }

    clearStyleSheet(cssStyleSheet) {
        let len = cssStyleSheet.cssRules.length;
        while(len--) {
            cssStyleSheet.deleteRule(0);
        }
    }

    createStyleSheet() {
        let element = document.createElement('style');
        document.head.appendChild(element);
        return element.sheet;
    }

    copyStyle() {
        document.getElementById('style').select();
        document.execCommand('copy');
    }
    startsWith(str, searchString) {
        return str.slice(0, searchString.length) == searchString;
    }
    //#endregion

    render(){
        return (
            <div id="container">
                <div class="flexbox">
                    <div>
                        通常の状態
                        <GcComboBox
                                items= {orders}
                                displayMemberPath= {'product'}
                                valueMemberPath= {'product'}
                                dropDownWidth= {'auto'}
                                columns= {[
                                    { name: 'id', label: '商品コード', width: 80 },
                                    { name: 'product', label: '商品名', width: 200 },
                                ]}
                                footerTemplate= {'商品を選択してください'}
                                selectedIndex={0}></GcComboBox>
                    </div>
                    <div>
                        無効な状態
                        <GcComboBox
                                items= {orders}
                                displayMemberPath= {'product'}
                                valueMemberPath= {'product'}
                                dropDownWidth= {'auto'}
                                columns= {[
                                    { name: 'id', label: '商品コード', width: 80 },
                                    { name: 'product', label: '商品名', width: 200 },
                                ]}
                                footerTemplate= {'商品を選択してください'}
                                selectedIndex={0}
                                enabled={false}></GcComboBox>
                    </div>
                    <div>
                        ウォーターマーク
                        <GcComboBox
                                items= {orders}
                                displayMemberPath= {'product'}
                                valueMemberPath= {'product'}
                                dropDownWidth= {'auto'}
                                columns= {[
                                    { name: 'id', label: '商品コード', width: 80 },
                                    { name: 'product', label: '商品名', width: 200 },
                                ]}
                                footerTemplate= {'商品を選択してください'}
                                watermarkDisplayNullText={'商品'}
                                watermarkNullText={'商品を選択'}></GcComboBox>
                    </div>
                </div>
                <div class="peoperty-header">コントロール全般のスタイル</div>
                <div class="peoperty-panel">
                    <label><input type="checkbox" value=".gcim,width"/>コントロールの幅</label><br/>
                    <label><input type="checkbox" value=".gcim,height"/>コントロールの高さ</label><br/>
                    <label><input type="checkbox" value=".gcim,backgroundColor"/>背景色</label><br/>
                    <label><input type="checkbox" value=".gcim,borderColor"/>境界線の色</label><br/>
                    <label><input type="checkbox" value=".gcim,borderWidth"/>境界線の幅</label><br/>
                    <label><input type="checkbox" value=".gcim,borderStyle"/>境界線のスタイル</label><br/>
                    <label><input type="checkbox" value=".gcim,borderRadius"/>境界線の角丸</label><br/>
                    <label><input type="checkbox" value=".gcim__input,cursor"/>カーソルの形</label><br/>
                    <label><input type="checkbox" value=".gcim,boxShadow"/>コントロールの影</label><br/>
                </div>
                <div class="peoperty-header">テキストのスタイル</div>
                <div class="peoperty-panel">
                    <label><input type="checkbox" value=".gcim,color"/>文字色</label><br/>
                    <label><input type="checkbox" value=".gcim__input,fontSize"/>フォントのサイズ</label><br/>
                    <label><input type="checkbox" value=".gcim__input,fontWeight"/>フォントの太さ</label><br/>
                    <label><input type="checkbox" value=".gcim__input,fontStyle"/>フォントのスタイル</label><br/>
                    <label><input type="checkbox" value=".gcim__input,fontFamily"/>フォントの種類</label><br/>
                    <label><input type="checkbox" value=".gcim__input,textAlign"/>水平方向の位置</label><br/>
                    <label><input type="checkbox" value=".gcim__input,textShadow"/>テキストの影</label><br/>
                </div>
                <div class="peoperty-header">コントロール無効時のスタイル</div>
                <div class="peoperty-panel">
                    <label><input type="checkbox" value=".gcim__input:disabled,backgroundColor"/>背景色</label><br/>
                    <label><input type="checkbox" value=".gcim__input:disabled,color"/>文字色</label><br/>
                    <label><input type="checkbox" value=".gcim__input:disabled,cursor"/>カーソルの形</label><br/>
                </div>
                <div class="peoperty-header">フォーカス時のスタイル</div>
                <div class="peoperty-panel">
                    <label><input type="checkbox" value=".gcim_focused,backgroundColor"/>背景色</label><br/>
                    <label><input type="checkbox" value=".gcim_focused,borderColor"/>境界線の色</label><br/>
                    <label><input type="checkbox" value=".gcim_focused,color"/>文字色</label><br/>
                </div>
                <div class="peoperty-header">ウォーターマークのスタイル</div>
                <div class="peoperty-panel">
                    <label><input type="checkbox" value=".gcim_watermark_null,backgroundColor"/>背景色</label><br/>
                    <label><input type="checkbox" value=".gcim_watermark_null,borderColor"/>境界線の色</label><br/>
                    <label><input type="checkbox" value=".gcim_watermark_null,color"/>文字色</label><br/>
                </div>
                <div class="peoperty-header">ウォーターマークのフォーカス時のスタイル</div>
                <div class="peoperty-panel">
                    <label><input type="checkbox" value=".gcim_focused.gcim_watermark_null,backgroundColor"/>背景色</label><br/>
                    <label><input type="checkbox" value=".gcim_focused.gcim_watermark_null,borderColor"/>境界線の色</label><br/>
                    <label><input type="checkbox" value=".gcim_focused.gcim_watermark_null,color"/>文字色</label><br/>
                </div>
                <div class="peoperty-header">ドロップダウンリストの全般のスタイル</div>
                <div class="peoperty-panel">
                    <label><input type="checkbox" value=".gcim__listbox,backgroundColor"/>背景色</label><br/>
                    <label><input type="checkbox" value=".gcim__listbox,borderColor"/>境界線の色</label><br/>
                    <label><input type="checkbox" value=".gcim__listbox,borderWidth"/>境界線の幅</label><br/>
                    <label><input type="checkbox" value=".gcim__listbox,borderStyle"/>境界線のスタイル</label><br/>
                    <label><input type="checkbox" value=".gcim__listbox,borderRadius"/>境界線の角丸</label><br/>
                    <label><input type="checkbox" value=".gcim__listbox>.viewport,cursor"/>カーソルの形</label><br/>
                    <label><input type="checkbox" value=".gcim__listbox,boxShadow"/>コントロールの影</label><br/>
                </div>
                <div class="peoperty-header">ドロップダウンリストのテキストのスタイル</div>
                <div class="peoperty-panel">
                    <label><input type="checkbox" value=".gcim__listbox,color"/>文字色</label><br/>
                    <label><input type="checkbox" value=".gcim__listbox,fontSize"/>フォントのサイズ</label><br/>
                    <label><input type="checkbox" value=".gcim__listbox,fontWeight"/>フォントの太さ</label><br/>
                    <label><input type="checkbox" value=".gcim__listbox,fontStyle"/>フォントのスタイル</label><br/>
                    <label><input type="checkbox" value=".gcim__listbox,fontFamily"/>フォントの種類</label><br/>
                    <label><input type="checkbox" value="$spec:text_style_align"/>水平方向の位置</label><br/>
                    <label><input type="checkbox" value=".gcim__listbox,textShadow"/>テキストの影</label><br/>
                </div>
                <div class="peoperty-header">ドロップダウンリストのヘッダのスタイル</div>
                <div class="peoperty-panel">
                    <label><input type="checkbox" value=".gcim__listbox .column-header,color"/>文字色</label><br/>
                    <label><input type="checkbox" value=".gcim__listbox .column-header,fontSize"/>フォントのサイズ</label><br/>
                    <label><input type="checkbox" value=".gcim__listbox .column-header,fontWeight"/>フォントの太さ</label><br/>
                    <label><input type="checkbox" value=".gcim__listbox .column-header,fontStyle"/>フォントのスタイル</label><br/>
                    <label><input type="checkbox" value=".gcim__listbox .column-header,fontFamily"/>フォントの種類</label><br/>
                    <label><input type="checkbox" value=".gcim__listbox .column-header,textShadow"/>テキストの影</label><br/>
                </div>
                <div class="peoperty-header">ドロップダウンリストのフッタのスタイル</div>
                <div class="peoperty-panel">
                    <label><input type="checkbox" value=".gcim__listbox .footer_style,color"/>文字色</label><br/>
                    <label><input type="checkbox" value=".gcim__listbox .footer_style,fontSize"/>フォントのサイズ</label><br/>
                    <label><input type="checkbox" value=".gcim__listbox .footer_style,fontWeight"/>フォントの太さ</label><br/>
                    <label><input type="checkbox" value=".gcim__listbox .footer_style,fontStyle"/>フォントのスタイル</label><br/>
                    <label><input type="checkbox" value=".gcim__listbox .footer_style,fontFamily"/>フォントの種類</label><br/>
                    <label><input type="checkbox" value=".gcim__listbox .footer_style,textAlign"/>水平方向の位置</label><br/>
                    <label><input type="checkbox" value=".gcim__listbox .footer_style,textShadow"/>テキストの影</label><br/>
                </div>
                <button>CSSコードをコピー</button><br/>
                <textarea id="style" cols="60" rows="10"></textarea>
            </div>
        )
    }
}

ReactDom.render(<App styles={styles} specRules={specRules} />, document.getElementById("app"));