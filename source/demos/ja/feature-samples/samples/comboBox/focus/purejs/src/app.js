import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';
import products from './data';

// コントロールを初期化
const gcComboBox1 = new InputMan.GcComboBox(document.getElementById('gcComboBox1'), {
    items: products
});
const gcComboBox2 = new InputMan.GcComboBox(document.getElementById('gcComboBox2'), {
    items: products
});
const gcComboBox3 = new InputMan.GcComboBox(document.getElementById('gcComboBox3'), {
    items: products
});

// 矢印キーによるフォーカス移動
var ExitOnLeftRightKeys = [
    InputMan.ExitOnLeftRightKey.None,
    InputMan.ExitOnLeftRightKey.Both,
    InputMan.ExitOnLeftRightKey.Left,
    InputMan.ExitOnLeftRightKey.Right
];
document.getElementById('setExitOnLeftRightKey').addEventListener('change', (e) => {
    gcComboBox1.setExitOnLeftRightKey(ExitOnLeftRightKeys[e.target.selectedIndex]);
    gcComboBox2.setExitOnLeftRightKey(ExitOnLeftRightKeys[e.target.selectedIndex]);
    gcComboBox3.setExitOnLeftRightKey(ExitOnLeftRightKeys[e.target.selectedIndex]);
});

// Enterキーによるフォーカス移動
var ExitKeys = [
    InputMan.ExitKey.None,
    InputMan.ExitKey.Both,
    InputMan.ExitKey.Enter,
    InputMan.ExitKey.ShiftEnter
];
document.getElementById('setExitOnEnterKey').addEventListener('change', (e) => {
    gcComboBox1.setExitOnEnterKey(ExitKeys[e.target.selectedIndex]);
    gcComboBox2.setExitOnEnterKey(ExitKeys[e.target.selectedIndex]);
    gcComboBox3.setExitOnEnterKey(ExitKeys[e.target.selectedIndex]);
});