import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";
import data from './data';

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public data = data;
    public exitOnLeftRightKey: GC.InputMan.ExitOnLeftRightKey;
    public exitOnEnterKey: GC.InputMan.ExitKey;

    // 矢印キーによるフォーカス移動
    public ExitOnLeftRightKeys = [
        GC.InputMan.ExitOnLeftRightKey.None,
        GC.InputMan.ExitOnLeftRightKey.Both,
        GC.InputMan.ExitOnLeftRightKey.Left,
        GC.InputMan.ExitOnLeftRightKey.Right
    ];

    // Enterキーによるフォーカス移動
    public ExitKeys = [
        GC.InputMan.ExitKey.None,
        GC.InputMan.ExitKey.Both,
        GC.InputMan.ExitKey.Enter,
        GC.InputMan.ExitKey.ShiftEnter
    ];
}


enableProdMode();