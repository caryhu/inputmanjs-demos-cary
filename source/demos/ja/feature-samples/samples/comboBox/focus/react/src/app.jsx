import * as React from "react";
import * as ReactDom from "react-dom";
import { GcComboBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import products from './data';

const ExitOnLeftRightKeys = [
    InputMan.ExitOnLeftRightKey.None,
    InputMan.ExitOnLeftRightKey.Both,
    InputMan.ExitOnLeftRightKey.Left,
    InputMan.ExitOnLeftRightKey.Right
];

// Enterキーによるフォーカス移動
const ExitKeys = [
    InputMan.ExitKey.None,
    InputMan.ExitKey.Both,
    InputMan.ExitKey.Enter,
    InputMan.ExitKey.ShiftEnter
];
class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            exitOnLeftRightKey: InputMan.ExitOnLeftRightKey.None,
            exitOnEnterKey: InputMan.ExitKey.None
        };
    }
    
    render(){
        return (
            <div>
                <GcComboBox items= {products}
                    exitOnLeftRightKey={this.state.exitOnLeftRightKey} 
                    exitOnEnterKey={this.state.exitOnEnterKey}></GcComboBox>
                <GcComboBox items= {products}
                    exitOnLeftRightKey={this.state.exitOnLeftRightKey} 
                    exitOnEnterKey={this.state.exitOnEnterKey}></GcComboBox>
                <GcComboBox items= {products}
                    exitOnLeftRightKey={this.state.exitOnLeftRightKey} 
                    exitOnEnterKey={this.state.exitOnEnterKey}></GcComboBox>
                <table class="sample">
                    <tr>
                        <th>矢印キーによるフォーカス移動</th>
                        <td>
                            <select id="setExitOnLeftRightKey" onChange={(e)=>{this.setState({exitOnLeftRightKey: ExitOnLeftRightKeys[e.target.selectedIndex]})}}>
                                <option>なし</option>
                                <option>両方</option>
                                <option>左</option>
                                <option>右</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Enterキーによるフォーカス移動</th>
                        <td>
                            <select id="setExitOnEnterKey" onChange={(e)=>{this.setState({exitOnEnterKey: ExitKeys[e.target.selectedIndex]})}}>
                                <option>なし</option>
                                <option>両方</option>
                                <option>Enter</option>
                                <option>Shift+Enter</option>
                            </select>
                        </td>
                    </tr>
                </table>
        </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));