import * as React from "react";
import * as ReactDom from "react-dom";
import { GcComboBox } from "@grapecity/inputman.react";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{

    constructor(props, context) {
        super(props, context);
        this.state = {
            key: (new Date()).valueOf()
        };
    }

    columns =[
        { name: 'name', label: '製品名', width: '*' },
        { name: 'category', label: '製品カテゴリー', width: '1.5*' },
        { name: 'description', label: '製品紹介', width: '2.5*' }
    ];
    
    items = [
        { name: 'ActiveReports', category: '帳票・レポート', description: '日本仕様の帳票開発に必要な機能を搭載したコンポーネント' },
        { name: 'SPREAD', category: '表計算・グリッド', description: 'Excel風のビューと表計算機能を実現するUIコンポーネント' },
        { name: 'ComponentOne', category: 'コンポーネントセット', description: 'Visual Studioで利用する.NET Framework用コンポーネント' },
        { name: 'InputMan', category: '入力支援', description: '快適な入力を実現する日本仕様入力コンポーネントセット' },
        { name: 'MultiRow', category: '多段明細', description: '1レコード複数行&日付表示に最適なグリッドコンポーネント' },
        { name: 'DioDocs', category: 'ドキュメントAPI', description: 'ドキュメントを生成、更新するAPIライブラリ' },
    ]

    render(){
        return (
            <div>
            <GcComboBox
                ref="combo"
                key={this.state.key}
                items={this.items}
                columns={this.columns}
                dropDownWidth={700}
                allowDropDownResize= {true} />
            <table class="sample">
                <tr>
                    <th>列の操作</th>
                    <td>
                        <button onClick={(e)=>{
                            this.columns.push({ name: 'sample', label: '追加列', width: '*' });
                            this.setState({key: Date.now()});
                        }}>列を追加する</button>&nbsp;
                        <button onClick={(e)=>{
                            if(this.columns.length !== 0){
                                this.columns.pop();
                            }
                            this.setState({key: Date.now()});
                        }}>列を削除する</button>
                    </td>
                </tr>
            </table>
        </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));