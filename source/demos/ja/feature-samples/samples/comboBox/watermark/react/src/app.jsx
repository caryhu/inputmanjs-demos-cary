import * as React from "react";
import * as ReactDom from "react-dom";
import { GcComboBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import products from './data';

class App extends React.Component{

    render(){
        return <GcComboBox items= {products}
                // フォーカスがないときの代替テキスト
                watermarkDisplayNullText={'商品'}
                // フォーカスがあるときの代替テキスト
                watermarkNullText={'商品名を入力してください'}></GcComboBox>
    }
}

ReactDom.render(<App />, document.getElementById("app"));