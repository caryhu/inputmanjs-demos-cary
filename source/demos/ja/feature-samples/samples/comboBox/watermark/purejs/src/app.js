import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';
import products from './data';

const gcComboBox = new InputMan.GcComboBox(document.getElementById('gcComboBox'), {
    items: products
});
// フォーカスがないときの代替テキスト
gcComboBox.setWatermarkDisplayNullText('商品');
// フォーカスがあるときの代替テキスト
gcComboBox.setWatermarkNullText('商品名を入力してください');