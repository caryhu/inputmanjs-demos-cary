import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';
import orders from './data';

let showHeader = true,
    clickSort = true,
    autoWidth = false;
let gcComboBox;
const update = () => {
    let container = document.getElementById('container');
    container.innerHTML = '';
    gcComboBox = new InputMan.GcComboBox(container, {
        items: orders,
        displayMemberPath: 'product',
        valueMemberPath: 'product',
        dropDownWidth: 'auto',
        columns: [
            { name: 'id', label: '商品コード', width: autoWidth ? 'auto' : 80, clickSort },
            { name: 'product', label: '商品名', width: autoWidth ? 'auto' : 200, clickSort },
            { name: 'date', label: '受注日', width: autoWidth ? 'auto' : 120, clickSort },
            { name: 'price', label: '単価', width: autoWidth ? 'auto' : 80, clickSort },
            { name: 'amount', label: '数量', width: autoWidth ? 'auto' : 80, clickSort },
        ],
        showHeader
    });
}
update();

const attachChangeEvent = (selector, handler) => {
    document.getElementById(selector).addEventListener('change', handler);
}
attachChangeEvent('showHeader', (e) => {
    showHeader = e.target.checked;
    update()
});
attachChangeEvent('clickSort', (e) => {
    clickSort = e.target.checked;
    update()
});
attachChangeEvent('allowColumnResize', (e) => gcComboBox.setAllowColumnResize(e.target.checked));
attachChangeEvent('autoWidth', (e) => {
    autoWidth = e.target.checked;
    update()
});