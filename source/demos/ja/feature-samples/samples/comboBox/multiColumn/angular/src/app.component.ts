import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, ViewChild, ViewContainerRef, TemplateRef, AfterContentInit } from '@angular/core';
import orders from './data';

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public orders: Array<object> = orders;
    public columnClickSort: boolean = true;
    public allowColumnResize: boolean = true;
    public columnWidthArr: Array<number | string> =[80, 200, 120, 80, 80];
    public showHeader: boolean = true;

    public updateShowHeader(show: boolean) {
        this.showHeader = show;
        this.Render();
    }

    public updateColumnWidth(isAutoColumn: boolean): void {
        if (isAutoColumn) {
            this.columnWidthArr = ['auto', 'auto', 'auto', 'auto', 'auto'];
        } else {
            this.columnWidthArr = [80, 200, 120, 80, 80];
        }
        this.Render();
    }

    public updateColumnClickSort(columnClickSort: boolean) {
        this.columnClickSort = columnClickSort;
        this.Render();
    }

    @ViewChild("outlet", { read: ViewContainerRef })
    public outletRef: ViewContainerRef;
    @ViewChild("content", { read: TemplateRef })
    public contentRef: TemplateRef<any>;

    private Render() {
        this.outletRef.clear();
        this.outletRef.createEmbeddedView(this.contentRef);
    }

    ngAfterContentInit() {
        this.outletRef.createEmbeddedView(this.contentRef);
    }
}


enableProdMode();