import * as React from "react";
import * as ReactDom from "react-dom";
import { GcComboBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import orders from './data';

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            key: (new Date()).valueOf(),
            showHeader : true,
            clickSort : true,
            autoWidth : false,
            allowColumnResize: true
        };
    }

    render(){
        return (
            <div>
                <GcComboBox
                            key={this.state.key}
                            ref="combo"
                            items= {orders}
                            displayMemberPath= {'product'}
                            valueMemberPath={'product'}
                            dropDownWidth={'auto'}
                            columns= {[
                                { name: 'id', label: '商品コード', width: this.state.autoWidth ? 'auto' : 80, clickSort: this.state.clickSort },
                                { name: 'product', label: '商品名', width: this.state.autoWidth ? 'auto' : 200, clickSort: this.state.clickSort },
                                { name: 'date', label: '受注日', width: this.state.autoWidth ? 'auto' : 120, clickSort: this.state.clickSort },
                                { name: 'price', label: '単価', width: this.state.autoWidth ? 'auto' : 80, clickSort: this.state.clickSort },
                                { name: 'amount', label: '数量', width: this.state.autoWidth ? 'auto' : 80, clickSort: this.state.clickSort },
                            ]}
                            showHeader={this.state.showHeader}
                            allowDropDownResize={this.state.allowDropDownResize}
                            allowColumnResize= {this.state.allowColumnResize}></GcComboBox>
                <table class="sample">
                    <tr>
                        <th>ヘッダ</th>
                        <td>
                            <label><input type="checkbox" id="showHeader" checked={this.state.showHeader} onChange={(e)=>{
                                    this.setState({showHeader: e.target.checked, key: (new Date()).valueOf()});
                                }}/>ヘッダを表示</label>
                        </td>
                    </tr>
                    <tr>
                        <th>ソート</th>
                        <td>
                            <label><input type="checkbox" id="clickSort" checked={this.state.clickSort} onChange={(e)=>{
                                    this.setState({clickSort: e.target.checked, key: (new Date()).valueOf()});
                                }}/>ヘッダをクリックしてソート</label>
                        </td>
                    </tr>
                    <tr>
                        <th>リサイズ</th>
                        <td>
                            <label><input type="checkbox" id="allowColumnResize" checked={this.state.allowColumnResize} onChange={(e)=>{
                                    this.setState({allowColumnResize: e.target.checked});
                                    this.refs.combo.getNestedIMControl().setAllowColumnResize(e.target.checked);
                                }}/>列のリサイズを許可</label>
                        </td>
                    </tr>
                    <tr>
                        <th>列幅の自動調整</th>
                        <td>
                            <label><input type="checkbox" id="autoWidth" checked={this.state.autoWidth} onChange={(e)=>{
                                this.setState({autoWidth: e.target.checked, key: (new Date()).valueOf()});
                                }}/>列幅を自動調整する</label>
                        </td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));