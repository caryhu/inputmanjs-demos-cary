コンボコントロールでは複数列の一覧（マルチカラムリスト）をドロップダウンリストとして表示することができます。

## 概要

コンボコントロールのcolumnsプロパティに、列ごとの定義情報を設定したcolumnオブジェクトの配列を設定することで、ドロップダウンリストに複数列の一覧を表示することが可能です。

たとえば、以下のようなコードになります。

```js
// リストアイテムに表示する値
var data = [
  { id: 15, product: 'ピュアデミグラスソース', date: '2017/01/10', price: 200, amount: 30 },
  { id: 17, product: 'だしこんぶ', date: '2017/01/08', price: 290, amount: 50 },
  { id: 18, product: 'ピリカラタバスコ', date: '2017/01/12', price: 200, amount: 20 },
  { id: 84, product: 'なまわさび', date: '2017/01/21', price: 200, amount: 40 },
  { id: 85, product: 'なまからし', date: '2017/01/13', price: 200, amount: 40 },
  { id: 86, product: 'なましょうが', date: '2017/01/20', price: 200, amount: 40 },
];

// ドロップダウンリストに表示する列の情報（columnオブジェクトの配列）
var cols = [
  { name: 'id', label: '商品コード', width: 80 },
  { name: 'product', label: '商品名', width: 200 },
  { name: 'date', label: '受注日', width: 120 },
  { name: 'price', label: '単価', width: 80 },
  { name: 'amount', label: '数量', width: 80 },
];

var gcComboBox = new GC.InputMan.GcComboBox(document.getElementById('gcComboBox'), {
  items: data,
  columns: cols,
  displayMemberPath: 'product',
  valueMemberPath: 'product',
  dropDownWidth: 'auto'
});
```

なお、ドロップダウンリストに複数列の一覧を表示する場合、ヘッダ表示やソート機能などのさまざまな機能を使用できます。詳しくは、以下の説明をご覧ください。

## ヘッダ

各列のヘッダ（タイトル行）に表示する内容は、columnsプロパティに設定したcolumnオブジェクトのlabelプロパティによって決定されます。（labelプロパティを省略した場合、nameプロパティの値がヘッダに表示されます。）また、コンボコントロールのshowHeaderプロパティをfalseに設定することで、ヘッダ自体を非表示にすることも可能です。（showHeaderプロパティの既定値は、trueです。）

たとえば、ヘッダを非表示にする場合、以下のようなコードになります。

```js
var gcComboBox = new GC.InputMan.GcComboBox(document.getElementById('gcComboBox'), {
  items: data,
  columns: cols,
  displayMemberPath: 'product',
  valueMemberPath: 'product',
  dropDownWidth: 'auto',
  showHeader: false
});
```

なお、ヘッダを非表示にしている場合、後述のソート機能やリサイズ機能は使用できません。

## ソート

ドロップダウンリストに表示されている内容は、ヘッダをクリックすることでソートすることが可能です。ソートの可否は、columnsプロパティに設定したcolumnオブジェクトのclickSortプロパティによって決定されます。したがって、列ごとにソートの可否を設定可能です。（clickSortプロパティの既定値は、falseです。）

たとえば、「受注日」の列のみソート可能にする場合、以下のような配列をコンボコントロールのcolumnsプロパティに設定します。

```js
var cols = [
  { name: 'id', label: '商品コード', width: 80 },
  { name: 'product', label: '商品名', width: 200 },
  { name: 'date', label: '受注日', width: 120, clickSort: true },
  { name: 'price', label: '単価', width: 80 },
  { name: 'amount', label: '数量', width: 80 },
];
```

## リサイズ

ドロップダウンリストに表示されている列の幅は、columnsプロパティに設定したcolumnオブジェクトのwidthプロパティによって決定されます。また、ヘッダ上で列の境界線をドラッグすることで列幅を変更することも可能です。

なお、コンボコントロールのsetAllowColumnResizeメソッドを使用することで、列幅の変更可否を設定することも可能です。（デフォルトの状態では、変更可能です。）列幅の変更を不可にする場合、以下のようなコードになります。

```js
gcComboBox.setAllowColumnResize(false);
```

## 列幅の自動調整

列の境界線をダブルクリックすると、リストの列幅が自動的に調整されます。

コードでリストの列幅を自動的に調整するには、updateColumnAutoFitメソッドを実行して、nameプロパティ（列名）とautofitプロパティ（自動調整するかどうか）を持つオブジェクト配列を引数に指定します。

```js
gcComboBox.updateColumnAutoFit([
    { name: 'id', autofit: true },
    { name: 'product', autofit: true },
    { name: 'date', autofit: true },
    { name: 'price', autofit: true },
    { name: 'amount', autofit: true }
]);
```

また、columnオブジェクトのwidthプロパティに'auto'という文字列を設定することもできます。

```js
var cols = [
  { name: 'id', label: '商品コード', width: 'auto' },
  { name: 'product', label: '商品名', width: 'auto' },
  { name: 'date', label: '受注日', width: 'auto', clickSort: true },
  { name: 'price', label: '単価', width: 'auto' },
  { name: 'amount', label: '数量', width: 'auto' },
];
```
