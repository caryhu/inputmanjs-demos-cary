import * as React from "react";
import * as ReactDom from "react-dom";
import { GcComboBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import employees from './data';


class App extends React.Component{

    render(){
        return <GcComboBox
        items= {employees}
        itemHeight= {42}
        displayMemberPath= {'name'}
        valueMemberPath= {'name'}
        dropDownWidth= {290}
        itemTemplate= {`<div class="template-item">
           <div class="id">{!id}</div>
           <div class="image"><img src="{!image}"></div>
           <div class="name"><ruby>{!lastName}<rt>{!lastKana}</rt></ruby>&nbsp;<ruby>{!firstName}<rt>{!firstKana}</rt></ruby></div>
           <div class="note">{!office}<br>{!department}</div>
           </div>`}
        headerTemplate= {'<div class="template-item"><div class="id">ID</div><div class="image">写真</div><div class="name">氏名</div><div class="note">所属</div></div>'}
        footerTemplate= {`<div>
        <span>選択されている項目: {!instance.getSelectedItem() == null ? '何も選択されていません' : instance.getSelectedItem().name}</span>
        </div>`}></GcComboBox>
    }
}

ReactDom.render(<App />, document.getElementById("app"));