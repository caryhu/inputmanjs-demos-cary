﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';
import './styles.css';

const gcComboBox = new InputMan.GcComboBox(document.getElementById('gcComboBox'), {
    items: ['果汁100%オレンジ', '果汁100%グレープ', '果汁100%レモン', '果汁100%ピーチ', 'コーヒーマイルド', 'コーヒービター'],
    showClearButton: true,
});

gcComboBox.setSelectedIndex(0);

document.getElementById('showClearButton').addEventListener('click', (e) => {
    gcComboBox.setShowClearButton(e.target.checked);
});