import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public items: string[] = ['果汁100%オレンジ', '果汁100%グレープ', '果汁100%レモン', '果汁100%ピーチ', 'コーヒーマイルド', 'コーヒービター'];
    public showClearButton: boolean = true;
}

enableProdMode();