import * as React from "react";
import * as ReactDom from "react-dom";
import { GcComboBox } from "@grapecity/inputman.react";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            showClearButton: true
        };
    }

    render(){
        return (
            <div>
            <GcComboBox
                ref="combo"
                items={ ['果汁100%オレンジ', '果汁100%グレープ', '果汁100%レモン', '果汁100%ピーチ', 'コーヒーマイルド', 'コーヒービター']}
                showClearButton={this.state.showClearButton}
                selectedIndex= {0}/>
            <table class="sample">
            <tr>
                <th>クリアボタンの表示</th>
                <td>
                    <label><input type="checkbox" checked={this.state.showClearButton} onChange={(e)=>{this.setState({showClearButton: e.target.checked})}}></input>表示する</label>
                </td>
            </tr>
        </table>
        </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));