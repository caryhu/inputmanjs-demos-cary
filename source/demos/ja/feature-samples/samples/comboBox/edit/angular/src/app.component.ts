import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode,ViewChild, AfterViewInit } from '@angular/core';
import GC from "@grapecity/inputman";
import {GcComboBoxComponent} from "@grapecity/inputman.angular";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent implements AfterViewInit {
    
    public data: string[] = [
        '果汁100%オレンジ',
        '果汁100%グレープ',
        '果汁100%レモン',
        '果汁100%ピーチ',
        'コーヒーマイルド',
        'コーヒービター'
    ];
    public autoFilter: GC.InputMan.AutoFilter = GC.InputMan.AutoFilter.Contains;
    public autoFilters = [
        GC.InputMan.AutoFilter.Contains,
        GC.InputMan.AutoFilter.StartWith,
        GC.InputMan.AutoFilter.None
    ];
    public isEditable: boolean = true;

    @ViewChild(GcComboBoxComponent) combo: GcComboBoxComponent;

    public setText(){
        var gcComboBox = this.combo.getNestedIMControl();
        gcComboBox.removeEventListener(
            GC.InputMan.GcComboBoxEvent.TextChanged,
            this.textChangedHandler
        );
        var text = (document.getElementById('setText') as HTMLInputElement).value
        gcComboBox.setText(text);
        gcComboBox.addEventListener(
            GC.InputMan.GcComboBoxEvent.TextChanged,
            this.textChangedHandler
        );
    }

    ngAfterViewInit(): void {
        var gcComboBox = this.combo.getNestedIMControl();
        gcComboBox.addEventListener(
            GC.InputMan.GcComboBoxEvent.TextChanged,
            this.textChangedHandler
        );
    }

    private textChangedHandler = () => {
        var gcComboBox = this.combo.getNestedIMControl();
        var item = this.data.filter(product => product.includes(gcComboBox.getDisplayText())) 
        if(item.length === 0){
            gcComboBox.setText('');
        }       
    }
}


enableProdMode();