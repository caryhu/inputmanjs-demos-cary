import * as React from "react";
import * as ReactDom from "react-dom";
import { GcComboBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import products from './data';

const autoFilters = [
    InputMan.AutoFilter.Contains,
    InputMan.AutoFilter.StartWith,
    InputMan.AutoFilter.None
  ];
  
class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            isEditable: true,
            autoFilter: InputMan.AutoFilter.Contains
        };
    }

    textChangedHandler = (sender) => {
        var item = products.filter(product => product.includes(sender.getDisplayText())) 
        if(item.length === 0){
            sender.setText('');
        }  
    }

    componentDidMount(){
        var gcCombo = this.refs.combo.getNestedIMControl();
        gcCombo.addEventListener( InputMan.GcComboBoxEvent.TextChanged, this.textChangedHandler);
    }

    render(){
        return (
            <div>
                <div>
                    <GcComboBox 
                    ref="combo"    
                    items= {products}
                    isEditable= {this.state.isEditable}
                    autoFilter= {this.state.autoFilter}
                    emptyTemplate= {`<div style="height:150px">該当する項目はありません</div>`}></GcComboBox>
                    </div>
                    <table class="sample">
                        <tr>
                            <th>テキストの編集</th>
                            <td>
                                <label><input type="checkbox" id="isEditable" checked={this.state.isEditable} onChange={(e)=>this.setState({isEditable: e.target.checked})}/>テキストの編集を許可する</label>
                            </td>
                        </tr>
                        <tr>
                            <th>オートフィルタ</th>
                            <td>
                                <select id="autoFilter" onChange={(e)=>this.setState({autoFilter: autoFilters[e.target.selectedIndex]})}>
                                    <option>文字列を含む</option>
                                    <option>文字列で始まる</option>
                                    <option>なし</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>テキストの設定</th>
                            <td>
                                <input type="text" id='setText'/><button onClick={(e)=>{
                                    var gcCombo = this.refs.combo.getNestedIMControl();
                                    gcCombo.removeEventListener(InputMan.GcComboBoxEvent.TextChanged, this.textChangedHandler);
                                    var text = document.getElementById('setText').value;
                                    gcCombo.setText(text);
                                    gcCombo.addEventListener( InputMan.GcComboBoxEvent.TextChanged, this.textChangedHandler);
                                }}>実行</button>
                            </td>
                        </tr>
                    </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));