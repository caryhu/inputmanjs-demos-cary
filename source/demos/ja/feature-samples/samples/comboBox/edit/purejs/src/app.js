import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';
import products from './data';

const gcComboBox = new InputMan.GcComboBox(document.getElementById('gc-combo'), {
    items: products,
    isEditable: true,
    autoFilter: InputMan.AutoFilter.Contains,
    emptyTemplate: `<div style="height:150px">該当する項目はありません</div>`,
});

document.getElementById('autoFilter').addEventListener('change', (e) => {
    gcComboBox.setAutoFilter(autoFilters[e.target.selectedIndex]);
});

document.getElementById('setTextbtn').addEventListener('click', (e) => {
    gcComboBox.removeEventListener(InputMan.GcComboBoxEvent.TextChanged, textChangedHandler);
    gcComboBox.setText(document.getElementById('setText').value);
    gcComboBox.addEventListener(InputMan.GcComboBoxEvent.TextChanged, textChangedHandler);
});

const autoFilters = [
    GC.InputMan.AutoFilter.Contains,
    GC.InputMan.AutoFilter.StartWith,
    GC.InputMan.AutoFilter.None
  ];
  
document.getElementById('isEditable').addEventListener('change', (e) => gcComboBox.setEditable(e.target.checked));

const textChangedHandler = function(){
    var item = products.filter(product => product.includes(gcComboBox.getDisplayText())) 
    if(item.length === 0){
         gcComboBox.setText('');
    }       
};

gcComboBox.addEventListener(
    InputMan.GcComboBoxEvent.TextChanged,
    textChangedHandler
);