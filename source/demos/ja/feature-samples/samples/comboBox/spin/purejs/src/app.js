import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';
import products from './data';

var spinButtonPositions = [
    InputMan.SpinButtonAlignment.RightSide,
    InputMan.SpinButtonAlignment.LeftSide
];
var dropDownButtonPositions = [
    InputMan.DropDownButtonAlignment.RightSide,
    InputMan.DropDownButtonAlignment.LeftSide
];
  
const update = () => {
    let container = document.getElementById('container');
    container.innerHTML = '';
    let gcComboBox = new InputMan.GcComboBox(container, {
        items: products,
        showSpinButton: document.getElementById('showSpinButton').checked,
        spinButtonPosition: spinButtonPositions[document.getElementById('spinButtonPosition').selectedIndex],
        showDropDownButton: document.getElementById('showDropDownButton').checked,
        dropDownButtonPosition: dropDownButtonPositions[document.getElementById('dropDownButtonPosition').selectedIndex]
    });
    gcComboBox.setSpinWheel(document.getElementById('setSpinWheel').checked);
}
update();

document.getElementById('spinButtonPosition').addEventListener('change', (e) => {
    update();
});

document.getElementById('dropDownButtonPosition').addEventListener('change', (e) => {
    update();
});

document.getElementById('showDropDownButton').addEventListener('change', (e) => {
    update();
});

document.getElementById('showSpinButton').addEventListener('change', (e) => {
    update();
});
document.getElementById('setSpinWheel').addEventListener('change', (e) => {
    update();
});