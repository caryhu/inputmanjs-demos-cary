import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, NgZone } from '@angular/core';
import GC from "@grapecity/inputman";
import products from './data';
import data from './data';


@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public products: Array<string> = products;
    public showDropDownButton: boolean = true;
    public showSpinButton: boolean = true;
    public spinButtonPosition: GC.InputMan.SpinButtonAlignment = GC.InputMan.SpinButtonAlignment.RightSide;
    public dropDownButtonPosition: GC.InputMan.DropDownButtonAlignment = GC.InputMan.DropDownButtonAlignment.RightSide;
    public spinWheel: boolean = true;

    public spinButtonPositions = [
        GC.InputMan.SpinButtonAlignment.RightSide,
        GC.InputMan.SpinButtonAlignment.LeftSide
    ];
    public dropDownButtonPositions = [
        GC.InputMan.DropDownButtonAlignment.RightSide,
        GC.InputMan.DropDownButtonAlignment.LeftSide
    ];

    public items: Array<Data> = [{
        showDropDownButton: this.showDropDownButton,
        showSpinButton: this.showSpinButton,
        spinButtonPosition: this.spinButtonPosition,
        dropDownButtonPosition: this.dropDownButtonPosition
    }];

    public trackByIndex(index: number, value: Data) {
        if (this.isDataChange(value)) {
            this.updateData(value);
            return Date.now();
        }
        return index;
    }

    private isDataChange(data: Data): boolean {
        if (!data) {
            return false;
        }

        if (data.dropDownButtonPosition === this.dropDownButtonPosition
            && data.spinButtonPosition === this.spinButtonPosition
            && data.showSpinButton === this.showSpinButton
            && data.showDropDownButton === this.showDropDownButton) {
            return false;
        }
        return true;
    }

    private updateData(data: Data): void {
        if (!data) {
            return;
        }

        data.showSpinButton = this.showSpinButton;
        data.showDropDownButton = this.showDropDownButton;
        data.dropDownButtonPosition = this.dropDownButtonPosition;
        data.spinButtonPosition = this.spinButtonPosition;
    }


}

export interface Data {
    showDropDownButton: boolean;
    showSpinButton: boolean;
    spinButtonPosition: GC.InputMan.SpinButtonAlignment;
    dropDownButtonPosition: GC.InputMan.DropDownButtonAlignment;
}

enableProdMode();