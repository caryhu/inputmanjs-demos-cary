コンボコントロールには、ドロップダウンリストの項目を順番に指定できるスピン機能が搭載されています。以下では、このスピン機能を含む、コンボコントロールでの入力方法について説明します。

## 概要

コンボコントロールでは、ドロップダウンリストに設定された項目を選択する方法として、以下のような方法を提供しています。

1. ドロップダウンボタンをクリックし、リストアイテムを選択する
2. コントロールにフォーカスがある状態で、[↓]・[↑]・[Page Up]・[Page Down]のいずれかのボタンを押下する
3. コントロールにフォーカスがある状態で、マウスホイールを回転させる
4. スピンボタンをクリックする

<!-- -->

なお、コンボコントロールのisMultiSelectプロパティをtrueに設定し、複数のアイテムを同時に選択できる設定にしている場合、上記2～4の方法は無効です。

また、1のドロップダウンボタンと4のスピンボタンは、表示有無や表示位置を変更することが可能です。

さらに、3のマウスホイールの操作は、setSpinWheelメソッドで制御することができます。

## スピンボタン

スピンボタンの表示有無は、showSpinButtonプロパティで設定することが可能です。showSpinButtonプロパティをtrueに設定すると、スピンボタンが表示されます。（既定値はfalseです。）

また、spinButtonPositionプロパティを使用することで、スピンボタンの表示位置を設定することが可能です。spinButtonPositionプロパティに設定可能な値（SpinButtonAlignment列挙型）は、以下の通りです。

| 値                            | 説明                           |
| ---------------------------- | ---------------------------- |
| LeftSide                     | スピンボタンをコントロールの左側に配置します。      |
| RightSide                    | スピンボタンをコントロールの右側に配置します。（既定値） |

コンボコントロールの左側にスピンボタンを表示する場合、以下のようなコードになります。

```
var gcComboBox = new GC.InputMan.GcComboBox(document.getElementById('gcComboBox'), {
  showSpinButton: true,
  spinButtonPosition: GC.InputMan.SpinButtonAlignment.LeftSide
});
```

## ドロップダウンボタン

ドロップダウンボタンの表示有無は、showDropDownButtonプロパティで設定することが可能です。showDropDownButtonプロパティをtrueに設定すると、ドロップダウンボタンが表示されます。（既定値はtrueです。）

また、dropDownButtonPositionプロパティを使用することで、スピンボタンの表示位置を設定することが可能です。dropDownButtonPositionプロパティに設定可能な値（DropDownButtonAlignment列挙型）は、以下の通りです。

| 値                                | 説明                               |
| -------------------------------- | -------------------------------- |
| LeftSide                         | ドロップダウンボタンをコントロールの左側に配置します。      |
| RightSide                        | ドロップダウンボタンをコントロールの右側に配置します。（既定値） |

コンボコントロールの左側にドロップダウンボタンを表示する場合、以下のようなコードになります。

```
var gcComboBox = new GC.InputMan.GcComboBox(document.getElementById('gcComboBox'), {
  showDropDownButton: true,
  dropDownButtonPosition: GC.InputMan.DropDownButtonAlignment.LeftSide
});
```

## 備考

スピンボタンとドロップダウンボタンを同じ方向に配置した場合、スピンボタンはドロップダウンボタンの **外側** に表示されます。たとえば、spinButtonPositionとdropDownButtonPositionの両プロパティをどちらもRightSideに設定した場合、スピンボタンがドロップダウンボタンの右側に表示されます。

なお、スピンボタンを表示する方法としては、showSpinButtonプロパティを使用する以外に、addSpinButtonメソッドを使用する方法があります。addSpinButtonメソッドのパラメータに設定可能な値は、以下の通りです。

| 値                            | 説明                           |
| ---------------------------- | ---------------------------- |
| false                        | スピンボタンをコントロールの左側に配置します。（既定値） |
| true                         | スピンボタンをコントロールの右側に配置します。      |

たとえば、スピンボタンをコントロールの左側に配置したい場合、以下のようなコードになります。。

```
gcComboBox.addSpinButton(true);
```

ただし、addSpinButtonメソッドを使用してドロップダウンボタンと同じ方向に追加した場合、スピンボタンはドロップダウンボタンの **内側** に表示されます。また、showSpinButtonプロパティがtrueに設定された状態で、addSpinButtonメソッドを実行しても、表示位置は変わりません。

