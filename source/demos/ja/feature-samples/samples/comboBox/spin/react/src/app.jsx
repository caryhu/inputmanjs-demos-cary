import * as React from "react";
import * as ReactDom from "react-dom";
import { GcComboBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import products from './data';

var spinButtonPositions = [
    InputMan.SpinButtonAlignment.RightSide,
    InputMan.SpinButtonAlignment.LeftSide
];
var dropDownButtonPositions = [
    InputMan.DropDownButtonAlignment.RightSide,
    InputMan.DropDownButtonAlignment.LeftSide
];
class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            key : (new Date()).valueOf(),
            showSpinButton: true,
            showDropDownButton: true,
            spinWheel: true
        };
    }

    render(){
        return (
            <div>
                <GcComboBox
                  key={this.state.key}
                  items= {products}
                  showSpinButton= {this.state.showSpinButton}
                  spinButtonPosition= {this.state.spinButtonPosition}
                  showDropDownButton= {this.state.showDropDownButton}
                  dropDownButtonPosition= {this.state.dropDownButtonPosition}
                  spinWheel= {this.state.spinWheel}>
                </GcComboBox>
                <table class="sample">
                    <tr>
                        <th>スピンボタン</th>
                        <td>
                            <label><input type="checkbox" id="showSpinButton" checked={this.state.showSpinButton} onChange={(e)=>{
                                this.setState({showSpinButton: e.target.checked, key: (new Date()).valueOf()});
                             }}/>スピンボタンを表示する</label>
                        </td>
                    </tr>
                    <tr>
                        <th>スピンボタンの位置</th>
                        <td>
                            <select id="spinButtonPosition" onChange={(e)=>{this.setState({spinButtonPosition: spinButtonPositions[e.target.selectedIndex], key: (new Date()).valueOf()}); }}>
                                <option>右</option>
                                <option>左</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>ドロップダウンボタン</th>
                        <td>
                            <label><input type="checkbox" id="showDropDownButton" checked={this.state.showDropDownButton} onChange={(e)=>{
                                   this.setState({showDropDownButton: e.target.checked, key: (new Date()).valueOf()});
                                }}/>ドロップダウンボタンを表示する</label>
                        </td>
                    </tr>
                    <tr>
                        <th>ドロップダウンボタンの位置</th>
                        <td>
                            <select id="dropDownButtonPosition" onChange={(e)=>{this.setState({dropDownButtonPosition: dropDownButtonPositions[e.target.selectedIndex], key: (new Date()).valueOf()}); }}>
                                <option>右</option>
                                <option>左</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>マウスホイールでのスピン操作</th>
                        <td><label><input type="checkbox" checked={this.state.spinWheel} onChange={(e)=>{this.setState({spinWheel: e.target.checked})}}/>有効にする</label></td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));