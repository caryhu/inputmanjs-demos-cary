コンボコントロールでは、ドロップダウンリストに表示するデータを動的に読み込む（ロードオンデマンド）ことが可能です。

## 概要

loadプロパティを使用することで、ドロップダウンリストのリストアイテムがロードされるときに実行する処理を設定することができます。また、pageSizeプロパティを使用することで、ロードオンデマンドでデータを表示する場合に、一度に読み込むアイテムの数を設定することが可能です。

以下のサンプルコードでは、ロードオンデマンドで一度に読み込むアイテムの数を「20」に設定しているので、ドロップダウンリストを最後のアイテムまでスクロールさせると、20個ずつリストアイテムが動的に追加されます。

```
var gcComboBox = new GC.InputMan.GcComboBox(document.getElementById('gcComboBox'), {
  virtualMode: true,
  pageSize: 20,
  load: function(context){
    setTimeout(function() {
      var items = [];
      for (var i = 0; i < context.pageSize; i++) {
        items.push('項目' + (context.pageNumber * context.pageSize + i + 1));
      }
      context.success(items);
    }, 500)
  },
});
```

また、clearItemsメソッドを実行することで、読み込まれていたドロップダウンリストのアイテムをクリアし、最初のアイテムからリロードすることが可能です。
```
gcComboBox.clearItems();
```