import * as React from "react";
import * as ReactDom from "react-dom";
import { GcComboBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

const pageSize = 20;
const generateItems = (pageNumber) => {
    let items = [];
    for (let i = 0; i < pageSize; i++) {
        items.push('項目' + (pageNumber * pageSize + i));
    }
    return items;
}

class App extends React.Component{
    render(){
        return (
            <div>
                <GcComboBox
                    className={'displayStyle'}
                    ref="combo"
                    virtualMode= {true}
                    load= {(context) => {
                        setTimeout(() => {
                            var items = generateItems(context.pageNumber);
                            context.success(items);
                        }, 500)
                    }}/>&nbsp;
                    <button onClick={(e)=>{this.refs.combo.getNestedIMControl().clearItems();}}>最初からリロードする</button>
                </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));