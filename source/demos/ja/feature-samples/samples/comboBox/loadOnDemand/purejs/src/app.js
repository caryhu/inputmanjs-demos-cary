import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';

const gcComboBox = new InputMan.GcComboBox(document.getElementById('gcComboBox'), {
    virtualMode: true,
    load: (context) => {
        setTimeout(() => {
            var items = generateItems(context.pageNumber);
            context.success(items);
        }, 500)
    },
});

const pageSize = 20;
const generateItems = (pageNumber) => {
    let items = [];
    for (let i = 0; i < pageSize; i++) {
        items.push('項目' + (pageNumber * pageSize + i));
    }
    return items;
}

document.getElementById('clearBtn').addEventListener('click', (e) => {
    gcComboBox.clearItems();
});