import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, ViewChild } from '@angular/core';
import GC from "@grapecity/inputman";
import {GcComboBoxComponent} from "@grapecity/inputman.angular";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    @ViewChild(GcComboBoxComponent)
    combo: GcComboBoxComponent;

    public generateItems(pageNumber: number): Array<any> {
        const pageSize = 20;
        let items = [];
        for (let i = 0; i < pageSize; i++) {
            items.push('項目' + (pageNumber * pageSize + i));
        }
        return items;
    }

    public load(context: GC.InputMan.ILoadContext) {
        setTimeout(() => {
            var items = this.generateItems(context.pageNumber);
            context.success(items);
        }, 500);
    }

    public clearItems(){
        this.combo.getNestedIMControl().clearItems();
    }
}


enableProdMode();