import * as React from "react";
import * as ReactDom from "react-dom";
import { GcComboBox, GcTextBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import products from './data';

const ellipsisModes = [
    InputMan.EllipsisMode.None,
    InputMan.EllipsisMode.EllipsisEnd,
    InputMan.EllipsisMode.EllipsisPath
];

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            ellipsisString: '...',
            ellipsis: InputMan.EllipsisMode.None
        };
    }
    render(){
        return (
            <div>
                <GcComboBox     
                    selectedIndex={0}
                    items= {products.map((item) => {
                        return 'おいしい' + item;
                    })} 
                    ellipsis={this.state.ellipsis} 
                    ellipsisString={this.state.ellipsisString}></GcComboBox>
                <table class="sample">
                    <tr>
                        <th>省略文字の表示</th>
                        <td>
                        <select id="setEllipsis" onChange={(e)=>this.setState({ellipsis: ellipsisModes[e.target.selectedIndex]})}>
                            <option>表示しない</option>
                            <option>文字列の末尾に表示</option>
                            <option>文字列の中央部分に表示</option>
                        </select>
                        </td>
                    </tr>
                    <tr>
                        <th>省略文字に使用する文字</th>
                        <td><GcTextBox text={this.state.ellipsisString} onTextChanged={(e)=>{
                            this.setState({ellipsisString: e.getText()});
                            }}/></td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));