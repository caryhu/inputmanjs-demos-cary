import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";
import products from './data';

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public ellipsisModes = [
        GC.InputMan.EllipsisMode.None,
        GC.InputMan.EllipsisMode.EllipsisEnd,
        GC.InputMan.EllipsisMode.EllipsisPath
    ];
    public ellipsisString: string = "...";
    public ellipsis: GC.InputMan.EllipsisMode;

    public items: Array<string> = products.map((item) => {
        return 'おいしい' + item;
    });

}


enableProdMode();