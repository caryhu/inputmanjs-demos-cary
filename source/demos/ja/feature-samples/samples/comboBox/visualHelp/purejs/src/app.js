import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';
import products from './data';

const gcComboBox = new InputMan.GcComboBox(document.getElementById('gcComboBox'), {
    items: products.map((item) => {
        return 'おいしい' + item;
    })
});
gcComboBox.setSelectedIndex(0);

document.getElementById('setEllipsis').addEventListener('change', (e) => {
    gcComboBox.setEllipsis(ellipsisModes[e.target.selectedIndex]);
});

document.getElementById('setEllipsisString').addEventListener('change', (e) => {
    gcComboBox.setEllipsisString(e.target.value);
});

var ellipsisModes = [
    InputMan.EllipsisMode.None,
    InputMan.EllipsisMode.EllipsisEnd,
    InputMan.EllipsisMode.EllipsisPath
];