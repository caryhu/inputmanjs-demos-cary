import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public virtualMode: boolean = true;
    public items: Array<string>;

    constructor() {
        this.updateItems(1000);
    }

    public generateItems(length: number) {
        var items = [];
        for (var i = 0; i < length; i++) {
            items.push('項目' + i);
        }
        return items;
    }

    public updateItems(length: number): void {
        this.items = this.generateItems(length);
    }

    public dataArr: Array<any> = [this.virtualMode];

    public trackByIndex(index: number, value: boolean) {
        if (this.virtualMode !== value) {
            this.dataArr[index] = this.virtualMode;
            return Date.now();
        }
        return index;
    }
}


enableProdMode();