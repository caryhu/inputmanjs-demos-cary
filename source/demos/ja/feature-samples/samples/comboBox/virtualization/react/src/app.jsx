import * as React from "react";
import * as ReactDom from "react-dom";
import { GcComboBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            key : (new Date()).valueOf(),
            virtualMode: true,
            length: 1000
        };
    }

    generateItems = (length) => {
        var items = [];
        for (var i = 0; i < length; i++) {
            items.push('項目' + i);
        }
        return items;
    }

    render(){
        return (
            <div>
               <GcComboBox
                    key={this.state.key}
                    items= {this.generateItems(Number(this.state.length))}
                    virtualMode= {this.state.virtualMode}></GcComboBox>
                <table class="sample">
                    <tr>
                        <th>仮想化</th>
                        <td>
                            <label><input type="checkbox" id="virtualMode" checked={this.state.virtualMode} onChange={(e)=>{this.setState({virtualMode: e.target.checked, key : (new Date()).valueOf()})}}/>仮想化を有効にする</label>
                        </td>
                    </tr>
                    <tr>
                        <th>項目数</th>
                        <td>
                            <select id="length" onChange={(e)=> this.setState({length: e.target.value, key : (new Date()).valueOf()})}>
                                <option value='1000'>1000</option>
                                <option value='10000'>10000</option>
                                <option value='100000'>100000</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));