import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';

const update = () => {
    let container = document.getElementById('container');
    container.innerHTML = '';
    let gcComboBox = new InputMan.GcComboBox(container, {
        items: generateItems(Number(document.getElementById('length').value)),
        virtualMode: document.getElementById('virtualMode').checked
    });
}

const generateItems = (length) => {
    var items = [];
    for (var i = 0; i < length; i++) {
        items.push('項目' + i);
    }
    return items;
}
update();

document.getElementById('length').addEventListener('change', (e) => {
    update();
});

document.getElementById('virtualMode').addEventListener('change', (e) => {
    update();
});