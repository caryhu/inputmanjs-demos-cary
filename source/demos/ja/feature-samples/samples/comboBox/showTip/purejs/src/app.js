import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';
import './styles.css';

var gcComboBox = new InputMan.GcComboBox(document.getElementById('gcComboBox1'), {
    items: [
        { name: 'ActiveReports', category: '帳票・レポート', description: '日本仕様の帳票開発に必要な機能を搭載したコンポーネント' },
        { name: 'SPREAD', category: '表計算・グリッド', description: 'Excel風のビューと表計算機能を実現するUIコンポーネント' },
        { name: 'ComponentOne', category: 'コンポーネントセット', description: 'Visual Studioで利用する.NET Framework用コンポーネント' },
        { name: 'InputMan', category: '入力支援', description: '快適な入力を実現する日本仕様入力コンポーネントセット' },
        { name: 'MultiRow', category: '多段明細', description: '1レコード複数行&日付表示に最適なグリッドコンポーネント' },
        { name: 'DioDocs', category: 'ドキュメントAPI', description: 'ドキュメントを生成、更新するAPIライブラリ' },
    ],
    columns: [
        { name: 'name', label: '製品名', width: 50 },
        { name: 'category', label: '製品カテゴリー', width: 50 },
        { name: 'description', label: '製品紹介', width: 100 }
    ],
    showTip: true,
    dropDownWidth: 200,
    displayMemberPath: 'name',
    valueMemberPath: 'name'
});

var gcComboBox2 = new InputMan.GcComboBox(document.getElementById('gcComboBox2'), {
    items: [
        { name: 'ActiveReports', category: '帳票・レポート', description: '日本仕様の帳票開発に必要な機能を搭載したコンポーネント' },
        { name: 'SPREAD', category: '表計算・グリッド', description: 'Excel風のビューと表計算機能を実現するUIコンポーネント' },
        { name: 'ComponentOne', category: 'コンポーネントセット', description: 'Visual Studioで利用する.NET Framework用コンポーネント' },
        { name: 'InputMan', category: '入力支援', description: '快適な入力を実現する日本仕様入力コンポーネントセット' },
        { name: 'MultiRow', category: '多段明細', description: '1レコード複数行&日付表示に最適なグリッドコンポーネント' },
        { name: 'DioDocs', category: 'ドキュメントAPI', description: 'ドキュメントを生成、更新するAPIライブラリ' },
    ],
    columns: [
        { name: 'name', label: '製品名', width: 50 },
        { name: 'category', label: '製品カテゴリー', width: 50 },
        { name: 'description', label: '製品紹介', width: 100, showTip: true }
    ],
    dropDownWidth: 200,
    displayMemberPath: 'name',
    valueMemberPath: 'name'
});