コンボコントロールでは、表示したアイテムが見切れてしまうときにチップ表示させることが可能です。

## 概要
コンボコントロール全体でチップを表示させるには、コンストラクタのshowTipオプションにtrueを設定します。また、列のshowTipオプションにfalseを設定することで、その列のみチップを非表示にできます。

```
var gcComboBox = new InputMan.GcComboBox(document.getElementById('gcComboBox1'), {
    columns: [
        { name: 'name',label:'製品名',width: 50},
        { name: 'category',label:'製品カテゴリー',width: 50},
        { name: 'description',label:'製品紹介',width: 150, showTip: false}
    ],
    showTip: true,
});
```

任意の列のみにチップを表示させるには、列のshowTipオプションをtrueに設定します。

```
var gcComboBox2 = new InputMan.GcComboBox(document.getElementById('gcComboBox2'), {
    columns: [
        { name: 'name',label:'製品名',width: 50},
        { name: 'category',label:'製品カテゴリー',width: 50},
        { name: 'description',label:'製品紹介',width: 100, showTip: true}
    ]
});
```