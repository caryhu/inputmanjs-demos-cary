import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';
import products from './data';

const gcComboBox = new InputMan.GcComboBox(document.getElementById('gcComboBox'), {
    items: products
});

// イベントハンドラ
gcComboBox.addEventListener(
    InputMan.GcComboBoxEvent.CheckedChanged,
    () => log('CheckedChanged')
);
gcComboBox.addEventListener(
    InputMan.GcComboBoxEvent.DropDownClosed,
    () => log('DropDownClosed')
);
gcComboBox.addEventListener(
    InputMan.GcComboBoxEvent.DropDownOpened, 
    () => log('DropDownOpened')
);
gcComboBox.addEventListener(
    InputMan.GcComboBoxEvent.ItemsChanged,
    () => log('ItemsChanged')
);
gcComboBox.addEventListener(
    InputMan.GcComboBoxEvent.SelectedChanged, 
    () => log('SelectedChanged')
);
gcComboBox.addEventListener(
    InputMan.GcComboBoxEvent.SpinDown, 
    () => log('SpinDown')
);
gcComboBox.addEventListener(
    InputMan.GcComboBoxEvent.SpinUp,
    () => log('SpinUp')
);
gcComboBox.addEventListener(
    InputMan.GcComboBoxEvent.TextChanged,
    () => log('TextChanged')
);

gcComboBox.getDropDownWindow().onClosing((sender, eArgs) => {
    log('onClosing');
});
gcComboBox.getDropDownWindow().onOpening((sender, eArgs) => {
    log('onOpening');
});

// テキストボックスにログを出力
const log = (message) =>  {
    var textarea = document.getElementById('log');
    textarea.value += message + '\n';
    textarea.scrollTop = textarea.scrollHeight;
}