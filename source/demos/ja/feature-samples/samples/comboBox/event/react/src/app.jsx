import * as React from "react";
import * as ReactDom from "react-dom";
import { GcComboBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import products from './data';

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            message: ''
        };
    }

    log(msg){
        var newMessage = this.state.message + msg + '\n';
        this.refs.log.scrollTop = this.refs.log.scrollHeight;
        this.setState({message: newMessage});
    }

    componentDidMount(){
        var dropdown = this.refs.combo.getNestedIMControl().getDropDownWindow();
        dropdown.onClosing((sender, eArgs) => {
            this.log('onClosing');
        });
        dropdown.onOpening((sender, eArgs) => {
            this.log('onOpening');
        });
    }

    render(){
        return (
            <div>
                <GcComboBox 
                ref= "combo"
                items={products}
                checkedChanged={(eArgs) => {
                    this.log('checkedChanged');
                }}
                selectedChanged={(eArgs) => {
                    this.log('selectedChanged');
                }}
                dropDownClosed={(eArgs) => {
                    this.log('dropDownClosed');
                }}
                dropDownOpened={(eArgs) => {
                    this.log('dropDownOpened');
                }}
                textChanged={(eArgs) => {
                    this.log('textChanged');
                }}
                spinDown={(eArgs) => {
                    this.log('spinDown');
                }}
                spinUp={(eArgs) => {
                    this.log('spinUp');
                }}
                itemsChanged={(eArgs) => {
                    this.log('itemsChanged');
                }}
                ></GcComboBox><br/>
                イベント<br/>
                <textarea ref="log" id="log" rows="10" cols="30" value={this.state.message}></textarea>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));