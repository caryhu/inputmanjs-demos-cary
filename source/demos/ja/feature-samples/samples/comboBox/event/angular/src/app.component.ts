import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, ViewChild, ElementRef } from '@angular/core';
import GC from "@grapecity/inputman";
import data from './data';
import {GcComboBoxComponent} from '@grapecity/inputman.angular';

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public data = data;

    public logStr: string = "";
    @ViewChild('logPanel')
    public logPanel: ElementRef;

    @ViewChild(GcComboBoxComponent)
    combo: GcComboBoxComponent

    public ngAfterViewInit(){
        var dropdown = this.combo.getNestedIMControl().getDropDownWindow();
        dropdown.onClosing(() => this.log('onClosing'));
        dropdown.onOpening(() => this.log('onOpening'));
    }

    public log(str: string) {
        this.logStr = this.logStr + str + '\n';
        this.logPanel.nativeElement.scrollTop = this.logPanel.nativeElement.scrollHeight;
    }
}


enableProdMode();