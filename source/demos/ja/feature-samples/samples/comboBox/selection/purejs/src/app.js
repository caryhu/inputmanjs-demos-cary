import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';
import products from './data';

const render = (isMultiSelect) => {
    let container = document.getElementById('container');
    container.innerHTML = '';
    let gcComboBox = new InputMan.GcComboBox(container, {
        items: products,
        isMultiSelect
    });
}
render(true);

document.getElementById('isMultiSelect')
    .addEventListener('change', (e) => render(e.target.checked));