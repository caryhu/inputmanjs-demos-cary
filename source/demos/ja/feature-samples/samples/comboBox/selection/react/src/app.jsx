import * as React from "react";
import * as ReactDom from "react-dom";
import { GcComboBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import products from './data';

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            key: (new Date()).valueOf(),
            isMultiSelect: true
        };
    }

    render(){
        return (
            <div>
                <GcComboBox 
                    key= {this.state.key}
                    items= {products}
                    isMultiSelect={this.state.isMultiSelect}>
                </GcComboBox>
                <table class="sample">
                    <tr>
                        <th>複数選択</th>
                        <td>
                            <label><input type="checkbox" id="isMultiSelect" checked={this.state.isMultiSelect} onChange={(e)=> {
                                    this.setState({isMultiSelect: e.target.checked, key: (new Date()).valueOf()});
                                }}/>複数選択を許可</label>
                        </td>
                    </tr>
                </table>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));