import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public products = [
        '果汁100%オレンジ',
        '果汁100%グレープ',
        '果汁100%レモン',
        '果汁100%ピーチ',
        'コーヒーマイルド',
        'コーヒービター'
    ];
    public isMultiSelect: boolean = true;

    public items: Array<any> = [this.isMultiSelect];

    public trackByIndex(index: number, value: boolean) {
        if (this.isMultiSelect !== value) {
            this.items[index] = this.isMultiSelect;
            return Date.now();
        }
        return index;
    }
}


enableProdMode();