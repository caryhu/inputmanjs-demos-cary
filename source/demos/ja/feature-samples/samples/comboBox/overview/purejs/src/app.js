import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';
import { products, gcProducts, orders } from './data';

const gcComboBox1 = new InputMan.GcComboBox(document.getElementById('gcComboBox1'), {
    items: products
});

const gcComboBox2 = new InputMan.GcComboBox(document.getElementById('gcComboBox2'), {
    items: orders,
    columns: [
        { name: 'id', label: '商品コード', width: 80, clickSort: true },
        { name: 'product', label: '商品名', width: 200 },
        { name: 'date', label: '受注日', width: 120, clickSort: true },
        { name: 'price', label: '単価', width: 80, clickSort: true },
        { name: 'amount', label: '数量', width: 80, clickSort: true },
    ],
    displayMemberPath: 'product',
    valueMemberPath: 'product',
    dropDownWidth: 'auto'
});

const gcComboBox3 = new InputMan.GcComboBox(document.getElementById('gcComboBox3'), {
    items: gcProducts,
    itemHeight: 50,
    displayMemberPath: 'name',
    valueMemberPath: 'name',
    itemTemplate: `<div class="template-item">
         <div class="image"><img src="{!logo}"></div>
         <div class="names"><div class="name">{!name}</div><div class="category">{!category}</div></div>
         <div class="description">{!description}</div>
         </div>`,
    dropDownWidth: 'auto',
    generatingItem: (args) => {
        args.item.classList.add(args.index % 2 ? 'odd' : 'even');
    }
});