import * as React from "react";
import * as ReactDom from "react-dom";
import { GcComboBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import {products, orders, gcProducts} from './data';

class App extends React.Component{

    render(){
        return (
            <div class="flexbox">
                <div>
                    単純なリスト
                   <GcComboBox items= {products}></GcComboBox>
                </div>
                <div>
                    複数列
                    <GcComboBox     
                    items= {orders}
                    columns= {[
                        { name: 'id', label: '商品コード', width: 80, clickSort: true },
                        { name: 'product', label: '商品名', width: 200 },
                        { name: 'date', label: '受注日', width: 120, clickSort: true },
                        { name: 'price', label: '単価', width: 80, clickSort: true },
                        { name: 'amount', label: '数量', width: 80, clickSort: true },
                    ]}
                    displayMemberPath= {'product'}
                    valueMemberPath= {'product'}
                    dropDownWidth= {'auto'}></GcComboBox>
                </div>
                <div>
                    テンプレート
                    <GcComboBox
                        items= {gcProducts}
                        itemHeight= {50}
                        displayMemberPath= {'name'}
                        valueMemberPath= {'name'}
                        itemTemplate= {`<div class="template-item">
                             <div class="image"><img src="{!logo}"></div>
                             <div class="names"><div class="name">{!name}</div><div class="category">{!category}</div></div>
                             <div class="description">{!description}</div>
                             </div>`}
                        dropDownWidth= {'auto'}
                        generatingItem= {(args) => {
                            args.item.classList.add(args.index % 2 ? 'odd' : 'even');
                        }}></GcComboBox>
                </div>
            </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));