import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';

// 文字配列に連結
const gcComboBoxStrings = new InputMan.GcComboBox(document.getElementById('gcComboBoxStrings'), {
    items: [
        '果汁100%オレンジ',
        '果汁100%グレープ',
        '果汁100%レモン',
        '果汁100%ピーチ',
        'コーヒーマイルド',
        'コーヒービター'
    ]
});

// オブジェクト配列に連結
const gcComboBoxObjects = new InputMan.GcComboBox(document.getElementById('gcComboBoxObjects'), {
    items: [
        { name: '果汁100%オレンジ' },
        { name: '果汁100%グレープ' },
        { name: '果汁100%レモン' },
        { name: '果汁100%ピーチ' },
        { name: 'コーヒーマイルド' },
        { name: 'コーヒービター' }
    ],
    displayMemberPath: 'name',
    valueMemberPath: 'name'
});

// option要素から生成
const gcComboBoxDom = new InputMan.GcComboBox(document.getElementById('gcComboBoxDom'), {});