import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, ViewChild, ElementRef, OnInit } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent implements OnInit {
    ngOnInit(): void {
        new GC.InputMan.GcComboBox(this.selectElement.nativeElement, {});
    }

    public comboStringSource = ['果汁100%オレンジ',
        '果汁100%グレープ',
        '果汁100%レモン',
        '果汁100%ピーチ',
        'コーヒーマイルド',
        'コーヒービター'];

    public comboObjSource = [{ name: '果汁100%オレンジ' },
    { name: '果汁100%グレープ' },
    { name: '果汁100%レモン' },
    { name: '果汁100%ピーチ' },
    { name: 'コーヒーマイルド' },
    { name: 'コーヒービター' }];

    @ViewChild('selectElement')
    selectElement: ElementRef;


}


enableProdMode();