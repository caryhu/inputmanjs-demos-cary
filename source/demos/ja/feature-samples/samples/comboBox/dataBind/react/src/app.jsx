import * as React from "react";
import * as ReactDom from "react-dom";
import { GcComboBox } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{
    componentDidMount(){
        const gcComboBoxDom = new InputMan.GcComboBox(document.getElementById('gcComboBoxDom'), {});
    }

    render(){
        return (
            <div class="flexbox">
                <div>
                    文字配列に連結
                    <GcComboBox
                        items= {[
                            '果汁100%オレンジ',
                            '果汁100%グレープ',
                            '果汁100%レモン',
                            '果汁100%ピーチ',
                            'コーヒーマイルド',
                            'コーヒービター'
                        ]}></GcComboBox>
                </div>
                <div>
                    オブジェクト配列に連結
                    <GcComboBox
                        items= {[
                            { name: '果汁100%オレンジ' },
                            { name: '果汁100%グレープ' },
                            { name: '果汁100%レモン' },
                            { name: '果汁100%ピーチ' },
                            { name: 'コーヒーマイルド' },
                            { name: 'コーヒービター' }
                        ]}
                        displayMemberPath= {'name'}
                        valueMemberPath= {'name'}></GcComboBox>
                </div>
                <div>
                    option要素から生成<br/>
                    <select id="gcComboBoxDom">
                        <option>果汁100%オレンジ</option>
                        <option>果汁100%グレープ</option>
                        <option>果汁100%レモン</option>
                        <option>果汁100%ピーチ</option>
                        <option>コーヒーマイルド</option>
                        <option>コーヒービター</option>
                    </select>
                </div>
        </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));