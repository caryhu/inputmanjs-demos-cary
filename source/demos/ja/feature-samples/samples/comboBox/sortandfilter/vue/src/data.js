module.exports = [
    { id: 18, product: 'ピリカラタバスコ', date: '2020/01/07', price: 200, amount: 20 },
    { id: 17, product: 'だしこんぶ', date: '2020/01/08', price: 80, amount: 50 },
    { id: 15, product: 'ピュアデミグラスソース', date: '2020/01/10', price: 300, amount: 30 },
    { id: 17, product: 'だしこんぶ', date: '2020/01/10', price: 100, amount: 100 },
    { id: 17, product: 'だしこんぶ', date: '2020/01/12', price: 70, amount: 100 },
    { id: 15, product: 'ピュアデミグラスソース', date: '2020/01/12', price: 450, amount: 30 },
    { id: 18, product: 'ピリカラタバスコ', date: '2020/01/12', price: 180, amount: 20 },
    { id: 85, product: 'なまからし', date: '2020/01/13', price: 150, amount: 40 },
    { id: 84, product: 'なまわさび', date: '2020/01/21', price: 150, amount: 40 },
    { id: 86, product: 'なましょうが', date: '2020/01/20', price: 150, amount: 40 },
];