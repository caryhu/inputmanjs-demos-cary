import * as React from "react";
import * as ReactDom from "react-dom";
import { GcComboBox, GcNumber } from "@grapecity/inputman.react";
import { InputMan } from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import data from './data';

class App extends React.Component{
    constructor(){
        super();
        this.items = data;
        this.columns = [
            { name: 'id', label: '商品コード', width: 80 },
            { name: 'product', label: '商品名', width: 200 },
            { name: 'date', label: '受注日', width: 120 },
            { name: 'price', label: '単価', width: 80 },
            { name: 'amount', label: '数量', width: 80 },
        ]
        this.filterValue = 0;
        this.combo1 = React.createRef();
        this.combo2 = React.createRef();
    }

    //ソート条件によるソート(昇順)
    updateSort(e){
        let sortIdInfo = { name: 'id', isAsc: true };
        let gcCombo = this.combo1.current.getNestedIMControl();
        e.target.checked ? gcCombo.sort(sortIdInfo) : gcCombo.sort(null);
    }

    //フィルタ条件によるフィルタ
    updateFilter(e){
        let filterIdInfo = [{ name: 'product', comparator: GC.InputMan.FilterComparator.Contains, filterString: 'なま' }];
        let gcCombo = this.combo1.current.getNestedIMControl();
        if (e.target.checked === true) {
            gcCombo.filter(filterIdInfo);
        } else {
            gcCombo.filter(null);
        }
    }

    ////ソート関数によるソート
    doCustomSort(e){
        //昇順関数
        const ascFunc = (first, second) => {
            if (first.item.id == second.item.id) {
                return first.item.price - second.item.price;
            }
            return first.item.id - second.item.id;
        }

        let gcCombo = this.combo2.current.getNestedIMControl();
        if (e.target.checked === true) {
            gcCombo.sort(ascFunc);
        } else {
            gcCombo.sort(null);
        }
    }

    //フィルタ関数によるフィルタ
    doFilter(){
        this.combo2.current.getNestedIMControl().filter((v) => {
            //売上がフィルタ値より大きければ表示する
            return v.price * v.amount > this.filterValue;
        });
    }

    render(){
        return <React.Fragment>
            <h3>ソート条件、フィルタ条件を利用したサンプル</h3>
            <GcListBox ref={this.combo1} items={this.items} columns={this.columns} />
            <table class="sample">
                <tr>
                    <th>ソート条件によるソート</th>
                    <td>
                        <label><input type="checkbox" onChange={this.updateSort.bind(this)} />商品コードを昇順にする</label>
                    </td>
                </tr>
                <tr>
                    <th>フィルタ条件によるフィルタ</th>
                    <td>
                        <label><input type="checkbox" onChange={this.updateFilter.bind(this)} />「なま」を含む商品名でフィルタする</label>
                    </td>
                </tr>
            </table>
            <br />
            <h3>ソート関数、フィルタ関数を利用したサンプル</h3>
            <GcListBox ref={this.combo2} items={this.items} columns={this.columns} />
            <table class="sample">
                <tr>
                    <th>ソート関数によるソート</th>
                    <td>
                        <label><input type="checkbox" onChange={this.doCustomSort.bind(this)} />商品コードと単価を昇順にする</label>
                    </td>
                </tr>

                <tr>
                    <th>フィルタ関数によるフィルタ<br />※入力した売上以上の項目を表示する</th>
                    <td>
                        <GcNumber value={this.filterValue} minValue={0} onValueChanged={(s) => this.filterValue = s.value} className='inline-div'/>
                        <button onClick={this.doFilter.bind(this)}>実行</button>
                    </td>
                </tr>
            </table>
        </React.Fragment>
    }
}

ReactDom.render(<App />, document.getElementById("app"));