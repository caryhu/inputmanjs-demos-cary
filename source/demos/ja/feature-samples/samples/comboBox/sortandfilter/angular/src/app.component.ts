import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { Component, enableProdMode, ViewChild } from '@angular/core';
import data from './data';
import {GcComboBoxComponent} from "@grapecity/inputman.angular";
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    @ViewChild('combo1') combo1: GcComboBoxComponent;
    @ViewChild('combo2') combo2: GcComboBoxComponent;

    items = data;
    columns = [
        { name: 'id', label: '商品コード', width: 80 },
        { name: 'product', label: '商品名', width: 200 },
        { name: 'date', label: '受注日', width: 120 },
        { name: 'price', label: '単価', width: 80 },
        { name: 'amount', label: '数量', width: 80 },
    ];
    filterValue = 0;

    //ソート条件によるソート(昇順)
    updateSort(e: any){
        let sortIdInfo = { name: 'id', isAsc: true };
        let gcCombo = this.combo1.getNestedIMControl();
        e.target.checked ? gcCombo.sort(sortIdInfo) : gcCombo.sort(null);
    }

    //フィルタ条件によるフィルタ
    updateFilter(e: any){
        let filterIdInfo = [{ name: 'product', comparator: GC.InputMan.FilterComparator.Contains, filterString: 'なま' }];
        let gcCombo = this.combo1.getNestedIMControl();
        if (e.target.checked === true) {
            gcCombo.filter(filterIdInfo);
        } else {
            gcCombo.filter(null);
        }
    }

    ////ソート関数によるソート
    doCustomSort(e: any){
        //昇順関数
        const ascFunc = (first: any, second: any) => {
            if (first.item.id == second.item.id) {
                return first.item.price - second.item.price;
            }
            return first.item.id - second.item.id;
        }

        let gcCombo = this.combo2.getNestedIMControl();
        if (e.target.checked === true) {
            gcCombo.sort(ascFunc);
        } else {
            gcCombo.sort(null);
        }
    }

    //フィルタ関数によるフィルタ
    doFilter(){
        this.combo2.getNestedIMControl().filter((v: any) => {
            //売上がフィルタ値より大きければ表示する
            return v.price * v.amount > this.filterValue;
        });
    }
}


enableProdMode();