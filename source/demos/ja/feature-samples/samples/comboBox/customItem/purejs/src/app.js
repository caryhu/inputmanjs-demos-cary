import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';

const employees = [
    { lastName: '森上', firstName: '偉久馬', office: '東京本社' },
    { lastName: '葛城', firstName: '孝史', office: '東京本社' },
    { lastName: '加藤', firstName: '泰江', office: '東京本社' },
    { lastName: '川村', firstName: '匡', office: '大阪支社' },
    { lastName: '松沢', firstName: '誠一', office: '大阪支社' },
    { lastName: '成宮', firstName: '真紀', office: '大阪支社' },
];

const gcComboBox = new InputMan.GcComboBox(document.getElementById('gcComboBox'), {
    items: employees,
    displayMemberPath: 'fullName',
    formatItem: (args) => {
        var item = args.itemObject;
        item.fullName = item.lastName + ' ' + item.firstName + '（' + item.office + '）';
        employees[args.index].fullName = item.fullName;
    },
    generatingItem: (args) => {
        args.item.classList.add(args.index % 2 ? 'odd' : 'even');
    }
});