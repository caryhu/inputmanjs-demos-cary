import './styles.css';
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public employees: Array<Employee> = [
        { lastName: '森上', firstName: '偉久馬', office: '東京本社' },
        { lastName: '葛城', firstName: '孝史', office: '東京本社' },
        { lastName: '加藤', firstName: '泰江', office: '東京本社' },
        { lastName: '川村', firstName: '匡', office: '大阪支社' },
        { lastName: '松沢', firstName: '誠一', office: '大阪支社' },
        { lastName: '成宮', firstName: '真紀', office: '大阪支社' },
    ];

    public formatItem(args: GC.InputMan.IItemArgs) {
        var item = args.itemObject as Employee;
        item.fullName = item.lastName + ' ' + item.firstName + '（' + item.office + '）';
        this.employees[args.index].fullName = item.fullName;
    }

    public generatingItem(args: GC.InputMan.IItemGeneratingArgs) {
        args.item.classList.add(args.index % 2 ? 'odd' : 'even');
    }
}

export interface Employee {
    lastName: string;
    firstName: string;
    office: string;
    fullName?: string;
}


enableProdMode();