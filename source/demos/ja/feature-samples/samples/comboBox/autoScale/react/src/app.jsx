import * as React from "react";
import * as ReactDom from "react-dom";
import { GcComboBox } from "@grapecity/inputman.react";
import '@grapecity/inputman/CSS/gc.inputman-js.css';

class App extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            autoScale: true,
            minScaleFactor: 0.5
        };
    }

    render(){
        return (
            <div>
            <GcComboBox
                ref="combo"
                items={[
                    'おいしい果汁100%オレンジ',
                    'おいしい果汁100%グレープ',
                    'おいしい果汁100%レモン',
                    'おいしい果汁100%ピーチ',
                    'おいしいコーヒーマイルド',
                    'おいしいコーヒービター'
                ]}
                autoScale={this.state.autoScale}
                minScaleFactor={this.state.minScaleFactor}
                isEditable='true' /><br></br>
            <button onClick={(e)=>{this.refs.combo.getNestedIMControl().setText('')}}>テキストクリア</button>
            <table class="sample">
                <tr>
                    <th>長体表示</th>
                    <td>
                        <label><input type="checkbox" checked={this.state.autoScale}  onChange={(e)=> {
                                    this.setState({autoScale: e.target.checked})}}></input>有効にする</label>
                    </td>
                </tr>
                <tr>
                    <th>最小倍率</th>
                    <td>
                        <input type="number" max="1.0" min="0.1" step="0.1" value={this.state.minScaleFactor}  onChange={(e)=> {
                                    this.setState({minScaleFactor: e.target.value})}}></input>
                    </td>
                </tr>
            </table>
        </div>
        )
    }
}

ReactDom.render(<App />, document.getElementById("app"));