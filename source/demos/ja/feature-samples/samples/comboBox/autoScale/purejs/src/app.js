﻿import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { InputMan } from '@grapecity/inputman';

const gcComboBox = new InputMan.GcComboBox(document.getElementById('gcComboBox'), {
    autoScale:true,
    minScaleFactor:0.5,
    items: [
        'おいしい果汁100%オレンジ',
        'おいしい果汁100%グレープ',
        'おいしい果汁100%レモン',
        'おいしい果汁100%ピーチ',
        'おいしいコーヒーマイルド',
        'おいしいコーヒービター'
    ],
    isEditable: true,
});

document.getElementById('clearText').addEventListener('click', () => {
    gcComboBox.setText('');
});

document.getElementById('setAutoScale').addEventListener('click', (e) => {
    gcComboBox.setAutoScale(e.target.checked);
});

document.getElementById('setMinScaleFactor').addEventListener('change', (e) => {
    gcComboBox.setMinScaleFactor(Number(e.target.value));
});