コンボコントロールでは、入力したテキストが長すぎて見切れてしまうときに、テキストがコントロールに収まるように長体で表示することができます。

## 長体表示

コンストラクタのautoScaleオプションにtrueを設定するか、setAutoScaleメソッドを使用すると、テキスト表示が見切れるときに、長体表示にすることができます。
```
new GC.InputMan.GcComboBox(document.getElementById('gcComboBox'), {
    autoScale : true
});

gcComboBox.setAutoScale(true);
```

また、setMinScaleFactorメソッドを使用して、長体表示されるときの最小倍率を指定することができます。
```
gcComboBox.setMinScaleFactor(0.5);
```

## 長体表示における注意点
コントロールを生成後にコントロールの幅を変更した場合は、明示的にautoScaleTextメソッドを実行する必要があります。