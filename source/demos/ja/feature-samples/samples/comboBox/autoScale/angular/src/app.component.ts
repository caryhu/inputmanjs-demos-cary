import '@grapecity/inputman/CSS/gc.inputman-js.css';
import { Component, enableProdMode, ViewChild } from '@angular/core';
import { GcComboBoxComponent } from "@grapecity/inputman.angular";


@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    public items: string[] = [
        'おいしい果汁100%オレンジ',
        'おいしい果汁100%グレープ',
        'おいしい果汁100%レモン',
        'おいしい果汁100%ピーチ',
        'おいしいコーヒーマイルド',
        'おいしいコーヒービター'
    ];
    public autoScale: boolean = true;
    public minScaleFactor: number = 0.5;

    @ViewChild(GcComboBoxComponent) combo: GcComboBoxComponent;

    public clearText(){
        this.combo.getNestedIMControl().setText('');
    }
}

enableProdMode();