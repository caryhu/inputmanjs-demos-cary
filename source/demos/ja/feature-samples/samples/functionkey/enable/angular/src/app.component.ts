import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { Component, enableProdMode, ViewChild } from '@angular/core';
import GC from "@grapecity/inputman";
import { GcFunctionKeyComponent } from '@grapecity/inputman.angular';

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    InputMan = GC.InputMan;
    enabled = true;
    selected = GC.InputMan.FunctionKey.F1;
    @ViewChild(GcFunctionKeyComponent) functionkey: GcFunctionKeyComponent;


    onActived(e: any) {
        window.alert(e.description + 'が押下されました。');
    }

    updateKeyEnabled(e: any){
        this.functionkey.getNestedIMControl().setEnabled(e.target.checked, Number(this.selected));
    }

}

enableProdMode();