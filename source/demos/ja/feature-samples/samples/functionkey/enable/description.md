ファンクションキーコントロールは、コントロールと任意のキーの有効化を切り替えることができます。
一時的に設定したファンクションキーを無効にしておきたい場面などに有効です。

## コントロールの有効化を切り替える方法
コントロール全体を無効化するには、enabledプロパティにfalseを指定します。
また再度有効化するには、enabledプロパティにtrueを指定します。

```javascript
let gcFunctionKey = new GC.InputMan.GcFunctionKey(document.getElementById('functionkey'));
gcFunctionKey.addFunctionKey(
    {
        key: GC.InputMan.FunctionKey.F1,
        description: 'F1キーです'
    },
    {
        key: GC.InputMan.FunctionKey.Home,
        description: 'Homeキーです'
    }
);
//コントロールを無効化する
gcFunctionKey.enabled = false;

//コントロールを有効化する
gcFunctionKey.enabled = true;
```

## 任意のキーの有効化を切り替える方法
任意のキーを無効化する場合、setEnabledメソッドの第1引数にfalse、第2引数に無効化したいキーを指定します。
また再度有効化するには、setEnabledメソッドの第1引数にtrue、第2引数に有効化したいキーを指定します。

```javascript
let gcFunctionKey = new GC.InputMan.GcFunctionKey(document.getElementById('functionkey'));
gcFunctionKey.addFunctionKey(
    {
        key: GC.InputMan.FunctionKey.F1,
        description: 'F1キーです'
    },
    {
        key: GC.InputMan.FunctionKey.Home,
        description: 'Homeキーです'
    }
);
//F1キーを無効化する
gcFunctionKey.setEnabled(false,GC.InputMan.FunctionKey.F1);

//F1キーを有効化する
gcFunctionKey.setEnabled(true,GC.InputMan.FunctionKey.F1);
```