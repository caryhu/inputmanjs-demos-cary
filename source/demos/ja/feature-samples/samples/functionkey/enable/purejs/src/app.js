import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';

// ファンクションキー
let gcFunctionKey = new InputMan.GcFunctionKey(document.getElementById('functionkey'), {
    functionKeys: [
        {
            key: GC.InputMan.FunctionKey.F1,
            description: 'F1キー'
        },
        {
            key: GC.InputMan.FunctionKey.F3,
            description: 'F3キー'
        },
        {
            key: GC.InputMan.FunctionKey.Home,
            description: 'Home'
        },
        {
            key: GC.InputMan.FunctionKey.End,
            description: 'End'
        }
    ],
    onActived: function (s, e) {
        window.alert(e.description + 'が押下されました。');
    }
});

document.getElementById('controlEnable').addEventListener('click', (e) => {
    gcFunctionKey.enabled = e.target.checked;
});

document.getElementById('keyEnable').addEventListener('click', (e) => {
    let selectKey = keyArrays[document.getElementById('targetKey').selectedIndex];
    gcFunctionKey.setEnabled(e.target.checked,selectKey);
});

const keyArrays = [
    GC.InputMan.FunctionKey.F1,
    GC.InputMan.FunctionKey.F3,
    GC.InputMan.FunctionKey.Home,
    GC.InputMan.FunctionKey.End,
]