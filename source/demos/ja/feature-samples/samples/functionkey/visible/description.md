ファンクションキーコントロールは、コントロールと任意のキーの可視化を切り替えることができます。
コントロールを非表示にしながらコントロールの機能を利用したい場面などに有効です。

## コントロールの可視化を切り替える方法
コントロール全体を非表示には、visibleプロパティをfalseに指定します。
この時、コントロールは非表示になりますがコントロールの機能が無効になるわけではありません。

また非表示になったコントロールを再度表示するには、visibleプロパティをtrueに指定します。

```javascript
let gcFunctionKey = new GC.InputMan.GcFunctionKey(document.getElementById('functionkey'));
gcFunctionKey.addFunctionKey(
    {
        key: GC.InputMan.FunctionKey.F1,
        description: 'F1キーです'
    },
    {
        key: GC.InputMan.FunctionKey.Home,
        description: 'Homeキーです'
    }
);
//コントロールを非表示にする
gcFunctionKey.visible = false;

//コントロールを表示する
gcFunctionKey.visible = true;
```

## 任意のキーの可視化を切り替える方法
任意のキーを非表示にする場合、setVisibleメソッドの第1引数にfalse、第2引数に非表示にしたいキーを指定します。
また非表示になったキーを再表示するには、setVisibleメソッドの第1引数にtrue、第2引数に表示したいキーを指定します。

```javascript
let gcFunctionKey = new GC.InputMan.GcFunctionKey(document.getElementById('functionkey'));
gcFunctionKey.addFunctionKey(
    {
        key: GC.InputMan.FunctionKey.F1,
        description: 'F1キーです'
    },
    {
        key: GC.InputMan.FunctionKey.Home,
        description: 'Homeキーです'
    }
);
//F1キーを非表示にする
gcFunctionKey.setVisible(false,GC.InputMan.FunctionKey.F1);

//F1キーを表示する
gcFunctionKey.setVisible(true,GC.InputMan.FunctionKey.F1);
```