import * as React from "react";
import * as ReactDom from "react-dom";
import { GcFunctionKey, GcFunctionKeyInfo } from "@grapecity/inputman.react";
import {InputMan} from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{
    constructor(){
        super();
        this.state = {
            visible: true,
            selected: InputMan.FunctionKey.F1,
            checked: true,
        };
        this.functionKey = React.createRef();
    }

    updateKeyEnabled(e){
        this.setState({checked: e.target.checked});
        this.functionKey.current.getNestedIMControl().setVisible(e.target.checked, this.state.selected);
    }

    render(){
        return <React.Fragment>
            <GcFunctionKey ref={this.functionKey} visible={this.state.visible} onActived={(s, e) => window.alert(e.description + 'が押下されました。')}>
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F1} description='F1キー' />
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F3} description='F3キー' />
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.Home} description='Home' />
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.End} description='End' />
            </GcFunctionKey>
            <table class="sample">
                <tr>
                    <th>コントロール全体</th>
                    <td>
                        <label><input type="checkbox" checked={this.state.visible} onChange={e=> this.setState({visible: e.target.checked})} />可視化する</label>
                    </td>
                </tr>
                <tr>
                    <th>キーの指定</th>
                    <td>
                        <select value={this.state.selected} onChange={e=> this.setState({selected: Number(e.target.value)})}>
                            <option value={InputMan.FunctionKey.F1}>F1</option>
                            <option value={InputMan.FunctionKey.F3}>F3</option>
                            <option value={InputMan.FunctionKey.Home}>Home</option>
                            <option value={InputMan.FunctionKey.End}>End</option>
                        </select>
                        <label><input type="checkbox" id="keyEnable" checked={this.state.checked} onChange={this.updateKeyEnabled.bind(this)} />可視化する</label>
                    </td>
                </tr>
            </table>
            <br/>
            コントロールもしくはキーが非表示の場合でも、キーに割り当てられた機能は有効なままです。
        </React.Fragment>
    }
}

ReactDom.render(<App />, document.getElementById("app"));