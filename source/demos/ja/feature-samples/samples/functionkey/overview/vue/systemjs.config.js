(function (global) {
    System.config({
        transpiler: 'plugin-babel',
        babelOptions: {
            es2015: true
        },
        meta: {
            '*.css': { loader: 'css' },
            '*.vue': { loader: 'vue-loader' }
            //'*.vue': { loader: 'systemjs-plugin-vue' }
        },
        paths: {
            // paths serve as alias
            'npm:': 'node_modules/'
        },
        // map tells the System loader where to look for things
        map: {
            "@grapecity/inputman": "npm:@grapecity/inputman/index.js",
            "@grapecity/inputman/CSS": "npm:@grapecity/inputman/CSS",
            "@grapecity/inputman.vue": "npm:@grapecity/inputman.vue/lib/gc.inputman.plugin.vue.js",
            'css': 'npm:systemjs-plugin-css/css.js',
            'vue': 'npm:vue/dist/vue.min.js',
            'vue-loader': 'npm:systemjs-vue-browser/index.js',
            'plugin-babel': 'npm:systemjs-plugin-babel/plugin-babel.js',
            'systemjs-babel-build': 'npm:systemjs-plugin-babel/systemjs-babel-browser.js'
        },
        // packages tells the System loader how to load when no filename and/or no extension
        packages: {
            src: {
                defaultExtension: 'js'
            },
            rxjs: {
                defaultExtension: 'js'
              },
            // "node_modules": {
            //     defaultExtension: 'js'
            // },
            "@grapecity/inputman": {
                defaultExtension: 'js',
            }
        }
    });
})(this);
