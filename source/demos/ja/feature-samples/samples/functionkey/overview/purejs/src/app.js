import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';

// ファンクションキー
let gcFunctionKey = new InputMan.GcFunctionKey(document.getElementById('functionkey'), {
    functionKeys: [
        { key: InputMan.FunctionKey.F1, description: 'F1キー' },
        { key: InputMan.FunctionKey.F2, description: 'F2キー' },
        { key: InputMan.FunctionKey.F3, description: 'F3キー' },
        { key: InputMan.FunctionKey.F4, description: 'F4キー' },
        { key: InputMan.FunctionKey.F5, description: 'F5キー' },
        { key: InputMan.FunctionKey.F6, description: 'F6キー' },
        { key: InputMan.FunctionKey.F7, description: 'F7キー' },
        { key: InputMan.FunctionKey.F8, description: 'F8キー' },
        { key: InputMan.FunctionKey.Shift | InputMan.FunctionKey.F9, description: 'Shift+F9キー' },
        { key: InputMan.FunctionKey.Shift | InputMan.FunctionKey.F10, description: 'Shift+F10キー' },
        { key: InputMan.FunctionKey.Shift | InputMan.FunctionKey.F11, description: 'Shift+F11キー' },
        { key: InputMan.FunctionKey.Shift | InputMan.FunctionKey.F12, description: 'Shift+F12キー' },
        { key: InputMan.FunctionKey.Ctrl | InputMan.FunctionKey.Home, description: 'Ctrl+Homeキー' },
        { key: InputMan.FunctionKey.Ctrl | InputMan.FunctionKey.End, description: 'Ctrl+Endキー' },
        { key: InputMan.FunctionKey.Alt | InputMan.FunctionKey.PageUp, description: 'Alt+PageUpキー' },
        { key: InputMan.FunctionKey.Alt | InputMan.FunctionKey.PageDown, description: 'Alt+PageDownキー' },
    ],
    onActived: function (s, e) {
        window.alert(e.description + 'が押下されました。');
    }
});

document.getElementById('controlEnable').addEventListener('change', (e) => {
    gcFunctionKey.combinationKeyDisplayMode = e.target.value;
});