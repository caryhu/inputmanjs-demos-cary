ファンクションキーコントロール（GcFunctionKey）は、F1～F12キーをはじめ、[FunctionKey列挙型](</inputmanjs/api/enums/gc.inputman.functionkey.html>)のキーに対して独自の機能を割り当てることのできるコントロールです。

ファンクションキーを割り当てるにはaddFunctionKeyメソッドに割り当てたいファンクションキーを指定します。

```javascript
let gcFunctionKey = new GC.InputMan.GcFunctionKey(document.getElementById('functionkey'));
// 割り当てるキーを追加します。
gcFunctionKey.addFunctionKey(
    {
        key: GC.InputMan.FunctionKey.F11,
        description: 'F11キーです'
    },
    {
        key: GC.InputMan.FunctionKey.F12,
        description: 'F12キーです'
    }
);
```

また、次のように修飾キーと組み合わせて指定することもできます。
```javascript
let gcFunctionKey = new GC.InputMan.GcFunctionKey(document.getElementById('functionkey'));
gcFunctionKey.addFunctionKey(
    {
        key: GC.InputMan.FunctionKey.Ctrl | GC.InputMan.FunctionKey.F1,
        description: 'Ctrl + F1キーです'
    },
    {
        key: GC.InputMan.FunctionKey.Ctrl | GC.InputMan.FunctionKey.Shift | GC.InputMan.FunctionKey.Home,
        description: 'Ctrl + Shift + Home キーです'
    },
);
```

ファンクションキーに装飾キーを設定した場合、combinationKeyDisplayModeプロパティを利用して2種類の表示方法で表示することができます。

| combinationKeyDisplayModeの値                    | 説明                                          |
| --------------------------------------------- | --------------------------------------------- |
| Dynamic(デフォルト値)                                 | 装飾キーを押下した時に、設定したファンクションキーが表示されます|
| Always                                         | 設定したファンクションキーは常に表示されます              |