import * as React from "react";
import * as ReactDom from "react-dom";
import { GcFunctionKey, GcFunctionKeyInfo } from "@grapecity/inputman.react";
import {InputMan} from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{
    constructor(){
        super();
        this.state = {
            combinationKeyDisplayMode: InputMan.CombinationKeyDisplayMode.Dynamic
        };
    }

    render(){
        return <React.Fragment>
            <GcFunctionKey onActived={(s, e) => window.alert(e.description + 'が押下されました。')}
                combinationKeyDisplayMode={this.state.combinationKeyDisplayMode}>
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F1} description="F1キー" />
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F2} description="F2キー" />
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F3} description="F3キー" />
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F4} description="F4キー" />
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F5} description="F5キー" />
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F6} description="F6キー" />
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F7} description="F7キー" />
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F8} description="F8キー" />
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.Shift | InputMan.FunctionKey.F9} description="Shift+F9"キー />
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.Shift | InputMan.FunctionKey.F10} description="Shift+F10キー" />
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.Shift | InputMan.FunctionKey.F11} description="Shift+F11キー" />
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.Shift | InputMan.FunctionKey.F12} description="Shift+F12キー" />
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.Ctrl | InputMan.FunctionKey.Home} description="Ctrl+Homeキー" />
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.Ctrl | InputMan.FunctionKey.End} description="Ctrl+Endキー" />
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.Alt | InputMan.FunctionKey.PageUp} description="Alt+PageUpキー" />
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.Alt | InputMan.FunctionKey.PageDown} description="Alt+PageDownキー" />
            </GcFunctionKey>
            F1〜F5キーを押してください。<br/>
            また、CtrlキーやShiftキーを押しながらF1からF5キーを押してください。<br/><br/>

            <table class="sample">
                <tr>
                    <th>装飾キーの表示</th>
                    <td>
                        <select value={this.state.combinationKeyDisplayMode} onChange={e=> this.setState({combinationKeyDisplayMode: e.target.value})}>
                            <option value={InputMan.CombinationKeyDisplayMode.Dynamic}>装飾キー押下時に装飾キーを含むファンクションキーだけを表示</option>
                            <option value={InputMan.CombinationKeyDisplayMode.Always}>すべてのファンクションキーを常に表示</option>
                        </select>
                    </td>
                </tr>
            </table>
        </React.Fragment>
    }
}

ReactDom.render(<App />, document.getElementById("app"));