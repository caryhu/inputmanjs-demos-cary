import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    InputMan = GC.InputMan;
    combinationKeyDisplayMode = GC.InputMan.CombinationKeyDisplayMode.Dynamic;
    SF9 = GC.InputMan.FunctionKey.Shift | GC.InputMan.FunctionKey.F9;
    SF10 = GC.InputMan.FunctionKey.Shift | GC.InputMan.FunctionKey.F10;
    SF11 = GC.InputMan.FunctionKey.Shift | GC.InputMan.FunctionKey.F11;
    SF12 = GC.InputMan.FunctionKey.Shift | GC.InputMan.FunctionKey.F12;
    CFH = GC.InputMan.FunctionKey.Ctrl | GC.InputMan.FunctionKey.Home;
    CFE = GC.InputMan.FunctionKey.Ctrl | GC.InputMan.FunctionKey.End;
    AFPU = GC.InputMan.FunctionKey.Alt | GC.InputMan.FunctionKey.PageUp;
    AFPD = GC.InputMan.FunctionKey.Alt | GC.InputMan.FunctionKey.PageDown;

    onActived(s: any, e: any) {
        window.alert(e.description + 'が押下されました。')
    }
}

enableProdMode();