import * as React from "react";
import * as ReactDom from "react-dom";
import { GcFunctionKey, GcFunctionKeyInfo } from "@grapecity/inputman.react";
import {InputMan} from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{
    constructor(){
        super();
        this.state = {
            keyRepeat: true,
            showTip: true,
            handleMouse: true,
        };
    }

    render(){
        return <React.Fragment>
            <GcFunctionKey onActived={(s, e) => console.log(e.description + 'が押下されました。')}
                keyRepeat={this.state.keyRepeat}
                showTip={this.state.showTip}
                handleMouse={this.state.handleMouse}>
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F1} description='F1キー' />
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F3} description='F3キー' />
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.Home} description='Home' />
                <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.End} description='End' />
            </GcFunctionKey>
            <table class="sample">
                <tr>
                    <th>イベントの繰り返し</th>
                    <td>
                        <label><input type="checkbox" checked={this.state.keyRepeat} onChange={e=> this.setState({keyRepeat: e.target.checked})} />繰り返し実行する※1</label>
                    </td>
                </tr>
                <tr>
                    <th>ツールチップの表示</th>
                    <td>
                        <label><input type="checkbox" checked={this.state.showTip} onChange={e=> this.setState({showTip: e.target.checked})} />表示する</label>
                    </td>
                </tr>
                <tr>
                    <th>マウスクリックの有効化</th>
                    <td>
                        <label><input type="checkbox" checked={this.state.handleMouse} onChange={e=> this.setState({handleMouse: e.target.checked})} />有効化する</label>
                    </td>
                </tr>
            </table>
            <br />
            
            <p>※1 ファンクションキー長押ししている時のみ、イベントの繰り返しは実行されます。</p>
        </React.Fragment>
    }
}

ReactDom.render(<App />, document.getElementById("app"));