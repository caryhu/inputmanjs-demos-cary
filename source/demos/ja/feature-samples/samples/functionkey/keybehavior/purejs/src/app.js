import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';

// ファンクションキー
let gcFunctionKey = new InputMan.GcFunctionKey(document.getElementById('functionkey'), {
    functionKeys: [
        {
            key: GC.InputMan.FunctionKey.F1,
            description: 'F1キー'
        },
        {
            key: GC.InputMan.FunctionKey.F3,
            description: 'F3キー'
        },
        {
            key: GC.InputMan.FunctionKey.Home,
            description: 'Home'
        },
        {
            key: GC.InputMan.FunctionKey.End,
            description: 'End'
        }
    ],
    onActived: function (s, e) {
        console.log(e.description + 'が押下されました。');
    },
    keyRepeat:true
});

document.getElementById('keyEvent').addEventListener('click', (e) => {
    gcFunctionKey.keyRepeat = e.target.checked;
});

document.getElementById('tooltip').addEventListener('click', (e) => {
    gcFunctionKey.showTip = e.target.checked;
});

document.getElementById('mouseClick').addEventListener('click', (e) => {
    gcFunctionKey.handleMouse = e.target.checked;
});