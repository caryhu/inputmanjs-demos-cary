import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { Component, enableProdMode } from '@angular/core';
import GC from "@grapecity/inputman";

@Component({
    selector: 'app-component',
    templateUrl: './src/app.component.html',
})
export class AppComponent {
    InputMan = GC.InputMan;
    handleMouse = true;
    keyRepeat = true;
    showTip = true;

    onActived(s: any, e: any) {
        console.log(e.description + 'が押下されました。');
    }
}

enableProdMode();