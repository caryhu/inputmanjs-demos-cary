import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';
import { InputMan } from '@grapecity/inputman';

let gcFunctionKey1 = new InputMan.GcFunctionKey(document.getElementById('functionkey1'), {
    functionKeys:[
        {
            key: GC.InputMan.FunctionKey.F1,
            description: 'メニュー画面へ'
        },
        {
            key: GC.InputMan.FunctionKey.F2,
            description: '更新'
        },
        {
            key: GC.InputMan.FunctionKey.F3,
            description: "削除"
        },
        {
            key: GC.InputMan.FunctionKey.F4,
            description: "印刷"
        },
    ],
    onActived: function (s, e) {
        window.alert(e.description + 'が押下されました。');
    }
});

let gcFunctionKey2 = new InputMan.GcFunctionKey(document.getElementById('functionkey2'), {
    functionKeys:[
        {
            key: GC.InputMan.FunctionKey.F1,
            description: 'メニュー画面へ'
        },
        {
            key: GC.InputMan.FunctionKey.F2,
            description: '更新'
        },
        {
            key: GC.InputMan.FunctionKey.F3,
            description: "削除"
        },
        {
            key: GC.InputMan.FunctionKey.F4,
            description: "印刷"
        },
    ],
    onActived: function (s, e) {
        window.alert(e.description + 'が押下されました。');
    }
});