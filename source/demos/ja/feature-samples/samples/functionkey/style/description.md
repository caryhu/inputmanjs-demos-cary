コントロールで使用されている表示スタイルについて解説します。

## スタイルの設定方法

ファンクションキーコントロールのスタイルは、CSSで設定します。ファンクションキーコントロールを構成する要素のクラス名を指定して、そのクラスのスタイルを設定します。

次のサンプルコードは、ファンクションキーのテキスト要素のスタイルを設定する方法を示します。

```
/* ファンクションキーのテキスト要素のスタイル */
.functionkey-text {
    color: aqua
}

```

ここではcolorプロパティで文字色を設定していますが、その他にも、背景色、フォント、余白、境界線などの様々なスタイルを設定できます。CSSにおけるスタイル設定方法については、CSSのリファレンスを参照してください。

## クラス名

ファンクションキーを構成する要素のクラス名は次の通りです。

| 要素                                                                                      | クラス名                                                                                    |
| --------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------- |
| ファンクションキー全体                                                                                  | functionkey-container                                                      |
| ファンクションキーの項目全体                                                                                     | functionkey-item-container                                                            |
| ファンクションキーの画像項目                                                                                  | functionkey-image                                                               |
| ファンクションキーのテキスト項目                                                    | functionkey-text                                                               |


また、以下のようにfunctionkey-item-containerクラスにdata-functionkeyを追加することでキー単位でのスタイルを指定することができます。
data-functionkeyには、[FunctionKey列挙型](</inputmanjs/api/enums/gc.inputman.functionkey.html>)の値を指定します。

```
/* F1キーのスタイルを指定する */
.functionkey-item-container[data-functionkey="16"] {
    background: red
}

/* F2キーのスタイルを指定する */
.functionkey-item-container[data-functionkey="32"] {
    background: blue
}

```

