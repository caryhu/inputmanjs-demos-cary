import * as React from "react";
import * as ReactDom from "react-dom";
import { GcFunctionKey, GcFunctionKeyInfo } from "@grapecity/inputman.react";
import {InputMan} from "@grapecity/inputman";
import '@grapecity/inputman/CSS/gc.inputman-js.css';
import './styles.css';

class App extends React.Component{
    constructor(){
        super();
        this.state = {
            combinationKeyDisplayMode: InputMan.CombinationKeyDisplayMode.Dynamic
        };
    }

    render(){
        return <div class="flexbox">
            <div>通常のスタイル
                <GcFunctionKey id="functionkey1" onActived={(s,e)=>window.alert(e.description + 'が押下されました。')}>
                    <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F1} description="メニュー画面へ" />
                    <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F2} description="更新" />
                    <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F3} description="削除" />
                    <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F4} description="印刷" />
                </GcFunctionKey>
            </div>
            <div>スタイルを変更する
                <GcFunctionKey id="functionkey2" onActived={(s,e)=>window.alert(e.description + 'が押下されました。')}>
                    <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F1} description="メニュー画面へ" />
                    <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F2} description="更新" />
                    <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F3} description="削除" />
                    <GcFunctionKeyInfo functionKey={InputMan.FunctionKey.F4} description="印刷" />
                </GcFunctionKey>
            </div>
        </div>
    }
}

ReactDom.render(<App />, document.getElementById("app"));