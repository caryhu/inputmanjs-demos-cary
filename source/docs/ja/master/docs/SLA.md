# 使用許諾契約書

本製品の使用許諾契約書については、次のサイトをご覧ください。

- [ソフトウェア使用許諾契約書](https://docs.grapecity.com/license/inputman/inputman-js-developmentlicense.pdf)
- [配布ライセンス使用許諾契約書](https://docs.grapecity.com/license/inputman/inputman-js-distributionlicense.pdf)
- [InputManJSサブスクリプションサービス利用規約](https://docs.grapecity.com/license/inputman/inputman-js-subscriptionservice.pdf)