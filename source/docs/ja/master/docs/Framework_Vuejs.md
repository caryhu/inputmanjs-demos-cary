# Vue.jsで使用する方法

Vue.jsアプリケーションでは、次の手順でInputManJSのコンポーネントを使用することができます。

- まだVue CLIがインストールされていない場合は、次のコマンドを実行して、Vue CLIをインストールします。

  `npm install -g @vue/cli`

- Vue.jsアプリケーションを作成します。

  `vue create inputmanjs-vue -d`

- アプリケーションプロジェクトのフォルダに移動します。

  `cd inputmanjs-vue`

- InputManJSのVue.jsパッケージをインストールします。

  `npm install @grapecity/inputman.vue`

- src/App.vueファイルで、InputManJSのスタイルとコンポーネントをインポートします。

  ```javascript
  import '@grapecity/inputman/CSS/gc.inputman-js.css';
  import '@grapecity/inputman.vue';
  ```

- src/App.vueファイルで、データを設定して、InputManJSのマスクコンポーネントを追加します。 

```vue
<template>
  <div id="app">
    <gc-mask :value="maskValue" format-pattern="maskFormat"></gc-mask>
        値: {{maskValue}}
  </div>
</template>

<script>
 :
export default {
   :
  data: function () {
    return {
      maskValue: '1234567',
      maskFormat: '〒\\D{3}-\\D{4}'
    }
  }
}
</script>
```

- Vue.jsアプリを実行します。

  `npm run serve`