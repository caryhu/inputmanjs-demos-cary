# Reactで使用する方法

[Create React App](https://github.com/facebook/create-react-app)
（Reactのコマンドラインツール）を用いてReactアプリケーションを作成する場合は、次の手順でInputManJSのコンポーネントを使用することができます。

- まだCreate React Appがインストールされていない場合は、次のコマンドを実行して、Create React Appパッケージをグローバルにインストールします。

  `npm install -g create-react-app`

- Reactアプリケーションを作成します。

  `create-react-app inputmanjs-react`

- アプリケーションプロジェクトのフォルダに移動します。

  `cd inputmanjs-react`

- InputManJSのReactパッケージをインストールします。

  `npm install @grapecity/inputman.react`

- src/App.jsファイルの先頭で、InputManJSのコンポーネントとCSSファイルをインポートします。

  ```javascript
  import { GcMask, GcCalendar, GcDatetime, GcTextBox, GcNumber } from '@grapecity/inputman.react';
  import '@grapecity/inputman/CSS/gc.inputman-js.css';
  ```

- データを設定して、InputManJSのマスクコンポーネントを追加します。

  ```javascript
  function App() {
    const [maskValue, setValue] = useState('1234567');
    return (
      <div className="App">
        <GcMask value={maskValue} formatPattern={'〒\\D{3}-\\D{4}'}></GcMask>
      </div>
      
    );
  }
  ```

- アプリケーションを実行します。

  `npm start`