# TypeScriptの使用

本製品にはTypeScriptのサポートを追加できます。TypeScriptにより、強い型付けが可能となり、作成するコードについての適切な情報を提供できます。

TypeScriptをダウンロードし、詳細情報を参照するには、TypeScriptのWebサイト（[http://www.typescriptlang.org/](http://www.typescriptlang.org/)
）にアクセスしてください。

TypeScriptのInputManJS用定義ファイルの場所は、メインの製品フォルダ内にある definitionフォルダ内です（definition¥gc.inputman-js.d.ts）。

TypeScriptのサポートをInputManJSに追加するには、次の手順を実行します。

1. typescriptファイルの先頭に、gc.inputman-js.d.tsへの参照を追加します。

    `/// <reference path="ts/gc.inputman-js.d.ts" />`

2. アプリケーションにコードを追加します。次の例に示すように、InputManJSのコントロールのメンバーやメソッドのパラメータについての情報が表示されます。
![undefined](assets/imjs_images/typescript1.png)
![undefined](assets/imjs_images/typescript2.png)