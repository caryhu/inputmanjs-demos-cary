# カレンダーコントロールのCSS

本製品のカレンダーコントロールに適用できるCSSの一覧をこちらに記載します。

これらのCSSは以下のコントロールに適用されます。

- カレンダーコントロール（GcCalendar）
- ドロップダウンカレンダー（GcDropDownCalendar）

## スタイルの適用範囲

カレンダーの以下の部分のスタイルが適用されます。
![](assets/imjs_images/gccalendar_css.png)

## 休日のスタイル

<div>
    <table>
        <thead>
            <tr>
                <th>CSSクラス</th>
                <th>適用範囲</th>
                <th>適用箇所</th>
                <th>CSSプロパティ</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td rowspan="4">[calendar-holiday-type]</td>
                <td rowspan="4">休日、休業日全体</td>
                <td>文字色</td>
                <td>color</td>
            </tr>
            <tr>
                <td>背景色</td>
                <td>background-color</td>
            </tr>
            <tr>
                <td>境界線</td>
                <td>border, border-color, border-radiusなど</td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
            </tr>
            <tr>
                <td rowspan="4">[calendar-holiday-type|="祝日定義の名前"]</td>
                <td rowspan="4">特定の休日、休業日</td>
                <td>文字色</td>
                <td>color</td>
            </tr>
            <tr>
                <td>背景色</td>
                <td>background-color</td>
            </tr>
            <tr>
                <td>境界線</td>
                <td>border, border-color, border-radiusなど</td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
            </tr>
        </tbody>
    </table>
</div>

## カレンダーのスタイル

<div>
    <table>
        <thead>
            <tr>
                <th width="40%">CSSクラス（適用範囲）</th>
                <th width="80">適用箇所</th>
                <th>CSSプロパティ</th>
                <th>備考</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td rowspan="2">.gcim-calendar（カレンダー全体）</td>
                <td>幅</td>
                <td>width</td>
                <td></td>
            </tr>
            <tr>
                <td>高さ</td>
                <td>height</td>
                <td>ただし、個別エリアの調整が必要</td>
            </tr>
            <tr>
                <td rowspan="3">.gcim-calendar__disabled（無効（Disable）時のスタイル）</td>
                <td>透過度</td>
                <td>opacity</td>
                <td></td>
            </tr>
            <tr>
                <td>背景</td>
                <td>background</td>
                <td></td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
                <td></td>
            </tr>
            <tr>
                <td rowspan="5">.gcim-calendar__header（ヘッダスタイル）</td>
                <td>背景</td>
                <td>background</td>
                <td></td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
                <td></td>
            </tr>
            <tr>
                <td>高さ</td>
                <td>height</td>
                <td></td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
                <td>太字はヘッダテキストで設定</td>
            </tr>
            <tr>
                <td>境界線</td>
                <td>border</td>
                <td></td>
            </tr>
            <tr>
                <td rowspan="2">.gcim-calendar__header__text（ヘッダテキスト）</td>
                <td>太字</td>
                <td>font-weight</td>
                <td></td>
            </tr>
            <tr>
                <td>文字高さ</td>
                <td>line-height</td>
                <td></td>
            </tr>
            <tr>
                <td rowspan="5">.gcim-calendar__month-body（ヘッダ、今日の日付を除いたエリア）</td>
                <td>高さ</td>
                <td>height</td>
                <td></td>
            </tr>
            <tr>
                <td>背景</td>
                <td>background</td>
                <td></td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
                <td></td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
                <td>曜日、週番号領域以外</td>
            </tr>
            <tr>
                <td>境界線</td>
                <td>border</td>
                <td>IE、Edgeではbackground-clip: padding-box;も必要</td>
            </tr>
            <tr>
                <td rowspan="5">.gcim-calendar__month-body__common-day（日付（通常時）スタイル）</td>
                <td>高さ</td>
                <td>height</td>
                <td></td>
            </tr>
            <tr>
                <td>背景</td>
                <td>background</td>
                <td></td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
                <td></td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
                <td></td>
            </tr>
            <tr>
                <td>境界線</td>
                <td>border</td>
                <td>IE、Edgeではbackground-clip: padding-box;も必要</td>
            </tr>
            <tr>
                <td rowspan="5">
                    .gcim-calendar__month-body__week-title（曜日エリアスタイル）「日曜日」のタイトルに適用する場合.gcim-calendar__month-body__week-title[weekvalue="0"]
                </td>
                <td>高さ</td>
                <td>height</td>
                <td></td>
            </tr>
            <tr>
                <td>背景</td>
                <td>background</td>
                <td></td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
                <td></td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
                <td></td>
            </tr>
            <tr>
                <td>境界線</td>
                <td>border</td>
                <td>IE、Edgeではbackground-clip: padding-box;も必要</td>
            </tr>
            <tr>
                <td rowspan="5">
                    .gcim-calendar__weekday_sunday.gcim-calendar__weekday_monday.gcim-calendar__weekday_tuesday.gcim-calendar__weekday_wednesday.gcim-calendar__weekday_thursday.gcim-calendar__weekday_friday.gcim-calendar__weekday_saturday（各曜日スタイル、setWeekFlagsメソッドで設定した場合に有効）
                </td>
                <td>高さ</td>
                <td>height</td>
                <td></td>
            </tr>
            <tr>
                <td>背景</td>
                <td>background</td>
                <td></td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
                <td></td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
                <td></td>
            </tr>
            <tr>
                <td>境界線</td>
                <td>border</td>
                <td>IE、Edgeではbackground-clip: padding-box;も必要</td>
            </tr>
            <tr>
                <td rowspan="5">.gcim-calendar__month-body__week-number（週番号エリアスタイル）</td>
                <td>高さ</td>
                <td>height</td>
                <td></td>
            </tr>
            <tr>
                <td>背景</td>
                <td>background</td>
                <td></td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
                <td></td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
                <td></td>
            </tr>
            <tr>
                <td>境界線</td>
                <td>border</td>
                <td>IE、Edgeではbackground-clip: padding-box;も必要</td>
            </tr>
            <tr>
                <td rowspan="6">.gcim-calendar__month-body__trailing-day（隣接日スタイル）</td>
                <td>高さ</td>
                <td>height</td>
                <td></td>
            </tr>
            <tr>
                <td>背景</td>
                <td>background</td>
                <td></td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
                <td></td>
            </tr>
            <tr>
                <td>透過度</td>
                <td>opacity</td>
                <td></td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
                <td></td>
            </tr>
            <tr>
                <td>境界線</td>
                <td>border</td>
                <td>IE、Edgeではbackground-clip: padding-box;も必要</td>
            </tr>
            <tr>
                <td rowspan="5">.gcim-calendar__month-body__selected-day（選択日付スタイル）</td>
                <td>高さ</td>
                <td>height</td>
                <td></td>
            </tr>
            <tr>
                <td>背景</td>
                <td>background</td>
                <td></td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
                <td></td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
                <td></td>
            </tr>
            <tr>
                <td>境界線</td>
                <td>border</td>
                <td>IE、Edgeではbackground-clip: padding-box;も必要</td>
            </tr>
            <tr>
                <td rowspan="5">.gcim-calendar__month-body__focusd-day（フォーカス日付スタイル）</td>
                <td>高さ</td>
                <td>height</td>
                <td></td>
            </tr>
            <tr>
                <td>背景</td>
                <td>background</td>
                <td></td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
                <td></td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
                <td></td>
            </tr>
            <tr>
                <td>境界線</td>
                <td>border</td>
                <td>IE、Edgeではbackground-clip: padding-box;も必要</td>
            </tr>
            <tr>
                <td rowspan="5">.gcim-calendar__month-body__today（今日の日付スタイル）</td>
                <td>高さ</td>
                <td>height</td>
                <td></td>
            </tr>
            <tr>
                <td>背景</td>
                <td>background</td>
                <td></td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
                <td></td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
                <td></td>
            </tr>
            <tr>
                <td>境界線</td>
                <td>border</td>
                <td>IE、Edgeではbackground-clip: padding-box;も必要</td>
            </tr>
            <tr>
                <td>.gcim-calendar__month-body__corner-cell（曜日、週番号のコーナー）</td>
                <td>背景</td>
                <td>background</td>
                <td></td>
            </tr>
            <tr>
                <td rowspan="5">.gcim-calendar__month-body__rokuyou（六曜のスタイル）</td>
                <td>高さ</td>
                <td>height</td>
                <td></td>
            </tr>
            <tr>
                <td>背景</td>
                <td>background</td>
                <td></td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
                <td></td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
                <td></td>
            </tr>
            <tr>
                <td>境界線</td>
                <td>border</td>
                <td></td>
            </tr>
            <tr>
                <td rowspan="5">.gcim-calendar__today-button（「今日」エリアスタイル）</td>
                <td>高さ</td>
                <td>height</td>
                <td></td>
            </tr>
            <tr>
                <td>背景</td>
                <td>background</td>
                <td></td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
                <td></td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
                <td></td>
            </tr>
            <tr>
                <td>境界線</td>
                <td>border</td>
                <td></td>
            </tr>
            <tr>
                <td rowspan="5">.gcim-calendar__year-container（年月カレンダーの全体スタイル）</td>
                <td>高さ</td>
                <td>height</td>
                <td></td>
            </tr>
            <tr>
                <td>背景</td>
                <td>background</td>
                <td></td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
                <td></td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
                <td></td>
            </tr>
            <tr>
                <td>境界線</td>
                <td>border</td>
                <td></td>
            </tr>
            <tr>
                <td rowspan="5">.gcim-calendar__year-view__month（年月カレンダーの月スタイル）</td>
                <td>高さ</td>
                <td>height</td>
                <td></td>
            </tr>
            <tr>
                <td>背景</td>
                <td>background</td>
                <td></td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
                <td></td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
                <td></td>
            </tr>
            <tr>
                <td>境界線</td>
                <td>border</td>
                <td></td>
            </tr>
            <tr>
                <td rowspan="5">.gcim-calendar__year-view__selected-month（年月カレンダーの選択月スタイル）</td>
                <td>高さ</td>
                <td>height</td>
                <td></td>
            </tr>
            <tr>
                <td>背景</td>
                <td>background</td>
                <td></td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
                <td></td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
                <td></td>
            </tr>
            <tr>
                <td>境界線</td>
                <td>border</td>
                <td></td>
            </tr>
            <tr>
                <td rowspan="5">.gcim-calendar__year-view__focused-month（年月カレンダーのフォーカス月スタイル）</td>
                <td>高さ</td>
                <td>height</td>
                <td></td>
            </tr>
            <tr>
                <td>背景</td>
                <td>background</td>
                <td></td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
                <td></td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
                <td></td>
            </tr>
            <tr>
                <td>境界線</td>
                <td>border</td>
                <td></td>
            </tr>
            <tr>
                <td>.gcim-calendar__header__left-navigate（戻るボタン画像）</td>
                <td>背景画像</td>
                <td>background-image</td>
                <td>既定のナビゲーター</td>
            </tr>
            <tr>
                <td>.gcim-calendar__header__right-navigate（進むボタン画像）</td>
                <td>背景画像</td>
                <td>background-image</td>
                <td>既定のナビゲーター</td>
            </tr>
            <tr>
                <td>.gcim-calendar__header__zoom-out（年月カレンダー切替アイコン）</td>
                <td>背景画像</td>
                <td>background-image</td>
                <td>既定のナビゲーター</td>
            </tr>
            <tr>
                <td>.gcim-calendar__header__zoom-in（年月カレンダー切替アイコン）</td>
                <td>背景画像</td>
                <td>background-image</td>
                <td>既定のナビゲーター</td>
            </tr>
            <tr>
                <td>.gcim-calendar__navigator_vertical .left-navigator（ナビゲーター垂直表示時の戻るボタン画像）</td>
                <td>背景画像</td>
                <td>background-image</td>
                <td>setNavigatorOrientaionをRight、Leftに設定時</td>
            </tr>
            <tr>
                <td>.gcim-calendar__navigator_vertical .right-navigator（ナビゲーター垂直表示時の進むボタン画像）</td>
                <td>背景画像</td>
                <td>background-image</td>
                <td>setNavigatorOrientaionをRight、Leftに設定時</td>
            </tr>
            <tr>
                <td>.gcim-calendar__navigator_vertical .zoom-in（ナビゲーター垂直表示時の切替アイコン）</td>
                <td>背景画像</td>
                <td>background-image</td>
                <td>setNavigatorOrientaionをRight、Leftに設定時</td>
            </tr>
            <tr>
                <td>.gcim-calendar__navigator_vertical .zoom-out（ナビゲーター垂直表示時の切替アイコン）</td>
                <td>背景画像</td>
                <td>background-image</td>
                <td>setNavigatorOrientaionをRight、Leftに設定時</td>
            </tr>
            <tr>
                <td>.gcim-calendar__navigator_vertical .left-most（ナビゲーター垂直表示時の前の年へ進むボタン）</td>
                <td>背景画像</td>
                <td>background-image</td>
                <td>setNavigatorOrientaionをRight、Leftに設定時</td>
            </tr>
            <tr>
                <td>.gcim-calendar__navigator_vertical .right-most（ナビゲーター垂直表示時の次の年へ進むボタン）</td>
                <td>背景画像</td>
                <td>background-image</td>
                <td>setNavigatorOrientaionをRight、Leftに設定時</td>
            </tr>
            <tr>
                <td>.gcim-calendar__navigator_vertical .today（ナビゲーター垂直表示時の今日ボタン）</td>
                <td>背景画像</td>
                <td>background-image</td>
                <td>setNavigatorOrientaionをRight、Leftに設定時</td>
            </tr>
            <tr>
                <td>.gcim-calendar__navigator_horizontal .left-navigator（ナビゲーター水平表示時の戻るボタン画像）</td>
                <td>背景画像</td>
                <td>background-image</td>
                <td>setShowNavigatorをScrollBar、Buttonに設定時</td>
            </tr>
            <tr>
                <td>.gcim-calendar__navigator_horizontal .right-navigator（ナビゲーター水平表示時の進むボタン画像）</td>
                <td>背景画像</td>
                <td>background-image</td>
                <td>setShowNavigatorをScrollBar、Buttonに設定時</td>
            </tr>
            <tr>
                <td>.gcim-calendar__navigator_horizontal .zoom-in（ナビゲーター水平表示時の切替アイコン）</td>
                <td>背景画像</td>
                <td>background-image</td>
                <td>setShowNavigatorをScrollBar、Buttonに設定時</td>
            </tr>
            <tr>
                <td>.gcim-calendar__navigator_horizontal .zoom-out（ナビゲーター水平表示時の切替アイコン）</td>
                <td>背景画像</td>
                <td>background-image</td>
                <td>setShowNavigatorをScrollBar、Buttonに設定時</td>
            </tr>
            <tr>
                <td>.gcim-calendar__navigator_horizontal .left-most（ナビゲーター水平表示時の前の年へ進むボタン）</td>
                <td>背景画像</td>
                <td>background-image</td>
                <td>setShowNavigatorをScrollBar、Buttonに設定時</td>
            </tr>
            <tr>
                <td>.gcim-calendar__navigator_horizontal .right-most（ナビゲーター水平表示時の次の年へ進むボタン）</td>
                <td>背景画像</td>
                <td>background-image</td>
                <td>setShowNavigatorをScrollBar、Buttonに設定時</td>
            </tr>
            <tr>
                <td>.gcim-calendar__navigator_horizontal .today（ナビゲーター水平表示時の今日ボタン）</td>
                <td>背景画像</td>
                <td>background-image</td>
                <td>setShowNavigatorをScrollBar、Buttonに設定時</td>
            </tr>
        </tbody>
    </table>
</div>