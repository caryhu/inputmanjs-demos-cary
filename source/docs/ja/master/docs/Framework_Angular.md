# Angularで使用する方法

[Angular CLI](https://cli.angular.io/)を用いてAngularアプリケーションを作成する場合は、次の手順でInputManJSのコンポーネントを使用することができます。

- まだAngular CLIがインストールされていない場合は、次のコマンドを実行して、Angular CLIパッケージをグローバルにインストールします。

  `npm install -g @angular/cli`

- Angularアプリケーションを作成します。

  `ng new inputmanjs-angular`

- アプリケーションプロジェクトのフォルダに移動します。

  `cd inputmanjs-angular`

- InputManJSのAngularパッケージをインストールします。

  `npm install @grapecity/inputman.angular`

- src/styles.cssファイルで、スタイルをインポートします。

  `import '@grapecity/inputman/CSS/gc.inputman-js.css';`

- src/app/app.modules.tsファイルで、InputManModuleモジュールをインポートします。

  ```typescript
  import { InputManModule } from '@grapecity/inputman.angular';

  @NgModule({
  :
    imports: [
  :
      InputManModule
    ]
  })
  ```

- src/app/app.component.htmlファイルで、InputManJSのマスクコンポーネントを追加します。

`<gc-mask [(value)]="maskValue" [formatPattern]="'〒\\D{3}-\\D{4}'"></gc-mask><br>値: {{maskValue}}`

- src/app/app.component.tsファイルで、マスクコンポーネントに設定するデータを作成します。

  ```javascript
  export class AppComponent {
    maskValue = '1234567';
  }
  ```

- アプリケーションを実行します。

  `npm start`