# 各種オープンソースソフトウェアについて

**各種オープンソースソフトウェア（以下、「OSS」といいます）について**

当社では、各種OSSの使用許諾契約および使用方法に基づき、複数の種類の OSS プログラムを使用しています。

本ソフトウェアの一部には以下の OSS が含まれます。

**InputManJSデザイナ**

- archiver

    Copyright (c) 2012-2014 Chris Talkington, contributors.(MIT License)
- classnames

    Copyright (c) 2018 Jed Watson(MIT License)
- core-js

    Copyright (c) 2014-2020 Denis Pushkarev(MIT License)
- es6-promise

    Copyright (c) 2014 Yehuda Katz, Tom Dale, Stefan Penner and contributors(MIT License)
- immutability-helper

    Copyright (c) 2017 Moshe Kolodny(MIT License)
- js-base64

    Copyright (c) 2014, Dan Kogai All rights reserved.(BSD 3-Clause "New" or "Revised" License)
- lodash

    Copyright JS Foundation and other contributors (MIT License)
- prop-types

    Copyright (c) 2013-present, Facebook, Inc.(MIT License)
- react-bootstrap

    Copyright (c) 2014-present Stephen J. Collings, Matthew Honnibal, Pieter Vanderwerff(MIT License)
- react-color

    Copyright (c) 2015 Case Sandberg(MIT License)
- reactcss

    Copyright (c) 2015 Case Sandberg(MIT License)
- string-format

    Copyright (c) 2018 David Chambers (MIT License)
- stringify-object

    Copyright (c) 2015, Yeoman team All rights reserved.(BSD 2-Clause "Simplified" License)
- xml-js

    Copyright (c) 2016-2017 Yousuf Almarzooqi(MIT License)
- xml2js

    Copyright 2010, 2011, 2012, 2013. All rights reserved.(MIT License)
- react

    Copyright (c) Facebook, Inc. and its affiliates.(MIT License)
- react-dom

    Copyright (c) Facebook, Inc. and its affiliates.(MIT License)
- react-redux

    Copyright (c) 2015-present Dan Abramov(MIT License)
- redux

    Copyright (c) 2015-present Dan Abramov(MIT License)

**Visual Studio Code 拡張機能**

- cors

    Copyright (c) 2013 Troy Goode (MIT License)
- body-parser

    Copyright (c) 2014 Jonathan Ong
    Copyright (c) 2014-2015 Douglas Christopher Wilson (MIT License)
- express

    Copyright (c) 2009-2014 TJ Holowaychuk
    Copyright (c) 2013-2014 Roman Shtylman
    Copyright (c) 2014-2015 Douglas Christopher Wilson (MIT License)
- request

    Copyright 2010-2012 Mikeal Rogers(Apache License 2.0)