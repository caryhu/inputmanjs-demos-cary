# リストコントロールのCSS

本製品のリストコントロールに適用できるCSSの一覧をこちらに記載します。

これらのCSSは以下のコントロールに適用されます。

- リストコントロール（GcListBox）
- コンボコントロール（GcCombpoBox）のドロップダウンリスト
- リストの1列目に表示するチェックボックス

## 全般

<div>
    <table>
        <thead>
            <tr>
                <th width="40%">CSSクラス（概要）</th>
                <th width="20%">適用箇所</th>
                <th width="40%">CSSプロパティ</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td rowspan="5">.gcim__listbox（コントロールの基本スタイル）</td>
                <td>境界線</td>
                <td>border, border-color</td>
            </tr>
            <tr>
                <td>角丸</td>
                <td>border-radius</td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
            </tr>
            <tr>
                <td>背景（色、画像など）</td>
                <td>background, background-color, background-imageなど</td>
            </tr>
            <tr>
                <td rowspan="3">.gcim__listbox .column-header .column-header__inner（ヘッダー部分のスタイル）</td>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
            </tr>
            <tr>
                <td>背景（色、画像など）</td>
                <td>background, background-color, background-imageなど</td>
            </tr>
            <tr>
                <td>.gcim__listbox .column-header .column-header__sort-indicator[sort-state="asc"] div（昇順時のソートインジケータ）
                </td>
                <td>インジケータの画像</td>
                <td>background-image</td>
            </tr>
            <tr>
                <td>.gcim__listbox .column-header .column-header__sort-indicator[sort-state="desc"] div（降順時のソートインジケータ）
                </td>
                <td>インジケータの画像</td>
                <td>background-image</td>
            </tr>
            <tr>
                <td rowspan="3">.gcim__listbox .viewport .list-item（リストアイテムのスタイル）</td>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
            </tr>
            <tr>
                <td>背景（色、画像など）</td>
                <td>background, background-color, background-imageなど</td>
            </tr>
            <tr>
                <td rowspan="3">.gcim__listbox .viewport .list-item:hover（ホバー中のリストアイテムのスタイル）</td>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
            </tr>
            <tr>
                <td>背景（色、画像など）</td>
                <td>background, background-color, background-imageなど</td>
            </tr>
            <tr>
                <td rowspan="3">.gcim__listbox .viewport .list-item[focused="true"]（フォーカスがあるアイテムのスタイル）</td>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
            </tr>
            <tr>
                <td>背景（色、画像など）</td>
                <td>background, background-color, background-imageなど</td>
            </tr>
            <tr>
                <td rowspan="3">.gcim__listbox .viewport .list-item[selected="true"]（選択されているアイテムのスタイル）</td>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
            </tr>
            <tr>
                <td>背景（色、画像など）</td>
                <td>background, background-color, background-imageなど</td>
            </tr>
            <tr>
                <td rowspan="5">.gcim__listbox .footer_style（フッター部分のスタイル）</td>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
            </tr>
            <tr>
                <td>背景（色、画像など）</td>
                <td>background, background-color, background-imageなど</td>
            </tr>
            <tr>
                <td>フッターの高さ</td>
                <td>hight</td>
            </tr>
            <tr>
                <td>テキストの省略表示のスタイル</td>
                <td>text-overflow（clip または ellipsis）</td>
            </tr>
            <tr>
                <td>.gcim__listbox .corner（リストコントロールの右下角のスタイル）</td>
                <td>背景色</td>
                <td>background-color</td>
            </tr>
            <tr>
                <td>.gcim__listbox .corner[resize="true"]（リストコントロールのリサイズ可の場合の右下角のスタイル）</td>
                <td>背景（色、画像など）</td>
                <td>background-color, background-image</td>
            </tr>
            <tr>
                <td>.gcim__listbox .grid-line--bottom--right（リストコントロールのグリッド線のスタイル）</td>
                <td>線の色、線種、太さ</td>
                <td>border-right, border-bottom</td>
            </tr>
            <tr>
                <td>.gcim__listbox .resize-line（列のリサイズ線のスタイル）</td>
                <td>線の色、線種、太さ</td>
                <td>border-left</td>
            </tr>
            <tr>
                <td>.gcim__listbox .text-truncate-style（テキストが表示幅を超えた場合の表示方法）</td>
                <td>テキストの省略表示のスタイル</td>
                <td>text-overflow（clip または ellipsis）</td>
            </tr>
            <tr>
                <td rowspan="2">.list_overlay（コントロール無効時のスタイル）</td>
                <td>オーバーレイ（色、画像など）</td>
                <td>background, background-color, background-image</td>
            </tr>
            <tr>
                <td>透過度</td>
                <td>opacity</td>
            </tr>
        </tbody>
    </table>
</div>

## チェックボックス

<div>
    <table>
        <thead>
            <tr>
                <th width="40%">CSSクラス（概要）</th>
                <th width="20%">適用箇所</th>
                <th width="40%">CSSプロパティ</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td rowspan="3">.gcim_checkbox .checkbox-span（非チェック時のチェックボックスのスタイル）</td>
                <td>チェックボックスの枠</td>
                <td>border</td>
            </tr>
            <tr>
                <td>角丸</td>
                <td>border-radius</td>
            </tr>
            <tr>
                <td>サイズ</td>
                <td>width, height</td>
            </tr>
            <tr>
                <td rowspan="2">.gcim_checkbox .checkbox:checked + .checkbox-span（チェック時のチェックボックスのスタイル）</td>
                <td>チェックボックスの枠</td>
                <td>border</td>
            </tr>
            <tr>
                <td>背景色</td>
                <td>background-color</td>
            </tr>
            <tr>
                <td rowspan="4">.gcim_checkbox .checkbox-span::after（チェックマークのスタイル）</td>
                <td>チェックの色</td>
                <td>border-color</td>
            </tr>
            <tr>
                <td>チェックの太さ</td>
                <td>border-width</td>
            </tr>
            <tr>
                <td>チェックの大きさ</td>
                <td>width, height</td>
            </tr>
            <tr>
                <td>表示位置</td>
                <td>top, left</td>
            </tr>
        </tbody>
    </table>
</div>