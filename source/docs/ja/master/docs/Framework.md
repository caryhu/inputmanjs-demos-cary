# フレームワークでの使用

本製品を、Angular、React、Vue.jsなどのJavaScriptフレームワークで使用することが可能です。

各フレームワークでの基本的な使用方法を以下に説明します。

- [Angularで使用する方法](Framework_Angular)
- [Reactで使用する方法](Framework_React)
- [Vue.jsで使用する方法](Framework_Vuejs)

また本製品では、各フレームワークで使用するための「InputManJSコンポーネント」を提供します。「InputManJSコンポーネント」については以下をご参照ください。

- [InputManJSコンポーネント](InputManJS_Component)