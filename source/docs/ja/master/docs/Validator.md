# 検証コントロール

本製品では、入力値を検証し、検証結果をグラフィカルなUIで通知する検証コントロールを提供します。

![undefined](assets/imjs_images/validator.png)

(図)検証コントロールの例

## 適用可能なコントロール

検証コントロールを使った検証機能は以下のコントロールに適用できます。

- テキストコントロール(GcTextBox)
- マスクコントロール(GcMask)
- コンボコントロール(GcComboBox)
- 数値コントロール(GcNumber)
- 日付時刻コントロール(GcDateTime)
- 複数行テキストコントロール(GcMultiLineTextBox)

## 検証方法と通知方法

検証コントロールでは、以下の検証方法と通知方法を提供します。

**【検証規則】**

- 必須入力
- 範囲外の入力(数値コントロール、日付時刻コントロールのみ有効)
- 独自の検証ロジック

**【検証タイミング】**

- フォーカス移動時(コントロールからフォーカスを失ったとき)
- 入力中(リアルタイム)
- 手動による検証(ボタン押下などの任意のタイミング)

**【通知方法】**

- ツールチップ通知(コントロールの周辺にツールチップを表示します。)
- アイコン通知(コントロールの内側または外側にアイコンを表示します。)
- コントロール状態による通知(コントロールの枠やテキストの色により通知します。)

## 基本的な使用方法

検証コントロールのインスタンスを初期化する際に、コンストラクタのオプションで検証方法を設定します。以下の例では、フォーカスを失ったタイミングで、必須入力の検証を実行するようにテキストコントロールに設定します。

- [JavaScript](#TabContent-JavaScript)

```javascript
var gcTextBox1 = new GC.InputMan.GcTextBox(document.getElementById("gcTextBox1"));

var validator1 = new GC.InputMan.GcValidator({
    items: [
        {
            control: gcTextBox1,
            ruleSet: [
                {
                    // 必須入力
                    rule: GC.InputMan.ValidateType.Required
                }
            ],
            // フォーカスを失ったときに検証する
            validateWhen: GC.InputMan.ValidateWhen.LostFocus
        }
    ],
    defaultNotify: {
        // ツールチップを表示する                                  
        tip: true
    }
});
```

検証コントロールの使用方法の詳細は[オンラインデモ](https://demo.grapecity.com/inputmanjs/demos/validator/overview/purejs)をご参照ください。