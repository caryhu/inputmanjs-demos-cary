# 検証コントロールのCSS

本製品の検証コントロールに適用できるCSSの一覧をこちらに記載します。

これらのCSSは以下の通知に適用されます。

- ツールチップ通知（GcTipNotifier）
- アイコン通知（GcIconNotifier）
- コントロール状態通知（GcControlStateNotifier）

## ツールチップ通知

<div>
    <table>
        <thead>
            <tr>
                <th width="40%">CSSクラス（概要）</th>
                <th width="20%">適用箇所</th>
                <th width="40%">CSSプロパティ</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td rowspan="4">.gcim-notify__tip-content（ツールチップの基本スタイル）</td>
                <td>境界線</td>
                <td>border-width</td>
            </tr>
            <tr>
                <td>角丸</td>
                <td>border-radius</td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-weight</td>
            </tr>
            <tr>
                <td>影</td>
                <td>box-shadow</td>
            </tr>
            <tr>
                <td rowspan="3">【検証失敗時のスタイル】[gcim-notify__tip-state='fail']
                    .gcim-notify__tip-content【検証成功時のスタイル】[gcim-notify__tip-state='success']
                    .gcim-notify__tip-content（ツールチップの色）</td>
                <td>文字色</td>
                <td>color</td>
            </tr>
            <tr>
                <td>背景色</td>
                <td>background-color</td>
            </tr>
            <tr>
                <td>境界線の色</td>
                <td>border-color</td>
            </tr>
            <tr>
                <td>.gcim-notify__tip-arrow（ツールチップの矢印）</td>
                <td>矢印の大きさ</td>
                <td>border-width</td>
            </tr>
            <tr>
                <td>【検証失敗時のスタイル】[gcim-notify__tip-state='fail']
                    .gcim-notify__tip-arrow.right【検証成功時のスタイル】[gcim-notify__tip-state='success']
                    .gcim-notify__tip-arrow.right（ツールチップをコントロールの右に表示する場合の矢印）</td>
                <td>矢印の色</td>
                <td>border-color: [上] [右] [下] [左](例) border-color: transparent #ff0000 transparent transparent</td>
            </tr>
            <tr>
                <td>【検証失敗時のスタイル】[gcim-notify__tip-state='fail']
                    .gcim-notify__tip-arrow.left【検証成功時のスタイル】[gcim-notify__tip-state='success']
                    .gcim-notify__tip-arrow.left（ツールチップをコントロールの左に表示する場合の矢印）</td>
                <td>矢印の色</td>
                <td>border-color: [上] [右] [下] [左](例) border-color: transparent transparent transparent #ff0000</td>
            </tr>
            <tr>
                <td>【検証失敗時のスタイル】[gcim-notify__tip-state='fail']
                    .gcim-notify__tip-arrow.top【検証成功時のスタイル】[gcim-notify__tip-state='success']
                    .gcim-notify__tip-arrow.top（ツールチップをコントロールの上に表示する場合の矢印）</td>
                <td>矢印の色</td>
                <td>border-color: [上] [右] [下] [左](例) border-color: #ff0000 transparent transparent transparent</td>
            </tr>
            <tr>
                <td>【検証失敗時のスタイル】[gcim-notify__tip-state='fail']
                    .gcim-notify__tip-arrow.bottom【検証成功時のスタイル】[gcim-notify__tip-state='success']
                    .gcim-notify__tip-arrow.bottom（ツールチップをコントロールの下に表示する場合の矢印）</td>
                <td>矢印の色</td>
                <td>border-color: [上] [右] [下] [左](例) border-color: transparent transparent #ff0000 transparent</td>
            </tr>
            <tr>
                <td>【検証失敗時のスタイル】[gcim-notify__tip-state='fail'] .gcim-notify__tip-close
                    div【検証成功時のスタイル】[gcim-notify__tip-state='success'] .gcim-notify__tip-close div（ツールチップのクローズボタン）</td>
                <td>ボタンの色</td>
                <td>background-color</td>
            </tr>
            <tr>
                <td>【検証失敗時のスタイル】[gcim-notify__tip-state='fail'] .gcim-notify__tip-close:hover
                    div【検証成功時のスタイル】[gcim-notify__tip-state='success'] .gcim-notify__tip-close:hover div（ホバー時のクローズボタン）
                </td>
                <td>ボタンの色</td>
                <td>background-color</td>
            </tr>
        </tbody>
    </table>
</div>

## アイコン通知

<div>
    <table>
        <thead>
            <tr>
                <th width="40%">CSSクラス（概要）</th>
                <th width="20%">適用箇所</th>
                <th width="40%">CSSプロパティ</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>【検証失敗時のスタイル】.gcim-notify__icon[gcim-notify__icon-state='fail']【検証成功時のスタイル】.gcim-notify__icon[gcim-notify__icon-state='success']（通知アイコン）
                </td>
                <td>アイコン画像</td>
                <td>background-image</td>
            </tr>
        </tbody>
    </table>
</div>

## コントロール状態通知

<div>
    <table>
        <thead>
            <tr>
                <th width="40%">CSSクラス（概要）</th>
                <th width="20%">適用箇所</th>
                <th width="40%">CSSプロパティ</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td rowspan="3">
                    【検証失敗時のスタイル】.gcim-notify__state-container[gcim-notify__control-state='fail']【検証成功時のスタイル】.gcim-notify__state-container[gcim-notify__control-state='success']（コントロールの境界線）
                </td>
                <td>境界線の色</td>
                <td>border-color</td>
            </tr>
            <tr>
                <td>境界線のスタイル</td>
                <td>border-style</td>
            </tr>
            <tr>
                <td>境界線の太さ</td>
                <td>border-width</td>
            </tr>
            <tr>
                <td>【検証失敗時のスタイル】.gcim-notify__state-container[gcim-notify__control-state='fail']
                    .gcim-notify__state-input【検証成功時のスタイル】.gcim-notify__state-container[gcim-notify__control-state='success']
                    .gcim-notify__state-input（コントロールのテキスト）</td>
                <td>文字色</td>
                <td>color</td>
            </tr>
        </tbody>
    </table>
</div>