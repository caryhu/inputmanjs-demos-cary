# InputManJSの使い方

InputManJSの基本的な使い方について、以下の項目に分けて説明します。

- [ライセンスの組み込み](License)
- [アプリケーションの配布](Redistribution)
- [TypeScriptの使用](TypeScript)
- [コントロールの外観設定](Appearance)
- [タッチ機能](TouchFunction)
- [ドロップダウンプラグイン](DropDownPlugin)
- [検証コントロール](Validator)
- [InputManJSデザイナ](Designer)
- [フレームワークでの使用](Framework)

このほかの各コントロールの具体的な使用例については[オンラインデモ](https://demo.grapecity.com/inputmanjs/demos/quickStart/purejs)
をご参照ください。

- [コントロール一覧](https://demo.grapecity.com/inputmanjs/demos/common/controls/purejs)
- [テキストコントロール](https://demo.grapecity.com/inputmanjs/demos/textBox/overview/purejs)
- [複数行テキストコントロール](https://demo.grapecity.com/inputmanjs/demos/multiLineTextBox/overview/purejs)
- [マスクコントロール](https://demo.grapecity.com/inputmanjs/demos/mask/overview/purejs)
- [数値コントロール](https://demo.grapecity.com/inputmanjs/demos/number/overview/purejs)
- [日付時刻コントロール](https://demo.grapecity.com/inputmanjs/demos/dateTime/overview/purejs)
- [カレンダーコントロール](https://demo.grapecity.com/inputmanjs/demos/calendar/overview/purejs)
- [コンボコントロール](https://demo.grapecity.com/inputmanjs/demos/comboBox/overview/purejs)
- [リストコントロール](https://demo.grapecity.com/inputmanjs/demos/listBox/overview/purejs)
- [ドロップダウンプラグイン](https://demo.grapecity.com/inputmanjs/demos/dropDown/overview)
- [検証コントロール](https://demo.grapecity.com/inputmanjs/demos/validator/overview/purejs)