# 日付時刻ピッカーコントロールのCSS

本製品の日付時刻ピッカーコントロールに適用できるCSSの一覧をこちらに記載します。

これらのCSSは以下のコントロールに適用されます。

- 日付時刻ピッカー（GcDateTimePicker）
- ドロップダウンピッカー（GcDropDownPicker：日付時刻コントロールのドロップダウンピッカー）

## 全般

### 対象：日付時刻ピッカー、ドロップダウンピッカー

<div>
    <table>
        <thead>
            <tr>
                <th width="40%">CSSクラス（概要）</th>
                <th width="20%">適用箇所</th>
                <th width="40%">CSSプロパティ</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td rowspan="4">.gcim-date-time-picker__outter（日付時刻ピッカーのコンテナのスタイル）</td>
                <td>背景（色、画像など）</td>
                <td>background, background-color, background-image</td>
            </tr>
            <tr>
                <td>境界線</td>
                <td>border, border-color, border-style, border-width</td>
            </tr>
            <tr>
                <td>角丸</td>
                <td>border-radius</td>
            </tr>
            <tr>
                <td>サイズ</td>
                <td>width, height</td>
            </tr>
            <tr>
                <td rowspan="2">.gcim-date-time-picker__picker（日付選択領域のスタイル）</td>
                <td>背景（色、画像など）</td>
                <td>background, background-color, background-image</td>
            </tr>
            <tr>
                <td>表示位置</td>
                <td>margin</td>
            </tr>
            <tr>
                <td rowspan="2">.gcim-date-time-picker__item（アイテムのテキストスタイル）</td>
                <td>フォント</td>
                <td>font, font-size, font-familyなど</td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
            </tr>
            <tr>
                <td rowspan="3">.gcim-date-time-picker__selected-column-line（選択したアイテムの背景スタイル）</td>
                <td>境界線</td>
                <td>border-top, border-bottom</td>
            </tr>
            <tr>
                <td>背景（色、画像など）</td>
                <td>background, background-color, background-image</td>
            </tr>
            <tr>
                <td>サイズ</td>
                <td>width, height</td>
            </tr>
            <tr>
                <td rowspan="2">.gcim-date-time-picker__item[Gc_pickerItem_isSelected="true"]（選択したアイテムのテキストのスタイル）</td>
                <td>フォント</td>
                <td>font, font-size, font-familyなど</td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
            </tr>
            <tr>
                <td rowspan="2">.gcim-date-time-picker__item[Gc_pickerItem_isDisabled="true"]（無効な選択アイテムのテキストのスタイル）</td>
                <td>フォント</td>
                <td>font, font-size, font-familyなど</td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
            </tr>
            <tr>
                <td rowspan="3">.gcim-date-time-picker__tab-label（タブ領域のスタイル）</td>
                <td>境界線</td>
                <td>border, border-color, border-style, border-width</td>
            </tr>
            <tr>
                <td>角丸</td>
                <td>border-radius</td>
            </tr>
            <tr>
                <td>背景（色、画像など）</td>
                <td>background, background-color, background-image</td>
            </tr>
            <tr>
                <td rowspan="5">.gcim-date-time-picker__tab（タブのスタイル）</td>
                <td>フォント</td>
                <td>font, font-size, font-family, font-weightなど</td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
            </tr>
            <tr>
                <td>境界線</td>
                <td>border, border-color, border-style, border-width</td>
            </tr>
            <tr>
                <td>角丸</td>
                <td>border-radius</td>
            </tr>
            <tr>
                <td>背景（色、画像など）</td>
                <td>background, background-color, background-image</td>
            </tr>
            <tr>
                <td rowspan="5">.gcim-date-time-picker__tab--active（選択中のタブのスタイル）</td>
                <td>フォント</td>
                <td>font, font-size, font-family, font-weightなど</td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
            </tr>
            <tr>
                <td>境界線</td>
                <td>border, border-color, border-style, border-width</td>
            </tr>
            <tr>
                <td>角丸</td>
                <td>border-radius</td>
            </tr>
            <tr>
                <td>背景（色、画像など）</td>
                <td>background, background-color, background-image</td>
            </tr>
        </tbody>
    </table>
</div>

##  ドロップダウンピッカー

> ドロップダウンピッカーには上記のスタイルに加え、以下のスタイルを設定できます。

<div>
    <table>
        <thead>
            <tr>
                <th width="40%">CSSクラス（概要）</th>
                <th width="20%">適用箇所</th>
                <th width="40%">CSSプロパティ</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td rowspan="3">.gcim-date-time-picker__drop-down-container（ドロップダウンピッカーのコンテナのスタイル）</td>
                <td>境界線</td>
                <td>border, border-color, border-style, border-width</td>
            </tr>
            <tr>
                <td>角丸</td>
                <td>border-radius</td>
            </tr>
            <tr>
                <td>背景（色、画像など）</td>
                <td>background, background-color, background-image</td>
            </tr>
            <tr>
                <td rowspan="6">.gcim-date-time-picker__button--ok（ドロップダウンピッカーのOKボタンのスタイル）</td>
                <td>フォント</td>
                <td>font, font-size, font-family, font-weightなど</td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
            </tr>
            <tr>
                <td>境界線</td>
                <td>border, border-color, border-style, border-width</td>
            </tr>
            <tr>
                <td>角丸</td>
                <td>border-radius</td>
            </tr>
            <tr>
                <td>背景（色、画像など）</td>
                <td>background, background-color, background-image</td>
            </tr>
            <tr>
                <td>表示位置</td>
                <td>margin</td>
            </tr>
        </tbody>
    </table>
</div>