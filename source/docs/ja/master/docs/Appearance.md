# コントロールの外観設定

本製品ではコントロールの高さや幅などのサイズのほか、背景色や文字色などを含め、コントロールの外観はすべてCSS(Cascading Style Sheet)により設定します。

ここではCSSによるスタイルの基本的な設定方法を説明します。

なお、コントロールで使用されるCSSクラスの詳細については以下の一覧をご参照ください。

- [入力コントロールのCSS](CSS_Input)
- [カレンダーコントロールのCSS](CSS_Calendar)
- [リストコントロールのCSS](CSS_ListBox)
- [日付時刻ピッカーコントロールのCSS](CSS_Picker)
- [検証コントロールのCSS](CSS_Notify)

## CSSの適用例

### すべての入力コントロールに適用する場合

以下の設定は、ページ上のすべての入力コントロールに適用されます。

```css
.gcim{
    width: 200px;
    color: blue;
}
```

### 固有のコントロールに適用する場合

1. 入力コントロールへの適用

    「gcText1」というクラス名のテキストコントロールに適用します。

    ```css
    .gcText1 .gcim{
        width: 150px;
        background- color: #cccccc;
    }
    ```

    ```html
    <input id="textbox1" class="gcText1" type="text">
    ```

2. カレンダーコントロールへの適用

    「calendar1」というID名のカレンダーコントロールに適用します。

    ```css
    #calendar1 .gcim-calendar {
        font-size:18px;
        width: 300px;
        height: 300px;
    }
    ```

    ```html
    <div id="calendar1"></div>
    ```

**CSS適用時の注意事項**

- 基本的にすべてのCSSを使用可能ですが、位置関係を定義する「display」「position」などのCSSを設定するとレイアウトが崩れる場合があります。
- 「padding」「margin」「font-size」などの設定もレイアウト崩れの要因になる場合があるため注意が必要です。なお、「width」「height」の設定で調整できる場合もあります。
- 疑似クラスの使用も可能です。ただし、コントロールのフォーカス取得時は「:active」や「:focus」ではなく「.gcim_focused」クラスを使用します。
- 固有のコントロールに適用する場合
  - 入力系コントロールはID名での指定では動作しません。クラス名を指定する必要があります。
  - カレンダーコントロールはID名での指定が可能です。またIDやクラス名を指定しない場合は、同一ページ上のドロップダウンカレンダーにも適用されます。
