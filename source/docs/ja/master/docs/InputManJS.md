# GrapeCity InputManJS

このたびは「InputManJS（以下、本製品）」をお求めいただき、誠にありがとうございます。

本製品は用途別に最適化された日本仕様の入力用JavaScriptコントロールセットです。文字種制限、日付書式、定型書式など、あらゆるタイプの入力方式に対応し、リアルタイムな入力チェックといった機能も備えるため、入力ミスやストレスが少ない生産性の高い業務アプリケーションを開発できます。

このヘルプを十分にご利用いただき、さまざまなアプリケーション開発に本製品をご活用いただけることを願っております。

![undefined](assets/01Welcome/01_01.png)

Web: [https://www.grapecity.co.jp/developer](https://www.grapecity.co.jp/developer)

お問合せ: [https://www.grapecity.co.jp/developer/about-us/contact](https://www.grapecity.co.jp/developer/about-us/contact)