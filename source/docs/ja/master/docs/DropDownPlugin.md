# ドロップダウンプラグイン

本製品では、任意のオブジェクトやHTML要素を入力コントロールのドロップダウンオブジェクトとして使用することができる、ドロップダウンプラグイン機能を提供します。

![undefined](assets/imjs_images/dropdownplugin.png)

(図)HTML要素と組み合わせた例

## 適用可能なコントロール

ドロップダウンプラグインでは以下のコントロールに、FlexGrid([Wijmo](https://www.grapecity.co.jp/developer/wijmo))や、Spread.Sheet／Spread.View([SpreadJS](https://www.grapecity.co.jp/developer/spreadjs))などのオブジェクトや、任意のHTML要素を入力コントロールのドロップダウンとして組み合わせることができます。

- テキストコントロール(GcTextBox)
- マスクコントロール(GcMask)
- 数値コントロール(GcNumber)
- 日付時刻コントロール(GcDateTime)
- 複数行テキストコントロール(GcMultiLineTextBox)

ドロップダウンプラグインの使用方法の詳細は[オンラインデモ](https://demo.grapecity.com/inputmanjs/demos/dropDown/overview)
をご参照ください。

## 基本的な使用方法

ドロップダウンプラグインの基本的な使用手順は以下の通りです。

1. ドロップダウンを追加するコントロールでcreateDropDownメソッドを実行し、ドロップダウンオブジェクトのコンテナを作成します。
2. 作成したコンテナに任意のオブジェクトまたはHTML要素を設定します。

以下の例では、リストコントロールをテキストコントロールのドロップダウンに設定します。

```javascript
var gcTextBox = new GC.InputMan.GcTextBox(document.getElementById("gcTextBox1"));
var gcTextBoxDropDown = gcTextBox.createDropDown(GC.InputMan.DropDownButtonAlignment.RightSide);
var gcTextBoxDropDownList = new GC.InputMan.GcListBox(gcTextBoxDropDown.getElement(), {
    items:  ["item0", "item1", "item2", "item3", "item4", "item5", "item6", "item7"],
});
gcTextBoxDropDown.onOpen(function(){
    gcTextBoxDropDownList.layout();
});                            
gcTextBoxDropDownList.addEventListener(GC.InputMan.GcListBoxEvent.ItemClick, function(sender, args){
    gcTextBox.setText(args.itemObject.Text);
    gcTextBoxDropDown.close();
});
```