# 入力コントロールのCSS

本製品の入力コントロールに適用できるCSSの一覧をこちらに記載します。

これらのCSSは以下のコントロールに適用されます。

- テキストコントロール(GcTextBox)
- 複数行テキストコントロール(GcMultiLineTextBox)
- マスクコントロール(GcMask)
- コンボコントロール(GcComboBox)
- 数値コントロール(GcNumber)
- 日付時刻コントロール(GcDateTime)

## 全般

### 対象：すべての入力コントロール

<div>
    <table>
        <thead>
            <tr>
                <th>CSSクラス(概要)</th>
                <th>適用箇所</th>
                <th>CSSプロパティ</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td rowspan="6">.gcim(コントロールの基本スタイル)</td>
                <td>境界線</td>
                <td>border, border-color, border-radiusなど</td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
            </tr>
            <tr>
                <td>幅</td>
                <td>width</td>
            </tr>
            <tr>
                <td>高さ</td>
                <td>height</td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
            </tr>
            <tr>
                <td>背景(色、画像など)</td>
                <td>background, background-color, background-imageなど</td>
            </tr>
            <tr>
                <td rowspan="7">.gcim__input:disabled(コントロール無効時のスタイル)</td>
                <td>境界線</td>
                <td>border, border-color, border-radiusなど</td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
            </tr>
            <tr>
                <td>幅</td>
                <td>width</td>
            </tr>
            <tr>
                <td>高さ</td>
                <td>height</td>
            </tr>
            <tr>
                <td>透明度</td>
                <td>opacity</td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
            </tr>
            <tr>
                <td>背景(色、画像など)</td>
                <td>background, background-color, background-imageなど</td>
            </tr>
            <tr>
                <td rowspan="6">.gcim_focused(フォーカス取得時のスタイル)</td>
                <td>境界線</td>
                <td>border, border-color, border-radiusなど</td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
            </tr>
            <tr>
                <td>幅</td>
                <td>width</td>
            </tr>
            <tr>
                <td>高さ</td>
                <td>height</td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
            </tr>
            <tr>
                <td>背景(色、画像など)</td>
                <td>background, background-color, background-imageなど</td>
            </tr>
        </tbody>
    </table>
</div>

## ウォーターマーク

### 対象：すべての入力コントロール

<div>
    <table>
        <thead>
            <tr>
                <th width="50%">CSSクラス(概要)</th>
                <th>適用箇所</th>
                <th>CSSプロパティ</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td rowspan="3">.gcim_watermark_null(フォーカスがないときのウォーターマークのスタイル)</td>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
            </tr>
            <tr>
                <td>背景色</td>
                <td>background-color</td>
            </tr>
            <tr>
                <td rowspan="3">.gcim_focused.gcim_watermark_null(フォーカス時のウォーターマークのスタイル)</td>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
            </tr>
            <tr>
                <td>文字色</td>
                <td>color</td>
            </tr>
            <tr>
                <td>背景色</td>
                <td>background-color</td>
            </tr>
        </tbody>
    </table>
</div>

### 対象：日付時刻コントロール

<table>
    <thead>
        <tr>
            <th width="50%">CSSクラス(概要)</th>
            <th>適用箇所</th>
            <th>CSSプロパティ</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan="3">.gcim_watermark_empty-era(フォーカスがなく和暦表示できないときのウォーターマークのスタイル)</td>
            <td>フォント</td>
            <td>font-family, font-sizeなど</td>
        </tr>
        <tr>
            <td>文字色</td>
            <td>color</td>
        </tr>
        <tr>
            <td>背景色</td>
            <td>background-color</td>
        </tr>
        <tr>
            <td rowspan="3">.gcim_focused.gcim_watermark_empty-era(フォーカス時の和暦表示できないときのウォーターマークのスタイル)</td>
            <td>フォント</td>
            <td>font-family, font-sizeなど</td>
        </tr>
        <tr>
            <td>文字色</td>
            <td>color</td>
        </tr>
        <tr>
            <td>背景色</td>
            <td>background-color</td>
        </tr>
    </tbody>
</table>

### 対象：数値コントロール

<table>
    <thead>
        <tr>
            <th width="50%">CSSクラス(概要)</th>
            <th>適用箇所</th>
            <th>CSSプロパティ</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan="3">.gcim_watermark_zero(フォーカスがなく値がゼロのときのウォーターマークのスタイル)</td>
            <td>フォント</td>
            <td>font-family, font-sizeなど</td>
        </tr>
        <tr>
            <td>文字色</td>
            <td>color</td>
        </tr>
        <tr>
            <td>背景色</td>
            <td>background-color</td>
        </tr>
        <tr>
            <td rowspan="3">.gcim_focused.gcim_watermark_zero(フォーカス時の値がゼロのときのウォーターマークのスタイル)</td>
            <td>フォント</td>
            <td>font-family, font-sizeなど</td>
        </tr>
        <tr>
            <td>文字色</td>
            <td>color</td>
        </tr>
        <tr>
            <td>背景色</td>
            <td>background-color</td>
        </tr>
    </tbody>
</table>

##  ドロップダウンボタン 

### 対象：すべての入力コントロール
<div>
    <table>
        <thead>
            <tr>
                <th width="50%">CSSクラス(概要)</th>
                <th>適用箇所</th>
                <th>CSSプロパティ</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>.gcim__side-button(ドロップダウンボタンスタイル)</td>
                <td>境界線</td>
                <td>border, border-color, border-radiusなど</td>
            </tr>
            <tr>
                <td>.gcim__side-button:hover(ドロップダウンボタンスタイル：ホバー時)</td>
                <td>背景色</td>
                <td>background-color</td>
            </tr>
            <tr>
                <td rowspan="2">.gcim__side-button:active(ドロップダウンボタンスタイル：押下時)</td>
                <td>幅</td>
                <td>width</td>
            </tr>
            <tr>
                <td>カーソル形状</td>
                <td>cursor</td>
            </tr>
            <tr>
                <td rowspan="2">.gcim__side-button_disabled(コントロール無効時のドロップダウンボタンスタイル)</td>
                <td>透過度</td>
                <td>opacity</td>
            </tr>
            <tr>
                <td>背景色</td>
                <td>background-color</td>
            </tr>
            <tr>
                <td>.gcim-icon_drop-down(ドロップダウンボタンのマーク画像)</td>
                <td>背景画像</td>
                <td>background-image</td>
            </tr>
        </tbody>
    </table>


### 対象：日付時刻コントロール

<table>
    <thead>
        <tr>
            <th width="50%">CSSクラス(概要)</th>
            <th>適用箇所</th>
            <th>CSSプロパティ</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>.gcim__side-button_drop-down-calendar(ドロップダウンカレンダーボタンスタイル)</td>
            <td>境界線</td>
            <td>border, border-color, border-radiusなど</td>
        </tr>
        <tr>
            <td>.gcim__side-button_drop-down-calendar:hover(ドロップダウンカレンダーボタンスタイル：ホバー時)</td>
            <td>背景色</td>
            <td>background-color</td>
        </tr>
        <tr>
            <td rowspan="2">.gcim__side-button_drop-down-calendar:active(ドロップダウンカレンダーボタンスタイル：押下時)</td>
            <td>幅</td>
            <td>width</td>
        </tr>
        <tr>
            <td>カーソル形状</td>
            <td>cursor</td>
        </tr>
        <tr>
            <td>.gcim__side-button_drop-down-calendar:hover .gcim-icon_drop-down(ドロップダウンボタンのマーク画像：ホバー時)</td>
            <td>背景画像</td>
            <td>background-image</td>
        </tr>
        <tr>
            <td>.gcim__side-button_drop-down-calendar:active .gcim-icon_drop-down(ドロップダウンボタンのマーク画像：押下時)</td>
            <td>背景画像</td>
            <td>background-image</td>
        </tr>
    </tbody>
</table>

### 対象：数値コントロール

<table>
    <thead>
        <tr>
            <th width="50%">CSSクラス(概要)</th>
            <th>適用箇所</th>
            <th>CSSプロパティ</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>.gcim__side-button_drop-down-numeric-pad(ドロップダウン数値パッドボタンスタイル)</td>
            <td>境界線</td>
            <td>border, border-color, border-radiusなど</td>
        </tr>
        <tr>
            <td>.gcim__side-button_drop-down-numeric-pad:hover(ドロップダウン数値パッドボタンスタイル：ホバー時)</td>
            <td>背景色</td>
            <td>background-color</td>
        </tr>
        <tr>
            <td rowspan="2">.gcim__side-button_drop-down-numeric-pad:active(ドロップダウン数値パッドボタンスタイル：押下時)</td>
            <td>幅</td>
            <td>width</td>
        </tr>
        <tr>
            <td>カーソル形状</td>
            <td>cursor</td>
        </tr>
        <tr>
            <td>.gcim__side-button_drop-down-numeric-pad:hover .gcim-icon_drop-down(ドロップダウンボタンのマーク画像：ホバー時)</td>
            <td>背景画像</td>
            <td>background-image</td>
        </tr>
        <tr>
            <td>.gcim__side-button_drop-down-numeric-pad:active .gcim-icon_drop-down(ドロップダウンボタンのマーク画像：押下時)</td>
            <td>背景画像</td>
            <td>background-image</td>
        </tr>
    </tbody>
</table>
</div>

## スピンボタン

### 対象：コンボ、数値、日付時刻、マスクコントロール

<div>
    <table>
        <thead>
            <tr>
                <th width="50%">CSSクラス(概要)</th>
                <th>適用箇所</th>
                <th>CSSプロパティ</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>.gcim__spin-button(スピンボタンスタイル)</td>
                <td>境界線</td>
                <td>border, border-color, border-radiusなど</td>
            </tr>
            <tr>
                <td>.gcim__spin-button:hover(スピンボタンスタイル：ホバー時)</td>
                <td>背景色</td>
                <td>background-color</td>
            </tr>
            <tr>
                <td rowspan="2">.gcim__spin-button:active(スピンボタンスタイル：押下時)</td>
                <td>幅</td>
                <td>width</td>
            </tr>
            <tr>
                <td>カーソル形状</td>
                <td>cursor</td>
            </tr>
            <tr>
                <td rowspan="2">.gcim__side-button_disabled .gcim__spin-button(コントロール無効時のスピンボタンスタイル)</td>
                <td>透過度</td>
                <td>opacity</td>
            </tr>
            <tr>
                <td>背景色</td>
                <td>background-color</td>
            </tr>
            <tr>
                <td>.gcim__spin-button__down(スピンダウンボタン)</td>
                <td>境界線</td>
                <td>border, border-color, border-radiusなど</td>
            </tr>
            <tr>
                <td>.gcim__spin-button__down:hover(スピンダウンボタン：ホバー時)</td>
                <td>背景色</td>
                <td>background-color</td>
            </tr>
            <tr>
                <td rowspan="2">.gcim__spin-button__down:active(スピンダウンボタン：押下時)</td>
                <td>幅</td>
                <td>width</td>
            </tr>
            <tr>
                <td>カーソル形状</td>
                <td>cursor</td>
            </tr>
            <tr>
                <td>.gcim__spin-button__up(スピンアップボタン)</td>
                <td>境界線</td>
                <td>border, border-color, border-radiusなど</td>
            </tr>
            <tr>
                <td>.gcim__spin-button__up:hover(スピンアップボタン：ホバー時)</td>
                <td>背景色</td>
                <td>background-color</td>
            </tr>
            <tr>
                <td rowspan="2">.gcim__spin-button__up:active(スピンアップボタン：押下時)</td>
                <td>幅</td>
                <td>width</td>
            </tr>
            <tr>
                <td>カーソル形状</td>
                <td>cursor</td>
            </tr>
            <tr>
                <td>.gcim-icon_spin-down(スピンダウンボタンのマーク画像)</td>
                <td>背景画像</td>
                <td>background-image</td>
            </tr>
            <tr>
                <td>.gcim__spin-button__down:hover .gcim-icon_spin-down(スピンダウンボタンのマーク画像：ホバー時)</td>
                <td>背景画像</td>
                <td>background-image</td>
            </tr>
            <tr>
                <td>.gcim__spin-button__down:active .gcim-icon_spin-down(スピンダウンボタンのマーク画像：押下時)</td>
                <td>背景画像</td>
                <td>background-image</td>
            </tr>
            <tr>
                <td>.gcim-icon_spin-up(スピンアップボタンのマーク画像)</td>
                <td>背景画像</td>
                <td>background-image</td>
            </tr>
            <tr>
                <td>.gcim__spin-button__up:hover .gcim-icon_spin-up(スピンアップボタンのマーク画像：ホバー時)</td>
                <td>背景画像</td>
                <td>background-image</td>
            </tr>
            <tr>
                <td>.gcim__spin-button__up:active .gcim-icon_spin-up(スピンアップボタンのマーク画像：押下時)</td>
                <td>背景画像</td>
                <td>background-image</td>
            </tr>
        </tbody>
    </table>
</div>

## 数値パッド

### 対象：数値コントロール

<div>
    <table>
        <thead>
            <tr>
                <th width="50%">CSSクラス(概要)</th>
                <th>適用箇所</th>
                <th>CSSプロパティ</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td rowspan="5">.gcim-numeric-pad(数値パッド全体の基本スタイル)</td>
                <td>境界線</td>
                <td>border, border-color, border-radiusなど</td>
            </tr>
            <tr>
                <td>背景色</td>
                <td>background-color</td>
            </tr>
            <tr>
                <td>フォント</td>
                <td>font-family, font-sizeなど</td>
            </tr>
            <tr>
                <td>幅</td>
                <td>width</td>
            </tr>
            <tr>
                <td>高さ</td>
                <td>height</td>
            </tr>
            <tr>
                <td>.gcim-numeric-button(数字ボタンの基本スタイル)</td>
                <td>境界線</td>
                <td>border, border-color, border-radiusなど</td>
            </tr>
            <tr>
                <td>.gcim-numeric-button:hover(数字ボタン：ホバー時)</td>
                <td>背景色</td>
                <td>background-color</td>
            </tr>
            <tr>
                <td rowspan="2">.gcim-numeric-button:active(数字ボタン：押下時)</td>
                <td>幅</td>
                <td>width</td>
            </tr>
            <tr>
                <td>高さ</td>
                <td>height</td>
            </tr>
            <tr>
                <td>.gcim-icon_back-space(BSボタンの画像)</td>
                <td>背景画像</td>
                <td>background-image</td>
            </tr>
        </tbody>
    </table>
</div>

## パスワードアイ

### 対象：テキストコントロール

<div>
    <table>
        <thead>
            <tr>
                <th width="50%">CSSクラス(概要)</th>
                <th>適用箇所</th>
                <th>CSSプロパティ</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>.gcim__fish-eye_normal(パスワードアイの画像)</td>
                <td>背景画像</td>
                <td>background-image</td>
            </tr>
            <tr>
                <td>.gcim__fish-eye_hover(パスワードアイの画像：ホバー時)</td>
                <td>背景画像</td>
                <td>background-image</td>
            </tr>
            <tr>
                <td>.gcim__fish-eye_pressed(パスワードアイの画像：押下時)</td>
                <td>背景画像</td>
                <td>background-image</td>
            </tr>
        </tbody>
    </table>
</div>

## タッチツールバー

### 対象：すべての入力コントロール

<div>
    <table>
        <thead>
            <tr>
                <th width="50%">CSSクラス(概要)</th>
                <th>適用箇所</th>
                <th>CSSプロパティ</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td rowspan="3">.gcim__touch-tool-bar(タッチツールバー全体のスタイル)</td>
                <td>高さ</td>
                <td>height</td>
            </tr>
            <tr>
                <td>境界線</td>
                <td>border</td>
            </tr>
            <tr>
                <td>背景色</td>
                <td>background-color</td>
            </tr>
            <tr>
                <td>.gcim__touch-toolbar__button(タッチツールバーボタンのスタイル)</td>
                <td>背景色など</td>
                <td>background-colorなど</td>
            </tr>
            <tr>
                <td>.gcim__touch-toolbar__separator(ボタンセパレーターの色)</td>
                <td>背景色</td>
                <td>background-color</td>
            </tr>
            <tr>
                <td>.gcim-icon_paste(「貼り付け」メニューの画像)</td>
                <td>背景画像</td>
                <td>background-image</td>
            </tr>
            <tr>
                <td>.gcim-icon_cut(「切り取り」メニューの画像)</td>
                <td>背景画像</td>
                <td>background-image</td>
            </tr>
            <tr>
                <td>.gcim-icon_copy(「コピー」メニューの画像)</td>
                <td>背景画像</td>
                <td>background-image</td>
            </tr>
            <tr>
                <td>.gcim-icon_clear(「削除」メニューの画像)</td>
                <td>背景画像</td>
                <td>background-image</td>
            </tr>
            <tr>
                <td>.gcim-icon_undo(「元に戻す」メニューの画像)</td>
                <td>背景画像</td>
                <td>background-image</td>
            </tr>
            <tr>
                <td>.gcim-icon_select-all(「すべて選択」メニューの画像)</td>
                <td>背景画像</td>
                <td>background-image</td>
            </tr>
        </tbody>
    </table>
</div>