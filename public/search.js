window.onload = function () {
    var header = document.querySelector('.app-header');
    var links = header.getElementsByTagName('a');
    for (var i = 0; i < links.length; i++) {
        var link = links[i];
        if (link.href && (link.href.indexOf("http:") > -1 || link.href.indexOf("https:") > -1)) {
            link.target = '_blank';
        }
    }
};
