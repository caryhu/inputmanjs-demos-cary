module.exports = {
    gtm: `<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-20295802-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());    gtag('config', 'UA-20295802-2');
    </script>
    <script src="/inputmanjs/demos/resource/search.js"></script>
    <meta name="google-site-verification" content="HLIWLZqEiMg64ddWU4FUkGLEiNX8vwKrszmPRT-5fP0" />
    <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
    <meta property="og:locale" content="ja_JP">
    <meta property="og:site_name" content="InputManJSデモアプリケーション | Developer Tools〈開発支援ツール〉 - グレープシティ株式会社">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://demo.grapecity.com/inputmanjs/demos/">
    <meta property="og:title" content="InputManJSデモアプリケーション | Developer Tools〈開発支援ツール〉 - グレープシティ株式会社">
    <meta property="og:description" content="快適な入力を実現する日本仕様の入力用JavaScriptコントロール「InputManJS」のサンプルコードを見て触れて学習することができます。">
    <meta property="og:image" content="https://demo.grapecity.com/inputmanjs/demos/resource/inputmanjs-ogp.png">
    <meta name="twitter:url" content="https://demo.grapecity.com/inputmanjs/demos/">
    <meta name="twitter:title" content="InputManJSデモアプリケーション | Developer Tools〈開発支援ツール〉 - グレープシティ株式会社">
    <meta name="twitter:card" content="summary">
    <meta name="description" content="快適な入力を実現する日本仕様の入力用JavaScriptコントロール「InputManJS」のサンプルコードを見て触れて学習することができます。" lang="ja" xml:lang="ja">`,
    beforeDownloadSampleFile: `function(fileName, fileContent, zip) {
        if(fileName == 'package.json'){
            package = JSON.parse(fileContent);
            delete package["scripts"]["setup"];
            delete package["scripts"]["preinstall"];
            fileContent = JSON.stringify(package, null, 4);
        }
        return fileContent;
    }`,
    apihash: `
    <script>
    window.addEventListener('load', function(){
        var node = document.getElementsByClassName("tree_selector")[0];
        node.addEventListener('click', function(event) {
            var clickDom = event.path[2]
            var selectedDom = document.querySelector('[data-selected="true"]');
                if (selectedDom) {
                    selectedDom.setAttribute('data-selected', 'false');
                    console.log(clickDom.children[0])
                    clickDom.children[0].setAttribute('data-selected', 'true');
                }
        })
        var aList = document.getElementsByClassName("tree_selector")[0].getElementsByTagName('a');

        var href = location.href;
        for (let index = 0; index < aList.length; index++) {
            const aItem = aList[index];
            if(aItem.href === href) {
                var selectedDom = document.querySelector('[data-selected="true"]');
                if (selectedDom) {
                    selectedDom.setAttribute('data-selected', 'false');
                    aItem.parentNode.setAttribute('data-selected', 'true');
                    aItem.parentElement.parentElement.parentElement.setAttribute('data-expand', 'true');
                    aItem.parentElement.parentElement.parentElement.previousElementSibling.children[0].children[0].setAttribute('data-expand', 'true');
                }
            }
        }
    })
</script>
    `

}