module.exports = {
    footer: `<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap-grid.css' rel='preload'>
    <div class='container'>
        <div class='row'>
            <div class='col-lg-6'>
                <ul class='company-info'>
                    <li>
                        <a href='https://www.grapecity.co.jp/' class='gc-logo'>
                            <i class='gcicon-logo-gc-horiz'></i>
                        </a>
                    </li>
                    <li>©
                        <span id='gcyear'>2018</span> GrapeCity inc. All Rights Reserved.
                    </li>
                </ul>
            </div>
            <div class='col-lg-6'>
                <ul class='util-nav'>
                    <li>
                        <a href='https://www.grapecity.co.jp/developer/purchase/how-to-buy/cancel'>特定商取引法に基づく表記</a>
                    </li>
                    <li>
                        <a href='https://www.grapecity.co.jp/developer/about-us'>会社情報</a>
                    </li>
                    <li>
                        <a href='https://www.grapecity.co.jp/developer/about-us/contact'>お問合せ</a>
                    </li>
                    <li>
                        <a href='https://www.grapecity.co.jp/about/privacy/'>プライバシーポリシー</a>
                    </li>
                </ul>
                <ul class='util-social'>
                    <li>
                        <a href='https://www.facebook.com/GrapeCity.dev.JP' target='_blank'>
                            <i class='fab fa-sm fa-facebook-f' aria-hidden='true'></i>
                        </a>
                    </li>
                    <li>
                        <a href='https://twitter.com/GrapeCity_dev' target='_blank'>
                            <i class='fab fa-sm fa-twitter' aria-hidden='true'></i>
                        </a>
                    </li>
                    <li>
                        <a href='https://devlog.grapecity.co.jp/' target='gc_rss' title='Latest blogs'>
                            <i class='fa fa-sm fa-rss' aria-hidden='true'></i>
                        </a>
                    </li>                  
                </ul>
            </div>
        </div>
    </div>`
}