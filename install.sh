#!/bin/sh
base=$(pwd)/source/demos/ja/feature-samples/config
fws="purejs react vue angular"

# npm i --only=prod

for fw in $fws;
do
    echo ****************${c}:${fw} install start******************
    cd ${base}/${fw}
    npx rimraf node_modules/@grapecity
    npm i --only=prod
    echo ****************${c}:${fw} install end******************
done
