echo "Installing rsync in node.js docker."
echo "If you are not using node.js docker image, please provide the rsync command."

apt-get update && apt-get install rsync -y

rm -fr ./deploy-root
mkdir -p ./deploy-root


rsync -rv --exclude=.gitignore --exclude=.git --exclude ./deploy-root --exclude ./source/docs/en/docRoot --exclude ./node_modules ./ ./deploy-root

cd ./deploy-root

git init;
git config user.name "ci.teamcity"
git config user.email "teamcity@grapecity.com"
git add .
git commit -m 'deploy'
git config http.postBuffer 524288000
git remote add azuredeploy $AZURE_DEPLOY_GIT
echo "Pushing Code To Azure";
git push azuredeploy master:master -f
