const site = require('@grapecity/jsob-shell');
const express = require('express');
const template = require('./src/template');
const customjs = require('./src/customjs');

const localization = {
    ja: {
        "fontList": "メイリオ, meiryo, verdana, sans-serif",
        "title": "InputManJSデモアプリケーション | Developer Tools〈開発支援ツール〉 - グレープシティ株式会社"
    }
}

var currentVersion = "v0.0.1"
let docVersions = module.exports.docVersions = [
    {
        name: currentVersion,
        version: 'master',
        branch: "master"
    }
]

site.config({
    demoroot: 'source/demos',
    demoBasePath: '/inputmanjs/demos/',
    
    docroot:'source/docs', 
    docBasePath:'/inputmanjs/docs/',
    docAssetsFolder: 'assets',
    apiRoot:'./source/api/',
    docBaseUrl: '/inputmanjs/docs/',
    apiBasePath: '/inputmanjs/api/',
    apiVersions:  [
        {version: 'latest', name:'latest'},
    ] ,
    apiDefaultVersion: "latest",
    apiAssetFolder: 'assets',
    downloadUrl: 'https://www.grapecity.co.jp/developer/download#javascript',
    productHomePage: 'https://www.grapecity.co.jp/developer/inputmanjs',
    supportedFrameworks: [
        {name: 'purejs', label: 'JavaScript'},
        { name: 'angular', label: 'Angular',extraDescription:'このサンプルはAngularを使用しています。' },
        { name: 'react', label: 'React',extraDescription:'このサンプルはReactを使用しています。' },
        { name: 'vue', label: 'Vue',extraDescription:'このサンプルはVueを使用しています。' }
    ],
    defaultSandboxConfig: {
        height: 'auto',
        minHeight: '0'
    },
    frameworkDefaultFiles: {
        purejs: ['app.js', 'index.html', 'styles.css', 'data.js'],
        angular: ['app.component.ts', 'index.html', 'app.component.html', 'data.ts', 'app.component.module.ts', 'styles.css'],
        vue: ['app.vue', 'index.html', 'data.js', 'styles.css'],
        react: ['app.jsx', 'index.html', 'app.css', 'data.jsx'],
    },
    cultures: ['ja'],
    defaultCulture: 'ja',
    docVersions: docVersions.map(v => { return { name: v.name, version: v.version, repoUrl: v.repoUrl, commentPageUrl: v.commentPageUrl } }),
    docDefaultVersion: "master",  
    favicon: '/inputmanjs/demos/resource/favicon.png',
    logo: '/inputmanjs/demos/resource/logo.png',
    footerTemplate: template.footer,
    customCss: '/inputmanjs/demos/resource/custom.css',
    responsiveLayout: true,
    codeHighlight: true,
    additionalIntroduction: 'description.md',
    customHtml: '',
    customJs: customjs.gtm + customjs.apihash,
    docSearchOption: {
        apiKey: process.env.DOCSEARCH_API_KEY,
        indexName: process.env.DOCSEARCH_INDEX_NAME,
        appId: process.env.DOCSEARCH_APP_ID
    },
    localization: localization,
    allowDownloadSample: true,
    beforeDownloadSampleFile: customjs.beforeDownloadSampleFile,
    downloadSampleFilePattern: "**/*"
});

site.app.use('/inputmanjs/demos/resource', express.static('public'));

if(process.argv[2] == "deb"){
    site.app.use('/inputmanjs/demos/src', express.static('src'));
}

site.run();